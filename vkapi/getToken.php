<?php
require_once 'vkapi/vk_start.php';
include_once 'vkapi/config.php';

header('Content-Type: text/html; charset=utf-8');
$v = new Vk(array(
    'client_id' => $config['client_id'], // (обязательно) номер приложения
    'secret_key' => $config['secret_key'], // (обязательно) получить тут https://vk.com/editapp?id=12345&section=options где 12345 - client_id
    'user_id' => $config['user_id'] , // ваш номер пользователя в вк
    'scope' => $config['scope'], // права доступа
    'v' => '5.35' // не обяsзательно
));

$url = $v->get_code_token();

$response =  <<<TXT
<html>
<head>
    <title>Получить access_token</title>
    <style>
        .lghtd-txt {
            color: red;
        }
    </style>
</head>
<body>
    <a href="{$url}">Получить access_token</a>
    <br><br>
    <div>
        Перейдите по ссылке и скопируйте из url страницы (на которую попадете) параметр access_token.
        <br><br>
        <span>
            Например
            <br>
            "https://oauth.vk.com/blank.html#access_token=<b class="lghtd-txt">jf89006b30d31ba812jf7380ncyd633a3b3kjh5dc78b7c9ae332053fcd29c7ec4df8aa3a51312dd8cf7e4</b>&expires_in=0&user_id=1"
        </span>
    </div>
</body>
</html>
TXT;

echo $response;