<?php
/* пример реализации */

include_once 'vkapi/config.php';
?>
    <script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>

    <div id="log"></div>
    <div id="login_button" onclick="VK.Auth.login(authInfo);"></div>

    <script language="javascript">
        VK.init({
            apiId: <?=$config['client_id']?>
        });
        function authInfo(response) {
            if (response.session) {
                console.log('user: '+response.session.mid);
                VK.Api.call('users.get', {user_ids: response.session.mid}, function(r) {
                    if(r.response) {
                        alert('Привет, ' + r.response[0].first_name);

                        VK.Api.call('friends.get', {user_id: response.session.mid, fields: 'domain'}, function(r) {
                            if(r.response) {
                                var arrayLength = r.response.length;
                                for (var i = 0; i < arrayLength; i++) {
                                    document.getElementById('log').innerHTML = document.getElementById('log').innerHTML +
                                        '<span>' + r.response[i]["first_name"] + ' ' + r.response[i]["last_name"] + '</span> - ' +
                                        '<a href="/sendMessage.php?id=' + r.response[i]["uid"] + '" target="_blank">' +
                                        'Пригласить</a> <br>';
                                    //Do something
                                }

                            }
                        });
                    }
                });
            } else {
                console.log('not auth');
                showButt()
            }
        }
        function showButt() {
            VK.UI.button('login_button');
        }
        VK.Auth.getLoginStatus(authInfo);
    </script>