<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.01.16
 * Time: 18:52
 */

namespace SettingsBundle\Entity;

use DHorchler\ConfigBundle\Entity\Settings;
use Doctrine\ORM\Mapping as ORM;
use PageBundle\Entity\SeoPage;

/**
 * @ORM\Entity
 * @ORM\Table(name="my_settings")
 * @ORM\Entity(repositoryClass="SettingsBundle\Repository\SettingsRepository")
 */
class MySettings extends Settings
{
    /**
     *
     * @ORM\ManyToOne (targetEntity="\PageBundle\Entity\SeoPage", inversedBy="settingsjoin")
     * @ORM\JoinColumn (name="pagejoin_id", referencedColumnName="id")
     */
    protected $pagejoin;



    /**
     * Set pagejoin
     *
     * @param \PageBundle\Entity\SeoPage $pagejoin
     *
     * @return MySettings
     */
    public function setPagejoin(\PageBundle\Entity\SeoPage $pagejoin = null)
    {
        $this->pagejoin = $pagejoin;

        return $this;
    }

    /**
     * Get pagejoin
     *
     * @return \PageBundle\Entity\SeoPage
     */
    public function getPagejoin()
    {
        return $this->pagejoin;
    }
}
