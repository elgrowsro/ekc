<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.04.16
 * Time: 15:16
 */

namespace SettingsBundle\Admin;


use DHorchler\ConfigBundle\Admin\ConfigAdmin as MainConfigAdmin;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Validator\ErrorElement;

//use Sonata\AdminBundle\Validator\ErrorElement;

class ConfigAdmin extends MainConfigAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    protected function configureFormFields(FormMapper $mapper)
    {
        parent::configureFormFields($mapper);
        $mapper->remove('updated');
        $mapper->remove('section');
        $mapper->remove('filter');
        $mapper->remove('defaultValue');
        $mapper->remove('currentValue');
        $mapper->add('currentValue', null, array('required' => false, 'attr' => array('class' => 'defaultText', 'title' => 'enter current value')));
    }


    protected function configureListFields(ListMapper $mapper)
    {
        parent::configureListFields($mapper);
        $mapper
            ->remove('section')
            ->remove('created')
            ->remove('updated')
            ->remove('defaultValue')
            ->remove('filter');
    }

    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        parent::configureDatagridFilters($mapper);
        $mapper
            ->remove('section');
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        switch ((string)$object->getType()) {
            case 'date':
            case 'datetime':
                /*if (is_object($object->getDefaultValue())) $object->setDefaultValue($object->getDefaultValue()->getTimestamp());
                elseif ($object->getDefaultValue() != '') {$def = new \DateTime($object->getDefaultValue());$object->setDefaultValue($cur->getTimestamp());}
                if (is_object($object->getCurrentValue())) $object->setCurrentValue($object->getCurrentValue()->getTimestamp());
                elseif ($object->getCurrentValue() != '') {$cur = new \DateTime($object->getCurrentValue());$object->setCurrentValue($cur->getTimestamp());}
                if (is_object($object->getMin())) $object->setMin($object->getMin()->getTimestamp());
                elseif ($object->getMin() != '') {$min = new \DateTime($object->getMin());$object->setMin($min->getTimestamp());}
                if (is_object($object->getMax())) $object->setMax($object->getMax()->getTimestamp());
                elseif ($object->getMax() != '') {$max = new \DateTime($object->getMax());$object->setMax($max->getTimestamp());}*/
                break;
            case 'multiplechoice':
                if (is_array($object->getCurrentValue())) $object->setCurrentValue(implode(',', $object->getCurrentValue()));
                break;
        }
        $filter = $object->getFilter();
        if (!empty($filter) AND $separatorPos = strpos($filter, ':')) {
            $filterType = strtolower(substr($filter, 0, $separatorPos));
            $filterValue = substr($filter, $separatorPos + 1);
            switch ($filterType) {
                case 'min':
                    if ($filterValue > $object->getCurrentValue())
                        $errorElement
                            ->with('currentValue')
                            ->assertRange(array('min' => $object->getFilter()))
                            ->addViolation('currentValue should not be less than min')
                            ->end();
                    break;
                case 'max':
                    if ($filterValue < $object->getCurrentValue())
                        $errorElement
                            ->with('currentValue')
                            ->assertRange(array('max' => $object->getFilter()))
                            ->addViolation('currentValue should not be major than max')
                            ->end();
                    break;
                case 'range':
                default;
                    $filterArray = explode('..', $filterValue);
                    if (count($filterArray) < 2) break;
                    if ($filterArray[0] > $object->getCurrentValue())
                        $errorElement
                            ->with('currentValue')
                            ->assertRange(array('min' => $object->getFilter()))
                            ->addViolation('currentValue should not be less than min')
                            ->end();
                    if ($filterArray[1] < $object->getCurrentValue())
                        $errorElement
                            ->with('currentValue')
                            ->assertRange(array('max' => $object->getFilter()))
                            ->addViolation('currentValue should not be major than max')
                            ->end();
                    break;
            }
        }
    }

    public function preUpdate($object)
    {//object to string conversion
        switch ((string)$object->getType()) {
            case 'date':
            case 'datetime':
                $format = ($object->getType() == 'date') ? 'Y-m-d' : 'Y-m-d H:i:s';
                if ($object->getCurrentValue() != '') {
                    $cur = new \DateTime();
                    $cur->setTimestamp($object->getCurrentValue());
                    $object->setCurrentValue($cur->format($format));
                }
                if ($object->getMin() != '') {
                    $min = new \DateTime();
                    $min->setTimestamp($object->getMin());
                    $object->setMin($min->format($format));
                }
                if ($object->getMax() != '') {
                    $max = new \DateTime();
                    $max->setTimestamp($object->getMax());
                    $object->setMax($max->format($format));
                }
                break;
        }
    }

    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);

        $query
            ->where('o.id NOT IN (
        SELECT IDENTITY(ps.settingsjoin)
        FROM PageBundle\Entity\PageSettings ps
        where ps.settingsjoin is not NULL and ps.pagejoin is not NULL
        group by ps.settingsjoin
   )');


        return $query;
    }

}