<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace EventsBundle\EventListener;


use AppBundle\Entity\Comment;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Question;
use FeedBundle\Entity\Feed;

class EventStarsSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Product" entity
        if ($entity instanceof Comment) {
            $event = $entity->getEvent();
            if ($event) {
                $entityManager = $args->getEntityManager();

                $repository_comments = $entityManager->getRepository('AppBundle:Comment');
                $evaluationEvent = $repository_comments->getEvaluationEvent($event);

                if ($evaluationEvent) {
                    if (isset($evaluationEvent['calculation_interestingness'])) {
                        $event->setAvgInterestingness(floor($evaluationEvent['calculation_interestingness']));
                        $event->setAvgQuality(floor($evaluationEvent['calculation_quality']));
                        $event->setAvgSaturation(floor($evaluationEvent['calculation_saturation']));

                        $rating_avg = floor(($evaluationEvent['calculation_interestingness'] + $evaluationEvent['calculation_quality'] + $evaluationEvent['calculation_saturation']) / 3);

                        $event->setAvgRating($rating_avg);
                        $entityManager->persist($event);
                        $entityManager->flush();
                    }
                }
            }
        }
    }

}