<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace EventsBundle\EventListener;


use AppBundle\Entity\User;
use AppBundle\Service\EmailerController;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Ticket;
use EventsBundle\Entity\TimePrice;
use Sonata\MediaBundle\Entity\GalleryManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TPPRSubscriber implements EventSubscriber
{
    private $mg;
    private $container;
    private $message_array = [];
    private $set_money = [];

    public function __construct(GalleryManager $mg, ContainerInterface $container)
    {
        $this->mg = $mg;
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'preRemove',
            'postRemove',
        );
    }


    public function preRemove(LifecycleEventArgs $args)
    {
        $this->pre_remove($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->post_remove($args);
    }

    public function post_remove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TimePrice) {
            if (count($this->set_money)) {
                $entityManager = $args->getEntityManager();
                foreach ($this->set_money as $sm_id1 => $sm1) {
                    $user_update = $entityManager->find('AppBundle:User', $sm_id1);
                    $user_update->setMoney($sm1);
                    $entityManager->persist($user_update);
                }
                $entityManager->flush();
            }
        }
    }

    public function pre_remove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        /** @var EmailerController $mailer */
        $mailer = $this->container->get('app.emailer');

        /** @var Router $route */
        $route = $this->container->get('router');//->generate('payment_check', [], true);

        $entityManager = $args->getEntityManager();

        if ($entity instanceof TimePrice &&
            (
                ($entity->getDateEvent()->format("Y-m-d") > date("Y-m-d")) ||
                (
                    $entity->getDateEvent()->format("Y-m-d") == date("Y-m-d") &&
                    $entity->getTimefrom()->format("H:i") > date("H:i")
                )
            ) &&
            $entity->getCountTicketsBuy()
        ) {

            $qeury_tp = $entityManager->getRepository('EventsBundle:Ticket')->createQueryBuilder('ticket');
            $qeury_tp
                ->innerJoin('ticket.user', 'user')
                ->andWhere('ticket.timeprices = :tp')
                ->setParameter(':tp', $entity->getId());

            $tickets = $qeury_tp->getQuery()->getResult();

            if (count($tickets)) {
                /** @var Ticket $ticket */
                foreach ($tickets as $ticket) {
                    /** @var User $user */
                    $user = $ticket->getUser();


                    $org = $entity->getEvent()->getCompany();
                    $org_user = $org->getUsercompany();

                    $this->message_array[$user->getEmail()][$ticket->getId()] = [
                        '{{ FLName }}' => $user->getFLName(),
                        '{{ FLNameOrganizer }}' => $org->getNameCompany(),
                        '{{ sum }}' => $ticket->getFullPrice(),
                        '{{ linkOrganizer }}' => $route->generate('company_view', ['url' => $org->getUrl()], true),
                        '{{ nameEvent }}' => $entity->getEvent()->getName(),
                        '{{ linkEvent }}' => $route->generate('events_view', ['url' => $entity->getEvent()->getUrl()], true),
                        '{{ dateEvent }}' => $entity->getDateEvent()->format('d.m.Y'),
                        '{{ timeEvent }}' => $entity->getTimefrom()->format('H:i'),
                    ];
//
                    if (isset($this->set_money[$user->getId()])) {
                        $this->set_money[$user->getId()] = $this->set_money[$user->getId()] + $ticket->getFullPrice();
                    } else {
                        $this->set_money[$user->getId()] = $user->getMoney() + $ticket->getFullPrice();
                    }

//                    if (isset($this->set_money[$org_user->getId()])) {
//                        $this->set_money[$org_user->getId()] = $this->set_money[$org_user->getId()] - $ticket->getFullPrice();
//                    } else {
//                        $this->set_money[$org_user->getId()] = $org_user->getMoney() - $ticket->getFullPrice();
//                    }
                }

//                $entityManager->flush();

                if (count($this->message_array)) {
                    foreach ($this->message_array as $email => $data) {
                        foreach ($data as $ticket_id => $data_send) {
                            $mailer->sendMailToUser(10, $data_send, $email);
                        }
                    }
                }
            }
        }
    }
}