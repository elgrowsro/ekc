<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace EventsBundle\EventListener;


use AppBundle\Entity\User;
use AppBundle\Service\EmailerController;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Ticket;
use EventsBundle\Entity\TimePrice;
use Sonata\MediaBundle\Entity\GalleryManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EventPPSubscriber implements EventSubscriber
{
    private $mg;
    private $container;

    public function __construct(GalleryManager $mg, ContainerInterface $container)
    {
        $this->mg = $mg;
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
            'postRemove',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->p_update($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->post_remove($args);
    }


    public function post_remove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $entityManager = $args->getEntityManager();

        if ($entity instanceof Event) {
            if ($entity->getPostVkDateClosed()) {
                if ($entity->getPostVkDateClosed()->format('Y-m-d') > date("Y-m-d") || ($entity->getPostVkDateClosed()->format('Y-m-d') == date("Y-m-d") && date("H:i") > "12:00")) {
                    $user = $entity->getCompany()->getUsercompany();

                    $user->setMoney($user->getMoney() + ($entity->getPostVkSumWin() * $entity->getPostVkCntWin()));
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
            }
        }
    }

    public function p_update(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Event) {
            /** @var EmailerController $mailer */
            $mailer = $this->container->get('app.emailer');

            /** @var Router $route */
            $route = $this->container->get('router');//->generate('payment_check', [], true);

            $entityManager = $args->getEntityManager();

            $qeury_tp = $entityManager->getRepository('EventsBundle:TimePrice')->createQueryBuilder('timePrice');
            $qeury_tp
                ->innerJoin('timePrice.tickets', 'tickets')
                ->innerJoin('tickets.user', 'user')
                ->andWhere('timePrice.event = :event')
                ->andWhere($qeury_tp->expr()->orX(
                    $qeury_tp->expr()->gt('timePrice.date_event', 'CURRENT_DATE()'),
                    $qeury_tp->expr()->andX(
                        $qeury_tp->expr()->eq('timePrice.date_event', 'CURRENT_DATE()'),
                        $qeury_tp->expr()->gt('timePrice.timefrom', 'CURRENT_TIME()')
                    )
                ))
                ->setParameter(':event', $entity->getId());

            $tp = $qeury_tp->getQuery()->getResult();

            if (count($tp)) {
                /** @var TimePrice $tp_one */
                foreach ($tp as $tp_one) {
                    $tickets = $tp_one->getTickets();
                    if (count($tickets)) {
                        $array_user_send = [];
                        /** @var Ticket $ticket */
                        foreach ($tickets as $ticket) {
                            /** @var User $user */
                            $user = $ticket->getUser();
                            $array_user_send[$user->getId()] = $user;


                        }

                        if (count($array_user_send)) {
                            /** @var User $us1 */
                            foreach ($array_user_send as $us1) {
                                $message_array = [
                                    '{{ FLName }}' => $us1->getFLName(),
                                    '{{ nameEvent }}' => $entity->getName(),
                                    '{{ linkEvent }}' => $route->generate('events_view', ['url' => $entity->getUrl()], true),
                                    '{{ dateEvent }}' => $tp_one->getDateEvent()->format('d.m.Y'),
                                    '{{ timeEvent }}' => $tp_one->getTimefrom()->format('H:i'),
                                ];

                                $mailer->sendMailToUser(9, $message_array, $us1->getEmail());
                            }
                        }
                    }
                }
            }
        }
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();


        if ($entity instanceof Event) {
            $images = $entity->getImages();
            if (!$images) {
                $entityManager = $args->getEntityManager();

                $mediaGalleryManager = $this->mg;
                /** @var Gallery $gallery */
                $gallery = $mediaGalleryManager->create();
                $gallery->setContext('Events_gallery');
                $gallery->setDefaultFormat('Events_gallery');
                $gallery->setName('Картинки/Видео для мероприятия под id - "' . $entity->getId() . '"');
                $gallery->setEnabled(true);
                $mediaGalleryManager->save($gallery);

                $entity->setImages($gallery);

                $entityManager->persist($entity);
                $entityManager->flush();
            } else {
                $mediaGalleryManager = $this->mg;
                $images->setName($images->getName() . ' под id - "' . $entity->getId() . '"');
                $mediaGalleryManager->save($images);
            }
        }
    }

}