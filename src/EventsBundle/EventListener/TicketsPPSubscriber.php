<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace EventsBundle\EventListener;


use AppBundle\Entity\Charity;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use EventsBundle\Entity\Ticket;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TicketsPPSubscriber implements EventSubscriber
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();


        if ($entity instanceof Ticket) {
            $entityManager = $args->getEntityManager();

            $entity->setCode(strtoupper(base_convert($entity->getId(), 10, 20)) . $this->get_random_after_code(75, 89, 5));

            $tp = $entity->getTimeprices();
            $event = $tp->getEvent();
            $event->setCntBuyTickets($event->getCntBuyTickets() + $entity->getCntTickets());


            $settings = $this->container->get('settings.all')->findMainSectionSettings();
            $charity = new Charity();
            $charity->setUser($entity->getUser());
            $charity->setTimeprice($tp);
            $percent = $settings["charity"]->value;
            $charity->setSum($entity->getFullPrice() / 100 * $percent);
            $entityManager->persist($charity);


            $entityManager->persist($event);
            $entityManager->flush();
        }
    }

    private function get_random_after_code($start, $end, $cnt)
    {
        $str = "";

        for ($i = 0; $i < $cnt; $i++) {
            $str .= chr(rand($start, $end));
        }

        return $str;
    }
}