<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.04.16
 * Time: 9:04
 */

namespace EventsBundle\Command;


use AppBundle\Entity\Gifts;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Ticket;
use FeedBundle\Entity\Feed;
use PersonalAreaBundle\Service\Vk;
use Sonata\UserBundle\Entity\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ulogin\AuthBundle\Entity\UloginUser;
use Symfony\Component\DependencyInjection\Container;


class CheckTickets extends Command
{
    protected $defaultName;
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
//        $this->defaultName = $defaultName;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('check:tickets')
            ->setDescription('Check tickets for gifts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();
        /** @var $entityManager EntityManager */
        $entityManager = $this->container->get('doctrine')->getEntityManager();
        /** @var $userManager UserManager */
        $userManager = $this->container->get('fos_user.user_manager');

        $ticket_query = $entityManager->getRepository('EventsBundle:Ticket')->createQueryBuilder('ticket');
        $ticket_query
            ->innerJoin('ticket.timeprices', 'timeprices', 'WITH', 'timeprices.date_event_end = CURRENT_DATE() and timeprices.timeto <= CURRENT_TIME()')
            ->innerJoin('timeprices.event', 'event')
            ->innerJoin('event.company', 'company')
            ->innerJoin('company.usercompany', 'usercompany')
            ->andWhere('ticket.getMoney = false');
        $events_result = $ticket_query->getQuery()->getResult();


        if (count($events_result)) {
            /** @var Ticket $item */
            foreach ($events_result as $item) {
                $output->writeln($item->getId());
                $item->setGetMoney(true);
                $entityManager->persist($item);

                if ($item->getFullPrice()) {
                    $event = $item->getTimeprices()->getEvent();
                    $user = $event->getCompany()->getUsercompany();

                    $user->setWithdrawMoney($user->getWithdrawMoney() + $item->getFullPrice());
                    $entityManager->persist($user);


                    $repo_feed = new Feed();
                    $repo_feed->setTicket($item);
                    $repo_feed->setOnlyuser($item->getUser());
                    $repo_feed->setCity($event->getCity());
                    $entityManager->persist($repo_feed);
                }
            }

            $entityManager->flush();
        }

        $output->writeln('close');
    }
}