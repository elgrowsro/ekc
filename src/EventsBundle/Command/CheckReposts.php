<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.04.16
 * Time: 9:04
 */

namespace EventsBundle\Command;


use AppBundle\Entity\Gifts;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use EventsBundle\Entity\Event;
use PersonalAreaBundle\Service\Vk;
use Sonata\UserBundle\Entity\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ulogin\AuthBundle\Entity\UloginUser;


class CheckReposts extends Command
{
    protected $defaultName;
    protected $container;

    public function __construct(/*$defaultName*/)
    {
//        $this->defaultName = $defaultName;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('check:reposts')
            ->setDescription('Check reposts for gifts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();
        /** @var $entityManager EntityManager */
        $entityManager = $this->container->get('doctrine')->getEntityManager();
        /** @var $userManager UserManager */
        $userManager = $this->container->get('fos_user.user_manager');


        /** @var Vk $vk */
        $vk = $this->container->get('vk.api');

//        $vars = array(
//            'owner_id' => $this->getParameter('vk_api.owner_id'),
//            'post_id' => '160',
//        );

        $events_query = $entityManager->getRepository('EventsBundle:Event')->createQueryBuilder('event');
        $events_query->andWhere('event.postVkDateClosed = CURRENT_DATE()');
        $events_result = $events_query->getQuery()->getResult();

        if ($events_result) {
            /** @var Event $event */
            foreach ($events_result as $event) {
                $vars = [
                    'owner_id' => $this->container->getParameter('vk_api.owner_id'),//'-19334776',
                    'post_id' => $event->getIdPostVk(),
                ];
                $response = $vk->api('wall.getReposts', $vars);

                $reposts = $response['items'];
                $array_user = [];

                if (count($reposts)) {
                    foreach ($reposts as $repost) {
                        $array_user[] = 'http://vk.com/id' . $repost['from_id'];
                    }
                }

                if (count($array_user)) {
                    $ulogin_query = $entityManager->getRepository('UloginAuthBundle:UloginUser')->createQueryBuilder('uloginUser');
                    $ulogin_query
                        ->innerJoin('uloginUser.userId', 'userId', 'WITH', ' userId.enabled = true ')
                        ->andWhere($ulogin_query->expr()->in('uloginUser.identity', $array_user));

                    $users_result = $ulogin_query->getQuery()->getResult();

                    if (count($users_result)) {
                        if (count($users_result) > $event->getPostVkCntWin()) {
                            $users_result_rand = array_rand($users_result, $event->getPostVkCntWin());
                        } else {
                            $users_result_rand = $users_result;
                        }
                        /** @var UloginUser $item_user */
                        foreach ($users_result_rand as $item_user) {
                            $gift = new Gifts();
                            $gift->setSum($event->getPostVkSumWin());
                            $gift->setUser($item_user->getUserId());
                            $gift->setEvent($event);
                            $entityManager->persist($gift);

                            $link_event = $this->container->get('router')->generate('events_view', ['url' => $event->getUrl()], true);
                            $message_array = [
                                '{{ FLName }}' => $item_user->getUserId()->getFLName(),
                                '{{ sum }}' => $event->getPostVkSumWin(),
                                '{{ eventName }}' => $event->getName(),
                                '{{ eventLink }}' => $link_event,
                                '{{ groupLink }}' => $this->container->getParameter('vk_api.link_group'),
                            ];

                            $this->container->get('app.emailer')->sendMailToUser(8, $message_array, $item_user->getUserId()->getEmail());
                        }
                    }
                }
            }
            $entityManager->flush();
        }

        $text = '';
        $output->writeln($text);
    }
}