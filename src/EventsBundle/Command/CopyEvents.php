<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.04.16
 * Time: 9:04
 */

namespace EventsBundle\Command;


use AppBundle\Entity\Gifts;
use AppBundle\Entity\User;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Doctrine\ORM\EntityManager;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Ticket;
use FeedBundle\Entity\Feed;
use PersonalAreaBundle\Service\Vk;
use Sonata\UserBundle\Entity\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ulogin\AuthBundle\Entity\UloginUser;
use Symfony\Component\DependencyInjection\Container;


class CopyEvents extends Command
{
    protected $defaultName;
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
//        $this->defaultName = $defaultName;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('copy:events')
            ->setDescription('Check tickets for gifts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();
        /** @var $entityManager EntityManager */
        $entityManager = $this->container->get('doctrine')->getEntityManager();
        /** @var $userManager UserManager */
        $userManager = $this->container->get('fos_user.user_manager');
        $cnt_copy = 10;

        $ticket_query = $entityManager->getRepository('EventsBundle:Event')->createQueryBuilder('event');
        $ticket_query
            ->andWhere('event.isActive = true')
            ->andWhere('event.charity = false')/*->setMaxResults(1)*/
        ;
        $events_result = $ticket_query->getQuery()->getResult();


        if (count($events_result)) {
            /** @var Event $item */
            foreach ($events_result as $item) {
                for ($i = 0; $i < $cnt_copy; $i++) {
                    $event_copy = clone $item;
                    $event_copy->setNullMainImage();

                    $mediaGalleryManager = $this->container->get('sonata.media.manager.gallery');
                    /** @var Gallery $gallery */
                    $gallery = $mediaGalleryManager->create();
                    $gallery->setContext('Events_gallery');
                    $gallery->setDefaultFormat('Events_gallery');
                    $gallery->setName('Картинки/Видео для мероприятия под id - clone"' . $event_copy->getId() . '"');
                    $gallery->setEnabled(true);
                    $mediaGalleryManager->save($gallery);

                    $event_copy->setImages($gallery);

                    $entityManager->persist($event_copy);
                }
            }

            $entityManager->flush();
        }

        $output->writeln('close');
    }
}