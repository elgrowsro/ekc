<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.06.16
 * Time: 14:00
 */

namespace EventsBundle\Validator;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EventTimeValidator extends ConstraintValidator
{
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }


    public function validate($value, Constraint $constraint)
    {

        if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
            // If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();

            // If you're using the old 2.4 validation API
            /*
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => $value)
            );
            */
        }
    }
}