<?php

namespace EventsBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use EventsBundle\Entity\AddService;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Question;
use EventsBundle\Entity\Ticket;
use EventsBundle\Entity\TimePrice;
use EventsBundle\Form\CommentEventType;
use EventsBundle\Form\DonationType;
use EventsBundle\Form\GiftSendType;
use EventsBundle\Form\QuestionType;
use EventsBundle\Form\TicketType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function viewAction(Request $request, $url)
    {
        /** @var User $user */
        $user = $this->getUser();

        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Event');

        $query = $repository->createQueryBuilder('e');
        $query->select(["e", "tp", "tick"])
            ->leftJoin('e.getTimePrice', 'tp', 'WITH', 'tp.places > 0')
            ->leftJoin('tp.tickets', 'tick')
            ->where('e.url = :url')
            ->setParameter('url', $url);

        /** @var Event $event */
        $event = $query->getQuery()->getOneOrNullResult();

        if (!$event) {
            throw new NotFoundHttpException("Page not found");
        }

        /** @var EntityRepository $mediaGalleryManager */
        $mediaGalleryManager = $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media');
        if ($event->getImages()) {
            $event_images = $mediaGalleryManager->createQueryBuilder('i')
                ->select(["i"])
                ->innerJoin('i.galleryHasMedias', 'galleryHasMedias')
                ->innerJoin('galleryHasMedias.gallery', 'gallery')
                ->andWhere('gallery.id = :gid')
                ->andWhere("i.providerName = 'sonata.media.provider.image'")
                ->setParameter(':gid', $event->getImages()->getId())
                ->getQuery();
            $gallary_images = $event_images->getResult();
        } else {
            $gallary_images = null;
        }

        if ($event->getImages()) {
            $youtubes_video = $mediaGalleryManager->createQueryBuilder('m')
                ->select(["m"])
                ->innerJoin('m.galleryHasMedias', 'galleryHasMedias')
                ->innerJoin('galleryHasMedias.gallery', 'gallery')
                ->andWhere('gallery.id = :gid')
                ->andWhere("m.providerName = 'sonata.media.provider.youtube'")
                ->setParameter(':gid', $event->getImages()->getId())
                ->getQuery();
            $gallary_youtube = $youtubes_video->getResult();
        } else {
            $gallary_youtube = null;
        }

        $ticket_class = new Ticket();

        $services = $event->getGetService();

        $price_by_id = [];
        /** @var AddService $service */
        foreach ($services as $service) {
            $price_by_id[$service->getId()] = $service->getPrice();
        }

        if ($event->getCharity()) {
            $form_donation = $this->createForm(DonationType::class, null, [
                'validation_groups' => ['donation_event'],
                'action' => $this->generateUrl('events_view.charity.donate', ['url' => $event->getUrl()]),
            ]);
        }

        if (!$event->getCharity() || !$user) {
            $ticket_class->setService($services);
            $ticket_class->setCntTickets(1);
            $form_ticket = $this->createForm(TicketType::class, $ticket_class, [
                'validation_groups' => ['buy_ticket'],
                'action' => $this->generateUrl('ticket.buy', ['event_id' => $event->getId()]),
                'event_id' => $event->getId()
            ]);
        }

        if ($user) {
            $question = new Question();
            $question->setEvent($event);
            $form_question = $this->createForm(new QuestionType(), $question, [
                'validation_groups' => ['create_question'],
                'action' => $this->generateUrl('create_question')
            ]);

            $comment = new Comment();
            $comment->setEvent($event);

            $form_comment = $this->createForm(new CommentEventType(), $comment, [
                'validation_groups' => ['create_comment_event'],
                'action' => $this->generateUrl('check_comment')
            ]);

            $form_gift = $this->createForm(GiftSendType::class, null, [
                'validation_groups' => ['gift_ticket'],
                'action' => $this->generateUrl('ticket.gift', ['event_id' => $event->getId()])
            ]);
            $form_gift->get('cntTickets')->setData(1);
        }

        $timeprice_array = [];
        $timeprice_full_array = [];

        $tp_from_event = $event->getGetTimePrice();

        /** @var TimePrice $item_tp */
        foreach ($tp_from_event as $item_tp) {
            if ($item_tp->getPlaces() && ($item_tp->getCountTicketsBuy() < $item_tp->getPlaces())) {
                $timeprice_array[$item_tp->getDateEvent()->format('Y-m-d')][] = $item_tp->getTimefrom()->format('H:i');
                $timeprice_full_array[$item_tp->getDateEvent()->format('Y-m-d') . ' ' . $item_tp->getTimefrom()->format('H:i')] = [
                    'id' => $item_tp->getId(),
                    'price' => $item_tp->getPrice()
                ];
            }
        }

        if ($user) {
            if ($event->getCharity()) {
                return $this->render('EventsBundle:Default:view.html.twig', [
                    'event' => $event,
                    'form_donation' => $form_donation->createView(),
                    'form_question' => $form_question->createView(),
                    'form_comment' => $form_comment->createView(),
                    'price_by_id' => $price_by_id,
                    'json_tp' => $timeprice_array,
                    'json_prices' => $timeprice_full_array,
                    'gallary_youtube' => $gallary_youtube,
                    'gallary_images' => $gallary_images,
                ]);
            } else {
                return $this->render('EventsBundle:Default:view.html.twig', [
                    'event' => $event,
                    'form_ticket' => $form_ticket->createView(),
                    'form_question' => $form_question->createView(),
                    'form_comment' => $form_comment->createView(),
                    'form_gift' => $form_gift->createView(),
                    'price_by_id' => $price_by_id,
                    'json_tp' => $timeprice_array,
                    'json_prices' => $timeprice_full_array,
                    'gallary_youtube' => $gallary_youtube,
                    'gallary_images' => $gallary_images,
                ]);
            }
        } else {
            if ($event->getCharity()) {
                return $this->render('EventsBundle:Default:view.html.twig', [
                    'event' => $event,
                    'form_donation' => $form_donation->createView(),
                    'price_by_id' => $price_by_id,
                    'json_tp' => $timeprice_array,
                    'json_prices' => $timeprice_full_array,
                    'gallary_youtube' => $gallary_youtube,
                    'gallary_images' => $gallary_images,
                ]);
            } else {
                return $this->render('EventsBundle:Default:view.html.twig', [
                    'event' => $event,
                    'form_ticket' => $form_ticket->createView(),
                    'price_by_id' => $price_by_id,
                    'json_tp' => $timeprice_array,
                    'json_prices' => $timeprice_full_array,
                    'gallary_youtube' => $gallary_youtube,
                    'gallary_images' => $gallary_images,
                ]);
            }
        }
    }
}
