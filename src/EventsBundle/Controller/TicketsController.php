<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.07.16
 * Time: 17:05
 */

namespace EventsBundle\Controller;


use AppBundle\Entity\Gifts;
use AppBundle\Entity\User;
use EventsBundle\Entity\AddService;
use EventsBundle\Entity\Ticket;
use EventsBundle\Entity\TimePrice;
use EventsBundle\Form\GiftSendType;
use EventsBundle\Form\TicketType;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TicketsController extends Controller
{
    public function buyAction(Request $request, $event_id)
    {
        $ticket_class = new Ticket();


        $form_ticket = $this->createForm(TicketType::class, $ticket_class, [
            'validation_groups' => ['buy_ticket'],
            'event_id' => $event_id
        ]);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_ticket->handleRequest($request);
//            $ticket_class->setCntTickets($form_ticket->get('cntTickets')->getData());
            $get_tp = $ticket_class->getTimeprices();


            if ($form_ticket->isValid()) {

                if (!($get_tp->getPlaces() && ($get_tp->getCountTicketsBuy() < $get_tp->getPlaces()))) {
                    $form_ticket->get('timeprices')->addError(new FormError('На выбранное Вами время билеты уже отсутствуют'));

                    $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    )));

                }

                $em = $this->get('em');

                $full_price_ticket = $get_tp->getPrice() * $ticket_class->getCntTickets();

                /** @var AddService $item_service */
                foreach ($ticket_class->getService() as $item_service) {
                    $full_price_ticket += (int)$item_service->getPrice();
                }


                if ($full_price_ticket > $user->getMoney()) {
                    $form_ticket->addError(new FormError("У Вас недостаточно средств для покупки билета"));

                    $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    )));
                }

                $user->setMoney($user->getMoney() - $full_price_ticket);


                $ticket_class->setUser($user);
                $ticket_class->setFullPrice($full_price_ticket);

                $em->persist($user);

                $em->persist($ticket_class);
                $em->flush();

                $timeprice_buy = $ticket_class->getTimeprices();
                $event_buy = $timeprice_buy->getEvent();

                $services = "";
                /** @var AddService $item */
                foreach ($ticket_class->getService() as $item) {
                    if ($services) {
                        $services .= ', ' . $item->getName();
                    } else {
                        $services = $item->getName();
                    }
                }

                $message_array = [
                    '{{ FLNameBuyer }}' => $user->getFLName(),
                    '{{ cntTicket }}' => $ticket_class->getCntTickets(),
                    '{{ nameEvent }}' => $event_buy->getName(),
                    '{{ linkEvent }}' => $this->generateUrl('events_view', ['url' => $event_buy->getUrl()], true),
                    '{{ dateEvent }}' => $timeprice_buy->getDateEvent()->format('d.m.Y'),
                    '{{ timeEvent }}' => $timeprice_buy->getTimefrom()->format('H:i'),
                    '{{ services }}' => $services,
                    '{{ code }}' => $ticket_class->getCode(),
                ];

                $this->get('app.emailer')->sendMailToUser(4, $message_array, $user->getEmail());

                /** @var User $usercompany */
                $usercompany = $event_buy->getCompany()->getUsercompany();
                $message_array = [
                    '{{ FLName }}' => $usercompany->getFLName(),
                    '{{ FLNameBuyer }}' => $user->getFLName(),
                    '{{ linkBuyer }}' => $this->generateUrl('profiles_view', ['url' => $user->getUrl()], true),
                    '{{ cntTicket }}' => $ticket_class->getCntTickets(),
                    '{{ eventName }}' => $event_buy->getName(),
                    '{{ linkEvent }}' => $this->generateUrl('events_view', ['url' => $event_buy->getUrl()], true),
                    '{{ dateEvent }}' => $timeprice_buy->getDateEvent()->format('d.m.Y'),
                    '{{ timeEvent }}' => $timeprice_buy->getTimefrom()->format('H:i'),
                    '{{ services }}' => $services,
                    '{{ sum }}' => $ticket_class->getFullPrice(),
                ];

                $this->get('app.emailer')->sendMailToUser(5, $message_array, $usercompany->getEmail());


                return new Response(json_encode(array(
                    'status' => 'success',
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function giftAction(Request $request, $event_id)
    {
        $ticket_class = new Ticket();
        $form_ticket = $this->createForm(GiftSendType::class, null, [
            'validation_groups' => ['gift_ticket'],
            'event_id' => $event_id
        ]);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_ticket->handleRequest($request);
//            $ticket_class->setCntTickets($form_ticket->get('cntTickets')->getData());
            $get_tp = $form_ticket->get('timeprices')->getData();

            if ($form_ticket->isValid()) {
                if (!($get_tp->getPlaces() && ($get_tp->getCountTicketsBuy() < $get_tp->getPlaces()))) {
                    $form_ticket->get('timeprices')->addError(new FormError('На выбранное Вами время билеты уже отсутствуют'));

                    $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    )));
                }

                $em = $this->get('em');
                $gift = new Gifts();

                $full_price_ticket = $get_tp->getPrice() * $form_ticket->get('cntTickets')->getData();

                /** @var AddService $item_service */
                foreach ($form_ticket->get('service')->getData() as $item_service) {
                    $full_price_ticket += (int)$item_service->getPrice();
                }


                if ($full_price_ticket > $user->getMoney()) {
                    $form_ticket->addError(new FormError("У Вас недостаточно средств для покупки билета"));

                    $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    )));
                }
                $userManager = $this->get('fos_user.user_manager');
                /** @var User $user_gift */
                $user_gift = $userManager->findUserByEmail($form_ticket->get('email')->getData());
                if (!$user_gift) {
                    $user_gift = $userManager->createUser();
                    $user_gift->setFirstname($form_ticket->get('firstname')->getData());
                    $user_gift->setLastname($form_ticket->get('secondname')->getData());
                    $user_gift->setPhone($form_ticket->get('phone')->getData());
                    $user_gift->setEmail($form_ticket->get('email')->getData());

                    $formNewHandler = $this->get('sonata.user.registration.form.handler');
                    $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

                    $process = $formNewHandler->processNew($user_gift, $confirmationEnabled);

                }

                $user->setMoney($user->getMoney() - $full_price_ticket);


                $ticket_class->setCntTickets($form_ticket->get('cntTickets')->getData());
                $ticket_class->setTimeprices($get_tp);
                $ticket_class->setUser($user_gift);
                $ticket_class->setFullPrice($full_price_ticket);


                $em->persist($user);

                $em->persist($ticket_class);

                $gift->setUser($user);
                $gift->setUserMake($user_gift);
                $gift->setGet(true);
                $gift->setTicket($ticket_class);
                $em->persist($gift);

                $em->flush();

                $timeprice_buy = $ticket_class->getTimeprices();
                $event_buy = $timeprice_buy->getEvent();

                $services = "";
                /** @var AddService $item */
                foreach ($ticket_class->getService() as $item) {
                    if ($services) {
                        $services .= ', ' . $item->getName();
                    } else {
                        $services = $item->getName();
                    }
                }

                $message_array = [
                    '{{ FLNameBuyer }}' => $user_gift->getFLName(),
                    '{{ FLNameGift }}' => $user->getFLName(),
                    '{{ cntTicket }}' => $ticket_class->getCntTickets(),
                    '{{ nameEvent }}' => $event_buy->getName(),
                    '{{ linkEvent }}' => $this->generateUrl('events_view', ['url' => $event_buy->getUrl()], true),
                    '{{ dateEvent }}' => $timeprice_buy->getDateEvent()->format('d.m.Y'),
                    '{{ timeEvent }}' => $timeprice_buy->getTimefrom()->format('H:i'),
                    '{{ services }}' => $services,
                    '{{ code }}' => $ticket_class->getCode(),
                ];

                $this->get('app.emailer')->sendMailToUser(7, $message_array, $user_gift->getEmail());

                /** @var User $usercompany */
                $usercompany = $event_buy->getCompany()->getUsercompany();
                $message_array = [
                    '{{ FLName }}' => $usercompany->getFLName(),
                    '{{ FLNameBuyer }}' => $user_gift->getFLName(),
                    '{{ linkBuyer }}' => $this->generateUrl('profiles_view', ['url' => $user_gift->getUrl()], true),
                    '{{ cntTicket }}' => $ticket_class->getCntTickets(),
                    '{{ eventName }}' => $event_buy->getName(),
                    '{{ linkEvent }}' => $this->generateUrl('events_view', ['url' => $event_buy->getUrl()], true),
                    '{{ dateEvent }}' => $timeprice_buy->getDateEvent()->format('d.m.Y'),
                    '{{ timeEvent }}' => $timeprice_buy->getTimefrom()->format('H:i'),
                    '{{ services }}' => $services,
                    '{{ sum }}' => $ticket_class->getFullPrice(),
                ];

                $this->get('app.emailer')->sendMailToUser(5, $message_array, $usercompany->getEmail());


                return new Response(json_encode(array(
                    'status' => 'success',
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

}