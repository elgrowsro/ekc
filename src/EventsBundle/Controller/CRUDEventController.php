<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 25.03.16
 * Time: 14:03
 */

namespace EventsBundle\Controller;

use EventsBundle\Entity\Event;
use PersonalAreaBundle\Service\Vk;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Exception\LockException;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\Form\Form;

class CRUDEventController extends CRUDController
{
    /**
     * The related Admin class.
     *
     * @var AdminInterface
     */
    protected $admin;

    public function editAction($id = null)
    {
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());
        /** @var Event $object */
        $object = $this->admin->getObject($id);
        $isActive_old = $object->getIsActive();

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $this->admin->checkAccess('edit', $object);

        $preResponse = $this->preEdit($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        /** @var $form Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($object);
            }
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                try {
                    $object = $this->admin->update($object);


                    $isActive_new = $object->getIsActive();
                    if ($isActive_old == false && $isActive_new == true) {

                        $sonata_admin_edit_object = $this->generateUrl('events_view', ['url' => $object->getUrl()], true);

                        $user_event = $object->getCompany()->getUsercompany();

                        $message_array = [
                            '{{ FLName }}' => $user_event->getFLName(),
                            '{{ eventName }}' => $object->getName(),
                        ];
                        $this->get('app.emailer')->sendMailToUser(3, $message_array, $user_event->getEmail());


                        if ($object->getPostVkDateClosed() && !$object->getIdPostVk()) {
                            /** @var Vk $vk */
                            $vk = $this->get('vk.api');

                            $vars = [
                                'owner_id' => $this->getParameter('vk_api.owner_id'),
                                'guid' => $object->getId() . $this->getParameter('vk_api.after_id'),
                                'from_group' => 1,
                                'message' => 'Всем привет! На нашем сайте появилось новое мероприятие!!!',
                                'attachments' => 'http://fordealers.pro/',// $this->generateUrl('events_view', ['url' => $event->getUrl()], true),
                                'lat' => $object->getLat(),
                                'lng' => $object->getLng(),
                            ];

                            $response = $vk->api('wall.post', $vars);

                            if (isset($response['post_id'])) {
                                $vk_post_id = $response['post_id'];
                                $object->setIdPostVk($vk_post_id);

//                                try {
//                                    $hash_widget = $this->getHashWidget();
//                                } catch (\Exception $e) {
                                    $hash_widget = false;
//                                }

                                if ($hash_widget) {
                                    $group_id = $this->getParameter('vk_api.owner_id');

                                    $widget_for_event = '<script type="text/javascript">
                                      (function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//vk.com/js/api/openapi.js?127"; fjs.parentNode.insertBefore(js, fjs); }(document, \'script\', \'vk_openapi_js\'));
                                      (function() {
                                        if (!window.VK || !VK.Widgets || !VK.Widgets.Post || !VK.Widgets.Post(\'vk_post_' . $group_id . '_' . $vk_post_id . '\', ' . $group_id . ', ' . $vk_post_id . ', \'' . $hash_widget . '\', {redesign: 1, width: 665})) setTimeout(arguments.callee, 50);
                                      }());
                                    </script>';
                                    $object->setPostVkWidget($widget_for_event);
                                }
                                $object = $this->admin->update($object);
                            }
                        }
                    }

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object),
                            'objectName' => $this->escapeHtml($this->admin->toString($object)),
                        ), 200, array());
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            'flash_edit_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->admin->trans('flash_lock_error', array(
                        '%name%' => $this->escapeHtml($this->admin->toString($object)),
                        '%link_start%' => '<a href="' . $this->admin->generateObjectUrl('edit', $object) . '">',
                        '%link_end%' => '</a>',
                    ), 'SonataAdminBundle'));
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->admin->trans(
                            'flash_edit_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ), null);
    }

    private function getHashWidget()
    {
        $login = $this->getParameter('vk_api.login');
        $password = $this->getParameter('vk_api.password');
        $security_check_code = 'XXXXXXXX';
        $security_check_code = '89126892037';
        $headers = array(
            'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'content-type' => 'application/x-www-form-urlencoded',
            'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'
        );

// получаем главную страницу
        $get_main_page = $this->post('https://vk.com', array(
            'headers' => array(
                'accept: ' . $headers['accept'],
                'content-type: ' . $headers['content-type'],
                'user-agent: ' . $headers['user-agent']
            )
        ));

// парсим с главной страницы параметры ip_h и lg_h
        preg_match('/name=\"ip_h\" value=\"(.*?)\"/s', $get_main_page['content'], $ip_h);
        preg_match('/name=\"lg_h\" value=\"(.*?)\"/s', $get_main_page['content'], $lg_h);


// посылаем запрос на авторизацию
        $post_auth = $this->post('https://login.vk.com/?act=login', array(
            'params' => 'act=login&role=al_frame&_origin=' . urlencode('http://vk.com') . '&ip_h=' . $ip_h[1] . '&lg_h=' . $lg_h[1] . '&email=' . urlencode($login) . '&pass=' . urlencode($password),
            'headers' => array(
                'accept: ' . $headers['accept'],
                'content-type: ' . $headers['content-type'],
                'user-agent: ' . $headers['user-agent']
            ),
            'cookies' => $get_main_page['cookies']
        ));

// получаем ссылку для редиректа после авторизации
        preg_match('/Location\: (.*)/s', $post_auth['headers'], $post_auth_location);

        if (!preg_match('/\_\_q\_hash=/s', $post_auth_location[1])) {
            mail($this->getParameter('errors.log'), 'Ошибка VK', 'Не удалось авторизоваться <br /> <br />' . $post_auth['headers']);

            return false;
        }
// переходим по полученной для редиректа ссылке
        $get_auth_location = $this->post($post_auth_location[1], array(
            'headers' => array(
                'accept: ' . $headers['accept'],
                'content-type: ' . $headers['content-type'],
                'user-agent: ' . $headers['user-agent']
            ),
            'cookies' => $post_auth['cookies']
        ));

// получаем ссылку на свою страницу
        $my_page_id = $this->getParameter('vk_api.sender_id');


        $get_my_page = $this->getUserPage($my_page_id, $get_auth_location['cookies']);

// если запрошена проверка безопасности
        if (preg_match('/act=security\_check/s', $get_my_page['headers'])) {
            preg_match('/Location\: (.*)/s', $get_my_page['headers'], $security_check_location);

            // переходим на страницу проверки безопасности
            $get_security_check_page = $this->post('https://vk.com' . $security_check_location[1], array(
                'headers' => array(
                    'accept: ' . $headers['accept'],
                    'content-type: ' . $headers['content-type'],
                    'user-agent: ' . $headers['user-agent']
                ),
                'cookies' => $get_auth_location['cookies']
            ));

            // получаем hash для запроса на проверку мобильного телефона
            preg_match('/hash: \'(.*?)\'/s', $get_security_check_page['content'], $get_security_check_page_hash);

            // вводим запрошенные цифры мобильного телефона
            $post_security_check_code = $this->post('https://vk.com/login.php', array(
                'params' => 'act=security_check&code=' . $security_check_code . '&al_page=2&hash=' . $get_security_check_page_hash[1],
                'headers' => array(
                    'accept: ' . $headers['accept'],
                    'content-type: ' . $headers['content-type'],
                    'user-agent: ' . $headers['user-agent']
                ),
                'cookies' => $get_auth_location['cookies']
            ));

            mail($this->getParameter('errors.log'), 'Ошибка VK', 'Запрошена проверка безопасности. Необходимо изменить значение в переменной $security_check_code и обновить страницу.');
            $post_hash = false;

        } else {

            $post_hash = $this->vkGetPostHash('https://vk.com/wall-66335977_154', $get_auth_location['cookies']);
        }

        return $post_hash;
    }


    private function getUserPage($id = null, $cookies = null)
    {
        global $headers;

        $get = $this->post('https://vk.com/id' . $id, array(
            'headers' => array(
                'accept: ' . $headers['accept'],
                'content-type: ' . $headers['content-type'],
                'user-agent: ' . $headers['user-agent']
            ),
            'cookies' => $cookies
        ));

        return $get;
    }

    private function vkGetPostHash($url = null, $cookies = null)
    {
        global $headers;

        preg_match('/(wall)(\-?[0-9]+)\_([0-9]+)/', $url, $matches);

        $get = $this->post('https://vk.com/dev.php', array(
            'params' => 'act=a_get_post_hash&al=1&post=' . $matches[2] . '_' . $matches[3],
            'headers' => array(
                'accept: ' . $headers['accept'],
                'content-type: ' . $headers['content-type'],
                'user-agent: ' . $headers['user-agent']
            ),
            'cookies' => $cookies
        ));

        preg_match('/\<(.*)\>(.*)/is', $get['content'], $hash_matches);

        $hash = $hash_matches[2];

        return $hash;
    }

    private function post($url = null, $params = null)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

        if (isset($params['params'])) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params['params']);
        }

        if (isset($params['headers'])) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']);
        }

        if (isset($params['cookies'])) {
            curl_setopt($ch, CURLOPT_COOKIE, $params['cookies']);
        }

        $result = curl_exec($ch);

        list($headers, $result) = explode("\r\n\r\n", $result, 4);

        preg_match_all('|Set-Cookie: (.*);|U', $headers, $parse_cookies);

        $cookies = implode(';', $parse_cookies[1]);

        curl_close($ch);

        return array('headers' => $headers, 'cookies' => $cookies, 'content' => $result);
    }

}