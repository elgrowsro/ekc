<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 01.07.16
 * Time: 11:24
 */

namespace EventsBundle\Controller;

use AppBundle\Entity\User;
use EventsBundle\Entity\Question;
use EventsBundle\Form\QuestionType;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionsController extends Controller
{
    public function loadAction(Request $request, $event_id = null, $user_id = null, $page)
    {
        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Question');

        $query = $repository->createQueryBuilder('q')
            ->select(["q"])
            ->andWhere('q.isActive = true')
            ->orderBy('q.id', 'DESC');

        $query
            ->andWhere("q.answer != ''");

        if ($event_id) {
            $query
                ->andWhere('q.event = :event_id')
                ->andWhere("q.answer != ''")
                ->setParameter('event_id', $event_id);
        }
        if ($user_id) {
            $query
                ->andWhere('q.user = :user_id')
                ->setParameter('user_id', $user_id);
        }

        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_events_questions */
        $pagination_events_questions = $paginator->paginate(
            $query,
            $page,
            10
        );

        if ($event_id) {
            $pagination_events_questions->setUsedRoute('events_load_questions');
        } elseif ($user_id) {
            $pagination_events_questions->setUsedRoute('company_load_questions');
        }


        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('EventsBundle:question:load.html.twig', [
                'questions' => $pagination_events_questions,
                'event_id' => $event_id,
                'user_id' => $user_id
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('EventsBundle:question:load.html.twig', [
                'questions' => $pagination_events_questions,
                'event_id' => $event_id,
                'user_id' => $user_id
            ]);
            return $events_render;
        }
    }

    public function addAction(Request $request)
    {
        $question_class = new Question();


        $form_question = $this->createForm(QuestionType::class, $question_class, [
            'validation_groups' => ['create_question'],
        ]);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {
                $em = $this->get('em');

                $question_class->setUser($user);

                $em->persist($question_class);
                $em->flush();


                return new Response(json_encode(array(
                    'status' => 'success',
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }
}