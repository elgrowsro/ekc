<?php

namespace EventsBundle\Entity;

/**
 * Donations
 */
class Donations
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Donations
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Donations
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \EventsBundle\Entity\Event
     */
    private $event;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Donations
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return Donations
     */
    public function setEvent(\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \EventsBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }
    /**
     * @var string
     */
    private $money;


    /**
     * Set money
     *
     * @param string $money
     *
     * @return Donations
     */
    public function setMoney($money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * Get money
     *
     * @return string
     */
    public function getMoney()
    {
        return $this->money;
    }
}
