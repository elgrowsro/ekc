<?php

namespace EventsBundle\Entity;

/**
 * TimePrice
 */
class TimePrice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $timefrom;

    /**
     * @var \DateTime
     */
    private $timeto;

    /**
     * @var integer
     */
    private $price;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timefrom
     *
     * @param \DateTime $timefrom
     *
     * @return TimePrice
     */
    public function setTimefrom($timefrom)
    {
        $this->timefrom = $timefrom;

        return $this;
    }

    /**
     * Get timefrom
     *
     * @return \DateTime
     */
    public function getTimefrom()
    {
        return $this->timefrom;
    }

    /**
     * Set timeto
     *
     * @param \DateTime $timeto
     *
     * @return TimePrice
     */
    public function setTimeto($timeto)
    {
        $this->timeto = $timeto;

        return $this;
    }

    /**
     * Get timeto
     *
     * @return \DateTime
     */
    public function getTimeto()
    {
        return $this->timeto;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return TimePrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function __toString()
    {
        return (string)($this->getDateEvent()->format('d.m.Y') . ' ' . $this->getTimefrom()->format('H:i') . ' - ' . $this->getDateEventEnd()->format('d.m.Y') . ' ' . $this->getTimeto()->format('H:i'));
    }

    /**
     * @var \DateTime
     */
    private $date_event;


    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     *
     * @return TimePrice
     */
    public function setDateEvent($dateEvent)
    {
//        echo 123;
//        var_dump($dateEvent);
        $this->date_event = $dateEvent;

        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime
     */
    public function getDateEvent()
    {
        return $this->date_event;
    }

    /**
     * @var \DateTime
     */
    private $date_event_end;


    /**
     * Set dateEventEnd
     *
     * @param \DateTime $dateEventEnd
     *
     * @return TimePrice
     */
    public function setDateEventEnd($dateEventEnd)
    {
        $this->date_event_end = $dateEventEnd;

        return $this;
    }

    /**
     * Get dateEventEnd
     *
     * @return \DateTime
     */
    public function getDateEventEnd()
    {
        return $this->date_event_end;
    }

    /**
     * @var \EventsBundle\Entity\Event
     */
    private $event;


    /**
     * Set event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return TimePrice
     */
    public function setEvent(\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \EventsBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @var integer
     */
    private $places;


    /**
     * Set places
     *
     * @param integer $places
     *
     * @return TimePrice
     */
    public function setPlaces($places)
    {
        $this->places = $places;

        return $this;
    }

    /**
     * Get places
     *
     * @return integer
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tickets;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tickets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ticket
     *
     * @param \EventsBundle\Entity\Ticket $ticket
     *
     * @return TimePrice
     */
    public function addTicket(\EventsBundle\Entity\Ticket $ticket)
    {
        $this->tickets[] = $ticket;

        return $this;
    }

    /**
     * Remove ticket
     *
     * @param \EventsBundle\Entity\Ticket $ticket
     */
    public function removeTicket(\EventsBundle\Entity\Ticket $ticket)
    {
        $this->tickets->removeElement($ticket);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Get count tickets
     *
     * @return integer
     */
    public function getCountTicketsBuy()
    {
        $sum_cnt = 0;
        /** @var Ticket $ticket */
        foreach ($this->getTickets() as $ticket) {
            $sum_cnt += $ticket->getCntTickets();
        }
        return $sum_cnt;
    }
    /**
     * @var \AppBundle\Entity\Charity
     */
    private $charitys;


    /**
     * Set charitys
     *
     * @param \AppBundle\Entity\Charity $charitys
     *
     * @return TimePrice
     */
    public function setCharitys(\AppBundle\Entity\Charity $charitys = null)
    {
        $this->charitys = $charitys;

        return $this;
    }

    /**
     * Get charitys
     *
     * @return \AppBundle\Entity\Charity
     */
    public function getCharitys()
    {
        return $this->charitys;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $gifts;


    /**
     * Add gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     *
     * @return TimePrice
     */
    public function addGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts[] = $gift;

        return $this;
    }

    /**
     * Remove gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     */
    public function removeGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts->removeElement($gift);
    }

    /**
     * Get gifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGifts()
    {
        return $this->gifts;
    }
}
