<?php

namespace EventsBundle\Entity;

/**
 * Ticket
 */
class Ticket
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Ticket
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Ticket
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Ticket
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $question_event;

    /**
     * @var \EventsBundle\Entity\TimePrice
     */
    private $timeprices;

    /**
     * Add questionEvent
     *
     * @param \EventsBundle\Entity\Question $questionEvent
     *
     * @return Ticket
     */
    public function addQuestionEvent(\EventsBundle\Entity\Question $questionEvent)
    {
        $this->question_event[] = $questionEvent;

        return $this;
    }

    /**
     * Remove questionEvent
     *
     * @param \EventsBundle\Entity\Question $questionEvent
     */
    public function removeQuestionEvent(\EventsBundle\Entity\Question $questionEvent)
    {
        $this->question_event->removeElement($questionEvent);
    }

    /**
     * Get questionEvent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionEvent()
    {
        return $this->question_event;
    }

    /**
     * Set timeprices
     *
     * @param \EventsBundle\Entity\TimePrice $timeprices
     *
     * @return Ticket
     */
    public function setTimeprices(\EventsBundle\Entity\TimePrice $timeprices = null)
    {
        $this->timeprices = $timeprices;

        return $this;
    }

    /**
     * Get timeprices
     *
     * @return \EventsBundle\Entity\TimePrice
     */
    public function getTimeprices()
    {
        return $this->timeprices;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $service;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->service = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set service
     *
     * @param \Doctrine\ORM\PersistentCollection $service
     *
     * @return Ticket
     */
    public function setService(\Doctrine\ORM\PersistentCollection $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Add service
     *
     * @param \EventsBundle\Entity\AddService $service
     *
     * @return Ticket
     */
    public function addService(\EventsBundle\Entity\AddService $service)
    {
        $this->service[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \EventsBundle\Entity\AddService $service
     */
    public function removeService(\EventsBundle\Entity\AddService $service)
    {
        $this->service->removeElement($service);
    }

    /**
     * Get service
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @var integer
     */
    private $cntTickets;


    /**
     * Set cntTickets
     *
     * @param integer $cntTickets
     *
     * @return Ticket
     */
    public function setCntTickets($cntTickets)
    {
        $this->cntTickets = $cntTickets;

        return $this;
    }

    /**
     * Get cntTickets
     *
     * @return integer
     */
    public function getCntTickets()
    {
        return $this->cntTickets;
    }

    /**
     * @var integer
     */
    private $fullPrice;


    /**
     * Set fullPrice
     *
     * @param integer $fullPrice
     *
     * @return Ticket
     */
    public function setFullPrice($fullPrice)
    {
        $this->fullPrice = $fullPrice;

        return $this;
    }

    /**
     * Get fullPrice
     *
     * @return integer
     */
    public function getFullPrice()
    {
        return $this->fullPrice;
    }

    /**
     * @var string
     */
    private $code;


    /**
     * Set code
     *
     * @param string $code
     *
     * @return Ticket
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $gifts;


    /**
     * Add gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     *
     * @return Ticket
     */
    public function addGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts[] = $gift;

        return $this;
    }

    /**
     * Remove gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     */
    public function removeGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts->removeElement($gift);
    }

    /**
     * Get gifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGifts()
    {
        return $this->gifts;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return (string)$this->getId();
    }

    /**
     * @var boolean
     */
    private $getMoney = false;


    /**
     * Set getMoney
     *
     * @param boolean $getMoney
     *
     * @return Ticket
     */
    public function setGetMoney($getMoney)
    {
        $this->getMoney = $getMoney;

        return $this;
    }

    /**
     * Get getMoney
     *
     * @return boolean
     */
    public function getGetMoney()
    {
        return $this->getMoney;
    }
}
