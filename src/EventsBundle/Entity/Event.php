<?php

namespace EventsBundle\Entity;

/**
 * Event
 */
class Event
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $isActive = 1;

    /**
     * @var \DateTime
     */
    private $create_event;

    /**
     * @var \DateTime
     */
    private $last_update;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Event
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return (boolean)$this->isActive;
    }

    /**
     * Set createEvent
     *
     * @param \DateTime $createEvent
     *
     * @return Event
     */
    public function setCreateEvent($createEvent)
    {
        $this->create_event = $createEvent;

        return $this;
    }

    /**
     * Get createEvent
     *
     * @return \DateTime
     */
    public function getCreateEvent()
    {
        return $this->create_event;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Event
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $getService;

    /**
     * Add getService
     *
     * @param \EventsBundle\Entity\AddService $getService
     *
     * @return Event
     */
    public function addGetService(\EventsBundle\Entity\AddService $getService)
    {
        $this->getService[] = $getService;

        return $this;
    }

    /**
     * Remove getService
     *
     * @param \EventsBundle\Entity\AddService $getService
     */
    public function removeGetService(\EventsBundle\Entity\AddService $getService)
    {
        $this->getService->removeElement($getService);
    }

    /**
     * Get getService
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGetService()
    {
        return $this->getService;
    }


    /**
     * @var boolean
     */
    private $forMan = 0;

    /**
     * @var boolean
     */
    private $forWoman = 0;

    /**
     * @var boolean
     */
    private $forElderly = 0;


    /**
     * Set forMan
     *
     * @param boolean $forMan
     *
     * @return Event
     */
    public function setForMan($forMan)
    {
        $this->forMan = $forMan;

        return $this;
    }

    /**
     * Get forMan
     *
     * @return boolean
     */
    public function getForMan()
    {
        return (boolean)$this->forMan;
    }

    /**
     * Set forWoman
     *
     * @param boolean $forWoman
     *
     * @return Event
     */
    public function setForWoman($forWoman)
    {
        $this->forWoman = $forWoman;

        return $this;
    }

    /**
     * Get forWoman
     *
     * @return boolean
     */
    public function getForWoman()
    {
        return (boolean)$this->forWoman;
    }

    /**
     * Set forElderly
     *
     * @param boolean $forElderly
     *
     * @return Event
     */
    public function setForElderly($forElderly)
    {
        $this->forElderly = $forElderly;

        return $this;
    }

    /**
     * Get forElderly
     *
     * @return boolean
     */
    public function getForElderly()
    {
        return (boolean)$this->forElderly;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $getMenu3;


    /**
     * Add getMenu3
     *
     * @param \AppBundle\Entity\Menu $getMenu3
     *
     * @return Event
     */
    public function addGetMenu3(\AppBundle\Entity\Menu $getMenu3)
    {
        $this->getMenu3[] = $getMenu3;

        return $this;
    }

    /**
     * Remove getMenu3
     *
     * @param \AppBundle\Entity\Menu $getMenu3
     */
    public function removeGetMenu3(\AppBundle\Entity\Menu $getMenu3)
    {
        $this->getMenu3->removeElement($getMenu3);
    }

    /**
     * Get getMenu3
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGetMenu3()
    {
        return $this->getMenu3;
    }

    /**
     * @var \CompanysBundle\Entity\Company
     */
    private $company;

    /**
     * Set company
     *
     * @param \CompanysBundle\Entity\Company $company
     *
     * @return Event
     */
    public function setCompany(\CompanysBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanysBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $keywords;


    /**
     * Set url
     *
     * @param string $url
     *
     * @return Event
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Event
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $getTimePrice;


    /**
     * Add getTimePrice
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $arrayGetTimePrice
     *
     * @return Event
     */
    public function setGetTimePrice(\Doctrine\Common\Collections\ArrayCollection $arrayGetTimePrice)
    {
        $this->getTimePrice = $arrayGetTimePrice;

        return $this;
    }

    /**
     * Add getTimePrice
     *
     * @param \EventsBundle\Entity\TimePrice $getTimePrice
     *
     * @return Event
     */
    public function addGetTimePrice(\EventsBundle\Entity\TimePrice $getTimePrice)
    {
        $this->getTimePrice[] = $getTimePrice;

        return $this;
    }

    /**
     * Remove getTimePrice
     *
     * @param \EventsBundle\Entity\TimePrice $getTimePrice
     */
    public function removeGetTimePrice(\EventsBundle\Entity\TimePrice $getTimePrice)
    {
        $this->getTimePrice->removeElement($getTimePrice);
//        $getTimePrice->setEvent(null);
    }

    /**
     * Get getTimePrice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGetTimePrice()
    {
        return $this->getTimePrice;
    }


    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $main_image;


    /**
     * Set mainImage
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $mainImage
     *
     * @return Event
     */
    public function setMainImage(\Application\Sonata\MediaBundle\Entity\Media $mainImage = null)
    {
        $this->main_image = $mainImage;

        return $this;
    }

    /**
     * Get mainImage
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMainImage()
    {
        return $this->main_image;
    }

    /**
     * Set null mainImage
     *
     * @return Event
     */
    public function setNullMainImage()
    {
        $this->main_image = null;
        return $this;
    }

    /**
     * @var string
     */
    private $keywords_seo;

    /**
     * @var string
     */
    private $description_seo;


    /**
     * Set keywordsSeo
     *
     * @param string $keywordsSeo
     *
     * @return Event
     */
    public function setKeywordsSeo($keywordsSeo)
    {
        $this->keywords_seo = $keywordsSeo;

        return $this;
    }

    /**
     * Get keywordsSeo
     *
     * @return string
     */
    public function getKeywordsSeo()
    {
        return $this->keywords_seo;
    }

    /**
     * Set descriptionSeo
     *
     * @param string $descriptionSeo
     *
     * @return Event
     */
    public function setDescriptionSeo($descriptionSeo)
    {
        $this->description_seo = $descriptionSeo;

        return $this;
    }

    /**
     * Get descriptionSeo
     *
     * @return string
     */
    public function getDescriptionSeo()
    {
        return $this->description_seo;
    }

    /**
     * @var \AppBundle\Entity\City
     */
    private $city;


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Event
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @var string
     */
    private $lat;

    /**
     * @var string
     */
    private $lng;


    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return Event
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return Event
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    public function setLatLng($latlng)
    {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);
        return $this;
    }

    /**
     * @Assert\NotBlank()
     * @OhAssert\LatLng()
     */
    public function getLatLng()
    {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Gallery
     */
    private $images;

    /**
     * Set images
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $images
     *
     * @return Event
     */
    public function setImages(\Application\Sonata\MediaBundle\Entity\Gallery $images = null)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery
     */
    public function getImages()
    {
        return $this->images;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getMinPrice()
    {
        $all_tp = $this->getGetTimePrice();
        $min_sum = false;
        /** @var TimePrice $item_tp */
        foreach ($all_tp as $item_tp) {
            if ($min_sum === false || $min_sum === null) {
                $min_sum = $item_tp->getPrice();
            } elseif ($min_sum > $item_tp->getPrice()) {
                $min_sum = $item_tp->getPrice();
            }
        }

        return $min_sum;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comments_event;

    /**
     * Add commentsEvent
     *
     * @param \AppBundle\Entity\Comment $commentsEvent
     *
     * @return Event
     */
    public function addCommentsEvent(\AppBundle\Entity\Comment $commentsEvent)
    {
        $this->comments_event[] = $commentsEvent;

        return $this;
    }

    /**
     * Remove commentsEvent
     *
     * @param \AppBundle\Entity\Comment $commentsEvent
     */
    public function removeCommentsEvent(\AppBundle\Entity\Comment $commentsEvent)
    {
        $this->comments_event->removeElement($commentsEvent);
    }

    /**
     * Get commentsEvent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentsEvent()
    {
        return $this->comments_event;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $question_event;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->question_event = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments_event = new \Doctrine\Common\Collections\ArrayCollection();
        $this->getService = new \Doctrine\Common\Collections\ArrayCollection();
        $this->getTimePrice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->getMenu3 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add questionEvent
     *
     * @param \EventsBundle\Entity\Question $questionEvent
     *
     * @return Event
     */
    public function addQuestionEvent(\EventsBundle\Entity\Question $questionEvent)
    {
        $this->question_event[] = $questionEvent;

        return $this;
    }

    /**
     * Remove questionEvent
     *
     * @param \EventsBundle\Entity\Question $questionEvent
     */
    public function removeQuestionEvent(\EventsBundle\Entity\Question $questionEvent)
    {
        $this->question_event->removeElement($questionEvent);
    }

    /**
     * Get questionEvent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionEvent()
    {
        return $this->question_event;
    }

    /**
     * @var \FeedBundle\Entity\Feed
     */
    private $feed;


    /**
     * Set feed
     *
     * @param \FeedBundle\Entity\Feed $feed
     *
     * @return Event
     */
    public function setFeed(\FeedBundle\Entity\Feed $feed = null)
    {
        $this->feed = $feed;

        return $this;
    }

    /**
     * Get feed
     *
     * @return \FeedBundle\Entity\Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * Get sum donate
     *
     * @return integer
     */
    public function getSumDonates()
    {
        $sum_donates = 0;

        /** @var Donations $donation */
        foreach ($this->getDonations() as $donation) {
            $sum_donates += $donation->getMoney();
        }
        return $sum_donates;
    }

    /**
     * Get count tickets
     *
     * @return integer
     */
    public function getCountWentPeoples()
    {
        $date_now = date('Y-m-d');
        $time_now = date('H:i');
        $sum_cnt_went = 0;
        /** @var TimePrice $tp */
        /** @var Ticket $ticket */
        foreach ($this->getGetTimePrice() as $tp) {
            if ($tp->getDateEvent()->format('Y-m-d') < $date_now) {
                foreach ($tp->getTickets() as $ticket) {
                    $sum_cnt_went += $ticket->getCntTickets();
                }
            } elseif (($tp->getDateEvent()->format('Y-m-d') == $date_now) && ($tp->getTimefrom()->format('H:i') <= $time_now)) {
                foreach ($tp->getTickets() as $ticket) {
                    $sum_cnt_went += $ticket->getCntTickets();
                }
            }
        }
        return $sum_cnt_went;
    }

    /**
     * @var integer
     */
    private $avg_rating = 0;

    /**
     * @var integer
     */
    private $avg_quality = 0;

    /**
     * @var integer
     */
    private $avg_saturation = 0;

    /**
     * @var integer
     */
    private $avg_interestingness = 0;


    /**
     * Set avgRating
     *
     * @param integer $avgRating
     *
     * @return Event
     */
    public function setAvgRating($avgRating)
    {
        $this->avg_rating = $avgRating;

        return $this;
    }

    /**
     * Get avgRating
     *
     * @return integer
     */
    public function getAvgRating()
    {
        return $this->avg_rating;
    }

    /**
     * Set avgQuality
     *
     * @param integer $avgQuality
     *
     * @return Event
     */
    public function setAvgQuality($avgQuality)
    {
        $this->avg_quality = $avgQuality;

        return $this;
    }

    /**
     * Get avgQuality
     *
     * @return integer
     */
    public function getAvgQuality()
    {
        return $this->avg_quality;
    }

    /**
     * Set avgSaturation
     *
     * @param integer $avgSaturation
     *
     * @return Event
     */
    public function setAvgSaturation($avgSaturation)
    {
        $this->avg_saturation = $avgSaturation;

        return $this;
    }

    /**
     * Get avgSaturation
     *
     * @return integer
     */
    public function getAvgSaturation()
    {
        return $this->avg_saturation;
    }

    /**
     * Set avgInterestingness
     *
     * @param integer $avgInterestingness
     *
     * @return Event
     */
    public function setAvgInterestingness($avgInterestingness)
    {
        $this->avg_interestingness = $avgInterestingness;

        return $this;
    }

    /**
     * Get avgInterestingness
     *
     * @return integer
     */
    public function getAvgInterestingness()
    {
        return $this->avg_interestingness;
    }

    /**
     * @var integer
     */
    private $cnt_buy_tickets = 0;


    /**
     * Set cntBuyTickets
     *
     * @param integer $cntBuyTickets
     *
     * @return Event
     */
    public function setCntBuyTickets($cntBuyTickets)
    {
        $this->cnt_buy_tickets = $cntBuyTickets;

        return $this;
    }

    /**
     * Get cntBuyTickets
     *
     * @return integer
     */
    public function getCntBuyTickets()
    {
        return $this->cnt_buy_tickets;
    }

    /**
     * @var path
     */
    private $pathWay;


    /**
     * Set pathWay
     *
     * @param path $pathWay
     *
     * @return Event
     */
    public function setPathWay($pathWay)
    {
        $this->pathWay = $pathWay;

        return $this;
    }

    /**
     * Get pathWay
     *
     * @return path
     */
    public function getPathWay()
    {
        return $this->pathWay;
    }
    /**
     * @var integer
     */
    private $idPostVk;


    /**
     * Set idPostVk
     *
     * @param integer $idPostVk
     *
     * @return Event
     */
    public function setIdPostVk($idPostVk)
    {
        $this->idPostVk = $idPostVk;

        return $this;
    }

    /**
     * Get idPostVk
     *
     * @return integer
     */
    public function getIdPostVk()
    {
        return $this->idPostVk;
    }
    /**
     * @var string
     */
    private $postVkWidget;


    /**
     * Set postVkWidget
     *
     * @param string $postVkWidget
     *
     * @return Event
     */
    public function setPostVkWidget($postVkWidget)
    {
        $this->postVkWidget = $postVkWidget;

        return $this;
    }

    /**
     * Get postVkWidget
     *
     * @return string
     */
    public function getPostVkWidget()
    {
        return $this->postVkWidget;
    }
    /**
     * @var string
     */
    private $postVkDateClosed;

    /**
     * @var integer
     */
    private $postVkCntWin;

    /**
     * @var string
     */
    private $postVkSumWin;


    /**
     * Set postVkCntWin
     *
     * @param integer $postVkCntWin
     *
     * @return Event
     */
    public function setPostVkCntWin($postVkCntWin)
    {
        $this->postVkCntWin = $postVkCntWin;

        return $this;
    }

    /**
     * Get postVkCntWin
     *
     * @return integer
     */
    public function getPostVkCntWin()
    {
        return $this->postVkCntWin;
    }

    /**
     * Set postVkSumWin
     *
     * @param string $postVkSumWin
     *
     * @return Event
     */
    public function setPostVkSumWin($postVkSumWin)
    {
        $this->postVkSumWin = $postVkSumWin;

        return $this;
    }

    /**
     * Get postVkSumWin
     *
     * @return string
     */
    public function getPostVkSumWin()
    {
        return $this->postVkSumWin;
    }

    /**
     * Set postVkDateClosed
     *
     * @param \DateTime $postVkDateClosed
     *
     * @return Event
     */
    public function setPostVkDateClosed($postVkDateClosed)
    {
        $this->postVkDateClosed = $postVkDateClosed;

        return $this;
    }

    /**
     * Get postVkDateClosed
     *
     * @return \DateTime
     */
    public function getPostVkDateClosed()
    {
        return $this->postVkDateClosed;
    }
    /**
     * @var boolean
     */
    private $charity = false;


    /**
     * Set charity
     *
     * @param boolean $charity
     *
     * @return Event
     */
    public function setCharity($charity)
    {
        $this->charity = (boolean)$charity;

        return $this;
    }

    /**
     * Get charity
     *
     * @return boolean
     */
    public function getCharity()
    {
        return $this->charity;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $donations;


    /**
     * Add donation
     *
     * @param \EventsBundle\Entity\Donations $donation
     *
     * @return Event
     */
    public function addDonation(\EventsBundle\Entity\Donations $donation)
    {
        $this->donations[] = $donation;

        return $this;
    }

    /**
     * Remove donation
     *
     * @param \EventsBundle\Entity\Donations $donation
     */
    public function removeDonation(\EventsBundle\Entity\Donations $donation)
    {
        $this->donations->removeElement($donation);
    }

    /**
     * Get donations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDonations()
    {
        return $this->donations;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $adverts;


    /**
     * Add advert
     *
     * @param \AdvertBundle\Entity\Advert $advert
     *
     * @return Event
     */
    public function addAdvert(\AdvertBundle\Entity\Advert $advert)
    {
        $this->adverts[] = $advert;

        return $this;
    }

    /**
     * Remove advert
     *
     * @param \AdvertBundle\Entity\Advert $advert
     */
    public function removeAdvert(\AdvertBundle\Entity\Advert $advert)
    {
        $this->adverts->removeElement($advert);
    }

    /**
     * Get adverts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdverts()
    {
        return $this->adverts;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $gifts;


    /**
     * Add gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     *
     * @return Event
     */
    public function addGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts[] = $gift;

        return $this;
    }

    /**
     * Remove gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     */
    public function removeGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts->removeElement($gift);
    }

    /**
     * Get gifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGifts()
    {
        return $this->gifts;
    }
}
