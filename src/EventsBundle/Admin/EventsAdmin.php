<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.16
 * Time: 22:10
 */

namespace EventsBundle\Admin;

use EventsBundle\Entity\AddService;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\TimePrice;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Validator\ErrorElement;

class EventsAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('acl');
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        /**
         * @var Event $object
         * @var TimePrice $tp
         */
        $array_tp = [];
        $array_tp_different = [];

        $i = 1;
        foreach ($object->getGetTimePrice() as $key => $tp) {
            $date_start = $tp->getDateEvent();
            $date_end = $tp->getDateEventEnd();
            $time_start = $tp->getTimefrom();
            $time_end = $tp->getTimeto();

            if ($date_start->format("Y-m-d") != $date_end->format("Y-m-d")) {
                $array_tp_different[$i] = [
                    'date_start' => $date_start,
                    'date_end' => $date_end,
                    'time_from' => $time_start,
                    'time_end' => $time_end,
                ];
            }
            $i++;
        }

        $i = 1;
        foreach ($object->getGetTimePrice() as $key => $tp) {
            $date_start = $tp->getDateEvent();
            $date_end = $tp->getDateEventEnd();
            $time_start = $tp->getTimefrom();
            $time_end = $tp->getTimeto();


            if ($date_start->format("Y-m-d") == $date_end->format("Y-m-d")) {
                if (isset($array_tp[$date_start->format("Y-m-d")])) {
                    foreach ($array_tp[$date_start->format("Y-m-d")] as $key_block => $item) {
                        if (
                            ($time_start <= $item['time_from'] && $time_end >= $item['time_from'])
                            || ($time_start <= $item['time_end'] && $time_end >= $item['time_end'])
                            || ($time_start > $item['time_from'] && $time_end > $item['time_from'] && $time_start < $item['time_end'] && $time_end < $item['time_end'])
                        ) {
                            $errorElement
                                ->with('getTimePrice')
                                ->addViolation('Ошибка: В планировании мероприятия в полях ' . $key_block . ' и ' . ($i) . ' обнарежено пересечение ')
                                ->end();
                        }
                    }
                } elseif (isset($array_tp[$date_end->format("Y-m-d")])) {
                    foreach ($array_tp[$date_end->format("Y-m-d")] as $key_block => $item) {
                        if (
                            ($time_start <= $item['time_from'] && $time_end >= $item['time_from'])
                            || ($time_start <= $item['time_end'] && $time_end >= $item['time_end'])
                            || ($time_start > $item['time_from'] && $time_end > $item['time_from'] && $time_start < $item['time_end'] && $time_end < $item['time_end'])
                        ) {
                            $errorElement
                                ->with('getTimePrice')
                                ->addViolation('Ошибка: В планировании мероприятия в полях ' . $key_block . ' и ' . ($i) . ' обнарежено пересечение ')
                                ->end();
                        }
                    }
                }

                foreach ($array_tp_different as $key_tp_diff => $item_tp_diff) {

                    $date_time_from_diff = date_create($item_tp_diff['date_start']->format("Y-m-d") . " " . $item_tp_diff['time_from']->format("H:i"));
                    $date_time_to_diff = date_create($item_tp_diff['date_end']->format("Y-m-d") . " " . $item_tp_diff['time_end']->format("H:i"));

                    $date_time_from = date_create($date_start->format("Y-m-d") . " " . $time_start->format("H:i"));
                    $date_time_to = date_create($date_end->format("Y-m-d") . " " . $time_end->format("H:i"));

                    if (
                        ($date_time_from <= $date_time_from_diff && $date_time_to >= $date_time_from_diff)
                        || ($date_time_from <= $date_time_to_diff && $date_time_to >= $date_time_to_diff)
                        || ($date_time_from > $date_time_from_diff && $date_time_to > $date_time_from_diff && $date_time_from < $date_time_to_diff && $date_time_to < $date_time_to_diff)
                    ) {
                        $errorElement
                            ->with('getTimePrice')
                            ->addViolation('Ошибка: В планировании мероприятия в полях ' . $key_tp_diff . ' и ' . ($i) . ' обнарежено пересечение ')
                            ->end();
                    }
                }

                $array_tp[$date_start->format("Y-m-d")][$i] = [
                    'date_start' => $date_start,
                    'date_end' => $date_end,
                    'time_from' => $time_start,
                    'time_end' => $time_end,
                ];
            }
            $i++;
        }

//        print_r($array_tp);
//        die();
//        $errorElement
//            ->with('getTimePrice')
//            ->addViolation('ошибка полюбе1')
//            ->addViolation('ошибка полюбе2')
//            ->end();
//        throw new \Exception(__METHOD__);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $date_start = new \DateTime();

        /** @var Event $object */
        $object = $this->getSubject();

        $formMapper
            ->tab('Основная информация')
            ->with('Основная информация', array('class' => 'col-md-6'))
            ->add('name', 'text', array(
                'required' => true,
                'label' => 'Название события',
            ))
            ->add('city', 'sonata_type_model_list',
                array(
                    'required' => true,
                    'label' => 'Город'
                )
            )
            ->add('company', 'sonata_type_model_list',
                array(
                    'required' => true,
                    'label' => 'Организатор'
                )
            )
            ->add('description', 'textarea', array(
                'required' => true,
                'label' => 'Описание',
            ))
            ->add('main_image', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Картинка'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'Events_main',
                        'provider' => 'sonata.media.provider.image'
                    )
                )
            )
            ->add('latlng', 'oh_google_maps', [
                'label' => 'Отметка на карте',
                'required' => false
            ])
            ->add('images', 'sonata_type_model_list',
                array(
                    'required' => false,
                    //                    'multiple' => true,
                    'label' => 'Галерея картинок',
                    'btn_add' => false,
                    'btn_list' => false,
                    'btn_delete' => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'Events_gallery',
                        //                        'provider' => 'sonata.media.provider.image'
                    )
                )
            )
            ->add('is_active', 'checkbox', [
                'label' => 'Пройдена модерация?',
                'required' => false
            ])
            ->add('charity', 'checkbox', [
                'label' => 'Благотворительное мероприятие?',
                'required' => false
            ])
            ->end()
            ->with('Для кого?', array('class' => 'col-md-3'))
            ->add('forMan', 'checkbox', array(
                'required' => false,
                'label' => 'Для него',
            ))
            ->add('forWoman', 'checkbox', array(
                'required' => false,
                'label' => 'Для неё',
            ))
            ->add('forElderly', 'checkbox', array(
                'required' => false,
                'label' => 'Для взрослых',
            ))
            ->end()
            ->with('Настройки Vk', array('class' => 'col-md-3'))
            ->add('idPostVk', 'integer', array(
                'required' => false,
                'label' => 'ID Поста в Vk',
            ))
//            ->add('postVkWidget', 'textarea', array(
//                'required' => false,
//                'label' => 'Widget поста в Vk',
//                'help' => ($object && $object->getIdPostVk() ? 'Получить виджет можно на странице <a href="https://vk.com/dev/Post" target="_blank">Vk</a>. Id поста: ' . $object->getIdPostVk() : 'Получить виджет можно на странице <a href="https://vk.com/dev/Post" target="_blank">Vk</a>.')
//            ))
            ->add('postVkDateClosed', 'sonata_type_date_picker', array(
                'required' => false,
                'label' => 'Дата окончания конкурса',
                'dp_language'=>'ru',
                'format'=>'dd.MM.yyyy'
            ))
            ->add('postVkCntWin', 'integer', array(
                'required' => false,
                'label' => 'Количество выигрышей',
            ))
            ->add('postVkSumWin', 'text', array(
                'required' => false,
                'label' => 'Сумма выигрыша для каждого победителя',
            ))
            ->end()
            ->end();


        $formMapper->tab('SEO')
            ->with('SEO поля', array('class' => 'col-md-6'))
            ->add('url', 'text', array(
                'required' => false,
                'label' => 'URL',
            ))
            ->add('description_seo', 'textarea', array(
                'required' => false,
                'label' => 'Description',
            ))
            ->add('keywords_seo', 'textarea', array(
                'required' => false,
                'label' => 'Keywords',
            ))
            ->end()
            ->end();


        $formMapper->tab('Доп. услуги')
            ->with('Список доп. услуг', array('class' => 'col-md-12'))
            ->add('getService', 'sonata_type_collection',
                array(
//                        'required' => false,
                    //                            'property' => 'name',
                    //                            'multiple' => true,
                    'label' => 'Список доп. услуг',
                    //                            'allow_add' => true,
                    //                            'allow_delete' => true,
//                        'by_reference' => false
                    //                            'btn_add'       => false,
                    //                            'btn_list'      => false,
                    //                            'btn_delete'    => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                )
            )
            ->end()
            ->end();
//
        $formMapper->tab('План мероприятия')
            ->with('Список времени и цен', array('class' => 'col-md-12'))
            ->add('getTimePrice', 'sonata_type_collection',
                array(
//                        'required' => true,
                    //                            'property' => 'name',
                    //                            'multiple' => true,
                    'label' => 'Список времени и цен',
                    //                            'allow_add' => true,
                    //                            'allow_delete' => true,
//                        'by_reference' => false
                    //                            'btn_add'       => false,
                    //                            'btn_list'      => false,
                    //                            'btn_delete'    => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                )
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, [
                'label' => 'Название компании',
            ]);
        $listMapper->add('is_active', 'boolean', array(
            'editable' => false,
            'label' => 'Пройдена модерация?'
        ))->add('charity', 'boolean', array(
            'editable' => true,
            'label' => 'Благотворительное мерпориятие?'
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('name', null, [
            'label' => 'Название события',
        ]);
    }

    public function prePersist($object)
    {
        /**
         * @var Event $object
         * @var AddService $service
         * @var TimePrice $tp
         */
        foreach ($object->getGetService() as $service) {
            $service->setEvent($object);
        }

        foreach ($object->getGetTimePrice() as $tp) {
            $tp->setEvent($object);
        }
    }

    public function preUpdate($object)
    {
        /**
         * @var Event $object
         * @var AddService $service
         * @var TimePrice $tp
         */
        foreach ($object->getGetService() as $service) {
            $service->setEvent($object);
        }

        foreach ($object->getGetTimePrice() as $tp) {
            $tp->setEvent($object);
        }
    }
}
