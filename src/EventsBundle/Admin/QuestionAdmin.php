<?php

namespace EventsBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class QuestionAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('acl');
    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('text')
            ->add('answer')
            ->add('isActive');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user', null, [
                'label' => 'Пользователь'
            ])
            ->add('event', null, [
                'label' => 'Мероприятие'
            ])
            ->add('text')
            ->add('answer')
            ->add('isActive', null, array(
                'editable' => true,
                'label' => 'Показывать?'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user', 'sonata_type_model_list',
                array(
                    'required' => true,
                    'label' => 'Автор вопроса'
                )
            )
            ->add('event', 'sonata_type_model_list',
                array(
                    'required' => true,
                    'label' => 'Мероприятие, на которое задали вопрос'
                )
            )
            ->add('text', 'textarea', [
                'label' => 'Вопрос'
            ])
            ->add('answer', 'textarea', [
                'label' => 'Ответ на вопрос',
                'required' => false
            ])
            ->add('isActive', 'checkbox', [
                'label' => 'Показывать?',
                'required' => true
            ]);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('text')
            ->add('answer')
            ->add('createAt')
            ->add('updateAt')
            ->add('isActive');
    }
}
