<?php

namespace EventsBundle\Admin;

use EventsBundle\Entity\TimePrice;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class TimePriceAdmin extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('acl');
    }

//    protected $formOptions = array(
//        'validation_groups' => array()
//    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('timefrom')
            ->add('timeto')
            ->add('date_event')
            ->add('date_event_end')
            ->add('price')
            ->add('places');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('price', null, [
                'label' => 'Цена'
            ])
            ->add('places', null, [
                'label' => 'Всего билетов доступно'
            ])
            ->add('countTicketsBuy', null, [
                'label' => 'Количество купленных билетов'
            ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('timefrom', 'time', [
                'label' => 'Время, когда мероприятие начинается',
                'required' => true,
            ])
            ->add('timeto', 'time', [
                'label' => 'Время, когда мероприятие заканчивается',
                'required' => true,
            ])
            ->add('date_event', 'sonata_type_date_picker', [
                'label' => 'Дата, когда мероприятие начинается',
                'format' => DateTimeType::HTML5_FORMAT,
                'required' => true,
            ])
            ->add('date_event_end', 'sonata_type_date_picker', [
                'label' => 'Дата, когда мероприятие кончается',
                'format' => DateTimeType::HTML5_FORMAT,
                'required' => true,
            ])
            ->add('price', 'text', [
                'label' => 'Цена',
                'required' => false
            ])
            ->add('places', 'integer', [
                'label' => 'Количество мест',
                'required' => true
            ])
            ->add('countTicketsBuy', 'integer', [
                'label' => 'Количество купленных мест',
                'required' => false,
//                'mapped' => false,
                'read_only' => true
            ]);
    }
}
