<?php

namespace EventsBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class TicketAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('acl');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('cntTickets')
            ->add('code')
            ->add('fullPrice');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('cntTickets', null, [
                'label' => 'Количество билетов куплено'
            ])
            ->addIdentifier('fullPrice', null, [
                'label' => 'Полная цена'
            ])
            ->add('timeprices', null, [
                'label' => 'Время проведения мероприятия'
            ])
            ->add('code', null, [
                'label' => 'Код билета'
            ])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user', null, [
                'label' => 'Покупатель'
            ])
            ->add('timeprices.event', null, [
                'label' => 'Мероприятие'
            ])
            ->add('timeprices', null, [
                'label' => 'Время прохождения мероприятия'
            ])
            ->add('cntTickets', null, [
                'label' => 'Количество купленных билетов'
            ])
            ->add('fullPrice', null, [
                'label' => 'Полная цена'
            ])
            ->add('code', null, [
                'label' => 'Код билета'
            ])
            ->add('service', null, [
                'label' => 'Доп. услуги'
            ])
            ->add('createAt', null, [
                'label' => 'Дата покупки'
            ]);
    }
}
