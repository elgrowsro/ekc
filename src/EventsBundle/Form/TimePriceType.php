<?php

namespace EventsBundle\Form;

use DateTime;
use EventsBundle\Entity\TimePrice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class TimePriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timefrom', 'text', [
                'label' => 'Время начала',
//                'widget' => 'single_text',
//                'format' => 'H:i',
                'attr' => [
                    'class' => 'time_input'
                ],
                'required' => true
            ])
            ->add('timeto', 'text', [
                'label' => 'Время окончания',
//                'widget' => 'single_text',
//                'format' => 'H:i',
                'attr' => [
                    'class' => 'time_input'
                ],
                'required' => true
            ])
            ->add('price', 'text', [
                'label' => 'Цена',
                'required' => false
            ])
            ->add('places', 'integer', [
                'label' => 'Количество мест',
                'required' => true
            ])
            ->add('date_event', 'text', [
                'label' => 'Дата начала мероприятия',
//                'widget' => 'single_text',
                'required' => true,
            ]);


        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            /** @var TimePrice $data */
            $data = $event->getData();
            $form = $event->getForm();

            if ($data) {
                $date = new DateTime(date("Y-m-d", strtotime($data['date_event'])));
                $data['date_event'] = $date;
                if ($data['timefrom'] == '__:__') {
                    $data['timefrom'] = '';
                } else {
                    $data['timefrom'] = new DateTime($data['timefrom']);
                }
                if ($data['timeto'] == '__:__') {
                    $data['timeto'] = '';
                } else {
                    $data['timeto'] = new DateTime($data['timeto']);
                }
                $event->setData($data);
            }
//            echo '---';
//            print_r($data);
//            echo '---\r\n';
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventsBundle\Entity\TimePrice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventsbundle_timeprice';
    }
}
