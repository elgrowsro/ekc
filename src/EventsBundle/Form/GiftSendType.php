<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.09.16
 * Time: 11:29
 */

namespace EventsBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use EventsBundle\Entity\AddService;
use EventsBundle\Form\DataTransformer\NumberToTPTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class GiftSendType extends AbstractType
{
    private $form_name;
    private $manager;

    public function __construct(ObjectManager $manager, $form_name = 'eventsbundle_gift')
    {
        $this->form_name = $form_name;
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new NumberToTPTransformer($this->manager);

        $event_id = $options['event_id'];

        $builder
            ->add('firstname', 'text', [
                'label' => 'Имя получателя',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Необходимо заполнить данное поле',
                        'groups' => 'gift_ticket'
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Минимальная длина имени - 3 символа',
                        'groups' => 'gift_ticket'
                    ]),
                ],
            ])
            ->add('secondname', 'text', [
                'label' => 'Фамилия получателя',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Необходимо заполнить данное поле',
                        'groups' => 'gift_ticket'
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Минимальная длина фамилии - 3 символа',
                        'groups' => 'gift_ticket'
                    ]),
                ],
            ])
            ->add('email', 'email', [
                'label' => 'Почта получателя',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Необходимо заполнить данное поле',
                        'groups' => 'gift_ticket'
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Минимальная длина почты - 3 символа',
                        'groups' => 'gift_ticket'
                    ]),
                    new Email([
                        'message' => "Почта {{ value }} введена не верно",
                        'groups' => 'gift_ticket'
                    ]),
                ],
            ])
            ->add('phone', 'text', [
                'label' => 'Телефон получателя',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Необходимо заполнить данное поле',
                        'groups' => 'gift_ticket'
                    ]),
                ],
            ])
            ->add('cntTickets', 'hidden', [
                'label' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Необходимо указать кол-во билетов',
                        'groups' => 'gift_ticket'
                    ]),
                    new Range([
                        'min' => 1,
                        'minMessage' => 'Минимальное количество билетов: 1',
                        'groups' => 'gift_ticket'
                    ])
                ],
            ])
            ->add('timeprices', 'hidden', [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Необходимо выбрать время посещения мероприятия',
                        'groups' => 'gift_ticket'
                    ]),
                ],
            ])
            ->add('service', 'entity', [
                'class' => 'EventsBundle\Entity\AddService',
                'label' => false,
                'required' => false,
                'property' => 'name',
//                'property_path' => '[id]', # in square brackets!
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) use ($event_id) {
                    return $er->createQueryBuilder('u')
                        ->groupBy('u.id')
                        ->orderBy('u.name', 'ASC')
                        ->andWhere(" u.event = :event ")
                        ->setParameter(':event', $event_id);
                },
            ]);


        $builder->get('timeprices')->addModelTransformer($transformer);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'event_id' => null,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ]);
    }

    /**
     * @return string
     */

    public function getName()
    {
        return $this->form_name;
    }

}