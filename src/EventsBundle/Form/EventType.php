<?php

namespace EventsBundle\Form;

use EventsBundle\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DateTime;

class EventType extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'eventsbundle_event')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Название мероприятия',
                'required' => true,
                'trim' => true
            ])
            ->add('city', 'entity', [
                'class' => 'AppBundle\Entity\City',
                'label' => 'Город проведения мероприятия',
                'choice_label' => 'name',
                'required' => true
            ])
            ->add('main_image', 'hidden', array(
                'required' => false,
                'mapped' => false,
                'trim' => true
            ))
            ->add('description', 'textarea', [
                'label' => 'Описание мероприятия',
                'required' => true,
                'trim' => true
            ])
            ->add('forMan', 'checkbox', [
                'label' => 'Него',
                'required' => false
            ])
            ->add('forWoman', 'checkbox', [
                'label' => 'Неё',
                'required' => false
            ])
            ->add('forElderly', 'checkbox', [
                'label' => 'Взрослым',
                'required' => false
            ])
            ->add('getTimePrice', 'collection', [
                'type' => new TimePriceType(),
                'label' => 'Запланировать мероприятие',
                'allow_add' => true,
//                'mapped' => false,
            ])
            ->add('getService', 'collection', [
                'type' => new AddServiceType(),
                'label' => 'Дополнительные услуги',
                'allow_add' => true,
//                'mapped' => false,
            ])
            ->add('lat', 'hidden', [
                'required' => false,
            ])
            ->add('lng', 'hidden', [
                'required' => false,
            ])
            ->add('pathWay', 'hidden', [
                'required' => false,
            ])
            ->add('postVkDateClosed', 'text', [
                'required' => false,
                'label' => 'Дата окончания конкурса'
            ])
            ->add('postVkCntWin', 'text', [
                'required' => false,
                'label' => 'Количество выигрышных пользователей'
            ])
            ->add('postVkSumWin', 'text', [
                'required' => false,
                'label' => 'Сумма выигрыша для каждого победителя'
            ])->add('charity', 'hidden', [
                'label' => false,
                'required' => false
            ]);


//        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
//            /** @var Event $data */
//            $data = $event->getData();
//            $form = $event->getForm();
//
//            if ($data) {
//                print_r($data);
//                $data["getTimePrice"] = array_values($data["getTimePrice"]);
//                print_r($data);
//                $event->setData($data);
//            }
////            echo '---';
////            print_r($data);
////            echo '---\r\n';
//        });


        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Event $data */
            $data = $event->getData();
            $form = $event->getForm();


            if ($data && $data->getPostVkDateClosed()) {
                if ($data->getPostVkDateClosed() == '__.__.____') {
                    $data->setPostVkDateClosed(null);
                } else {
                    $date = new DateTime(date("Y-m-d", strtotime($data->getPostVkDateClosed())));
                    $data->setPostVkDateClosed($date);
                    $event->setData($data);
                }
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventsBundle\Entity\Event',
            'cascade_validation' => true,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_with_dropzone form',
                'novalidate' => 'novalidate',
                'enctype' => 'multipart/form-data'
            ]
        ));
    }

    /**
     * @return string
     */

    public function getName()
    {
        return $this->form_name;
    }
}
