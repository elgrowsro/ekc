<?php

namespace EventsBundle\Form;

use AppBundle\Form\Type\CommentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentEventType extends CommentType
{
    public function __construct($form_name = 'eventbundle_comment')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('event', 'entity_hidden', [
                'class' => 'EventsBundle\Entity\Event'
            ]);
    }
}
