<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 06.07.16
 * Time: 18:12
 */

namespace EventsBundle\Form\DataTransformer;


use Doctrine\Common\Persistence\ObjectManager;
use EventsBundle\Entity\TimePrice;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class NumberToTPTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  TimePrice|null $issue
     * @return string
     */
    public function transform($issue)
    {
        if (null === $issue) {
            return "";
        }

        return $issue->getId();
    }

    /**
     * Transforms a string (number) to an object (timeprice).
     *
     * @param  string $number
     * @return TimePrice|null
     * @throws TransformationFailedException if object (timeprice) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $tp = $this->om
            ->getRepository('EventsBundle:TimePrice')
            ->findOneBy(array('id' => $number));

        if (null === $tp) {
            throw new TransformationFailedException(sprintf(
                'An issue with number "%s" does not exist!',
                $number
            ));
        }

        return $tp;
    }
}