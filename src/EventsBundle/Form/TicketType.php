<?php

namespace EventsBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use EventsBundle\Entity\AddService;
use EventsBundle\Entity\Ticket;
use EventsBundle\Form\DataTransformer\NumberToTPTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TicketType extends AbstractType
{
    private $form_name;
    private $manager;

    public function __construct(ObjectManager $manager, $form_name = 'eventsbundle_ticket')
    {
        $this->form_name = $form_name;
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new NumberToTPTransformer($this->manager);

        $event_id = $options['event_id'];

        $builder
            ->add('cntTickets', 'integer', [
                'label' => 'Кол-во человек:',
                'required' => true,
//                'disabled' => true
            ])
            ->add('timeprices', 'hidden')
            ->add('service', 'entity', [
                'class' => 'EventsBundle\Entity\AddService',
                'label' => 'Доп. пакеты',
                'required' => false,
                'property' => 'name',
//                'property_path' => '[id]', # in square brackets!
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) use ($event_id) {
                    return $er->createQueryBuilder('u')
                        ->groupBy('u.id')
                        ->orderBy('u.name', 'ASC')
                        ->andWhere(" u.event = :event ")
                        ->setParameter(':event', $event_id);
                },
//                'disabled' => true
            ])/* ->add('service', 'entity', array(
                'class' => 'EventsBundle\Entity\Ticket',
                'required' => false,
                'multiple' => true,
                'choice_label' => 'name',
            ))*/
        ;


        $builder->get('timeprices')->addModelTransformer($transformer);

//        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
//            /** @var Ticket $data */
//            $data = $event->getData();
//            $form = $event->getForm();
////
//            if ($data) {
////                print_r($data);
//                if ($data["timeprices"]) {
//                    $repo_tp = $this->em->getRepository('EventsBundle:TimePrice')->find($data["timeprices"]);
//                    $data["timeprices"] = $repo_tp;
//                }
////                $event->setData($data);
//            }
//////            echo '---';
//////            print_r($data);
//////            echo '---\r\n';
//        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => 'EventsBundle\Entity\Ticket',
            'event_id' => null,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }

    /**
     * @return string
     */

    public function getName()
    {
        return $this->form_name;
    }
}
