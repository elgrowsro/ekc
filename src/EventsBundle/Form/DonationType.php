<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 30.08.16
 * Time: 11:47
 */

namespace EventsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DonationType extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'event_donation')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('money', 'text', [
                'label' => 'Сумма:',
                'required' => true
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventsBundle\Entity\Donations',
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }
}