<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace FeedBundle\EventListener;


use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Question;
use EventsBundle\Entity\Ticket;
use FeedBundle\Entity\Feed;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class FeedSubscriber implements EventSubscriber
{
    private $container;
    private $tokenStorage;

    public function __construct(Container $container, TokenStorage $tokenStorage)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->indexPostUpdate($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function indexPostUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Question) {
            if ($entity->getAnswer()) {
                $entityManager = $args->getEntityManager();
                $repo_quest = $entityManager->getRepository('FeedBundle:Feed')->findOneBy(['question' => $entity->getId()]);
                if ($repo_quest) {
                    $entityManager->remove($repo_quest);
                }

                $repo_feed = new Feed();
                $repo_feed->setQuestion($entity);
                $repo_feed->setCity($this->container->get('ipgeobase')->getCity());
                $entityManager->persist($repo_feed);
                $entityManager->flush();
            }
        }
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Product" entity
        if ($entity instanceof Event) {
            $entityManager = $args->getEntityManager();
            $repo_feed = new Feed();
            $repo_feed->setEvent($entity);
            $repo_feed->setCity($this->container->get('ipgeobase')->getCity());
            $entityManager->persist($repo_feed);
            $entityManager->flush();
        }
        if ($entity instanceof Question) {
            $entityManager = $args->getEntityManager();
            $repo_feed = new Feed();
            $repo_feed->setQuestion($entity);
            $repo_feed->setCity($this->container->get('ipgeobase')->getCity());
            if (!$entity->getAnswer() && !$repo_feed->getOnlyuser()) {
                $repo_feed->setOnlyuser($this->tokenStorage->getToken()->getUser());
            }
            $entityManager->persist($repo_feed);
            $entityManager->flush();
        }
        if ($entity instanceof Comment) {
            $entityManager = $args->getEntityManager();
            $repo_feed = new Feed();
            $repo_feed->setReview($entity);
            $repo_feed->setCity($this->container->get('ipgeobase')->getCity());
            $entityManager->persist($repo_feed);
            $entityManager->flush();
        }
//        if ($entity instanceof Ticket) {
//            $entityManager = $args->getEntityManager();
//            $repo_feed = new Feed();
//            $repo_feed->setTicket($entity);
//            $repo_feed->setCity($this->container->get('ipgeobase')->getCity());
//            $entityManager->persist($repo_feed);
//            $entityManager->flush();
//        }
    }

}