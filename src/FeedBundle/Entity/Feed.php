<?php

namespace FeedBundle\Entity;

/**
 * Feed
 */
class Feed
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;

    /**
     * @var boolean
     */
    private $isActive = 1;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Feed
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Feed
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Feed
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return (boolean)$this->isActive;
    }
    /**
     * @var \EventsBundle\Entity\Event
     */
    private $event;

    /**
     * @var \EventsBundle\Entity\Question
     */
    private $question;

    /**
     * @var \AppBundle\Entity\Comment
     */
    private $review;


    /**
     * Set event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return Feed
     */
    public function setEvent(\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \EventsBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set question
     *
     * @param \EventsBundle\Entity\Question $question
     *
     * @return Feed
     */
    public function setQuestion(\EventsBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \EventsBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set review
     *
     * @param \AppBundle\Entity\Comment $review
     *
     * @return Feed
     */
    public function setReview(\AppBundle\Entity\Comment $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \AppBundle\Entity\Comment
     */
    public function getReview()
    {
        return $this->review;
    }
    /**
     * @var \AppBundle\Entity\City
     */
    private $city;


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Feed
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $onlyuser;


    /**
     * Set onlyuser
     *
     * @param \AppBundle\Entity\User $onlyuser
     *
     * @return Feed
     */
    public function setOnlyuser(\AppBundle\Entity\User $onlyuser = null)
    {
        $this->onlyuser = $onlyuser;

        return $this;
    }

    /**
     * Get onlyuser
     *
     * @return \AppBundle\Entity\User
     */
    public function getOnlyuser()
    {
        return $this->onlyuser;
    }
    /**
     * @var \EventsBundle\Entity\Ticket
     */
    private $ticket;


    /**
     * Set ticket
     *
     * @param \EventsBundle\Entity\Ticket $ticket
     *
     * @return Feed
     */
    public function setTicket(\EventsBundle\Entity\Ticket $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return \EventsBundle\Entity\Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }
}
