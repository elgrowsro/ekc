<?php

namespace FeedBundle\Form;

use AppBundle\Form\Type\CommentType;
use EventsBundle\Form\CommentEventType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentFeedType extends CommentEventType
{
    public function __construct($form_name = 'feedbundle_comment')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('feed', 'entity_hidden', [
                'class' => 'FeedBundle\Entity\Feed',
                'mapped' => false
            ])
            ->add('event', 'entity_hidden', [
                'class' => 'EventsBundle\Entity\Event'
            ]);
    }
}
