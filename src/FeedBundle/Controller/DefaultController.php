<?php

namespace FeedBundle\Controller;

use AppBundle\Entity\City;
use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use EventsBundle\Entity\Question;
use EventsBundle\Form\CommentEventType;
use FeedBundle\Form\CommentFeedType;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use PersonalAreaBundle\Form\Type\QuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction(Request $request, $type)
    {

        if ($type == 'all' ) {
            $question = new Question();
            $form_question = $this->createForm(new QuestionType('feed_city_question_answer_' . $type), $question, [
                'validation_groups' => ['create_question_answer'],
            ]);

            $comment = new Comment();
            $form_rewies = $this->createForm(new CommentFeedType(), $comment, [
                'validation_groups' => ['create_comment_event'],
                'action' => $this->generateUrl('check_comment')
            ]);

            return $this->render('FeedBundle:Default:index.html.twig', [
                'form_question' => $form_question->createView(),
                'form_rewies' => $form_rewies->createView()
            ]);
        } elseif ($type == 'reviews') {
            $question = new Question();
            $form_question = $this->createForm(new QuestionType('feed_city_question_answer_' . $type), $question, [
                'validation_groups' => ['create_question_answer'],
            ]);

            return $this->render('FeedBundle:Default:index.html.twig', [
                'form_question' => $form_question->createView()
            ]);
        } elseif ($type == 'reviews') {
            $comment = new Comment();
            $form_rewies = $this->createForm(new CommentFeedType(), $comment, [
                'validation_groups' => ['create_comment_event'],
                'action' => $this->generateUrl('check_comment')
            ]);
            return $this->render('FeedBundle:Default:index.html.twig', [
                'form_rewies' => $form_rewies->createView()
            ]);
        } else {

            return $this->render('FeedBundle:Default:index.html.twig', [
            ]);
        }
    }


    public function renderAction(Request $request, $type, $page)
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var City $city */
        $city = $this->get('ipgeobase')->getCity();

        $repository = $this->getDoctrine()
            ->getRepository('FeedBundle:Feed');
        $query = $repository->createQueryBuilder('f')
            ->select("f")
            ->andWhere('f.city = :city')
            ->setParameter(':city', $city->getId())
            ->orderBy('f.id', 'DESC');


        if ($type == 'all') {
            $query
                ->leftJoin('f.event', 'event')
                ->leftJoin('f.review', 'review');
        } elseif ($type == 'events') {
            $query
                ->innerJoin('f.event', 'event');
        } elseif ($type == 'reviews') {
            $query
                ->innerJoin('f.review', 'review');
        }

        if ($type == 'all' || $type == 'questions') {
            if (!$user) {
                if ($type == 'all') {
                    $query
                        ->leftJoin('f.question', 'question', 'WITH', $query->expr()->isNotNull('question.answer'));
                } else {
                    $query
                        ->innerJoin('f.question', 'question', 'WITH', $query->expr()->isNotNull('question.answer'));
                }
                $query
                    ->andWhere($query->expr()->isNull('f.onlyuser'));
            } else {
                if ($type == 'all') {
                    $query
                        ->leftJoin('f.question', 'question');
                } else {
                    $query
                        ->innerJoin('f.question', 'question');
                }
                $query
                    ->andWhere($query->expr()->orX(
                        $query->expr()->eq('f.onlyuser', $user->getId()),
                        $query->expr()->isNull('f.onlyuser')
                    ));
            }
        }

        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_feed */
        $pagination_feed = $paginator->paginate(
            $query,
            $page,
            10
        );
        $pagination_feed->setUsedRoute('feed.load');

        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('FeedBundle:Default:load.html.twig', [
                'feed' => $pagination_feed,
                'type' => $type
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('FeedBundle:Default:load.html.twig', [
                'feed' => $pagination_feed,
                'type' => $type
            ]);
            return $events_render;
        }
    }
}
