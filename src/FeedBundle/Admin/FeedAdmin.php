<?php

namespace FeedBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FeedAdmin extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('acl');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('isActive');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('event', null, [
                'label' => 'Мероприятие'
            ])
            ->add('question', null, [
                'label' => 'Вопрос'
            ])
            ->add('review', null, [
                'label' => 'Отзыв'
            ])
            ->add('ticket', null, [
                'label' => 'Билет'
            ])
            ->add('onlyuser', null, [
                'label' => 'Только для:'
            ])
            ->add('city', null, [
                'label' => 'Город'
            ])
            ->add('isActive', null, [
                'label' => 'Показывать?'
            ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('event', 'sonata_type_model_list',
                [
                    'required' => false,
                    'label' => 'Мероприятие'
                ]
            )
            ->add('question', 'sonata_type_model_list',
                [
                    'required' => false,
                    'label' => 'Вопрос'
                ]
            )
            ->add('review', 'sonata_type_model_list',
                [
                    'required' => false,
                    'label' => 'Отзыв'
                ]
            )
            ->add('ticket', 'sonata_type_model_list',
                [
                    'required' => false,
                    'label' => 'Билет'
                ]
            )
            ->add('onlyuser', 'sonata_type_model_list',
                [
                    'required' => false,
                    'label' => 'Только для:'
                ]
            )
            ->add('city', 'sonata_type_model_list',
                [
                    'required' => false,
                    'label' => 'Город'
                ]
            )
            ->add('isActive', 'checkbox', [
                'required' => false,
                'label' => 'Показывать?'
            ]);
    }
}
