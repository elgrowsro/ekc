<?php

namespace PersonalAreaBundle\Controller;

use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use CompanysBundle\Entity\Company;
use PersonalAreaBundle\Form\Type\RegistrateIndividual;
use PersonalAreaBundle\Form\Type\RegistrateIndividualWithoutLogin;
use PersonalAreaBundle\Form\Type\RegistrateOrganization;
use PersonalAreaBundle\Form\Type\RegistrateOrganizationWithoutLogin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistratedController extends Controller
{
    public function indexAction($organizer)
    {
        if ($organizer == 'individual') {
            if ($this->getUser()) {
                $form_filer = $this->get('form.factory')->create(new RegistrateIndividual(), null, [
                    'action' => $this->generateUrl('personal_area_registrate.check'),
                    'method' => 'POST'
                ]);
                $render = 'PersonalAreaBundle:registrated:form_individual.html.twig';
            } else {
                $form_filer = $this->get('form.factory')->create(new RegistrateIndividualWithoutLogin(), null, [
                    'action' => $this->generateUrl('personal_area_registrate.check'),
                    'method' => 'POST'
                ]);
                $render = 'PersonalAreaBundle:registrated:form_individual.html.twig';
            }
        } else {
            if ($this->getUser()) {
                $form_filer = $this->get('form.factory')->create(new RegistrateOrganization(), null, [
                    'action' => $this->generateUrl('personal_area_registrate.create'),
                    'method' => 'POST'
                ]);
                $render = 'PersonalAreaBundle:registrated:form.html.twig';
            } else {
                $form_filer = $this->get('form.factory')->create(new RegistrateOrganizationWithoutLogin(), null, [
                    'action' => $this->generateUrl('personal_area_registrate.create'),
                    'method' => 'POST'
                ]);
                $render = 'PersonalAreaBundle:registrated:form.html.twig';
            }
        }
        $id_type = 1;
        switch ($organizer) {
            case 'state-organization':
                $id_type = 1;
                break;
            case 'charity-organisations':
                $id_type = 2;
                break;
            case 'social-movement':
                $id_type = 3;
                break;
            case 'organizer':
                $id_type = 4;
                break;
            case 'individual':
                $id_type = 5;
                break;
        };

        $form_filer->get('typeCompany')->setData($id_type);


        return $this->render($render, [
            'form_registrate' => $form_filer->createView(),
        ]);
    }

    public function registrateCheckCompanyAction(Request $request)
    {

        $company = new Company();

        $request_all = $request->request->all();

        if (isset($request_all['registrate_organization_individual']) || isset($request_all['registrate_organization_individual_without_login'])) {
            if ($this->getUser()) {
                $form_organizer = $this->get('form.factory')->create(new RegistrateIndividual(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'create_individual_company',
                ]);
            } else {
                $form_organizer = $this->get('form.factory')->create(new RegistrateIndividualWithoutLogin(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'registrate_user_with_individual',
                ]);
            }
        } else {
            if ($this->getUser()) {
                $form_organizer = $this->get('form.factory')->create(new RegistrateOrganization(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'create_organizer_company',
                ]);
            } else {
                $form_organizer = $this->get('form.factory')->create(new RegistrateOrganizationWithoutLogin(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'registrate_user_with_company_organization',
                ]);
            }
        }


        if ($request->isMethod('POST')) {
            $form_organizer->handleRequest($request);
//            $form_organizer->get('fiodirector')->setData($request->request)
            if ($form_organizer->isValid()) {
                $next_url_form = $this->generateUrl('personal_area_registrate.create');

                return new Response(json_encode(array(
                    'status' => 'success',
                    'next_url' => $next_url_form,
                )));
            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_organizer, true, true);
                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function registrateCompanyAction(Request $request)
    {

        $company = new Company();

        $request_all = $request->request->all();

        if (isset($request_all['registrate_organization_individual']) || isset($request_all['registrate_organization_individual_without_login'])) {
            if ($this->getUser()) {
                $form_organizer = $this->get('form.factory')->create(new RegistrateIndividual(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'create_individual_company',
                ]);
            } else {
                $form_organizer = $this->get('form.factory')->create(new RegistrateIndividualWithoutLogin(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'registrate_user_with_individual',
                ]);
            }
        } else {
            if ($this->getUser()) {
                $form_organizer = $this->get('form.factory')->create(new RegistrateOrganization(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'create_organizer_company',
                ]);
            } else {
                $form_organizer = $this->get('form.factory')->create(new RegistrateOrganizationWithoutLogin(), $company, [
                    'method' => 'POST',
                    'validation_groups' => 'registrate_user_with_company_organization',
                ]);
            }
        }


        if ($request->isMethod('POST')) {
            $form_organizer->handleRequest($request);
//            $form_organizer->get('fiodirector')->setData($request->request)
            if ($form_organizer->isValid()) {
                if ($form_organizer->get('setlogo')->getData()) {
                    $web_path = $this->get('kernel')->getRootDir() . '/../web';
                    /* validate max min size for the file */
                    /* validate mime type for the file */
                    /* Get sonata media manager service from container    */
                    $mediaManager = $this->container->get('sonata.media.manager.media');
                    /* create new instance of sonata media class in my case entity is located at
                    * Application\Sonata\MediaBundle\Entity\Media
                    */
                    /** @var Media $media */
                    $media = $mediaManager->create();
                    $media->setBinaryContent($web_path . $form_organizer->get('setlogo')->getData());
                    $media->setContext('Companys_logo');
                    $media->setProviderName('sonata.media.provider.image');
                    /*If you have other providers in your app like vimeo /dailymotion etc
                    then do set here i have shown you the demo for adding image/file in sonata media */
                    /*other setters if you want to set like enabled/disable etc*/
                    $mediaManager->save($media);
                    $company->setLogo($media);
                }


                $uploadedFile = $request->files->get('file');

                if (count($uploadedFile)) {

                    $web_path = $this->get('kernel')->getRootDir() . '/../web';
                    $mediaManager = $this->container->get('sonata.media.manager.media');
                    $galleryManager = $this->container->get('sonata.media.manager.gallery');
                    $ImagemimeTypes = ['jpeg', 'jpg', 'png'];
                    $FilemimeTypes = ['doc', 'docx', 'pdf', 'excel', 'txt'];

                    /** @var Gallery $gallery */
                    $gallery = $galleryManager->create();
                    $gallery->setContext('Documents');
                    $gallery->setDefaultFormat('Documents_small');
                    $gallery->setName('Документы');
                    $gallery->setEnabled(true);

                    /** @var UploadedFile $document */
                    foreach ($uploadedFile as $document) {

                        $name = $document->getClientOriginalName();
                        $ext = $document->getClientOriginalExtension();

                        /** @var Media $media */
                        $media = $mediaManager->create();
                        $media->setContext('Documents');
                        switch ($ext) {
                            case "png";
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/png/';
                                break;
                            case "jpg";
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpg/';
                                break;
                            case "jpeg";
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpeg/';
                                break;
                            default:
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/files/';
                        }
                        $file = $document->move($directory, $name);

                        if (in_array($ext, $ImagemimeTypes)) {
                            $media->setProviderName('sonata.media.provider.image');
                            $media->setBinaryContent($directory . $name);
                        }

                        $mediaManager->save($media);

                        $galleryHasMedia = new GalleryHasMedia;
                        $galleryHasMedia->setMedia($media);
                        $galleryHasMedia->setGallery($gallery);
                        $galleryHasMedia->setEnabled(true);
                        $gallery->addGalleryHasMedias($galleryHasMedia);
                    }
                    $galleryManager->save($gallery);
                    $company->setDocuments($gallery);
                }

                $em = $this->getDoctrine()->getManager();
                if ($this->getUser()) {
                    $company->setUsercompany($this->getUser());
                } else {
                    $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

                    if ($confirmationEnabled) {
                        $user = $company->getUsercompany();
                        $user->setEnabled(false);
                        if (null === $user->getConfirmationToken()) {
                            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                            $user->setConfirmationToken($tokenGenerator->generateToken());
                        }

                        $mailer_confirm = $this->container->get('fos_user.mailer');
                        $mailer_confirm->sendConfirmationEmailMessage($user);
                    } else {
                        $user = $company->getUsercompany();
                        $user->setEnabled(true);
                    }

                    $user->setPhone($company->getPhoneForUsers());
                    $generate_password = $this->generateStrongPassword(8, false, 'lud');
                    $user->setPlainPassword($generate_password);
                    $em->persist($user);


                    $company->setUsercompany($user);
                }

                $em->persist($company);
                $em->flush();

                return new Response(json_encode(array(
                    'status' => 'success'
                )));
            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_organizer, true, true);
                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    private function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
}
