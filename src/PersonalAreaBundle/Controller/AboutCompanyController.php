<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\User;
use Application\Sonata\MediaBundle\Entity\Media;
use CompanysBundle\Entity\Company;
use PersonalAreaBundle\Form\Type\CompanyEdit;
use PersonalAreaBundle\Form\Type\UserInfoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AboutCompanyController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();

        if (!$user) throw $this->createAccessDeniedException('Нужно быть авторизованным');

        $form = $this->createForm(UserInfoType::class, $user);


        return $this->render('PersonalAreaBundle:AboutCompany:index.html.twig',
            array(
                'form_edit' => $form->createView(),
                'user' => $user,
            )
        );
    }

    public function editAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user) throw $this->createAccessDeniedException('Нужно быть авторизованным');

        $form = $this->createForm(UserInfoType::class, $user, [
            'validation_groups' => 'cabinet_edit_user',
        ]);

        if ($request->isMethod('POST')) {

            $ajax = $request->isXmlHttpRequest();
            $form->handleRequest($request);

            if ($form->isValid()) {

                if ($form->get('setavatar')->getData()) {

                    $web_path = $this->get('kernel')->getRootDir() . '/../web';

                    $mediaManager = $this->container->get('sonata.media.manager.media');

                    /** @var Media $media */
                    $media = $mediaManager->create();
                    $media->setBinaryContent($web_path . $form->get('setavatar')->getData());
                    $media->setContext('Users_avatars');
                    $media->setProviderName('sonata.media.provider.image');

                    $mediaManager->save($media);
                    $user->setAvatar($media);
                }


                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                if ($ajax) {
                    return new Response(json_encode(array(
                        'status' => 'success',
                    )));
                }

            } else {
                if ($ajax) {
                    $errors = $this->get('form_serializer')->serializeFormErrors($form, true, true);

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    )));
                }
            }
        }
    }

    public function indexCompanyAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        $company = $user->getCompany();

        if (!$company || !$company->getIsActive()) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(CompanyEdit::class, $company);


        return $this->render('PersonalAreaBundle:AboutCompany:indexCompany.html.twig',
            array(
                'form_edit' => $form->createView(),
                'user' => $user,
                'company' => $company,
            )
        );
    }


    public function editCompanyAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        /** @var Company $company */
        $company = $user->getCompany();
        if (!$company || !$company->getIsActive()) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(CompanyEdit::class, $company, [
            'validation_groups' => 'cabinet_edit_company',
        ]);

        if ($request->isMethod('POST')) {

            $ajax = $request->isXmlHttpRequest();
            $form->handleRequest($request);

            if ($form->isValid()) {

                if ($form->get('logo')->getData()) {

                    $web_path = $this->get('kernel')->getRootDir() . '/../web';

                    $mediaManager = $this->container->get('sonata.media.manager.media');

                    /** @var Media $media */
                    $media = $mediaManager->create();
                    $media->setBinaryContent($web_path . $form->get('logo')->getData());
                    $media->setContext('Companys_logo');
                    $media->setProviderName('sonata.media.provider.image');

                    $mediaManager->save($media);
                    $company->setLogo($media);
                }


                $em = $this->getDoctrine()->getManager();
                $em->persist($company);
                $em->flush();

                if ($ajax) {
                    return new Response(json_encode(array(
                        'status' => 'success',
                    )));
                }

            } else {
                if ($ajax) {
                    $errors = $this->get('form_serializer')->serializeFormErrors($form, true, true);

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    )));
                }
            }
        }
    }

}
