<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.04.16
 * Time: 17:10
 */

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\VKRelUser;
use AppBundle\Entity\VKUsers;
use PersonalAreaBundle\Service\Vk;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ulogin\AuthBundle\Entity\UloginUser;

class ReferralController extends Controller
{
    public function indexAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user) throw $this->createAccessDeniedException('Нужно быть авторизованным');


        $eventsRepo = $this->getDoctrine()->getRepository('AppBundle:User');


        $queryBuilder = $eventsRepo->createQueryBuilder('user');
        $queryBuilder
            ->addSelect(['userRelVK', 'vkusers', 'myReferral', 'userReferral'])
            ->leftJoin('user.userRelVK', 'userRelVK')
            ->leftJoin('userRelVK.vkusers', 'vkusers')
            ->leftJoin('user.myReferral', 'myReferral')
            ->leftJoin('myReferral.userReferral', 'userReferral')
            ->andWhere('user.id = :id')
            ->setParameter(':id', $user->getId());

        $userResult = $queryBuilder->getQuery()->getOneOrNullResult();

        $vk_auth = false;

        /** @var UloginUser $ulogin */
        foreach ($user->getUlogin() as $ulogin) {
            if ($ulogin->getNetwork() == 'vkontakte') {
                $vk_auth = substr($ulogin->getIdentity(), 16);
            }
        }

        $show_synh_vk_users = true;

        $dStart = $user->getLastUpdateVK();
        $days_diff = 0;

        if ($dStart) {
            $dEnd = new \DateTime('now');
            $dDiff = $dStart->diff($dEnd);
            $days_diff = $dDiff->days;
            if ($days_diff < 1) {
                $show_synh_vk_users = false;
            }
        }

        $vk_app_id = $this->getParameter('vk_api.app.id');

        return $this->render('PersonalAreaBundle:Referral:index.html.twig', [
            'user' => $userResult,
            'vk_auth' => $vk_auth,
            'show_synh_vk_users' => $show_synh_vk_users,
            'vk_app_id' => $vk_app_id,
        ]);
    }

    public function vkSendMessageAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user) throw $this->createAccessDeniedException('Нужно быть авторизованным');

        /** @var Vk $vk */
        $vk = $this->get('vk.api');

        /** @var LoggerInterface $logger */
        $logger = $this->get('logger');


        if ($request->request->get('user_id')) {

            $user_to = $request->request->get('user_id');

            $em = $this->getDoctrine()->getManager();

            $repo_vk_user = $em->getRepository('AppBundle:VKUsers');
            $find_vk_user = $repo_vk_user->findOneBy(['vkId' => $user_to]);

            if ($find_vk_user && !$find_vk_user->getSendRequest() && !$find_vk_user->getConfirmedSuccess()) {
                $repo_vk_rel_user = $em->getRepository('AppBundle:VKRelUser');
                $find_vk_rel_user = $repo_vk_rel_user->findOneBy(['user' => $user->getId(), 'vkusers' => $find_vk_user->getId()]);

                if ($find_vk_rel_user) {
                    $code = $find_vk_rel_user->getCode();
                    $message = "Привет! Заходи на сайт, регистрируйся и покупай билеты на мероприятия! <br> Переходи вот по этой ссылке: " . $this->generateUrl('referral_link_vk', ['code' => $code], true);

                    $vars = array(
                        'message' => $message,
                        'guid' => rand(1, 100) . date('His'),
                        'user_id' => $user_to,
                    );

                    $response = $vk->api('messages.send', $vars);
                    $date = date('d.m.Y H:i:s');

                    $data = "{$date} | Отправлено пользователю '{$user_to}' от " . $user->getId() . " | Ответ: {$response}";


                    $find_vk_user->setSendRequest(true);

                    $find_vk_rel_user->setDateSendRequest(new \DateTime('now'));


                    $em->persist($find_vk_user);
                    $em->persist($find_vk_rel_user);
                    $em->flush();

                    $logger->info($data);
                }
            }
        }

        return new Response(json_encode(['status' => 'success']));
    }

    public function checkVKAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();

        if (!$user) throw $this->createAccessDeniedException('Нужно быть авторизованным');

        $vk_auth = false;
        foreach ($user->getUlogin() as $ulogin) {
            if ($ulogin->getNetwork() == 'vkontakte') {
                $vk_auth = substr($ulogin->getIdentity(), 16);
            }
        }

        $dStart = $user->getLastUpdateVK();
        $days_diff = 0;

        if ($dStart) {
            $dEnd = new \DateTime('now');
            $dDiff = $dStart->diff($dEnd);
            $days_diff = $dDiff->days;
        }

        if ($vk_auth/* && (!$dStart || ($dStart && $days_diff >= 1))*/) {
            $em = $this->getDoctrine()->getManager();


            $user->setLastUpdateVK(new \DateTime('now'));
            $em->persist($user);

//            print_r(json_decode(file_get_contents('https://api.vk.com/method/friends.get?user_id=' . $vk_auth . '&order=name&fields=photo_100&v=5.53')));
            $data_from_vk = json_decode(file_get_contents('https://api.vk.com/method/friends.get?user_id=' . $vk_auth . '&order=name&fields=photo_100&v=5.53'));


            $repo_vk = $em->getRepository('AppBundle:VKUsers');
            $repo_vkRelUser = $em->getRepository('AppBundle:VKRelUser');


            foreach ($data_from_vk->response->items as $item) {
                $find_vk_user = $repo_vk->findOneBy(['vkId' => $item->id]);

                if (!$find_vk_user) {
                    $new_vk_user = new VKUsers();

                    $new_vk_user->setVkId($item->id);
                    $new_vk_user->setFirstName($item->first_name);
                    $new_vk_user->setLastName($item->last_name);
                    $new_vk_user->setPhoto100($item->photo_100);
                    $new_vk_user->setVkId($item->id);

                    $new_vk_rel_user = new VKRelUser();
                    $new_vk_rel_user->setUser($user);
                    $new_vk_rel_user->setVkusers($new_vk_user);
                    $new_vk_rel_user->setCode($user->getReferralLink() . '_' . $item->id);

                    $em->persist($new_vk_user);
                    $em->persist($new_vk_rel_user);

                    $user->addUserRelVK($new_vk_rel_user);
                } else {
                    if (!$repo_vkRelUser->findOneBy(['user' => $user->getId(), 'vkusers' => $find_vk_user->getId()])) {
                        $new_vk_rel_user = new VKRelUser();
                        $new_vk_rel_user->setUser($user);
                        $new_vk_rel_user->setVkusers($find_vk_user);
                        $new_vk_rel_user->setCode($user->getReferralLink() . '_' . $find_vk_user->getVkId());

                        $em->persist($new_vk_rel_user);

                        $user->addUserRelVK($new_vk_rel_user);
                    }
                }
            }
            $em->flush();
        }


        return new Response(json_encode(['status' => 'success']));
    }


}