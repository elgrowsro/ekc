<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\User;
use CompanysBundle\Entity\Company;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HistoryController extends Controller
{

    public function indexAction(Request $request, $type = 'buyMe')
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }


        return $this->render('PersonalAreaBundle:History:index.html.twig', [
            'type' => $type
        ]);
    }

    public function loadAction(Request $request, $page, $type = 'buyMe')
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }

        /** @var Company $company */
        $company = $user->getCompany();

        if (!$company && $type == 'buyMe') {
            throw new NotFoundHttpException("Page not found");
        }

        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Ticket');

        $query = $repository->createQueryBuilder('t')
            ->select("t")
            ->orderBy('t.id', 'DESC')/*
            ->andWhere("t.user = :user ")
            ->setParameter(":user", $user_id)*/
        ;

        if ($type == 'buyMe') {
            $query->innerJoin('t.timeprices', 'timeprices')
                ->innerJoin('timeprices.event', 'event')
                ->innerJoin('event.company', 'company')
                ->andWhere('company.usercompany = :usercompany')
                ->setParameter(':usercompany', $user);
        } elseif ($type == 'your') {
            $query->andWhere('t.user = :user')
                ->setParameter(':user', $user);
        }


        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_tickets */
        $pagination_tickets = $paginator->paginate(
            $query,
            $page,
            10
        );

        $pagination_tickets->setUsedRoute('personal_area_company.history.load');


        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('PersonalAreaBundle:History:ajax_load.html.twig', [
                'tickets' => $pagination_tickets,
                'type' => $type
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('PersonalAreaBundle:History:load.html.twig', [
                'tickets' => $pagination_tickets,
                'type' => $type
            ]);
            return $events_render;
        }
    }
}
