<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\City;
use AppBundle\Entity\User;
use CompanysBundle\Entity\Company;
use EventsBundle\Entity\Question;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use PersonalAreaBundle\Form\Type\QuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FeedController extends Controller
{
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }

        $question = new Question();
        $form_question = $this->createForm(new QuestionType('personalareabundle_feed_question_answer'), $question, [
            'validation_groups' => ['create_question_answer'],
        ]);


        return $this->render('PersonalAreaBundle:Feed:index.html.twig', [
            'form_question' => $form_question->createView()
        ]);
    }

    public function renderAction(Request $request, $page)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }

        /** @var Company $company */
        $company = $user->getCompany();

        /** @var City $city */
        $city = $this->get('ipgeobase')->getCity();

        $repository = $this->getDoctrine()
            ->getRepository('FeedBundle:Feed');
        $query = $repository->createQueryBuilder('f')
            ->select("f, review, question, ticket")
            ->leftJoin('f.ticket', 'ticket')
            ->leftJoin('ticket.service', 'service')
            ->leftJoin('f.review', 'review')
            ->leftJoin('f.question', 'question')
            ->andWhere('f.city = :city')
            ->setParameter(':city', $city->getId())
            ->orderBy('f.id', 'DESC');


        if ($company) {
            $query
                ->andWhere(
                    $query->expr()->orX(
                        $query->expr()->orX(
                            $query->expr()->eq('f.onlyuser', $user->getId()),
                            $query->expr()->isNull('f.onlyuser')
                        ),
                        $query->expr()->isNotNull('f.review'),
                        $query->expr()->isNotNull('f.ticket')
                    ));
        } else {
            $query
                ->andWhere(
                    $query->expr()->orX(
                        $query->expr()->andX(
                            $query->expr()->orX(
                                $query->expr()->eq('f.onlyuser', $user->getId()),
                                $query->expr()->isNull('f.onlyuser')
                            ),
                            $query->expr()->isNotNull('question.answer'),
                            $query->expr()->eq('question.user', $user->getId())
                        ),
                        $query->expr()->isNotNull('f.review'),
                        $query->expr()->isNotNull('f.ticket')
                    ));
        }

        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_feed */
        $pagination_feed = $paginator->paginate(
            $query,
            $page,
            10
        );
        $pagination_feed->setUsedRoute('personal_area_company.feed.load');

        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('PersonalAreaBundle:Feed:load.html.twig', [
                'feed' => $pagination_feed
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('PersonalAreaBundle:Feed:load.html.twig', [
                'feed' => $pagination_feed
            ]);
            return $events_render;
        }
    }
}
