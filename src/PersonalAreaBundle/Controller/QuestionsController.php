<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\User;
use CompanysBundle\Entity\Company;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\Question;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use PersonalAreaBundle\Form\Type\QuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QuestionsController extends Controller
{
    public function indexAction(Request $request, $type)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user){
            throw $this->createAccessDeniedException();
        }

        $company = $user->getCompany();
        if (!$company) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }


        if ($type == 'new') {
            $question = new Question();
            $form_question = $this->createForm(new QuestionType(), $question, [
                'validation_groups' => ['create_question_answer'],
            ]);
            return $this->render('PersonalAreaBundle:Questions:index.html.twig', [
                'company' => $company,
                'user' => $user,
                'type' => $type,
                'form_question' => $form_question->createView(),
            ]);
        } else {
            return $this->render('PersonalAreaBundle:Questions:oldquestions.html.twig', [
                'company' => $company,
                'user' => $user,
                'type' => $type
            ]);
        }

    }

    public function loadAction(Request $request, $answer = 1, $page)
    {
        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Question');

        $qb = $this->get('em')->createQueryBuilder();
        /**
         * @var User $user
         * @var Company $company
         */
        $user = $this->getUser();
        $company = $user->getCompany();
        $event_by_company = $company->getEvents();

        $query = $repository->createQueryBuilder('q')
            ->select(["q"])
            ->andWhere('q.isActive = true')
            ->orderBy('q.id', 'DESC');

        $array_id_events = [];
        /** @var Event $item */
        foreach ($event_by_company as $item) {
            $array_id_events[] = $item->getId();
        }

        $query->andWhere($qb->expr()->in('q.event', $array_id_events));


        if ($answer) {
            $query->andWhere($qb->expr()->isNotNull('q.answer'));
        } else {
            $query->andWhere($qb->expr()->isNull('q.answer'));
        }

        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_events_questions */
        $pagination_events_questions = $paginator->paginate(
            $query,
            $page,
            10
        );

        $pagination_events_questions->setUsedRoute('company_load_questions');

        if ($answer) {
            $template = 'PersonalAreaBundle:Questions:load.html.twig';
        } else {
            $template = 'PersonalAreaBundle:Questions:load_wa.html.twig';
        }


        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView($template, [
                'questions' => $pagination_events_questions,
                'answer' => $answer
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render($template, [
                'questions' => $pagination_events_questions,
                'answer' => $answer,
            ]);
            return $events_render;
        }
    }

    public function answerAction(Request $request, $id)
    {
        $question_class = $this->getDoctrine()->getRepository('EventsBundle:Question')->find($id);


        if ($request->request->has('personalareabundle_feed_question_answer')) {
            $form_question = $this->createForm(new QuestionType('personalareabundle_feed_question_answer'), $question_class, [
                'validation_groups' => ['create_question_answer'],
            ]);
        } elseif ($request->request->has('personalareabundle_question_answer')) {
            $form_question = $this->createForm(new QuestionType(), $question_class, [
                'validation_groups' => ['create_question_answer'],
            ]);
        } elseif ($request->request->has('feed_city_question_answer_all')) {
            $form_question = $this->createForm(new QuestionType('feed_city_question_answer_all'), $question_class, [
                'validation_groups' => ['create_question_answer'],
            ]);
        } elseif ($request->request->has('feed_city_question_answer_questions')) {
            $form_question = $this->createForm(new QuestionType('feed_city_question_answer_questions'), $question_class, [
                'validation_groups' => ['create_question_answer'],
            ]);
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {
                $em = $this->get('em');

                $em->persist($question_class);
                $em->flush();


                if ($request->request->has('personalareabundle_feed_question_answer')) {
                    $response_data = $this->forward('PersonalAreaBundle:Feed:render', array(
                        'page' => 1,
                    ));
                } elseif ($request->request->has('personalareabundle_question_answer')) {
                    $response_data = $this->forward('PersonalAreaBundle:Questions:load', array(
                        'page' => 1,
                        'answer' => 0,
                    ));
                } elseif ($request->request->has('feed_city_question_answer_all')) {
                    $response_data = $this->forward('FeedBundle:Default:render', array(
                        'page' => 1,
                        'type' => 'all',
                    ));
                } elseif ($request->request->has('feed_city_question_answer_questions')) {
                    $response_data = $this->forward('FeedBundle:Default:render', array(
                        'page' => 1,
                        'type' => 'questions',
                    ));
                }

                return new Response(json_encode(array(
                    'status' => 'success',
                    'content' => $response_data->getContent(),
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function deleteAction(Request $request, $id, $answer)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }
        /** @var Company $company */
        $company = $user->getCompany();

        if (!$company) {
            throw new NotFoundHttpException("Page not found");
        }

        $em = $this->get('em');
        $find_quest = $em->getRepository('EventsBundle:Question')->find($id);

        if ($find_quest) {
            $em->remove($find_quest);
            $em->flush();
        }

        $response_data = $this->forward('PersonalAreaBundle:Questions:load', array(
            'page' => 1,
            'answer' => $answer,
        ));

        return new Response(json_encode(array(
            'status' => 'success',
            'content' => $response_data->getContent(),
        )));
    }
}
