<?php

namespace PersonalAreaBundle\Controller;

use AdvertBundle\Entity\Advert;
use AdvertBundle\Entity\Places;
use AdvertBundle\Form\AdvertCreateType;
use AppBundle\Entity\Menu;
use AppBundle\Entity\User;
use CompanysBundle\Entity\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdvertController extends Controller
{
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException();
        }
        /** @var Company $company */
        $company = $user->getCompany();
        if (!$company) {
            throw $this->createAccessDeniedException();
        }

        $adverts_query = $this->getDoctrine()->getRepository('AdvertBundle:Advert')->createQueryBuilder('advert');
        $adverts_query
            ->addSelect(['event', 'getMenu3'])
            ->innerJoin('advert.event', 'event')
            ->innerJoin('event.getMenu3', 'getMenu3')
            ->andWhere('advert.user = :user')
            ->andWhere('advert.cntShow > 0')
            ->setParameter(':user', $user->getId());

        $adverts_result = $adverts_query->getQuery()->getResult();

        return $this->render('PersonalAreaBundle:Advert:index.html.twig', [
            'adverts' => $adverts_result
        ]);
    }

    public function createAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        /** @var Company $company */
        $company = $user->getCompany();
        if (!$company) {
            throw $this->createAccessDeniedException();
        }


        $advert = new Advert();
        $form_advertcreate = $this->createForm(AdvertCreateType::class, $advert, [
            'action' => $this->generateUrl('personal_area_advert.create'),
        ]);
        $em = $this->getDoctrine()->getManager();
        $ap_repository = $em->getRepository('AdvertBundle:Places');

        if ($request->isMethod('POST')) {
            $form_advertcreate->handleRequest($request);

            $select_places = $request->request->get('select_places');

            if (!$select_places || !count($select_places)) {
                $form_advertcreate->addError(new FormError('Необходимо выбрать места для размещения рекламы'));
            }

            $check_price = 0;

            $places_select_query = $ap_repository->createQueryBuilder('places');
            $places_select_query->andWhere($places_select_query->expr()->in('places.id', $select_places));

            /** @var ArrayCollection $places_select_result */
            $places_select_result = $places_select_query->getQuery()->getResult();
            /** @var Places $item */
            foreach ($places_select_result as $item) {
                $check_price += (float)($item->getPrice() * $form_advertcreate->get('cntShow')->getData());
            }

            if ($check_price > $user->getMoney()) {
                $form_advertcreate->addError(new FormError('У вас не достаточно денег на счете'));
            }


            if ($form_advertcreate->isValid()) {
                $advert->setUser($this->getUser());
                /** @var Places $item */
                foreach ($places_select_result as $item) {
                    $advert_persist = clone $advert;
                    $advert_persist->setPlace($item);
//                    $advert_persist->setFullSum(1);
                    $em->persist($advert_persist);
                }
                $user->setMoney($user->getMoney() - $check_price);
                $em->persist($user);
                $em->flush();

                return new Response(json_encode(array(
                    'status' => 'success',
                )));
            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_advertcreate, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }

        $event_repository = $em->getRepository('EventsBundle:Event');

        $events = $event_repository->findBy(['company' => $company->getId()]);

        $ap_query = $ap_repository->createQueryBuilder('places');
        $ap_result = $ap_query->getQuery()->getResult();

        $ap_array = [];
        /** @var Places $item */
        /** @var Menu $gm3 */
        foreach ($ap_result as $item) {
            $ap_array[$item->getId()] = $item->getPrice();
        }

        $array_ad = [];
        $max_cnt_ad = [];
        $search_ad = [];
        $repo_adverts = $this->getDoctrine()->getRepository('AdvertBundle:Advert');
        $result_adverts =
            $repo_adverts->createQueryBuilder('advert')
                ->innerJoin('advert.place', 'place')
                ->andWhere('advert.cntShow > 0')->getQuery()->getResult();

        /** @var Advert $item */
        foreach ($result_adverts as $item) {
            if (!isset($array_ad[$item->getPlace()->getId()])) {
                $array_ad[$item->getPlace()->getId()] = 1;
            } else {
                $array_ad[$item->getPlace()->getId()] += 1;
            }
            $max_cnt_ad[$item->getPlace()->getId()] = $item->getPlace()->getCntAdvert();
        }

        /**
         * @var integer $place_id
         * @var integer $max1_ad
         */
        foreach ($max_cnt_ad as $place_id => $max1_ad) {
            if ($max1_ad < $array_ad[$place_id]) {
                $search_ad[] = $place_id;
            }
        }

        $repo_menu = $this->getDoctrine()->getRepository('AppBundle:Menu');
        $query_menu = $repo_menu->createQueryBuilder('menu');
        $query_menu
            ->addSelect(['adplaces'])
            ->innerJoin('menu.adplaces', 'adplaces')
            ->andWhere('menu.lvl = 2');
        if (count($search_ad)) {
            $query_menu
                ->andWhere($query_menu->expr()->in('adplaces.id', $search_ad));
        }
        $menu = $query_menu->getQuery()->getResult()/*->getChildren()*/
        ;


        return $this->render('PersonalAreaBundle:Advert:create.html.twig', [
            'events' => $events,
            'menu' => $menu,
            'ap_menu' => $ap_array,
            'form_advertcreate' => $form_advertcreate->createView()
        ]);
    }
}
