<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\User;
use EventsBundle\Entity\TimePrice;
use PersonalAreaBundle\Form\Type\CheckTicketType;
use PersonalAreaBundle\Form\Type\DesktopTPType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DeskTopController extends Controller
{
    public function tableRenderAction(Request $request, $event)
    {
        $array_by_day = [];
        $em = $this->getDoctrine();
        $em_repository = $em->getRepository('EventsBundle:Event');

        $first_event = $em_repository->find($event);

        $repo_tp = $em->getRepository('EventsBundle:TimePrice');

        $queryBuilder = $repo_tp->createQueryBuilder('tp');
        $queryBuilder->andWhere('tp.event = :event');
        $queryBuilder
            ->addOrderBy('tp.date_event, tp.date_event_end, tp.timefrom, tp.timeto', 'ASC');
        $queryBuilder->setParameter(':event', $first_event);

        $tp_all_for_event = $queryBuilder->getQuery()->getResult();

        /** @var TimePrice $item */
        foreach ($tp_all_for_event as $item) {
            if ($item->getDateEvent()->format("Y-m-d") == $item->getDateEventEnd()->format("Y-m-d")) {
                $array_by_day[$item->getDateEvent()->format('Y-m-d')][] = $item;
            } else {
                $array_by_day[$item->getDateEvent()->format('Y-m-d') . " :diff"][] = $item;
            }
        }


        if ($request->isXmlHttpRequest()) {
            $desktop_render = $this->renderView('PersonalAreaBundle:DeskTop:table.html.twig', [
                'array_by_day' => $array_by_day
            ]);
            return new Response(json_encode([
                'status' => 'success',
                'data_load' => $desktop_render
            ]));
        } else {
            return $this->render('PersonalAreaBundle:DeskTop:table.html.twig', [
                'array_by_day' => $array_by_day
            ]);
        }
    }

    public function indexAction(Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();
        $events = [];
        $event_select = false;
        $tp_all_for_event = [];
        $em = $this->getDoctrine();

        if ($user) {

            $repo_event = $em->getRepository('EventsBundle:Event');

            $events = $repo_event->findBy(['company' => $user->getCompany(), 'isActive' => true]);
        }
        if (!count($events)) {
            $events = null;
            $table_desktop = "";
        } else {
            $event_select = $request->query->get('event');
            if ($event_select) {
                foreach ($events as $item) {
                    if ($event_select == $item->getId()) {
                        $first_event = $item;
                        break;
                    }
                }
            } else {
                $first_event = $events[0];
            }
            $table_desktop = $this->tableRenderAction($request, $first_event->getId())->getContent();
        }

        $form_tp_select = $this->createForm(DesktopTPType::class, null, [
            'action' => $this->generateUrl('personal_area_company.desktop.tp_change')
        ]);

        $form_check_ticket = $this->createForm(CheckTicketType::class, null, [
            'action' => $this->generateUrl('personal_area_company.desktop.check_by_code')
        ]);
        $form_tp_select->get('cnt_reserv')->setData('0');

        return $this->render('PersonalAreaBundle:DeskTop:index.html.twig', [
            'events' => $events,
            'tp_event' => $tp_all_for_event,
            'table_desktop' => $table_desktop,
            'event_select' => $event_select,
            'form_tp_select' => $form_tp_select->createView(),
            'form_check_ticket' => $form_check_ticket->createView()
        ]);
    }

    public function tpChangeAction(Request $request)
    {
        $form_question = $this->createForm(DesktopTPType::class, null);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {
                $em = $this->get('em');
                $type = $form_question->get('type_select')->getData();
                /** @var TimePrice $tp_class */
                $tp_class = $form_question->get('tp')->getData();
                $id_tp = $tp_class->getId();

                if ($type == 1) {
                    $em->remove($tp_class);
                } else {
                    $places = $form_question->get('cnt_reserv')->getData();

                    $tp_class->setPlaces($tp_class->getPlaces() - $places);
                }

                $em->flush();

                if ($type == 1) {
                    return new Response(json_encode(array(
                        'status' => 'success',
                        'type' => 1,
                        'id' => $id_tp
                    )));
                } else {
                    return new Response(json_encode(array(
                        'status' => 'success',
                        'type' => 2,
                        'places' => $tp_class->getPlaces() - $places,
                        'id' => $id_tp
                    )));
                }

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function checkCodeAction(Request $request)
    {
        $form_ticket = $this->createForm(CheckTicketType::class, null);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_ticket->handleRequest($request);

            if ($form_ticket->isValid()) {
                $em = $this->get('em');

                $code = $form_ticket->get('code')->getData();

                $ticket = $em->getRepository('EventsBundle:Ticket')->findOneBy(['code' => $code]);

                if ($ticket) {
                    $tp_event = $ticket->getTimeprices();
                    $event = $tp_event->getEvent();
                    $company_ticket = $event->getCompany();
                    if ($company_ticket->getId() == $user->getCompany()->getId()) {

                        $info = $this->renderView('PersonalAreaBundle:DeskTop:CheckTicketType.html.twig', [
                            'event' => $event,
                            'client' => $ticket->getUser(),
                            'tp_event' => $tp_event,
                            'ticket' => $ticket,
                        ]);

                        return new Response(json_encode(array(
                            'status' => 'success',
                            'find' => 'success',
                            'info' => $info,
                        )));
                    } else {
                        return new Response(json_encode(array(
                            'status' => 'success',
                            'find' => 'fail'
                        )));
                    }
                } else {
                    return new Response(json_encode(array(
                        'status' => 'success',
                        'find' => 'fail'
                    )));
                }

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_ticket, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }
}
