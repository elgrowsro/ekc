<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

class GiftsController extends Controller
{
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        return $this->render('PersonalAreaBundle:Gifts:index.html.twig');
    }


    public function getAction(Request $request, $get)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $repo_gift = $em->getRepository('AppBundle:Gifts')->findOneBy(['user' => $user->getId(), 'id' => $get]);
        if (!$repo_gift) {
            throw $this->createNotFoundException();
        }

        $set_money = $user->getMoney() + $repo_gift->getSum();
        $user->setMoney($set_money);
        $repo_gift->setGet(true);

        $em->persist($user);
        $em->persist($repo_gift);

        $em->flush();


        return new Response(json_encode([
            'status' => 'success',
            'money' => number_format($set_money, 2, '.', ' ')
        ]));
    }

    public function loadAction(Request $request, $page = 1)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Gifts');
        $query = $repository->createQueryBuilder('gifts')
            ->select("gifts, event, ticket, timeprices, userMake")
            ->leftJoin('gifts.event', 'event')
            ->leftJoin('gifts.userMake', 'userMake')
            ->leftJoin('gifts.ticket', 'ticket')
            ->leftJoin('ticket.timeprices', 'timeprices')
            ->andWhere('gifts.user = :user')
            ->setParameter(':user', $user->getId())
            ->orderBy('gifts.id', 'DESC');


        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_feed */
        $pagination_gifts = $paginator->paginate(
            $query,
            $page,
            10
        );

        $pagination_gifts->setUsedRoute('personal_area_gifts.load');


        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('PersonalAreaBundle:Gifts:load.html.twig', [
                'gifts' => $pagination_gifts
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('PersonalAreaBundle:Gifts:load.html.twig', [
                'gifts' => $pagination_gifts
            ]);
            return $events_render;
        }
    }
}
