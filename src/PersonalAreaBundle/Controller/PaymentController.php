<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.07.16
 * Time: 11:51
 */

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\Referrals;
use AppBundle\Entity\User;
use PersonalAreaBundle\Form\Type\MakeMoneyType;
use PersonalAreaBundle\Form\Type\WithdrawMoneyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

class PaymentController extends Controller
{
    public function renderAction()
    {
        $form_withdraw_money = $this->createForm(new WithdrawMoneyType(), null, [
            'action' => $this->generateUrl('personal_area_payment.money.withdraw')
        ]);
        $form_make_money = $this->createForm(new MakeMoneyType(), null, [
            'action' => $this->generateUrl('personal_area_payment.money.make')
        ]);

        return $this->render('PersonalAreaBundle:Payment:balance_cabinet.html.twig', [
            'form_withdraw_money' => $form_withdraw_money->createView(),
            'form_make_money' => $form_make_money->createView()
        ]);
    }

    public function makeAction(Request $request)
    {

        $form_question = $this->createForm(new MakeMoneyType(), null, [
            'action' => $this->generateUrl('personal_area_payment.money.withdraw')
        ]);
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {
                $em = $this->get('em');
                $add_money = $form_question->get('money')->getData();
                $money_user = $user->getMoney() + $add_money;
                $user->setMoney($money_user);

                $referrals = $user->getReferralsWithMe();

                if (count($referrals)) {
                    /** @var Referrals $referral */
                    $referral = $referrals[0];
                    $add_summ = $add_money / 100 * 5;
                    $referral->setSumm($referral->getSumm() + $add_summ);
                    $user_add_money = $referral->getUser();
                    $user_add_money->setMoney($user_add_money->getMoney() + $add_summ);

                    $em->persist($user_add_money);
                    $em->persist($referral);
                }


                $em->persist($user);
                $em->flush();

                return new Response(json_encode(array(
                    'status' => 'success',
                    'money_user' => $money_user
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function withdrawAction(Request $request)
    {

        $form_question = $this->createForm(new WithdrawMoneyType());
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {

                $minus_money = $form_question->get('money')->getData();
                $last_update = $user->getConclusionMoney();
                $last_update = date_add($last_update, date_interval_create_from_date_string('1 days'))->format('Y-m-d');

                if ($minus_money <= $user->getWithdrawMoney() && ($last_update <= date('Y-m-d'))) {
                    $adminCode = 'sonata.user.admin.user';
                    $sonata_admin = $this->container->get('sonata.admin.pool')->getAdminByAdminCode($adminCode);
                    $sonata_admin_edit_object = $sonata_admin->generateObjectUrl('edit', $user, [], true);

                    $em = $this->get('em');
                    $user->setConclusionMoney(new DateTime('now'));

                    $em->persist($user);
                    $em->flush();

                    $message_array = [
                        '{{ fio }}' => $user->getFLName(),
                        '{{ link }}' => $sonata_admin_edit_object,
                        '{{ sum }}' => $minus_money,
                    ];

                    $this->get('app.emailer')->sendMailToAdmin(3, $message_array);
                } else {
                    $global_error = [];
                    if ($minus_money > $user->getWithdrawMoney()) {
                        $global_error[] = 'Введенная сумма больше, чем возможная для вывода';
                    }
                    if ($last_update > date('Y-m-d')) {
                        $global_error[] = 'Вы уже подавали заявку на вывод денег за последние 24 часа';
                    }

                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => [
                            'global' => $global_error,
                            'fields' => [],
                        ]
                    )));
                }

                return new Response(json_encode(array(
                    'status' => 'success',
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }
}