<?php

namespace PersonalAreaBundle\Controller;

use AppBundle\Entity\Menu;
use AppBundle\Entity\User;
use AppBundle\Service\Geo;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use DateInterval;
use DatePeriod;
use DateTime;
use EventsBundle\Entity\AddService;
use EventsBundle\Entity\Event;
use EventsBundle\Entity\TimePrice;
use EventsBundle\Form\EventType;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use PersonalAreaBundle\Form\Type\DeleteEventType;
use PersonalAreaBundle\Form\Type\EditEventType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\EntityRepository;

class EventsController extends Controller
{
    public function indexAction(Request $request, $page = 1)
    {
        /** @var User $user */
        $user = $this->getUser();

        $pagination_events_cabinet = null;

        if ($user) {

            $eventsRepo = $this->getDoctrine()->getRepository('EventsBundle:Event');

            $queryBuilder = $eventsRepo->createQueryBuilder('event');
            $queryBuilder->where('event.isActive = true ');
            $queryBuilder->andWhere('event.company = :company');
            $queryBuilder->addOrderBy('event.id', 'DESC');
            $queryBuilder->setParameter(':company', $user->getCompany());


            $paginator = $this->get('knp_paginator');

            /** @var SlidingPagination $pagination_events_cabinet */
            $pagination_events_cabinet = $paginator->paginate(
//            $query, /* query NOT result */
                $queryBuilder, /* query NOT result */
                $page/*page number*/,
                10/*limit per page*/
            );
            $pagination_events_cabinet->setUsedRoute('personal_area_company.events.page');
        }

        $form_delete_event = $this->createForm(DeleteEventType::class, null, [
            'action' => $this->generateUrl('personal_area_company.events.delete')
        ]);

        return $this->render('PersonalAreaBundle:Events:index.html.twig', [
            'events' => $pagination_events_cabinet,
            'form_delete_event' => $form_delete_event->createView()
        ]);
    }

    public function createAction(Request $request, $type = null)
    {
        $event = new Event();
//        $event->setName('qqqwww');
//        $event->setDescription('1234567890123456');
        $timeprice = new TimePrice();
        $addservice = new AddService();

        /** @var Geo $ipgeobase */
        $ipgeobase = $this->get('ipgeobase');
        $event->setCity($ipgeobase->getCity());
        $event->getGetTimePrice()->add($timeprice);
        $event->getGetService()->add($addservice);


        $form_create = $this->createForm(new EventType(), $event, [
            'validation_groups' => ['create_event'],
            'action' => $this->generateUrl('personal_area_company.events.create.ajax', ['type' => 'check']),
        ]);

        /** @var User $user */
        $user = $this->getUser();


        if ($request->isMethod('POST') && $user && $user->getCompany()) {
            $data_all = $request->request->all();
            $data_form = $data_all[$form_create->getName()];
//            if (count($data_form['getTimePrice']) > 1) {
//                for ($i = 1; $i < count($data_form['getTimePrice']); $i++) {
//                    $event->getGetTimePrice()->add($timeprice);
//                }
//            }
//            if (count($data_form['getService']) > 1) {
//                for ($i = 1; $i < count($data_form['getService']); $i++) {
//                    $event->getGetService()->add($addservice);
//                }
//            }
            $form_create->setData($event);

            $form_create->handleRequest($request);


            if ($event->getPostVkDateClosed()) {
                if (!$event->getPostVkCntWin()) {
                    $form_create->get('postVkCntWin')->addError(new FormError('Необходимо выбрать количество победителей'));
                }
                if (!$event->getPostVkSumWin()) {
                    $form_create->get('postVkSumWin')->addError(new FormError('Необходимо ввести сумму для победителей'));
                }
            } elseif ($event->getPostVkCntWin()) {
                if (!$event->getPostVkDateClosed()) {
                    $form_create->get('postVkDateClosed')->addError(new FormError('Необходимо выбрать дату окончания конкурса'));
                }
                if (!$event->getPostVkSumWin()) {
                    $form_create->get('postVkSumWin')->addError(new FormError('Необходимо ввести сумму для победителей'));
                }
            } elseif ($event->getPostVkSumWin()) {
                if (!$event->getPostVkCntWin()) {
                    $form_create->get('postVkCntWin')->addError(new FormError('Необходимо выбрать количество победителей'));
                }
                if (!$event->getPostVkDateClosed()) {
                    $form_create->get('postVkDateClosed')->addError(new FormError('Необходимо выбрать дату окончания конкурса'));
                }
            }

            if ($event->getPostVkCntWin() && $event->getPostVkSumWin()) {
                $must_money = $event->getPostVkCntWin() * $event->getPostVkSumWin();
                if ($user->getMoney() < $must_money) {
                    $form_create->addError(new FormError("У Вас не достаточно денег на счета. Вам необходимо минимум $must_money р."));
                }
            }

            if ($form_create->isValid()) {
                $errors = [];
                $global_errors = [];

                $charity = $form_create->get('charity')->getData();

                /** @var AddService $itemService */
                foreach ($event->getGetService() as $keyService => $itemService) {
                    if ($itemService->getPrice() && !$itemService->getName()) {
                        $errors[$form_create->getName() . '_getService_' . $keyService . '_name'][] = 'Необходимо указать название дополнительной услуги';
                    } elseif (!$itemService->getPrice() && !$itemService->getName()) {
                        $event->removeGetService($itemService);
                    } else {
                        $itemService->setEvent($event);
                    }
                }


                $extend_price = [];

                if (!$charity) {
                    foreach ($data_all['dateEvent'] as $key => $item) {
                        $extend_price[] = [
                            'id' => $key,
                            'type' => $item,
                            'date' => ($item == 1 ? false : date("Y-m-d", strtotime($data_all['date_extend_' . $item][$key]))),
                        ];
                    }

                    $time_price_class_array = [];
                    /**
                     * @var integer $keyTP
                     * @var TimePrice $itemTP
                     */
                    foreach ($event->getGetTimePrice() as $keyTP => $itemTP) {
                        if (!$itemTP->getPrice() && !$itemTP->getTimeto() && !$itemTP->getTimefrom()) {
                            $event->removeGetTimePrice($itemTP);
                            unset($extend_price[$keyTP]);
                        } else {
                            $date_price = $extend_price[$keyTP];
//                        foreach ($extend_price as $date_price) {
                            $date_from = $itemTP->getDateEvent();
                            $time_from = $itemTP->getTimefrom();
                            $time_to = $itemTP->getTimeto();
                            $price = $itemTP->getPrice();
                            $places = $itemTP->getPlaces();
                            $event->removeGetTimePrice($itemTP);

                            if (!$places) {
                                $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать количество мест на мероприятие';
                                continue;
                            }
                            if ($date_price['type'] == 1) {
                                $begin = $date_from;
                                $end = $date_from;
                                $time_price_class = new TimePrice();

                                $time_price_class->setTimefrom($time_from);
                                $time_price_class->setTimeto($time_to);
                                $time_price_class->setPrice($price);
                                $time_price_class->setPlaces($places);
                                $time_price_class->setDateEvent($begin);
                                $time_price_class->setDateEventEnd($end);

                                $time_price_class_array[] = $time_price_class;
                            } elseif ($date_price['type'] == 2) {
                                $begin = $date_from;
                                if (!$date_price['date']) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать число, до которого продливается мероприятие';
                                    continue;
                                }

                                $end = new DateTime($date_price['date']);

                                if ($begin->format('Y-m-d') > $end->format('Y-m-d')) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Число, до которого продливается мероприятие, должно быть больше, чем дата начала';
                                    continue;
                                }

                                $interval = DateInterval::createFromDateString('1 day');
                                $period = new DatePeriod($begin, $interval, $end);


                                foreach ($period as $dt) {
                                    $time_price_class = new TimePrice();
                                    $time_price_class->setTimefrom($time_from);
                                    $time_price_class->setTimeto($time_to);
                                    $time_price_class->setPlaces($places);
                                    $time_price_class->setPrice($price);
                                    $time_price_class->setDateEvent($dt);
                                    $time_price_class->setDateEventEnd($dt);

                                    $time_price_class_array[] = $time_price_class;
                                }
                            } elseif ($date_price['type'] == 4) {
                                $begin = $date_from;
                                if (!$date_price['date']) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать число, до которого продливается мероприятие';
                                    continue;
                                }
                                $end = new DateTime($date_price['date']);

                                if ($begin->format('Y-m-d') > $end->format('Y-m-d')) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Число, до которого продливается мероприятие, должно быть больше, чем дата начала';
                                    continue;
                                }

                                $interval = DateInterval::createFromDateString('1 day');
                                $period = new DatePeriod($begin, $interval, $end);


                                foreach ($period as $dt) {
                                    if (date('w', $dt) >= 1 && date('w', $dt) <= 5) {
                                        $time_price_class = new TimePrice();
                                        $time_price_class->setTimefrom($time_from);
                                        $time_price_class->setTimeto($time_to);
                                        $time_price_class->setPlaces($places);
                                        $time_price_class->setPrice($price);
                                        $time_price_class->setDateEvent($dt);
                                        $time_price_class->setDateEventEnd($dt);

                                        $time_price_class_array[] = $time_price_class;
                                    }
                                }
                            } elseif ($date_price['type'] == 5) {
                                $begin = $date_from;
                                if (!$date_price['date']) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать число, до которого продливается мероприятие';
                                    continue;
                                }
                                $end = new DateTime($date_price['date']);

                                if ($begin->format('Y-m-d') > $end->format('Y-m-d')) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Число, до которого продливается мероприятие, должно быть больше, чем дата начала';
                                    continue;
                                }

                                $interval = DateInterval::createFromDateString('1 day');
                                $period = new DatePeriod($begin, $interval, $end);


                                foreach ($period as $dt) {
                                    if (in_array(date('w', $dt), [6, 0])) {
                                        $time_price_class = new TimePrice();
                                        $time_price_class->setTimefrom($time_from);
                                        $time_price_class->setTimeto($time_to);
                                        $time_price_class->setPlaces($places);
                                        $time_price_class->setPrice($price);
                                        $time_price_class->setDateEvent($dt);
                                        $time_price_class->setDateEventEnd($dt);

                                        $time_price_class_array[] = $time_price_class;
                                    }
                                }
                            }
                        }
                    }


                    $i = 1;
                    $break_tp = false;
                    /**
                     * @var integer $key
                     * @var TimePrice $tp
                     */
                    foreach ($time_price_class_array as $key => $tp) {
                        $date_start = $tp->getDateEvent();
                        $date_end = $tp->getDateEventEnd();
                        $time_start = $tp->getTimefrom();
                        $time_end = $tp->getTimeto();


                        if ($date_start->format("Y-m-d") == $date_end->format("Y-m-d")) {
                            if (isset($array_tp[$date_start->format("Y-m-d")])) {
                                foreach ($array_tp[$date_start->format("Y-m-d")] as $key_block => $item) {
                                    if (
                                        ($time_start <= $item['time_from'] && $time_end >= $item['time_from'])
                                        || ($time_start <= $item['time_end'] && $time_end >= $item['time_end'])
                                        || ($time_start > $item['time_from'] && $time_end > $item['time_from'] && $time_start < $item['time_end'] && $time_end < $item['time_end'])
                                    ) {
                                        $global_errors[] = 'Ошибка: В планировании мероприятия в полях обнарежено пересечение времени';
                                        $break_tp = true;
                                        break;
                                    }
                                }
                            } elseif (isset($array_tp[$date_end->format("Y-m-d")])) {
                                foreach ($array_tp[$date_end->format("Y-m-d")] as $key_block => $item) {
                                    if (
                                        ($time_start <= $item['time_from'] && $time_end >= $item['time_from'])
                                        || ($time_start <= $item['time_end'] && $time_end >= $item['time_end'])
                                        || ($time_start > $item['time_from'] && $time_end > $item['time_from'] && $time_start < $item['time_end'] && $time_end < $item['time_end'])
                                    ) {
                                        $global_errors[] = 'Ошибка: В планировании мероприятия обнарежено пересечение времени';
                                        $break_tp = true;
                                        break;
                                    }
                                }
                            }

                            $array_tp[$date_start->format("Y-m-d")][$i] = [
                                'date_start' => $date_start,
                                'date_end' => $date_end,
                                'time_from' => $time_start,
                                'time_end' => $time_end,
                            ];
                        }
                        if ($break_tp) {
                            break;
                        }
                        $i++;
                    }


                    /** @var TimePrice $item */
                    foreach ($time_price_class_array as $item) {
                        $item->setEvent($event);
                        $event->addGetTimePrice($item);
                    }
                } else {
                    $event->getGetTimePrice()->clear();
                }

                if (isset($data_all['select_menu'])) {
                    foreach ($data_all['select_menu'] as $key => $item) {
                        $menu3_em = $this->getDoctrine()
                            ->getRepository('AppBundle:Menu')
                            ->find($item);
                        if ($menu3_em && !$event->getGetMenu3()->contains($menu3_em)) {
                            $event->addGetMenu3($menu3_em);
                        }
                    }
                } else {
                    $global_errors[] = 'Необходимо выбрать разделы в которых будет находится мероприятие';
                }


                $youtube_videos = trim($request->request->get('youtube_videos'));

                if ($youtube_videos) {
                    $rx = '~
                    ^(?:https?://)?                  # Optional protocol
                     (?:www\.)?                  # Optional subdomain
                     (?:youtube\.com|youtu\.be)  # Mandatory domain name
                     /watch\?v=([^&]+)           # URI with video id as capture group 1
                     ~x';

                    $youtube_videos_array_by_dot = explode(',', $youtube_videos);
                    foreach ($youtube_videos_array_by_dot as $item) {
                        $youtube_videos_array_by_space = explode(' ', trim($item));
                        foreach ($youtube_videos_array_by_space as $item_space) {
                            $has_match = preg_match($rx, $item_space, $matches);
                            if (!isset($matches[1])) {
                                $errors['youtube_videos'][] = 'Данный url не соответствует ссылке с youtube.com: ' . $item_space;
                            }
                        }
                    }
                }


                if (count($errors) || count($global_errors)) {
                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => [
                            'global' => $global_errors,
                            'fields' => $errors,
                        ]
                    )));
                } else {
                    if ($type == 'check') {
                        return new Response(json_encode(array(
                            'status' => 'success',
                            'next_url' => $this->generateUrl('personal_area_company.events.create.ajax', ['type' => 'success'])
                        )));
                    } else {
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        if ($form_create->get('main_image')->getData()) {
                            $web_path = $this->get('kernel')->getRootDir() . '/../web';

                            /** @var Media $media */
                            $media = $mediaManager->create();
                            $media->setBinaryContent($web_path . $form_create->get('main_image')->getData());
                            $media->setContext('Events_main');
                            $media->setProviderName('sonata.media.provider.image');

                            $mediaManager->save($media);
                            $event->setMainImage($media);
                        }

                        $mediaGalleryManager = $this->container->get('sonata.media.manager.gallery');
                        /** @var Gallery $gallery */
                        $gallery = $mediaGalleryManager->create();
                        $gallery->setContext('Events_gallery');
                        $gallery->setDefaultFormat('Events_gallery_small');
                        $gallery->setName('Картинки/Видео для "' . $event->getName() . '"');
                        $gallery->setEnabled(true);


                        $uploadedFile = $request->files->get('file');

                        if (count($uploadedFile)) {
                            /** @var UploadedFile $item */
                            foreach ($uploadedFile as $item) {
                                /** @var Media $media */
                                $media = $mediaManager->create();
                                $media->setContext('Events_gallery');

                                $ext = $item->getClientOriginalExtension();
                                $name = $item->getClientOriginalName();

                                switch ($ext) {
                                    case "png";
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/png/';
                                        break;
                                    case "jpg";
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpg/';
                                        break;
                                    case "jpeg";
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpeg/';
                                        break;
                                    default:
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/files/';
                                }

                                $file = $item->move($directory, $name);

                                $media->setProviderName('sonata.media.provider.image');
                                $media->setBinaryContent($directory . $name);
                                $media->setAuthor($user);

                                $mediaManager->save($media);
                                $galleryHasMedia = new GalleryHasMedia();
                                $galleryHasMedia->setMedia($media);
                                $galleryHasMedia->setGallery($gallery);
                                $galleryHasMedia->setEnabled(true);
                                $gallery->addGalleryHasMedias($galleryHasMedia);
                            }
                        }


                        if (trim($youtube_videos)) {
                            $youtube_videos_array_by_dot = explode(',', $youtube_videos);
                            foreach ($youtube_videos_array_by_dot as $item) {
                                $youtube_videos_array_by_space = explode(' ', trim($item));
                                foreach ($youtube_videos_array_by_space as $item_space) {
                                    $media = $mediaManager->create();
                                    $media->setContext('Events_gallery');
                                    $media->setProviderName('sonata.media.provider.youtube');
                                    $media->setBinaryContent($item_space);
                                    $media->setAuthor($user);

                                    $mediaManager->save($media);
                                    $galleryHasMedia = new GalleryHasMedia();
                                    $galleryHasMedia->setMedia($media);
                                    $galleryHasMedia->setGallery($gallery);
                                    $galleryHasMedia->setEnabled(true);
                                    $gallery->addGalleryHasMedias($galleryHasMedia);
                                }
                            }
                        }

                        $mediaGalleryManager->save($gallery);
                        $event->setImages($gallery);

                        $event->setCompany($user->getCompany());
                        $event->setIsActive(false);


                        $em = $this->getDoctrine()->getManager();

                        if (isset($must_money)) {
                            $user->setMoney($user->getMoney() - $must_money);
                            $em->persist($user);
                        }
                        $em->persist($event);

                        $em->flush();

                        $adminCode = 'admin.events.list';

                        $sonata_admin = $this->container->get('sonata.admin.pool')->getAdminByAdminCode($adminCode);

                        $sonata_admin_edit_object = $sonata_admin->generateObjectUrl('edit', $event, [], true);


                        $message_array = [
                            '{{ FLName }}' => $user->getFLName(),
                            '{{ link }}' => $sonata_admin_edit_object,
                        ];

                        $this->get('app.emailer')->sendMailToAdmin(2, $message_array);


//                    return new Response(json_encode(array(
//                        'status' => 'error',
//                        'errors' => [
//                            'global' => '',
//                            'fields' => $errors,
//                        ]
//                    )));
                        return new Response(json_encode(array(
                            'status' => 'success',
                        )));
                    }
                }
            } else {

//                print_r($form_create->get('getTimePrice')->getErrors());
//                print_r($form_create->get('getService')->getErrors());
                $errors = $this->get('form_serializer')->serializeFormErrors($form_create, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }

        $repo_menu = $this->getDoctrine()->getRepository('AppBundle:Menu');
        $menu = $repo_menu->getChildren();


        return $this->render('PersonalAreaBundle:Events:create.html.twig', [
            'form_create' => $form_create->createView(),
            'menu' => $menu,
        ]);
    }

    public function deleteAction(Request $request)
    {
        $form_delete_event = $this->createForm(DeleteEventType::class);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_delete_event->handleRequest($request);


            if ($form_delete_event->isValid()) {
                $em = $this->get('em');

                /** @var Event $event */
                $event_id = $form_delete_event->get('id')->getData();
                $event = $em->getRepository('EventsBundle:Event')->find($event_id);

                if ($event->getCompany()->getId() == $user->getCompany()->getId()) {

                    $em->remove($event);
                    $em->flush();


                    $response_data = $this->forward('PersonalAreaBundle:Events:load', array(
                        'page' => 1,
                    ));

                    return new Response(json_encode(array(
                        'status' => 'success',
                        'content' => $response_data->getContent(),
                    )));
                } else {
                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => []
                    )));
                }
            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_delete_event, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function editAction(Request $request, $event, $type = null)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedHttpException("Page not found");
        }


        $em = $this->getDoctrine()->getManager();

        $event_repo = $em->getRepository('EventsBundle:Event');

        $query = $event_repo->createQueryBuilder('e');

        $query->select(["e"])
            ->andWhere('e.id = :id')
            ->andWhere('e.company = :company')
            ->setParameter(':id', $event)
            ->setParameter(':company', $user->getCompany()->getId());

        /** @var Event $event */
        $event = $query->getQuery()->getOneOrNullResult();
        if (!$event) {
            throw new AccessDeniedHttpException("Event not found");
        }

        if (!count($event->getGetService()) && !$request->isMethod('POST')) {
            $event->getGetService()->add(new AddService());
        }

        if (!count($event->getGetTimePrice()) && !$request->isMethod('POST')) {
            $event->getGetTimePrice()->add(new TimePrice());
        }

        $form_edit = $this->createForm(new EditEventType(), $event, [
            'validation_groups' => ['edit_event'],
            'action' => $this->generateUrl('personal_area_company.events.edit', ['event' => $event->getId(), 'type' => ($type == 'edit' ? 'check' : ($type == 'check' ? 'success' : 'edit'))]),
        ]);
//        var_dump($request->request->all());

        if ($request->isMethod('POST')) {
            $data_all = $request->request->all();

            $form_edit->handleRequest($request);

            if ($form_edit->isValid()) {
                $errors = [];
                $global_errors = [];

                $charity = $form_edit->get('charity')->getData();

                if (isset($data_all['select_menu'])) {
                    $menu_event = $event->getGetMenu3();
                    $menu_event_by_id = [];
                    /** @var Menu $item */
                    foreach ($menu_event as $item) {
                        $menu_event_by_id[$item->getId()] = $item;
                    }

//                    var_dump($data_all['select_menu']);
//                    var_dump(array_keys($menu_event_by_id));
//                    die();
                    /** @var integer $item */
                    foreach ($data_all['select_menu'] as $key => $item) {
                        if (!isset($menu_event_by_id[$item])) {
                            $menu3_em = $this->getDoctrine()
                                ->getRepository('AppBundle:Menu')
                                ->find($item);
                            if ($menu3_em && !$event->getGetMenu3()->contains($menu3_em)) {
                                $event->addGetMenu3($menu3_em);
                            }
                            unset($menu_event_by_id[$item]);
                        } else {
                            unset($menu_event_by_id[$item]);
                        }
                    }
                    if (count($menu_event_by_id)) {
                        /** @var Menu $item */
                        foreach ($menu_event_by_id as $item) {
                            $event->removeGetMenu3($item);
                        }
                    }
                } else {
                    $global_errors[] = 'Необходимо выбрать разделы в которых будет находится мероприятие';
                }


                /** @var AddService $itemService */
                foreach ($event->getGetService() as $keyService => $itemService) {
                    if ($itemService->getPrice() && !$itemService->getName()) {
                        $errors[$form_edit->getName() . '_getService_' . $keyService . '_name'][] = 'Необходимо указать название дополнительной услуги';
                    } elseif (!$itemService->getPrice() && !$itemService->getName()) {
                        $event->removeGetService($itemService);
                    } else {
                        $itemService->setEvent($event);
                    }
                }

                if (!$charity) {
                    $extend_price = [];
                    $time_price_class_array = [];

                    foreach ($data_all['dateEvent'] as $key => $item) {
                        $extend_price[$key] = [
                            'id' => $key,
                            'type' => $item,
                            'date' => ($item == 1 ? false : date("Y-m-d", strtotime($data_all['date_extend_' . $item][$key]))),
                        ];
                    }


                    /**
                     * @var integer $keyTP
                     * @var TimePrice $itemTP
                     */
                    foreach ($event->getGetTimePrice() as $keyTP => $itemTP) {
                        if (!$itemTP->getPrice() && !$itemTP->getTimeto() && !$itemTP->getTimefrom()) {
                            $event->removeGetTimePrice($itemTP);
                            unset($extend_price[$keyTP]);
                        } else {
                            $date_price = $extend_price[$keyTP];
//                        foreach ($extend_price as $date_price) {
                            $date_from = $itemTP->getDateEvent();
                            $time_from = $itemTP->getTimefrom();
                            $time_to = $itemTP->getTimeto();
                            $price = $itemTP->getPrice();
                            $places = $itemTP->getPlaces();
                            $event->removeGetTimePrice($itemTP);

                            if (!$places) {
                                $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать количество мест на мероприятие';
                                continue;
                            }
                            if ($date_price['type'] == 1) {
                                $begin = $date_from;
                                $end = $date_from;
                                $time_price_class = new TimePrice();

                                $time_price_class->setTimefrom($time_from);
                                $time_price_class->setTimeto($time_to);
                                $time_price_class->setPrice($price);
                                $time_price_class->setPlaces($places);
                                $time_price_class->setDateEvent($begin);
                                $time_price_class->setDateEventEnd($end);

                                $time_price_class_array[] = $time_price_class;
                            } elseif ($date_price['type'] == 2) {
                                $begin = $date_from;
                                if (!$date_price['date']) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать число, до которого продливается мероприятие';
                                    continue;
                                }
                                $end = new DateTime($date_price['date']);
                                date_add($end, date_interval_create_from_date_string('1 days'));

                                if ($begin->format('Y-m-d') > $end->format('Y-m-d')) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Число, до которого продливается мероприятие, должно быть больше, чем дата начала';
                                    continue;
                                }

                                $interval = DateInterval::createFromDateString('1 day');
                                $period = new DatePeriod($begin, $interval, $end);


                                foreach ($period as $dt) {
                                    $time_price_class = new TimePrice();
                                    $time_price_class->setTimefrom($time_from);
                                    $time_price_class->setTimeto($time_to);
                                    $time_price_class->setPlaces($places);
                                    $time_price_class->setPrice($price);
                                    $time_price_class->setDateEvent($dt);
                                    $time_price_class->setDateEventEnd($dt);

                                    $time_price_class_array[] = $time_price_class;
                                }
                            } elseif ($date_price['type'] == 4) {
                                $begin = $date_from;
                                if (!$date_price['date']) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать число, до которого продливается мероприятие';
                                    continue;
                                }
                                $end = new DateTime($date_price['date']);
                                date_add($end, date_interval_create_from_date_string('1 days'));

                                if ($begin->format('Y-m-d') > $end->format('Y-m-d')) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Число, до которого продливается мероприятие, должно быть больше, чем дата начала';
                                    continue;
                                }

                                $interval = DateInterval::createFromDateString('1 day');
                                $period = new DatePeriod($begin, $interval, $end);


                                foreach ($period as $dt) {
                                    if (date('w', $dt) >= 1 && date('w', $dt) <= 5) {
                                        $time_price_class = new TimePrice();
                                        $time_price_class->setTimefrom($time_from);
                                        $time_price_class->setTimeto($time_to);
                                        $time_price_class->setPlaces($places);
                                        $time_price_class->setPrice($price);
                                        $time_price_class->setDateEvent($dt);
                                        $time_price_class->setDateEventEnd($dt);

                                        $time_price_class_array[] = $time_price_class;
                                    }
                                }
                            } elseif ($date_price['type'] == 5) {
                                $begin = $date_from;
                                if (!$date_price['date']) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Необходимо выбрать число, до которого продливается мероприятие';
                                    continue;
                                }
                                $end = new DateTime($date_price['date']);
                                date_add($end, date_interval_create_from_date_string('1 days'));

                                if ($begin->format('Y-m-d') > $end->format('Y-m-d')) {
                                    $errors['error_date_event_' . $date_price['id']][] = 'Число, до которого продливается мероприятие, должно быть больше, чем дата начала';
                                    continue;
                                }

                                $interval = DateInterval::createFromDateString('1 day');
                                $period = new DatePeriod($begin, $interval, $end);


                                foreach ($period as $dt) {
                                    if (in_array(date('w', $dt), [6, 0])) {
                                        $time_price_class = new TimePrice();
                                        $time_price_class->setTimefrom($time_from);
                                        $time_price_class->setTimeto($time_to);
                                        $time_price_class->setPlaces($places);
                                        $time_price_class->setPrice($price);
                                        $time_price_class->setDateEvent($dt);
                                        $time_price_class->setDateEventEnd($dt);

                                        $time_price_class_array[] = $time_price_class;
                                    }
                                }
                            }
                        }
                    }

                    $i = 1;
                    $break_tp = false;
                    /**
                     * @var integer $key
                     * @var TimePrice $tp
                     */
                    foreach ($time_price_class_array as $key => $tp) {
                        $date_start = $tp->getDateEvent();
                        $date_end = $tp->getDateEventEnd();
                        $time_start = $tp->getTimefrom();
                        $time_end = $tp->getTimeto();


                        if ($date_start->format("Y-m-d") == $date_end->format("Y-m-d")) {
                            if (isset($array_tp[$date_start->format("Y-m-d")])) {
                                foreach ($array_tp[$date_start->format("Y-m-d")] as $key_block => $item) {
                                    if (
                                        ($time_start <= $item['time_from'] && $time_end >= $item['time_from'])
                                        || ($time_start <= $item['time_end'] && $time_end >= $item['time_end'])
                                        || ($time_start > $item['time_from'] && $time_end > $item['time_from'] && $time_start < $item['time_end'] && $time_end < $item['time_end'])
                                    ) {
                                        $global_errors[] = 'Ошибка: В планировании мероприятия в полях обнарежено пересечение времени';
                                        $break_tp = true;
                                        break;
                                    }
                                }
                            } elseif (isset($array_tp[$date_end->format("Y-m-d")])) {
                                foreach ($array_tp[$date_end->format("Y-m-d")] as $key_block => $item) {
                                    if (
                                        ($time_start <= $item['time_from'] && $time_end >= $item['time_from'])
                                        || ($time_start <= $item['time_end'] && $time_end >= $item['time_end'])
                                        || ($time_start > $item['time_from'] && $time_end > $item['time_from'] && $time_start < $item['time_end'] && $time_end < $item['time_end'])
                                    ) {
                                        $global_errors[] = 'Ошибка: В планировании мероприятия обнарежено пересечение времени';
                                        $break_tp = true;
                                        break;
                                    }
                                }
                            }

                            $array_tp[$date_start->format("Y-m-d")][$i] = [
                                'date_start' => $date_start,
                                'date_end' => $date_end,
                                'time_from' => $time_start,
                                'time_end' => $time_end,
                            ];
                        }
                        if ($break_tp) {
                            break;
                        }
                        $i++;
                    }


                    /** @var TimePrice $item */
                    foreach ($time_price_class_array as $item) {
                        $item->setEvent($event);
                        $event->addGetTimePrice($item);
                    }
                } else {
                    $event->getGetTimePrice()->clear();
                }


                $youtube_videos = trim($request->request->get('youtube_videos'));

                if ($youtube_videos) {
                    $rx = '~
                    ^(?:https?://)?                  # Optional protocol
                     (?:www\.)?                  # Optional subdomain
                     (?:youtube\.com|youtu\.be)  # Mandatory domain name
                     /watch\?v=([^&]+)           # URI with video id as capture group 1
                     ~x';

                    $youtube_videos_array_by_dot = explode(',', $youtube_videos);
                    foreach ($youtube_videos_array_by_dot as $item) {
                        $youtube_videos_array_by_space = explode(' ', trim($item));
                        foreach ($youtube_videos_array_by_space as $item_space) {
                            $has_match = preg_match($rx, $item_space, $matches);
                            if (!isset($matches[1])) {
                                $errors['youtube_videos'][] = 'Данный url не соответствует ссылке с youtube.com: ' . $item_space;
                            }
                        }
                    }
                }


                if (count($errors) || count($global_errors)) {
                    return new Response(json_encode(array(
                        'status' => 'error',
                        'errors' => [
                            'global' => $global_errors,
                            'fields' => $errors,
                        ]
                    )));
                } else {
                    if ($type == 'check') {
                        return new Response(json_encode(array(
                            'status' => 'success',
                            'next_url' => $this->generateUrl('personal_area_company.events.edit', ['event' => $event->getId(), 'type' => 'success'])
                        )));
                    } else {
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $repo_GalleryHasMedias = $em->getRepository('ApplicationSonataMediaBundle:GalleryHasMedia');

                        /** @var Gallery $gallery */
                        $gallery = $event->getImages();

                        $delete_image = $request->request->get('delete_image');
                        if ($delete_image && count($delete_image)) {
                            foreach ($delete_image as $item) {
                                /** @var Media $media_for_delete */
                                $media_obj_for_delete = $mediaManager->findOneBy(['id' => $item, 'author' => $user->getId()]);
                                if ($media_obj_for_delete) {
                                    /** @var GalleryHasMedia $media_for_delete */
                                    $media_for_delete = $repo_GalleryHasMedias->findOneBy(['media' => $item]);
                                    if ($media_for_delete) {
                                        $em->remove($media_for_delete);
                                    }

                                    if ($media_obj_for_delete) {
                                        $mediaManager->getObjectManager()->remove($media_obj_for_delete);
                                    }
                                }
                            }
                        }
                        $delete_video = $request->request->get('delete_video');
                        if ($delete_video && count($delete_video)) {
                            foreach ($delete_video as $item) {
                                /** @var Media $media_for_delete */
                                $media_obj_for_delete = $mediaManager->findOneBy(['id' => $item, 'author' => $user->getId()]);
                                if ($media_obj_for_delete) {
                                    /** @var GalleryHasMedia $media_for_delete */
                                    $media_for_delete = $repo_GalleryHasMedias->findOneBy(['media' => $item]);
                                    if ($media_for_delete) {
                                        $em->remove($media_for_delete);
                                    }

                                    if ($media_obj_for_delete) {
                                        $mediaManager->getObjectManager()->remove($media_obj_for_delete);
                                    }
                                }
                            }
                        }

                        if ($form_edit->get('main_image')->getData()) {
                            $web_path = $this->get('kernel')->getRootDir() . '/../web';

                            /** @var Media $media */
                            $media = $event->getMainImage();
                            $media->setBinaryContent($web_path . $form_edit->get('main_image')->getData());

                            $mediaManager->save($media);
                        }

                        $uploadedFile = $request->files->get('file');

                        if (count($uploadedFile)) {
                            /** @var UploadedFile $item */
                            foreach ($uploadedFile as $item) {
                                /** @var Media $media */
                                $media = $mediaManager->create();
                                $media->setContext('Events_gallery');

                                $ext = $item->getClientOriginalExtension();
                                $name = $item->getClientOriginalName();

                                switch ($ext) {
                                    case "png";
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/png/';
                                        break;
                                    case "jpg";
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpg/';
                                        break;
                                    case "jpeg";
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpeg/';
                                        break;
                                    default:
                                        $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/files/';
                                }

                                $file = $item->move($directory, $name);

                                $media->setProviderName('sonata.media.provider.image');
                                $media->setBinaryContent($directory . $name);
                                $media->setAuthor($user);

                                $mediaManager->save($media);
                                $galleryHasMedia = new GalleryHasMedia();
                                $galleryHasMedia->setMedia($media);
                                $galleryHasMedia->setGallery($gallery);
                                $galleryHasMedia->setEnabled(true);
                                $gallery->addGalleryHasMedias($galleryHasMedia);
                            }
                        }

                        if (trim($youtube_videos)) {
                            $youtube_videos_array_by_dot = explode(',', $youtube_videos);
                            foreach ($youtube_videos_array_by_dot as $item) {
                                $youtube_videos_array_by_space = explode(' ', trim($item));
                                foreach ($youtube_videos_array_by_space as $item_space) {
                                    $media = $mediaManager->create();
                                    $media->setContext('Events_gallery');
                                    $media->setProviderName('sonata.media.provider.youtube');
                                    $media->setBinaryContent($item_space);
                                    $media->setAuthor($user);

                                    $mediaManager->save($media);
                                    $galleryHasMedia = new GalleryHasMedia();
                                    $galleryHasMedia->setMedia($media);
                                    $galleryHasMedia->setGallery($gallery);
                                    $galleryHasMedia->setEnabled(true);
                                    $gallery->addGalleryHasMedias($galleryHasMedia);
                                }
                            }
                        }

                        $em->persist($event);


                        $em->flush();

                        return new Response(json_encode(array(
                            'status' => 'success',
                        )));
                    }
                }
            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_edit, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }

        /** @var EntityRepository $mediaGalleryManager */
        $mediaGalleryManager = $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media');
        if ($event->getImages()) {
            $event_images = $mediaGalleryManager->createQueryBuilder('i')
                ->select(["i"])
                ->innerJoin('i.galleryHasMedias', 'galleryHasMedias')
                ->innerJoin('galleryHasMedias.gallery', 'gallery')
                ->andWhere('gallery.id = :gid')
                ->andWhere("i.providerName = 'sonata.media.provider.image'")
                ->andWhere("i.author = :userid")
                ->setParameter(':gid', $event->getImages()->getId())
                ->setParameter(':userid', $user->getId())
                ->getQuery();
            $gallary_images = $event_images->getResult();
        } else {
            $gallary_images = null;
        }

        if ($event->getImages()) {
            $youtubes_video = $mediaGalleryManager->createQueryBuilder('m')
                ->select(["m"])
                ->innerJoin('m.galleryHasMedias', 'galleryHasMedias')
                ->innerJoin('galleryHasMedias.gallery', 'gallery')
                ->andWhere('gallery.id = :gid')
                ->andWhere("m.providerName = 'sonata.media.provider.youtube'")
                ->andWhere("m.author = :userid")
                ->setParameter(':gid', $event->getImages()->getId())
                ->setParameter(':userid', $user->getId())
                ->getQuery();
            $gallary_youtube = $youtubes_video->getResult();
        } else {
            $gallary_youtube = null;
        }


        $repo_menu = $this->getDoctrine()->getRepository('AppBundle:Menu');
        $menu = $repo_menu->getChildren();


        return $this->render('PersonalAreaBundle:Events:edit.html.twig', [
            'form_edit' => $form_edit->createView(),
            'menu' => $menu,
            'event' => $event,
            'gallary_youtube' => $gallary_youtube,
            'gallary_images' => $gallary_images,
        ]);
    }


    public function loadAction(Request $request, $page)
    {
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException();
        }

        $company = $this->getDoctrine()
            ->getRepository('CompanysBundle:Company')->findOneBy(['usercompany' => $user]);

        if (!$company) {
            throw $this->createAccessDeniedException();
        }

        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Event');
        $query = $repository->createQueryBuilder('e')
            ->select("e")
            ->where('e.company = :company')
            ->setParameter('company', $company);


        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_events */
        $pagination_events = $paginator->paginate(
            $query,
            $page,
            10
        );
        $pagination_events->setUsedRoute('personal_area_load_events');

        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('PersonalAreaBundle:Events:load.html.twig', [
                'events' => $pagination_events
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('PersonalAreaBundle:Events:load.html.twig', [
                'events' => $pagination_events
            ]);
            return $events_render;
        }
    }
}
