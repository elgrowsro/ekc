<?php

namespace PersonalAreaBundle\Form\Type;

use EventsBundle\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EditEventType extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'eventsbundle_event_edit')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Название мероприятия',
                'required' => true,
                'trim' => true
            ])
            ->add('city', 'entity', [
                'class' => 'AppBundle\Entity\City',
                'label' => 'Город проведения мероприятия',
                'choice_label' => 'name',
                'required' => true
            ])
            ->add('main_image', 'hidden', array(
                'required' => false,
                'mapped' => false,
                'trim' => true
            ))
            ->add('description', 'textarea', [
                'label' => 'Описание мероприятия',
                'required' => true,
                'trim' => true
            ])
            ->add('forMan', 'checkbox', [
                'label' => 'Него',
                'required' => false
            ])
            ->add('forWoman', 'checkbox', [
                'label' => 'Неё',
                'required' => false
            ])
            ->add('forElderly', 'checkbox', [
                'label' => 'Взрослым',
                'required' => false
            ])
            ->add('getTimePrice', 'collection', [
                'type' => new TimePriceEditType(),
                'label' => 'Запланировать мероприятие',
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('getService', 'collection', [
                'type' => new AddServiceEditType(),
                'label' => 'Дополнительные услуги',
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('lat', 'hidden', [
                'required' => false,
            ])
            ->add('lng', 'hidden', [
                'required' => false,
            ])
            ->add('pathWay', 'hidden', [
                'required' => false,
            ])
            ->add('charity', 'hidden', [
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventsBundle\Entity\Event',
            'cascade_validation' => true,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_with_dropzone form',
                'novalidate' => 'novalidate'
            ]
        ));
    }

    /**
     * @return string
     */

    public function getName()
    {
        return $this->form_name;
    }
}
