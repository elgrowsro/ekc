<?php
namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;

class RegistrateIndividual extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'registrate_organization_individual')
    {
        $this->form_name = $form_name;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textAbout', 'textarea', array(
                'label' => 'Информация о себе',
                'required' => true,
            ))
            ->add('typeCompany', 'hidden', array(
                'required' => true,
                'label' => 'Тип организации'
            ))
            ->add('passport', 'textarea', array(
                'label' => 'Паспортные данные',
                'required' => true,
            ))
            ->add('phoneForAdmin', 'text', array(
                'label' => 'Номер телефона для администрации сайта',
                'required' => true,
            ))
            ->add('phoneForUsers', 'text', array(
                'label' => 'Номер телефона для клиентов организатора',
                'required' => true,
            ))
            ->add('setlogo', 'hidden', [
                'required' => false,
                'mapped' => false,
            ])
            ->add('lat', 'hidden', [
                'required' => false,
            ])
            ->add('lng', 'hidden', [
                'required' => false,
            ])
            ->add('documents', 'collection', array(
                // each entry in the array will be an "email" field
                'entry_type' => 'hidden',
                'label' => false,
                'mapped' => false,
//                'mapped' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => 'CompanysBundle\Entity\Company',
//            'validation_groups' => array('aboutinfo'),
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_with_dropzone form',
                'novalidate' => 'novalidate',
                'enctype' => 'multipart/form-data'
            ]
        ));
    }

    public function getName()
    {
        return $this->form_name;
    }
}