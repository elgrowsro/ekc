<?php
namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;

class RegistrateOrganizationWithoutLogin extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'registrate_organization_without_login')
    {
        $this->form_name = $form_name;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textAbout', 'textarea', array(
                'label' => 'Об организаторе',
                'required' => true,
            ))
            ->add('typeCompany', 'hidden', array(
                'required' => true,
                'label' => 'Тип организации'
            ))
            ->add('nameCompany', 'text', array(
                'label' => 'Название компании',
                'required' => true,
            ))
            ->add('phoneForAdmin', 'text', array(
                'label' => 'Номер телефона для администрации сайта',
                'required' => true,
            ))
            ->add('phoneForUsers', 'text', array(
                'label' => 'Номер телефона для клиентов организатора',
                'required' => true,
            ))
            ->add('orgn', 'text', array(
                'label' => 'ОРГН',
                'required' => true,
            ))
            ->add('dirname', 'text', array(
                'label' => 'ФИО Директора',
                'required' => true,
            ))
            ->add('setlogo', 'hidden', [
                'required' => false,
                'mapped' => false,
            ])
            ->add('dateRegistrateCompany', 'date', array(
                'required' => true,
                'widget' => 'single_text',
                'label' => 'Дата регистрации компании',
            ))
            ->add('lat', 'hidden', [
                'required' => false,
            ])
            ->add('lng', 'hidden', [
                'required' => false,
            ])
            ->add('formOrganization', 'entity', [
                'class' => 'AppBundle\Entity\OrgType',
//                'property' => 'username',
                'label' => 'Форма организации',
                'multiple' => false,
                'required' => true,
                'choice_label' => 'name',
            ])
            ->add('usercompany', new RegistrationUserType('AppBundle\Entity\User'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
//            'data_class' => 'CompanysBundle\Entity\Company',
//            'validation_groups' => array('aboutinfo'),
            'cascade_validation' => true,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }

    public function getName()
    {
        return $this->form_name;
    }
}