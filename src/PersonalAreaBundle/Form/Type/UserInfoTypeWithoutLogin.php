<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.16
 * Time: 18:21
 */
namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class UserInfoTypeWithoutLogin extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'form_contact_offer_user_without_login')
    {
        $this->form_name = $form_name;
    }

    public function getName()
    {
        return $this->form_name;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array(
                'attr' => array(
                    'placeholder' => 'your-email@inbox.ru',
                    'pattern' => '.{2,}' //minlength
                ),
                'required' => true,
                'trim' => true
            ))
            ->add('setavatar', 'hidden', array(
                'required' => false,
                'mapped' => false,
                'trim' => true
            ))
            ->add('firstname', 'text', array(
                'required' => true,
                'trim' => true
            ))
            ->add('lastname', 'text', array(
                'required' => true,
                'trim' => true
            ))
            ->add('phone', 'text', array(
                'required' => true,
                'trim' => true
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }
}