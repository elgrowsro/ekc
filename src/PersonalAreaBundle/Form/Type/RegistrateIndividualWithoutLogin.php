<?php
namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;

class RegistrateIndividualWithoutLogin extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'registrate_organization_individual_without_login')
    {
        $this->form_name = $form_name;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textAbout', 'textarea', array(
                'label' => 'Информация о себе',
                'required' => true,
            ))
            ->add('typeCompany', 'hidden', array(
                'required' => true,
                'label' => 'Тип организации'
            ))
            ->add('passport', 'textarea', array(
                'label' => 'Паспортные данные',
                'required' => true,
            ))
            ->add('phoneForAdmin', 'text', array(
                'label' => 'Номер телефона для администрации сайта',
                'required' => true,
            ))
            ->add('phoneForUsers', 'text', array(
                'label' => 'Номер телефона для клиентов организатора',
                'required' => true,
            ))
            ->add('setlogo', 'hidden', [
                'required' => false,
                'mapped' => false,
            ])
            ->add('lat', 'hidden', [
                'required' => false,
            ])
            ->add('lng', 'hidden', [
                'required' => false,
            ])
            ->add('documents', 'collection', array(
                // each entry in the array will be an "email" field
                'entry_type' => 'hidden',
                'mapped' => false,
                'label' => false,
//                'mapped' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ))
            ->add('usercompany', new RegistrationUserType('AppBundle\Entity\User'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => 'CompanysBundle\Entity\Company',
            'cascade_validation' => true,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_with_dropzone form',
                'novalidate' => 'novalidate',
                'enctype' => 'multipart/form-data'
            ]
        ));
    }

    public function getName()
    {
        return $this->form_name;
    }
}