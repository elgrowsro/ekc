<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.07.16
 * Time: 12:07
 */

namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;


class MakeMoneyType extends AbstractType
{
    protected $form_name;

    public function __construct($form_name = 'make_money')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('money', 'number', [
                'label' => 'Введите сумму',
                'required' => true,
                'constraints' => array(
                    new NotBlank([
                        'message'=> 'Необходимо ввести необходимую сумму'
                    ]),
                    new Range([
                        'min' => 1,
                        'minMessage' => 'Минимальная сумма пополнение - 1 рубль'
                    ]),
                ),
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
//            'data_class' => 'AppBundle\Entity\Comment',
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate',
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }
}