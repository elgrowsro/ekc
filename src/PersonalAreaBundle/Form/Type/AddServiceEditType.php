<?php

namespace PersonalAreaBundle\Form\Type;

use EventsBundle\Entity\AddService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use DateTime;

class AddServiceEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Название',
                'required' => false
            ])
            ->add('price', 'text', [
                'label' => 'Цена',
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventsBundle\Entity\AddService'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventsbundle_addservice';
    }
}
