<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.16
 * Time: 18:21
 */
namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CompanyEdit extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'form_contact_company_info')
    {
        $this->form_name = $form_name;
    }

    public function getName()
    {
        return $this->form_name;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logo', 'hidden', array(
                'required' => false,
                'mapped' => false,
                'trim' => true
            ))
            ->add('lat', 'hidden', array(
                'trim' => true
            ))
            ->add('lng', 'hidden', array(
                'trim' => true
            ))
            ->add('nameCompany', 'text', array(
                'required' => true,
                'trim' => true
            ))
            ->add('textAbout', 'textarea', array(
                'required' => true,
                'trim' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CompanysBundle\Entity\Company',
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }
}