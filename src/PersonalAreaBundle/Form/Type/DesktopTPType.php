<?php

namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DesktopTPType extends AbstractType
{
    protected $form_name;

    public function __construct($form_name = 'personalareabundle_desktop_tp')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type_select', 'choice', array(
                'required' => true,
                'label' => false,
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    '1' => 'Закрыть на дату и время',
                    '2' => 'Забронировать',
                ]
            ))
            ->add('cnt_reserv', 'integer', array(
                'required' => false,
                'label' => 'мест',
                'attr' => [
                    'min' => 0
                ]
            ))
            ->add('tp', 'entity_hidden', [
                'class' => 'EventsBundle\Entity\TimePrice'
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }
}
