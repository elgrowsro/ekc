<?php

namespace PersonalAreaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DeleteEventType extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'personalareabundle_delete_event')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'hidden', [
                'required' => true,
                'label' => false
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
//            'data_class' => 'EventsBundle\Entity\Event',
            'validation_groups' => ['delete_event'],
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
//                'novalidate' => 'novalidate'
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }

    public function setName($name)
    {
        $this->form_name = $name;
    }
}
