/**
 * Created by anton on 12.05.16.
 */
var map_create_company,
    placemark_create_company;


$(document).ready(function () {

    // Дождёмся загрузки API и готовности DOM.
    ymaps.ready(init);

    if ($("#documents_offer").length) {
        documents_upload_individual_load();
    }

    logo_user_upload('registrate_organization_form', 'registrate_organization_setlogo', 'logoCompany', 'logoCompany')
});

function init() {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    map_create_company = new ymaps.Map('map_create_company', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [56.838607, 60.605514], // Москва
        zoom: 12,
        controls: ['zoomControl']
    }, {
        searchControlProvider: 'yandex#search'
    });


    map_create_company.events.add('click', function (e) {
        var coords = e.get('coords');
        if ($("#registrate_organization_lat").length) {
            $("#registrate_organization_lat").val(coords[0]);
            $("#registrate_organization_lng").val(coords[1]);
        } else if ($("#registrate_organization_individual_lat").length) {
            $("#registrate_organization_individual_lat").val(coords[0]);
            $("#registrate_organization_individual_lng").val(coords[1]);
        }

        // Если метка уже создана – просто передвигаем ее
        if (placemark_create_company) {
            placemark_create_company.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            placemark_create_company = createPlacemark(coords);
            map_create_company.geoObjects.add(placemark_create_company);
            // Слушаем событие окончания перетаскивания на метке.
            placemark_create_company.events.add('dragend', function () {
                getAddress(placemark_create_company.geometry.getCoordinates());
            });
        }
        getAddress(coords, placemark_create_company);
    });
}