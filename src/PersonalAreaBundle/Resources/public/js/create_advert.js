/**
 * Created by anton on 01.09.16.
 */
$(document).ready(function () {
//Cтраница рекламы
    $("#select_advert_places").multipleSelect({
        selectAllText: 'Выбрать все',
        countSelected: 'Выбрано # из %',
        allSelected: 'Выбрано все',
        noMatchesFound: 'Совпадений не найдено',
        filter: true,
        multiple: true,
        onClick: function (view) {
            /*
             view.label: the text of the checkbox item
             view.checked: the checked of the checkbox item
             */
            calculate_advert_price()
        }
    });

    $("#advertbundle_advert_create_cntShow").on('keyup', function () {
        calculate_advert_price();
    })
});

function calculate_advert_price() {
    var $cnt_input = $("#advertbundle_advert_create_cntShow"),
        val_cnt = $cnt_input.val(),
        values_selected = $('#select_advert_places').multipleSelect('getSelects'),
        sum_price = 0;

    for (var val in values_selected) {
        sum_price += parseFloat(price_advert[values_selected[val]]);
    }

    sum_price *= val_cnt;

    $(".fullPriceAdvert").html(sum_price)
}