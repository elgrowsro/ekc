/**
 * Created by anton on 01.09.16.
 */
$(document).ready(function () {
//Cтраница рекламы
    $('.levels div ul li').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        if ($(this).hasClass('select_level_menu')) {
            var id_element = $(this).data('id');
            if ($(this).hasClass('active')) {
                add_select_level_menu(id_element);
            } else {
                del_select_level_menu(id_element);
            }
        }
        if ($(this).closest('ul').data('target')) {
            var target_id = $(this).closest('ul').data('target'),
                id_element = $(this).data('id');
            if ($(this).hasClass('active')) {
                $("#" + target_id + " li[data-parent=" + id_element + "]").show();
                if ($("li[data-parenthop=" + id_element + "]").length) {
                    $("li[data-parenthop=" + id_element + "]").show();
                }
            } else {
                $("#" + target_id + " li[data-parent=" + id_element + "]").hide();
                if ($("li[data-parenthop=" + id_element + "]").length) {
                    $("li[data-parenthop=" + id_element + "]").hide();
                }
            }
        }
    });

    $("#menu_select").on('click', '.removeThisBranch', function () {
        console.log('removeThisBranch')
        var $this = $(this),
            $parent = $this.parent(),
            val_parent = $parent.attr('value');

        $parent.remove();
        $('#level-3 .select_level_menu[data-id=' + val_parent + ']').removeClass('active')
    });
});


function add_select_level_menu(id) {
    $("#menu_select_hidden").append("<input type='hidden' name='select_menu[]' class='select_menu_input' value='" + id + "' >")
    var $level_3 = $("#level-3"),
        $level_3_li = $level_3.find('li[data-id=' + id + ']'),
        $level_3_li_text = $level_3_li.html(),
        $level_3_li_parent = $level_3_li.data('parent'),
        $level_3_li_parenthop = $level_3_li.data('parenthop'),
        $level_2 = $("#level-2"),
        $level_2_li = $level_2.find('li[data-id=' + $level_3_li_parent + ']'),
        $level_2_li_text = $level_2_li.html(),
        $level_1 = $("#level-1"),
        $level_1_li = $level_1.find('li[data-id=' + $level_3_li_parenthop + ']'),
        $level_1_li_text = $level_1_li.html();


    $("#menu_select_text").append(
        "<div class='select_menu_input' value='" + id + "' >" +
        "<div>" + $level_1_li_text + " </div>-" +
        "<div> " + $level_2_li_text + " </div>-" +
        "<div> " + $level_3_li_text + "</div>" +
        "<div class='removeThisBranch'></div>" +
        "</div>")
}

function del_select_level_menu(id) {
    $(".select_menu_input[value=" + id + "]").remove();
}
