var map_create_event_marker,
    map_create_event_path,
    placemark_create_event_marker;

$(document).ready(function () {
    if (typeof ymaps != 'undefined' && $("#map_create_event_marcer").length) {
        ymaps.ready(init_map_create_event);
    }

    logo_user_upload('eventsbundle_event_form', 'eventsbundle_event_main_image', 'imageEventShow', 'imageEvent')
});


function init_map_create_event() {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    map_create_event_marker = new ymaps.Map('map_create_event_marcer', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [56.838607, 60.605514], // Москва
        zoom: 12,
        controls: ['zoomControl']
    }, {
        searchControlProvider: 'yandex#search'
    });

    map_create_event_path = new ymaps.Map('map_create_event_path', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [56.838607, 60.605514], // Москва
        zoom: 12,
        controls: ['zoomControl']
    }, {
        searchControlProvider: 'yandex#search'
    });


    // Создаем ломаную.
    var myPolyline = new ymaps.Polyline([], {}, {
        // Задаем опции геообъекта.
        // Цвет с прозрачностью.
        strokeColor: "#00000088",
        // Ширину линии.
        strokeWidth: 4,
        // Максимально допустимое количество вершин в ломаной.
        editorMaxPoints: 25,
        // Добавляем в контекстное меню новый пункт, позволяющий удалить ломаную.
        //editorMenuManager: function (items) {
        //    items.push({
        //        title: "Удалить линию",
        //        onClick: function () {
        //            map_create_event_path.geoObjects.remove(myPolyline);
        //        }
        //    });
        //    return items;
        //}
    });

    // Добавляем линию на карту.
    map_create_event_path.geoObjects.add(myPolyline);


    new PolylineVertexCounter(myPolyline);
    // Включаем режим редактирования.
    myPolyline.editor.startEditing();
    myPolyline.editor.startDrawing();

    //map_create_event_path.events.add('click', function (e) {
    //    var coords = e.get('coords'),
    //        lat = coords[0],
    //        lng = coords[1];
    //
    //    myPolyline.geometry.getCoordinates().push(coords);
    //
    //    console.log('click', myPolyline.geometry.getCoordinates(), coords)
    //});
    //
    //
    //myPolyline.events.add('geometrychange', function (event) {
    //    console.log('geometrychange', myPolyline.geometry.getCoordinates());
    //});

    map_create_event_marker.events.add('click', function (e) {
        var coords = e.get('coords');
        if ($("#eventsbundle_event_lat").length) {
            $("#eventsbundle_event_lat").val(coords[0]);
            $("#eventsbundle_event_lng").val(coords[1]);
        }

        // Если метка уже создана – просто передвигаем ее
        if (placemark_create_event_marker) {
            placemark_create_event_marker.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            placemark_create_event_marker = createPlacemark(coords);
            map_create_event_marker.geoObjects.add(placemark_create_event_marker);
            // Слушаем событие окончания перетаскивания на метке.
            placemark_create_event_marker.events.add('dragend', function () {
                getAddress(placemark_create_event_marker.geometry.getCoordinates());
            });
        }
        getAddress(coords, placemark_create_event_marker);
    });
}
