var map_create_event_marker,
    map_create_event_path,
    placemark_edit_event_marker;

$(document).ready(function () {
    ymaps.ready(init_map_event);

    logo_user_upload('eventsbundle_event_edit_form', 'eventsbundle_event_edit_main_image', 'imageEventShow', 'imageEvent');

    if ($(".select_menu_input").length) {
        $(".select_menu_input").each(function (i, m) {
            var $this = $(m),
                val = $this.val();

            var menu3 = $("#level-3 li[data-id=" + val + "]"),
                parent = menu3.data('parent'),
                parenthop = menu3.data('parenthop'),
                menu3_text = menu3.html();

            menu3.show().addClass('active');

            var menu2 = $("#level-2 li[data-id=" + parent + "]"),
                menu2_text = menu2.html();
            menu2.show().addClass('active');

            var menu1 = $("#level-1 li[data-id=" + parenthop + "]"),
                menu1_text = menu1.html();
            menu1.addClass('active');

            $("#menu_select_text").append("<div class='select_menu_input' value='" + val + "' >" + "<div>" + menu1_text + " </div>-<div> " + menu2_text + " </div>-<div> " + menu3_text + "</div><div class='removeThisBranch'></div></div>");
        })
    }
    ;

    removeDatesEvent();

    $(".dz-remove-self-image").on('click', function () {
        var $this = $(this),
            id = $this.data('id');

        $("#delete_images_form_event").append("<input type='hidden' name='delete_image[]' value='" + id + "' />")
        $this.closest('.dz-preview').remove();
    })

    $(".dz-remove-self-video").on('click', function () {
        var $this = $(this),
            id = $this.data('id');

        $("#delete_videos_form_event").append("<input type='hidden' name='delete_video[]' value='" + id + "' />")
        $this.closest('.videos_view_one').remove();
    })
});


function init_map_event() {

    if ($("#eventsbundle_event_edit_lat").val()) {
        var lat = $("#eventsbundle_event_edit_lat").val(),
            lng = $("#eventsbundle_event_edit_lng").val();

        map_create_event_marker = new ymaps.Map('map_create_event_marcer', {
            // При инициализации карты обязательно нужно указать
            // её центр и коэффициент масштабирования.
            center: [lat, lng], // Москва
            zoom: 12,
            controls: ['zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        });


        placemark_edit_event_marker = createPlacemark([lat, lng]);

        map_create_event_marker.geoObjects.add(placemark_edit_event_marker);
        // Слушаем событие окончания перетаскивания на метке.
        placemark_edit_event_marker.events.add('dragend', function () {
            getAddress(placemark_edit_event_marker.geometry.getCoordinates(), placemark_edit_event_marker);
        });
        getAddress(placemark_edit_event_marker.geometry.getCoordinates(), placemark_edit_event_marker);
    } else {

        map_create_event_marker = new ymaps.Map('map_create_event_marcer', {
            // При инициализации карты обязательно нужно указать
            // её центр и коэффициент масштабирования.
            center: [56.838607, 60.605514], // Москва
            zoom: 12,
            controls: ['zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        });
    }

    map_create_event_path = new ymaps.Map('map_create_event_path', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [56.838607, 60.605514], // Москва
        zoom: 12,
        controls: ['zoomControl']
    }, {
        searchControlProvider: 'yandex#search'
    });


    var val_parse = $("#eventsbundle_event_edit_pathWay").val(),
        coord1,
        coord2,
        coord1_coord2,
        array_poins = [];

    if (val_parse) {
        val_parse = val_parse.substr(2, val_parse.length - 4).split('),(')
        for (var val1 in val_parse) {
            coord1_coord2 = val_parse[val1].split(',');
            coord1 = parseFloat(coord1_coord2[0]);
            coord2 = parseFloat(coord1_coord2[1]);

            array_poins.push([coord1, coord2]);
        }
    }

    // Создаем ломаную.
    var myPolyline = new ymaps.Polyline(array_poins, {}, {
        // Задаем опции геообъекта.
        // Цвет с прозрачностью.
        strokeColor: "#00000088",
        // Ширину линии.
        strokeWidth: 4,
        // Максимально допустимое количество вершин в ломаной.
        editorMaxPoints: 25,
        // Добавляем в контекстное меню новый пункт, позволяющий удалить ломаную.
        //editorMenuManager: function (items) {
        //    items.push({
        //        title: "Удалить линию",
        //        onClick: function () {
        //            map_create_event_path.geoObjects.remove(myPolyline);
        //        }
        //    });
        //    return items;
        //}
    });

    // Добавляем линию на карту.
    map_create_event_path.geoObjects.add(myPolyline);


    new PolylineVertexCounter(myPolyline);
    // Включаем режим редактирования.
    myPolyline.editor.startEditing();
    myPolyline.editor.startDrawing();


    map_create_event_marker.events.add('click', function (e) {
        var coords = e.get('coords');
        if ($("#eventsbundle_event_lat").length) {
            $("#eventsbundle_event_lat").val(coords[0]);
            $("#eventsbundle_event_lng").val(coords[1]);
        }

        // Если метка уже создана – просто передвигаем ее
        if (placemark_edit_event_marker) {
            placemark_edit_event_marker.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            placemark_edit_event_marker = createPlacemark(coords);
            map_create_event_marker.geoObjects.add(placemark_edit_event_marker);
            // Слушаем событие окончания перетаскивания на метке.
            placemark_edit_event_marker.events.add('dragend', function () {
                getAddress(placemark_edit_event_marker.geometry.getCoordinates(), placemark_edit_event_marker);
            });
        }
        getAddress(coords, placemark_edit_event_marker);
    });
}
