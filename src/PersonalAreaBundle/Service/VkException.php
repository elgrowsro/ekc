<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 12.08.16
 * Time: 16:12
 */

namespace PersonalAreaBundle\Service;


// Возвращаемые ошибки https://vk.com/dev/errors

class VkException extends \Exception
{
    /**
     * @var $massage string
     * @var $code string
     * @var $mail string
     */
    public function __construct($message, $code, $mail)
    {
        mail($mail, "Ошибка '{$code}' отправки сообщения вк", print_r($message, true));
        return;
    }
}