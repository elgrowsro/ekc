<?php

namespace AppBundle\Entity;

/**
 * VKRelUser
 */
class VKRelUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateSendRequest;

    /**
     * @var string
     */
    private $code;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateSendRequest
     *
     * @param \DateTime $dateSendRequest
     *
     * @return VKRelUser
     */
    public function setDateSendRequest($dateSendRequest)
    {
        $this->dateSendRequest = $dateSendRequest;

        return $this;
    }

    /**
     * Get dateSendRequest
     *
     * @return \DateTime
     */
    public function getDateSendRequest()
    {
        return $this->dateSendRequest;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return VKRelUser
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\VKUsers
     */
    private $vkusers;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return VKRelUser
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set vkusers
     *
     * @param \AppBundle\Entity\VKUsers $vkusers
     *
     * @return VKRelUser
     */
    public function setVkusers(\AppBundle\Entity\VKUsers $vkusers = null)
    {
        $this->vkusers = $vkusers;

        return $this;
    }

    /**
     * Get vkusers
     *
     * @return \AppBundle\Entity\VKUsers
     */
    public function getVkusers()
    {
        return $this->vkusers;
    }
}
