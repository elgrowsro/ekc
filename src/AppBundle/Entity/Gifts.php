<?php

namespace AppBundle\Entity;

/**
 * Gifts
 */
class Gifts
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sum;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sum
     *
     * @param string $sum
     *
     * @return Gifts
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Gifts
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Gifts
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \EventsBundle\Entity\Event
     */
    private $event;

    /**
     * @var \EventsBundle\Entity\TimePrice
     */
    private $tp;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Gifts
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return Gifts
     */
    public function setEvent(\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \EventsBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set tp
     *
     * @param \EventsBundle\Entity\TimePrice $tp
     *
     * @return Gifts
     */
    public function setTp(\EventsBundle\Entity\TimePrice $tp = null)
    {
        $this->tp = $tp;

        return $this;
    }

    /**
     * Get tp
     *
     * @return \EventsBundle\Entity\TimePrice
     */
    public function getTp()
    {
        return $this->tp;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $userMake;


    /**
     * Set userMake
     *
     * @param \AppBundle\Entity\User $userMake
     *
     * @return Gifts
     */
    public function setUserMake(\AppBundle\Entity\User $userMake = null)
    {
        $this->userMake = $userMake;

        return $this;
    }

    /**
     * Get userMake
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserMake()
    {
        return $this->userMake;
    }
    /**
     * @var \EventsBundle\Entity\Ticket
     */
    private $ticket;


    /**
     * Set ticket
     *
     * @param \EventsBundle\Entity\Ticket $ticket
     *
     * @return Gifts
     */
    public function setTicket(\EventsBundle\Entity\Ticket $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return \EventsBundle\Entity\Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }
    /**
     * @var boolean
     */
    private $get = false;


    /**
     * Set get
     *
     * @param boolean $get
     *
     * @return Gifts
     */
    public function setGet($get)
    {
        $this->get = $get;

        return $this;
    }

    /**
     * Get get
     *
     * @return boolean
     */
    public function getGet()
    {
        return $this->get;
    }
}
