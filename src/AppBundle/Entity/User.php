<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\AttributeOverride;

class User extends BaseUser
{
    private $avatar;
    private $money = 0;
    private $withdrawMoney = 0;
    private $tickets;
    private $url;
    private $rl = "";
    private $referral_link;
    private $company;
    private $ulogin;
    private $generate_password;
    private $comments_user;
    private $question_user;
    private $myReferral;
    private $referralsWithMe;
    private $conclusionMoney;
    private $lastUpdateVK;
    private $confirmVK;
    private $userRelVK;
    private $donations;
    private $charitys;
    private $adverts;
    private $gifts;
    private $giftMake;

    public function setPhone($phone)
    {

        preg_match_all('/\d+/', $phone, $matches_phone);

        $implode_phone = implode('', $matches_phone[0]);

        parent::setPhone($implode_phone);
    }

    public function setEmail($email)
    {
        parent::setEmail($email);
        parent::setUsername($email);
    }

    /**
     * Set company
     *
     * @param \CompanysBundle\Entity\Company $company
     *
     * @return User
     */
    public function setCompany(\CompanysBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanysBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    public function setGeneratePassword($gen_pas)
    {
        $this->generate_password = $gen_pas;
        return $this;
    }

    public function getGeneratePassword()
    {
        return $this->generate_password;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        $this->setGeneratePassword($password);

        return $this;
    }

    /**
     * Add ulogin
     *
     * @param \Ulogin\AuthBundle\Entity\UloginUser $ulogin
     *
     * @return User
     */
    public function addUlogin(\Ulogin\AuthBundle\Entity\UloginUser $ulogin)
    {
        $this->ulogin[] = $ulogin;

        return $this;
    }

    /**
     * Remove ulogin
     *
     * @param \Ulogin\AuthBundle\Entity\UloginUser $ulogin
     */
    public function removeUlogin(\Ulogin\AuthBundle\Entity\UloginUser $ulogin)
    {
        $this->ulogin->removeElement($ulogin);
    }

    /**
     * Get ulogin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUlogin()
    {
        return $this->ulogin;
    }

    /**
     * Set avatar
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $avatar
     *
     * @return User
     */
    public function setAvatar(\Application\Sonata\MediaBundle\Entity\Media $avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $imagesUploads;


    /**
     * Add imagesUpload
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $imagesUpload
     *
     * @return User
     */
    public function addImagesUpload(\Application\Sonata\MediaBundle\Entity\Media $imagesUpload)
    {
        $this->imagesUploads[] = $imagesUpload;

        return $this;
    }

    /**
     * Remove imagesUpload
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $imagesUpload
     */
    public function removeImagesUpload(\Application\Sonata\MediaBundle\Entity\Media $imagesUpload)
    {
        $this->imagesUploads->removeElement($imagesUpload);
    }

    /**
     * Get imagesUploads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagesUploads()
    {
        return $this->imagesUploads;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return User
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add commentsUser
     *
     * @param \AppBundle\Entity\Comment $commentsUser
     *
     * @return User
     */
    public function addCommentsUser(\AppBundle\Entity\Comment $commentsUser)
    {
        $this->comments_user[] = $commentsUser;

        return $this;
    }

    /**
     * Remove commentsUser
     *
     * @param \AppBundle\Entity\Comment $commentsUser
     */
    public function removeCommentsUser(\AppBundle\Entity\Comment $commentsUser)
    {
        $this->comments_user->removeElement($commentsUser);
    }

    /**
     * Get commentsUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentsUser()
    {
        return $this->comments_user;
    }

    /**
     * Add questionUser
     *
     * @param \EventsBundle\Entity\Question $questionUser
     *
     * @return User
     */
    public function addQuestionUser(\EventsBundle\Entity\Question $questionUser)
    {
        $this->question_user[] = $questionUser;

        return $this;
    }

    /**
     * Remove questionUser
     *
     * @param \EventsBundle\Entity\Question $questionUser
     */
    public function removeQuestionUser(\EventsBundle\Entity\Question $questionUser)
    {
        $this->question_user->removeElement($questionUser);
    }

    /**
     * Get questionUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionUser()
    {
        return $this->question_user;
    }

    /**
     * Add ticket
     *
     * @param \EventsBundle\Entity\Ticket $ticket
     *
     * @return User
     */
    public function addTicket(\EventsBundle\Entity\Ticket $ticket)
    {
        $this->tickets[] = $ticket;

        return $this;
    }

    /**
     * Remove ticket
     *
     * @param \EventsBundle\Entity\Ticket $ticket
     */
    public function removeTicket(\EventsBundle\Entity\Ticket $ticket)
    {
        $this->tickets->removeElement($ticket);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Set money
     *
     * @param integer $money
     *
     * @return User
     */
    public function setMoney($money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * Get money
     *
     * @return integer
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * Set withdrawMoney
     *
     * @param integer $withdrawMoney
     *
     * @return User
     */
    public function setWithdrawMoney($withdrawMoney)
    {
        $this->withdrawMoney = $withdrawMoney;

        return $this;
    }

    /**
     * Get withdrawMoney
     *
     * @return integer
     */
    public function getWithdrawMoney()
    {
        return $this->withdrawMoney;
    }

    /**
     * Set rl
     *
     * @param string $rl
     *
     * @return User
     */
    public function setRl($rl)
    {
        $this->rl = $rl;

        return $this;
    }

    /**
     * Get rl
     *
     * @return string
     */
    public function getRl()
    {
        return $this->rl;
    }

    /**
     * Set referralLink
     *
     * @param string $referralLink
     *
     * @return User
     */
    public function setReferralLink($referralLink)
    {
        $this->referral_link = $referralLink;

        return $this;
    }

    /**
     * Get referralLink
     *
     * @return string
     */
    public function getReferralLink()
    {
        return $this->referral_link;
    }

    /**
     * Add myReferral
     *
     * @param \AppBundle\Entity\Referrals $myReferral
     *
     * @return User
     */
    public function addMyReferral(\AppBundle\Entity\Referrals $myReferral)
    {
        $this->myReferral[] = $myReferral;

        return $this;
    }

    /**
     * Remove myReferral
     *
     * @param \AppBundle\Entity\Referrals $myReferral
     */
    public function removeMyReferral(\AppBundle\Entity\Referrals $myReferral)
    {
        $this->myReferral->removeElement($myReferral);
    }

    /**
     * Get myReferral
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyReferral()
    {
        return $this->myReferral;
    }

    /**
     * Add referralsWithMe
     *
     * @param \AppBundle\Entity\Referrals $referralsWithMe
     *
     * @return User
     */
    public function addReferralsWithMe(\AppBundle\Entity\Referrals $referralsWithMe)
    {
        $this->referralsWithMe[] = $referralsWithMe;

        return $this;
    }

    /**
     * Remove referralsWithMe
     *
     * @param \AppBundle\Entity\Referrals $referralsWithMe
     */
    public function removeReferralsWithMe(\AppBundle\Entity\Referrals $referralsWithMe)
    {
        $this->referralsWithMe->removeElement($referralsWithMe);
    }

    /**
     * Get referralsWithMe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferralsWithMe()
    {
        return $this->referralsWithMe;
    }

    /**
     * Add confirmVK
     *
     * @param \AppBundle\Entity\VKUsers $confirmVK
     *
     * @return User
     */
    public function addConfirmVK(\AppBundle\Entity\VKUsers $confirmVK)
    {
        $this->confirmVK[] = $confirmVK;

        return $this;
    }

    /**
     * Remove confirmVK
     *
     * @param \AppBundle\Entity\VKUsers $confirmVK
     */
    public function removeConfirmVK(\AppBundle\Entity\VKUsers $confirmVK)
    {
        $this->confirmVK->removeElement($confirmVK);
    }

    /**
     * Get confirmVK
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfirmVK()
    {
        return $this->confirmVK;
    }


    /**
     * Set lastUpdateVK
     *
     * @param \DateTime $lastUpdateVK
     *
     * @return User
     */
    public function setLastUpdateVK($lastUpdateVK)
    {
        $this->lastUpdateVK = $lastUpdateVK;

        return $this;
    }

    /**
     * Get lastUpdateVK
     *
     * @return \DateTime
     */
    public function getLastUpdateVK()
    {
        return $this->lastUpdateVK;
    }

    /**
     * Add userRelVK
     *
     * @param \AppBundle\Entity\VKRelUser $userRelVK
     *
     * @return User
     */
    public function addUserRelVK(\AppBundle\Entity\VKRelUser $userRelVK)
    {
        $this->userRelVK[] = $userRelVK;

        return $this;
    }

    /**
     * Remove userRelVK
     *
     * @param \AppBundle\Entity\VKRelUser $userRelVK
     */
    public function removeUserRelVK(\AppBundle\Entity\VKRelUser $userRelVK)
    {
        $this->userRelVK->removeElement($userRelVK);
    }

    /**
     * Get userRelVK
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRelVK()
    {
        return $this->userRelVK;
    }

    public function getFLName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    /**
     * Set conclusionMoney
     *
     * @param \DateTime $conclusionMoney
     *
     * @return User
     */
    public function setConclusionMoney($conclusionMoney)
    {
        $this->conclusionMoney = $conclusionMoney;

        return $this;
    }

    /**
     * Get conclusionMoney
     *
     * @return \DateTime
     */
    public function getConclusionMoney()
    {
        return $this->conclusionMoney;
    }

    /**
     * Add donation
     *
     * @param \EventsBundle\Entity\Donations $donation
     *
     * @return User
     */
    public function addDonation(\EventsBundle\Entity\Donations $donation)
    {
        $this->donations[] = $donation;

        return $this;
    }

    /**
     * Remove donation
     *
     * @param \EventsBundle\Entity\Donations $donation
     */
    public function removeDonation(\EventsBundle\Entity\Donations $donation)
    {
        $this->donations->removeElement($donation);
    }

    /**
     * Get donations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDonations()
    {
        return $this->donations;
    }

    /**
     * Add charity
     *
     * @param \AppBundle\Entity\Charity $charity
     *
     * @return User
     */
    public function addCharity(\AppBundle\Entity\Charity $charity)
    {
        $this->charitys[] = $charity;

        return $this;
    }

    /**
     * Remove charity
     *
     * @param \AppBundle\Entity\Charity $charity
     */
    public function removeCharity(\AppBundle\Entity\Charity $charity)
    {
        $this->charitys->removeElement($charity);
    }

    /**
     * Get charitys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharitys()
    {
        return $this->charitys;
    }

    /**
     * Add advert
     *
     * @param \AdvertBundle\Entity\Advert $advert
     *
     * @return User
     */
    public function addAdvert(\AdvertBundle\Entity\Advert $advert)
    {
        $this->adverts[] = $advert;

        return $this;
    }

    /**
     * Remove advert
     *
     * @param \AdvertBundle\Entity\Advert $advert
     */
    public function removeAdvert(\AdvertBundle\Entity\Advert $advert)
    {
        $this->adverts->removeElement($advert);
    }

    /**
     * Get adverts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdverts()
    {
        return $this->adverts;
    }

    /**
     * Add gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     *
     * @return User
     */
    public function addGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts[] = $gift;

        return $this;
    }

    /**
     * Remove gift
     *
     * @param \AppBundle\Entity\Gifts $gift
     */
    public function removeGift(\AppBundle\Entity\Gifts $gift)
    {
        $this->gifts->removeElement($gift);
    }

    /**
     * Get gifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGifts()
    {
        return $this->gifts;
    }

    /**
     * Add giftMake
     *
     * @param \AppBundle\Entity\Gifts $giftMake
     *
     * @return User
     */
    public function addGiftMake(\AppBundle\Entity\Gifts $giftMake)
    {
        $this->giftMake[] = $giftMake;

        return $this;
    }

    /**
     * Remove giftMake
     *
     * @param \AppBundle\Entity\Gifts $giftMake
     */
    public function removeGiftMake(\AppBundle\Entity\Gifts $giftMake)
    {
        $this->giftMake->removeElement($giftMake);
    }

    /**
     * Get giftMake
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGiftMake()
    {
        return $this->giftMake;
    }
}
