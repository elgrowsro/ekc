<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 09.06.16
 * Time: 13:17
 */

namespace AppBundle\Entity;


class Menu
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $lft;

    /**
     * @var integer
     */
    private $rgt;

    /**
     * @var integer
     */
    private $root;

    /**
     * @var integer
     */
    private $lvl;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\Menu
     */
    private $parent;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Menu
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Menu
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return Menu
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Menu
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Menu $child
     *
     * @return Menu
     */
    public function addChild(\AppBundle\Entity\Menu $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Menu $child
     */
    public function removeChild(\AppBundle\Entity\Menu $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Menu $parent
     *
     * @return Menu
     */
    public function setParent(\AppBundle\Entity\Menu $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Menu
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $imageBG;


    /**
     * Set imageBG
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageBG
     *
     * @return Menu
     */
    public function setImageBG(\Application\Sonata\MediaBundle\Entity\Media $imageBG = null)
    {
        $this->imageBG = $imageBG;

        return $this;
    }

    /**
     * Get imageBG
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImageBG()
    {
        return $this->imageBG;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getTitle();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $adplaces;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adplaces = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add adplace
     *
     * @param \AdvertBundle\Entity\Places $adplace
     *
     * @return Menu
     */
    public function addAdplace(\AdvertBundle\Entity\Places $adplace)
    {
        $this->adplaces[] = $adplace;

        return $this;
    }

    /**
     * Remove adplace
     *
     * @param \AdvertBundle\Entity\Places $adplace
     */
    public function removeAdplace(\AdvertBundle\Entity\Places $adplace)
    {
        $this->adplaces->removeElement($adplace);
    }

    /**
     * Get adplaces
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdplaces()
    {
        return $this->adplaces;
    }
}
