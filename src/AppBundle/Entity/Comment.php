<?php

namespace AppBundle\Entity;

/**
 * Comment
 */
class Comment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;

    /**
     * @var boolean
     */
    private $isActive = 1;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Comment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Comment
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Comment
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Comment
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return (boolean)$this->isActive;
    }
    /**
     * @var \EventsBundle\Entity\Event
     */
    private $event;

    /**
     * @var \CompanysBundle\Entity\Company
     */
    private $company;


    /**
     * Set event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return Comment
     */
    public function setEvent(\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \EventsBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set company
     *
     * @param \CompanysBundle\Entity\Company $company
     *
     * @return Comment
     */
    public function setCompany(\CompanysBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanysBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Comment
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var integer
     */
    private $rating_quality = 0;

    /**
     * @var integer
     */
    private $rating_saturation = 0;

    /**
     * @var integer
     */
    private $rating_interestingness = 0;


    /**
     * Set ratingQuality
     *
     * @param integer $ratingQuality
     *
     * @return Comment
     */
    public function setRatingQuality($ratingQuality)
    {
        $this->rating_quality = $ratingQuality;

        return $this;
    }

    /**
     * Get ratingQuality
     *
     * @return integer
     */
    public function getRatingQuality()
    {
        return $this->rating_quality;
    }

    /**
     * Set ratingSaturation
     *
     * @param integer $ratingSaturation
     *
     * @return Comment
     */
    public function setRatingSaturation($ratingSaturation)
    {
        $this->rating_saturation = $ratingSaturation;

        return $this;
    }

    /**
     * Get ratingSaturation
     *
     * @return integer
     */
    public function getRatingSaturation()
    {
        return $this->rating_saturation;
    }

    /**
     * Set ratingInterestingness
     *
     * @param integer $ratingInterestingness
     *
     * @return Comment
     */
    public function setRatingInterestingness($ratingInterestingness)
    {
        $this->rating_interestingness = $ratingInterestingness;

        return $this;
    }

    /**
     * Get ratingInterestingness
     *
     * @return integer
     */
    public function getRatingInterestingness()
    {
        return $this->rating_interestingness;
    }
    /**
     * @var \FeedBundle\Entity\Feed
     */
    private $feed;


    /**
     * Set feed
     *
     * @param \FeedBundle\Entity\Feed $feed
     *
     * @return Comment
     */
    public function setFeed(\FeedBundle\Entity\Feed $feed = null)
    {
        $this->feed = $feed;

        return $this;
    }

    /**
     * Get feed
     *
     * @return \FeedBundle\Entity\Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return (string)$this->getId();
    }
    /**
     * @var \Application\Sonata\MediaBundle\Entity\Gallery
     */
    private $images;


    /**
     * Set images
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $images
     *
     * @return Comment
     */
    public function setImages(\Application\Sonata\MediaBundle\Entity\Gallery $images = null)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery
     */
    public function getImages()
    {
        return $this->images;
    }
    /**
     * @var boolean
     */
    private $requestRemove = 0;


    /**
     * Set requestRemove
     *
     * @param boolean $requestRemove
     *
     * @return Comment
     */
    public function setRequestRemove($requestRemove)
    {
        $this->requestRemove = $requestRemove;

        return $this;
    }

    /**
     * Get requestRemove
     *
     * @return boolean
     */
    public function getRequestRemove()
    {
        return $this->requestRemove;
    }
}
