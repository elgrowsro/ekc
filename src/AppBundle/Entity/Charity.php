<?php

namespace AppBundle\Entity;

/**
 * Charity
 */
class Charity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sum;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sum
     *
     * @param string $sum
     *
     * @return Charity
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Charity
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Charity
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }
    /**
     * @var \EventsBundle\Entity\TimePrice
     */
    private $timeprice;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Set timeprice
     *
     * @param \EventsBundle\Entity\TimePrice $timeprice
     *
     * @return Charity
     */
    public function setTimeprice(\EventsBundle\Entity\TimePrice $timeprice = null)
    {
        $this->timeprice = $timeprice;

        return $this;
    }

    /**
     * Get timeprice
     *
     * @return \EventsBundle\Entity\TimePrice
     */
    public function getTimeprice()
    {
        return $this->timeprice;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Charity
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
