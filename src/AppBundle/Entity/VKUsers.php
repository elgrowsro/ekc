<?php

namespace AppBundle\Entity;

/**
 * VKUsers
 */
class VKUsers
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vkId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $photo100;

    /**
     * @var boolean
     */
    private $sendRequest = false;

    /**
     * @var boolean
     */
    private $confirmedSuccess = false;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vkRelUser;

    /**
     * @var \AppBundle\Entity\User
     */
    private $confirmUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vkRelUser = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vkId
     *
     * @param integer $vkId
     *
     * @return VKUsers
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;

        return $this;
    }

    /**
     * Get vkId
     *
     * @return integer
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return VKUsers
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return VKUsers
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set photo100
     *
     * @param string $photo100
     *
     * @return VKUsers
     */
    public function setPhoto100($photo100)
    {
        $this->photo100 = $photo100;

        return $this;
    }

    /**
     * Get photo100
     *
     * @return string
     */
    public function getPhoto100()
    {
        return $this->photo100;
    }

    /**
     * Set sendRequest
     *
     * @param boolean $sendRequest
     *
     * @return VKUsers
     */
    public function setSendRequest($sendRequest)
    {
        $this->sendRequest = $sendRequest;

        return $this;
    }

    /**
     * Get sendRequest
     *
     * @return boolean
     */
    public function getSendRequest()
    {
        return $this->sendRequest;
    }

    /**
     * Set confirmedSuccess
     *
     * @param boolean $confirmedSuccess
     *
     * @return VKUsers
     */
    public function setConfirmedSuccess($confirmedSuccess)
    {
        $this->confirmedSuccess = $confirmedSuccess;

        return $this;
    }

    /**
     * Get confirmedSuccess
     *
     * @return boolean
     */
    public function getConfirmedSuccess()
    {
        return $this->confirmedSuccess;
    }

    /**
     * Add vkRelUser
     *
     * @param \AppBundle\Entity\VKRelUser $vkRelUser
     *
     * @return VKUsers
     */
    public function addVkRelUser(\AppBundle\Entity\VKRelUser $vkRelUser)
    {
        $this->vkRelUser[] = $vkRelUser;

        return $this;
    }

    /**
     * Remove vkRelUser
     *
     * @param \AppBundle\Entity\VKRelUser $vkRelUser
     */
    public function removeVkRelUser(\AppBundle\Entity\VKRelUser $vkRelUser)
    {
        $this->vkRelUser->removeElement($vkRelUser);
    }

    /**
     * Get vkRelUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVkRelUser()
    {
        return $this->vkRelUser;
    }

    /**
     * Set confirmUser
     *
     * @param \AppBundle\Entity\User $confirmUser
     *
     * @return VKUsers
     */
    public function setConfirmUser(\AppBundle\Entity\User $confirmUser = null)
    {
        $this->confirmUser = $confirmUser;

        return $this;
    }

    /**
     * Get confirmUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getConfirmUser()
    {
        return $this->confirmUser;
    }

    public function getFLName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }
}
