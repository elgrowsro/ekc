<?php

namespace AppBundle\Entity;

/**
 * Referrals
 */
class Referrals
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $summ = 0;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\User
     */
    private $userReferral;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summ
     *
     * @param float $summ
     *
     * @return Referrals
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;

        return $this;
    }

    /**
     * Get summ
     *
     * @return float
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Referrals
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userReferral
     *
     * @param \AppBundle\Entity\User $userReferral
     *
     * @return Referrals
     */
    public function setUserReferral(\AppBundle\Entity\User $userReferral = null)
    {
        $this->userReferral = $userReferral;

        return $this;
    }

    /**
     * Get userReferral
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserReferral()
    {
        return $this->userReferral;
    }
}
