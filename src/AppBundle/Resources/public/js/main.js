/*
 * Third party
 */

$('.menu li.parent a').click(function () {
    $(this).parent().toggleClass('active');
    if ($(this).parent().hasClass('city-select')) {
        $('.cityModal').toggle();
    }
});

$('#mobileClicker').click(function () {
    $('.menu').slideToggle();
});
$('#cityIsRight').click(function () {
    $('.cityModal').hide();
    $('.menu li.parent').removeClass('active');
});
$('#sellectCity').click(function () {
    $('.cityModal').hide();
    $('.menu li.parent').removeClass('active');
    $('.citySelectModal').show();
    $('html').addClass('fancybox-lock');
});
$('.citySelectModal .close').click(function () {
    $('.citySelectModal').hide();
    $('html').removeClass('fancybox-lock');
});
$('.leftColumn #arrowSlide').click(function () {
    var fullWidth = $(window).width();
    if ($(this).parents('.leftColumn').hasClass('active')) {
        if (fullWidth < 700) {
            $('.leftColumn').removeClass('active');
        }
    } else {
        if (fullWidth < 700) {
            $(this).parents('.leftColumn').addClass('active');
        }
    }
});
//Просмотр фотографий мероприятия
$('#Container > div').click(function () {
    var src = $(this).attr('style');
    src = src.split('background: url("');
    src = src[1].split('")');
    $('.imageView').show().addClass('nowViewsPhotoBlock');
    $('.imageView img').attr('src', src[0]);
    $('.viewsAll').addClass('nowViewsPhoto');
    $(this).addClass('active');
});
$('#showMorePhoto').click(function () {
    $('.viewsAll').addClass('showedAll');
    $('body').addClass('bodyHidden');
});
$('#showMoreVideo').click(function () {
    $('.viewsVideoAll').addClass('showedAll');
    $('body').addClass('bodyHidden');
});
$('.viewsVideoAll .close').click(function () {
    $('.viewsVideoAll').removeClass('showedAll');
    $('body').removeClass('bodyHidden');
});
$('.viewsAll .close').click(function () {
    $('.viewsAll').removeClass('showedAll');
    $('body').removeClass('bodyHidden');
});

//Рефералка
$('.buttonsReferal .change_referral_view_list').click(function () {
    var id = $(this).attr('id');
    $('.buttonsReferal .change_referral_view_list').removeClass('active');
    $('.referalUsers').hide();
    $('.' + id).show();
    $(this).addClass('active');
});
//Табы с картой
$('.mapTabs .navTab div').click(function () {
    var id = $(this).attr('id');
    $('.navTab div').removeClass('active');
    $('.panels>div').hide();
    $('.' + id).show();
    $(this).addClass('active');
});
//Меню категорий на главной
//$('.indexmenu .parent > a').click(function () {
//    if ($(this).hasClass('active')) {
//        $('.parent>ul').slideUp().siblings('a').removeClass('active');
//    } else {
//        $('.parent>ul').slideUp().siblings('a').removeClass('active');
//        $(this).toggleClass('active').parent().find('ul').slideToggle();
//    }
//});
var id2;
if ($(".tableEvent").length) {
    var max_index = 0;
    $(".tableEvent").each(function (i, m) {
        if ($(m).data('index') && (max_index < $(m).data('index'))) {
            max_index = $(m).data('index');
        }
    });
    id2 = max_index;
} else {
    id2 = 0;
}
var form_name_event_edit_or_add = "";

if ($("#eventsbundle_event_edit_form").length) {
    form_name_event_edit_or_add = "eventsbundle_event_edit";
} else {
    form_name_event_edit_or_add = "eventsbundle_event";
}

$('#addEvent').click(function () {
    id2++;
    var htmlDopTable =
        '<div class="tableEvent eventTimes"  id="timeprice_block_' + id2 + '">' +
        '<div class="remove"><i></i>Удалить</div>' +
        '<div class="colorLineIm"></div>' +
        '<div class="inner">' +

        '<div class="input timeEnd box-label-input">' +
        '<label for="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_date_event" class="required">Дата начала мероприятия</label>' +
        '<input type="text" id="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_date_event" name="' + form_name_event_edit_or_add + '[getTimePrice][' + id2 + '][date_event]" required="required" class="pickadate">' +
        '</div>' +
        '<div class="input timeStart box-label-input">' +
        '<label for="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_timefrom" class="required">Время начала</label>' +
        '<input type="text" id="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_timefrom" name="' + form_name_event_edit_or_add + '[getTimePrice][' + id2 + '][timefrom]" required="required" class="time">' +
        '</div>' +
        '<div class="input timeEnd box-label-input">' +
        '<label for="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_timeto" class="required">Время окончания</label>' +
        '<input type="text" id="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_timeto" name="' + form_name_event_edit_or_add + '[getTimePrice][' + id2 + '][timeto]" required="required" class="time">' +
        '</div>' +
        '<div class="input price">' +
        '<label for="priceEvent' + id2 + '">Цена</label>' +
        '<input type="text" id="price' + id2 + '">' +
        '<span>Р</span>' +
        '</div>' +
        '<div class="input price box-label-input">' +
        '<label for="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_places" class="required">Количество мест</label>' +
        '<input type="number" id="' + form_name_event_edit_or_add + '_getTimePrice_' + id2 + '_places" name="' + form_name_event_edit_or_add + '[getTimePrice][' + id2 + '][places]" required="required">' +
        '</div>' +
        '<div class="radiobox">' +
        '<input checked="checked" id="date1ID' + id2 + '" type="radio" name="dateEvent[' + id2 + ']" value="1">' +
        '<label for="date1ID' + id2 + '"><i class="t2"></i>Только эта дата и время</label>' +
        '</div>' +
        '<div class="radiobox">' +
        '<input id="date2ID' + id2 + '" type="radio" name="dateEvent[' + id2 + ']" value="2">' +
        '<label for="date2ID' + id2 + '"><i class="t2"></i>Каждый день недели в данное время до <input type="text" name="date_extend_2[' + id2 + ']" class="pickadate"/> <span>включительно</span></label>' +
        '</div>' +
            //'<div class="radiobox">' +
            //'<input id="date3ID' + id2 + '" type="radio" name="dateEvent[' + id2 + ']" value="3">' +
            //'<label for="date3ID' + id2 + '"><i class="t2"></i>Каждый день недели в данное время с выбранной даты <input type="text" name="date_extend_3[' + id2 + ']" class="pickadate"/> <span>включительно</span></label>' +
            //'</div>' +
        '<div class="radiobox">' +
        '<input id="date4ID' + id2 + '" type="radio" name="dateEvent[' + id2 + ']" value="4">' +
        '<label for="date4ID' + id2 + '"><i class="t2"></i>Каждый будний день недели до <input type="text" name="date_extend_4[' + id2 + ']" class="pickadate"/> <span>включительно</span></label>' +
        '</div>' +
        '<div class="radiobox">' +
        '<input id="date5ID' + id2 + '" type="radio" name="dateEvent[' + id2 + ']" value="5">' +
        '<label for="date5ID' + id2 + '"><i class="t2"></i>Каждый выходной до <input type="text" name="date_extend_5[' + id2 + ']" class="pickadate"/> <span>включительно</span></label>' +
        '</div>' +
        '<div class="input box-label-input">' +
        '<div id="error_date_event_' + id2 + '"></div>' +
        '</div>' +
        '</div>';
    $('#addEvent').before(htmlDopTable);
    removeDatesEvent();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;
    $("#timeprice_block_" + id2 + " .pickadate").datetimepicker({
        timepicker: false,
        mask: '99.99.9999',
        format: 'd.m.Y',
        minDate: today, //or 1986/12/08
        validateOnBlur: true,
    });

    $("#timeprice_block_" + id2 + " .time").datetimepicker({
        datepicker: false,
        mask: '99:99',
        format: 'H:i',
        step: 60
    });
});
function removeDatesEvent() {
    $('.eventTimes .remove').click(function () {
        $(this).parents('.eventTimes').remove();
    });
}
$(window).scroll(function () {
    $('.period_picker_box').removeClass('active').removeClass('visible').attr('unselectable', 'off');
});
//Навигация фотографий
//$('.slideRight').click(function () {
//    if ($('#Container div').last().hasClass('active')) {
//        $('#Container div').removeClass('active');
//        $('#Container div').eq(1).addClass('active');
//        var src = $('#Container div').eq(1).attr('style');
//    } else {
//        var src = $('#Container .active').next().addClass('active').attr('style');
//        $('#Container .active').first().removeClass('active');
//    }
//    src = src.split('background: url("');
//    src = src[1].split('")');
//    $('.imageView img').attr('src', src[0]);
//});
//$('.slideLeft').click(function () {
//    if ($('#Container div').eq(1).hasClass('active')) {
//        $('#Container div').removeClass('active');
//        $('#Container div').last().addClass('active');
//        var src = $('#Container div').last().attr('style');
//    } else {
//        var src = $('#Container .active').prev().addClass('active').attr('style');
//        $('#Container .active').last().removeClass('active');
//    }
//    src = src.split('background: url("');
//    src = src[1].split('")');
//    $('.imageView img').attr('src', src[0]);
//});
//$('.imageView .close').click(function () {
//    $('#Container div').removeClass('active');
//    $('.imageView').hide().removeClass('.nowViewsPhotoBlock');
//    $('.viewsAll').removeClass('nowViewsPhoto');
//});
//$('.photos>div').click(function () {
//    var index = $(this).index();
//    var src = $('#Container div').eq(index).addClass('active').attr('style');
//    src = src.split('background: url("');
//    src = src[1].split('")');
//    $('.imageView img').attr('src', src[0]);
//    $('.imageView').show().addClass('nowViewsPhotoBlock');
//});

//$(".reviews .images").each(function () {
//    var e = $(this).find("div").length;
//    if (e > 10) {
//        var t = e - 10;
//        $(this).addClass("moreThen10"), $(this).find("div").eq(9).addClass("blueImage").append("<span>+" + t + "</span>")
//    } else 10 == e ? $(this).addClass("moreThen10 ten") : 10 > e && e > 4 ? $(this).addClass("betweenTenAndFour") : 4 == e ? $(this).addClass("four") : 3 == e ? $(this).addClass("three") : 3 > e && $(this).addClass("twoAndOne")
//});
//$(".reviews .images div").click(function () {
//    if ($(this).hasClass("blueImage")) {
//        if ($(this).parents(".moreThen10").addClass("showAllPhotos").find("div").slideDown(), $(this).hasClass("noneBlue")) {
//            var e = $(this).addClass("active").attr("article");
//            $(".imageView img").attr("src", e), $(".imageView").show().addClass("nowViewsPhotoBlock"), $(this).parents(".review").addClass("activeReview")
//        }
//        $(this).addClass("noneBlue")
//    } else {
//        var e = $(this).addClass("active").attr("article");
//        $(".imageView img").attr("src", e), $(".imageView").show().addClass("nowViewsPhotoBlock"), $(this).parents(".review").addClass("activeReview")
//    }
//})
var checkboxFilter = {

    // Declare any variables we will need as properties of the object

    $filters: null,
    $reset: null,
    groups: [],
    outputArray: [],
    outputString: '',

    // The "init" method will run on document ready and cache any jQuery objects we will need.

    init: function () {
        var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "checkboxFilter" object so that we can share methods and properties between all parts of the object.

        self.$filters = $('#Filters');
        self.$reset = $('#Reset');
        self.$container = $('#Container');

        self.$filters.find('fieldset').each(function () {
            self.groups.push({
                $inputs: $(this).find('input'),
                active: [],
                tracker: false
            });
        });

        self.bindHandlers();
    },

    // The "bindHandlers" method will listen for whenever a form value changes.

    bindHandlers: function () {
        var self = this;

        self.$filters.on('change', function () {
            self.parseFilters();
        });

        self.$reset.on('click', function (e) {
            e.preventDefault();
            self.$filters[0].reset();
            self.parseFilters();
        });
    },

    // The parseFilters method checks which filters are active in each group:

    parseFilters: function () {
        var self = this;

        // loop through each filter group and add active filters to arrays

        for (var i = 0, group; group = self.groups[i]; i++) {
            group.active = []; // reset arrays
            group.$inputs.each(function () {
                $(this).is(':checked') && group.active.push(this.value);
            });
            group.active.length && (group.tracker = 0);
        }

        self.concatenate();
    },

    // The "concatenate" method will crawl through each group, concatenating filters as desired:

    concatenate: function () {
        var self = this,
            cache = '',
            crawled = false,
            checkTrackers = function () {
                var done = 0;

                for (var i = 0, group; group = self.groups[i]; i++) {
                    (group.tracker === false) && done++;
                }

                return (done < self.groups.length);
            },
            crawl = function () {
                for (var i = 0, group; group = self.groups[i]; i++) {
                    group.active[group.tracker] && (cache += group.active[group.tracker]);

                    if (i === self.groups.length - 1) {
                        self.outputArray.push(cache);
                        cache = '';
                        updateTrackers();
                    }
                }
            },
            updateTrackers = function () {
                for (var i = self.groups.length - 1; i > -1; i--) {
                    var group = self.groups[i];

                    if (group.active[group.tracker + 1]) {
                        group.tracker++;
                        break;
                    } else if (i > 0) {
                        group.tracker && (group.tracker = 0);
                    } else {
                        crawled = true;
                    }
                }
            };

        self.outputArray = []; // reset output array

        do {
            crawl();
        }
        while (!crawled && checkTrackers());

        self.outputString = self.outputArray.join();

        // If the output string is empty, show all rather than none:

        !self.outputString.length && (self.outputString = 'all');

        //console.log(self.outputString);

        // ^ we can check the console here to take a look at the filter string that is produced

        // Send the output string to MixItUp via the 'filter' method:

        if (self.$container.mixItUp('isLoaded')) {
            self.$container.mixItUp('filter', self.outputString);
        }
    }
};

// On document ready, initialise our code.

$(function () {

    // Initialize checkboxFilter code

    checkboxFilter.init();

    // Instantiate MixItUp

    $('#Container').mixItUp({
        controls: {
            enable: false // we won't be needing these
        },
        animation: {
            easing: 'cubic-bezier(0.86, 0, 0.07, 1)',
            duration: 600
        }
    });
});
//Рабочий стол
$('.popup .overlay, .popup .close, .popup .blueButton').click(function () {
    $('.popup').animate({opacity: 0}, 300, function () {
        $('.popup').hide();
    });
});
$('.otherInfo textarea').focusin(function () {
    $(this).parents('.feedWrap').addClass('focusedTextarea');
});
$('.otherInfo textarea').focusout(function () {
    $(this).parents('.feedWrap').removeClass('focusedTextarea');
});

//<div id="rating_1" class="stars set-stars">
//  <input type="hidden" name="vote-id" value="1">
//</div>

var i = $('.step1').length ? 0 : 1;
$('#nextStep').click(function () {
    i++;
    if (i == 1) {
        $('.step1').slideUp();
        var type_user = '';
        if ($("input[name='userType']:checked").val() == 1) {
            type_user = 'Пользователь';
            $('.step2_user').slideDown();
        } else {
            type_user = 'Организатор';
            $('.step2').slideDown();
        }
        $("#how_you").html(type_user);
    }
    if ((i >= 2) && ($("input[name='userType']:checked").val() == 1)) {
        form_ajax($("#fos_user_registration_form"));
    } else if ((i >= 2) && ($("input[name='userType']:checked").val() != 1)) {
        var type_organizer = $("input[name='organizer']:checked").val();

        window.location.href = Routing.generate('personal_area_registrate', {'organizer': type_organizer})
    }
});
$('.scrollDown').click(function () {
    var pos = $(this).offset();
    $('body,html').animate({
        scrollTop: pos.top + 36
    }, 600);
    return false;
});

var menu_select = [];

//Меню категорий на главной
$('.indexmenu .parent .inner_checkbox').on('click', function (e) {
    var $this = $(this),
        a_link = $this.closest('a'),
        prop = $this.prop('checked'),
        val = $this.val();

    if (a_link.hasClass('active')) {
        a_link.removeClass('active').siblings('ul').slideUp();
    } else {
        //$('.parent>ul').slideUp().siblings('a').removeClass('active');
        a_link.addClass('active').siblings('ul').slideDown();
    }

    if (prop) {
        if (!$this.closest('.parent').hasClass('level_0')) {
            var level_0 = $this.closest('.level_0').children('a').find('.inner_checkbox');
            var level_1 = $this.closest('.level_1').children('a').find('.inner_checkbox');
            level_0.prop('checked', true);
            level_1.prop('checked', true);
        }

        var index_block = $this.closest('.level_0').index();

        $(".level_0").each(function (i, m) {
            if (index_block != i) {
                $(m).find('.inner_checkbox').each(function (i, m) {
                    $(m).prop('checked', false)
                })
            }
        })

        if (!$this.closest('.parent').hasClass('level_1')) {
            $this.closest('.level_1').children('a').find('.inner_checkbox').prop('checked', true)
        }
    } else {
        $this.closest('li').find('.inner_checkbox').each(function (i, m) {
            $(m).prop('checked', false)
        })
    }
    build_array_menu();

    console.log(menu_select)
    $("#appbundle_event_search_menu").val(trim(menu_select.join(','), ','))
    ajax_send_event()
});

function trim(str, charlist) {    // Strip whitespace (or other characters) from the beginning and end of a string
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: mdsjack (http://www.mdsjack.bo.it)
    // +   improved by: Alexander Ermolaev (http://snippets.dzone.com/user/AlexanderErmolaev)
    // +      input by: Erkekjetter
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
    var re = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
    return str.replace(re, '');
}


function build_array_menu() {
    menu_select = [];
    var delete_values = [];
    $(".level_0").each(function (i, m) {
        var check_0 = $(m).children('a').find('.inner_checkbox'),
            val = check_0.val(),
            prop = check_0.prop('checked');

        if (prop) {
            menu_select.push(val)
            $(m).find('.level_1').each(function (i2, m2) {
                var check_1 = $(m2).children('a').find('.inner_checkbox'),
                    val = check_1.val(),
                    prop = check_1.prop('checked');

                if (prop) {
                    menu_select.push(val)
                    var delete_in = true;
                    var delete_concat = [];


                    $(m).find('.level_2').each(function (i3, m3) {
                        var check_1 = $(m3).children('a').find('.inner_checkbox'),
                            val = check_1.val(),
                            prop = check_1.prop('checked');

                        if (prop) {
                            menu_select.push(val)
                            if (delete_in) {
                                delete_concat.push(val);
                            }
                        } else {
                            delete_concat = [];
                            delete_in = false;
                        }
                    })
                    delete_values = delete_values.concat(delete_concat);
                }
            })
        }
    })

    for (var delete_value in delete_values) {
        var index_of = menu_select.indexOf(delete_values[delete_value])

        if (index_of != (-1)) {
            delete menu_select[index_of];
        }
    }
}

$('.indexmenu .parent .inner').on('click', function (e) {
    var a_link = $(this).closest('a');

    if (a_link.hasClass('active')) {
        a_link.removeClass('active').siblings('ul').slideUp();
    } else {
        //$('.parent>ul').slideUp().siblings('a').removeClass('active');
        a_link.addClass('active').siblings('ul').slideDown();
    }
});