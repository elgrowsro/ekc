/**
 * Created by anton on 16.05.16.
 */



function logo_user_upload(id_form, id_hidden, logo_img, type_route) {
    $('#' + id_form).fileupload({
        url: Routing.generate('upload_media', {_type: type_route}),
        dataType: 'json',
        add: function (e, data) {
            data.submit();
        },
        done: function (e, data) {
            if (data.result.result == "success") {
                $('#' + logo_img).css('background-image', 'url(' + data.result.impath + ')');
                //$('#' + logo_img).style('<img src="' + data.result.impath + '" />');
                $('#' + id_hidden).val(data.result.path);
            } else {
                add_error_field("form_create_offer_logouser", "При загрузке произошла ошибка")
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            console.log(progress)
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
}