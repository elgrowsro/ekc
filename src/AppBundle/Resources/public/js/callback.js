/**
 * Created by anton on 04.05.16.
 */


function registrate_organization_individual_form_callback(form, data) {
    success_registrate_organizate(form);
}

function registrate_organization_form_callback(form, data) {
    success_registrate_organizate(form);
}

function success_registrate_organizate(form_org) {
    form_org[0].reset();
    var effectIn = "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: '#registrate_organization_success',
        afterClose: function (current, previous) {
            location.reload();
        }
    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}

function fos_user_resetting_form_callback(form, data) {
    form[0].reset();
    var effectIn = "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: '#reset-pass_success',

        afterClose: function (current, previous) {
            window.location.href = '/';
        }

    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}

function fos_user_registration_form_callback(form, data) {
    form[0].reset();

    var effectIn = "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: '#registrate-success',

        afterClose: function (current, previous) {
            window.location.href = '/';
        }

    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}


function fos_user_resetting_request_callback(form, data) {
    form[0].reset();
    $.fancybox.close();
    show_popup("#restore-pass_success");
}

//function fos_user_resetting_form_callback(form, data) {
//    form[0].reset();
//    show_popup("#restore-pass__reset_success");
//    $("#fos_user_resetting_form").parent().append('<div class="center">' +
//        '<a href="/" class="blueButton">На главную</a>' +
//        '</div>')
//    $("#fos_user_resetting_form").remove()
//}

function fos_user_security_check_callback(form, data) {
    window.location.replace("/cabinet/");
}

function form_contact_offer_user_callback(form, data) {
}

function form_contact_company_info_callback(form, data) {
}

function eventsbundle_question_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();
    show_popup("#send-question_success");
}

function personalareabundle_question_answer_form_callback(form, data) {
    var block_question = form.closest('.js-block-question');
    var data = JSON.parse(data.content);

    form[0].reset();
    $("#form_answer_question").appendTo($('body'));

    block_question.remove();

    $("#list_new_questions").empty().html(data.data_load)
}

function personalareabundle_feed_question_answer_form_callback(form, data) {
    var block_question = form.closest('.js-block-question');
    var data = JSON.parse(data.content);

    form[0].reset();
    $("#form_answer_question").appendTo($('body'));

    block_question.remove();

    $("#list_feed_blocks").empty().html(data.data_load)
}

function feed_city_question_answer_all_form_callback(form, data) {
    var block_question = form.closest('.js-block-question');
    var data = JSON.parse(data.content);

    form[0].reset();
    $("#form_answer_question").appendTo($('body'));

    block_question.remove();

    $("#list_feed_blocks").empty().html(data.data_load)
}

function feed_city_question_answer_questions_form_callback(form, data) {
    var block_question = form.closest('.js-block-question');
    var data = JSON.parse(data.content);

    form[0].reset();
    $("#form_answer_question").appendTo($('body'));

    block_question.remove();

    $("#list_feed_blocks").empty().html(data.data_load)
}

function eventbundle_comment_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();
    show_popup("#send-comment-event_success");
    var data = JSON.parse(data.content);

    var $data_block = $(".reviews_block > .data_block"),
        $data_block_parent = $data_block.parent();
    $data_block_parent.empty().html(data.data_load);
}

function feedbundle_comment_form_callback(form, data) {
    form[0].reset();
    var $form = $("#feedbundle_comment_form")
    var box = $("#form_add_comment");
    var $switcher_form_button = $form.closest('.switcher_form_button');
    var eventbundle_comment_text = data.comment.text;

    $('.switch_to_form').show();
    $('.switch_to_button').hide();
    $form.appendTo(box);
    $switcher_form_button.html(eventbundle_comment_text)
}

function profilebundle_comment_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();
    show_popup("#send-comment-company_success");
    var data = JSON.parse(data.content);

    var $data_block = $("#list_comments_company");
    $data_block.html(data.data_load);
}

function eventsbundle_event_form_callback(form, data) {
    form[0].reset();

    var effectIn = "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: '#event-create-success',
        afterClose: function (current, previous) {
            location.reload();
        }
    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}
function eventsbundle_event_edit_form_callback(form, data) {
    form[0].reset();

    var effectIn = "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: '#event-create-success',
        afterClose: function (current, previous) {
            location.reload();
        }
    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}

function make_money_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();
    show_popup("#make_money_form_modal_success");
    $(".money_user").html(data.money_user)
}

function withdraw_money_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();
    show_popup("#withdraw_money_form_modal_success");
}

function appbundle_event_search_form_callback(form, data) {
    $("#list_main_events").html(data.data_load)

    //console.log('data.url', data.url)
    history.pushState('', $('title').html(), data.url);
}

function personalareabundle_desktop_tp_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();

    if (data.type == 1) {
        if ($(".tdTime[data-id='" + data.id + "']").parent().children().length == 1) {
            $(".tdTime[data-id='" + data.id + "']").closest('tr').remove();
        } else {
            $(".tdTime[data-id='" + data.id + "']").remove();
        }
    } else {
        $(".tdTime[data-id='" + data.id + "']").data('max', data.places);
    }
}

function personalareabundle_descktop_check_ticket_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();

    if (data.find == 'success') {
        $("#find_ticket_data").html(data.info)
        show_popup("#desktop_check_ticket_success");
    } else {
        show_popup("#desktop_check_ticket_fail");
    }
}

function event_donation_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();

    show_popup("#event_donation_success");
}

function advertbundle_advert_create_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();

    show_popup("#advertbundle_advert_create_success");
    $("#select_advert_places").multipleSelect("refresh");
}

function personalareabundle_delete_event_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();


    $("#personalareabundle_delete_event_form_global_errors").empty();

    var data = JSON.parse(data.content);

    $("#list_events_cabinet").empty().html(data.data_load)
}

function eventsbundle_ticket_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();

    show_popup("#eventsbundle_ticket_success");
}

function eventsbundle_gift_form_callback(form, data) {
    form[0].reset();
    $.fancybox.close();

    show_popup("#eventsbundle_gift_success");
}