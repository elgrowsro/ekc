/**
 * Created by anton on 04.05.16.
 */
var full_price_ticket = 0,
    prices_array,
    timer_ajax_search_event = false,
    timer_ajax_map_main = false,
    map_main_page = false,
    BalloonLayoutEvent = false,
    old_map_x1 = false,
    old_map_y1 = false,
    old_map_x2 = false,
    old_map_y2 = false,
    BalloonContentLayoutEvent = false;

if (!String.prototype.format) {
    String.prototype.format = function () {
        var str = this.toString();
        if (!arguments.length)
            return str;
        var args = typeof arguments[0],
            args = (("string" == args || "number" == args) ? arguments : arguments[0]);
        for (arg in args)
            str = str.replace(RegExp("\\{" + arg + "\\}", "gi"), args[arg]);
        return str;
    }
}

$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            var load_button_visible = $(".load_next_data:visible");
            console.log('bottom', load_button_visible.attr('data-href'))

            if (load_button_visible.length && load_button_visible.attr('data-href')) {
                load_button_visible.html('Загрузка...');
                load_button_visible.click();
                load_button_visible.removeAttr('data-href');
            }
            //page++;
            //$.ajax({
            //    type: "GET",
            //    cache: false,
            //    url: '[ПУТЬ]?page=' + page,
            //    success: function(data) {
            //        $('#container').append(data);
            //    }
            //}
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            // <DO YOUR WORK HERE>

            if ($('.citySelectModal').css('display') == 'block') {
                $('.citySelectModal').hide();
                $('html').removeClass('fancybox-lock');
            }

            if ($(".mapWrap.fixmap").length) {
                $(".mapWrap").removeClass("fixmap");
                $("body").removeClass("bodyHidden");

                $("#map_main").css('height', '120px');
                map_fitToViewport();

                $("#map_main .close").remove()
            }

            if ($(".viewsAll.showedAll").length) {
                $('.viewsAll').removeClass('showedAll');
                $('body').removeClass('bodyHidden');
            }

            if ($(".viewsVideoAll.showedAll").length) {
                $('.viewsVideoAll').removeClass('showedAll');
                $('body').removeClass('bodyHidden');
            }

            if ($(".city-select.active").length) {
                $('.city-select').removeClass('active');
                $('.cityModal').hide();
            }
        }
    });

    $(".fancybox-gallary").fancybox({
        openEffect: 'none',
        closeEffect: 'none',
    });

    $("a.fancybox-youtube").click(function () {
        $.fancybox({
            'padding': 0,
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'title': this.title,
            'width': 680,
            'height': 495,
            'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type': 'swf',
            'swf': {
                'wmode': 'transparent',
                'allowfullscreen': 'true'
            }
        });
        return false;
    });

    $(".getGift").on('click', function () {
        var $this = $(this),
            get_id = $this.data('id'),
            $otherInfo = $this.closest(".otherInfo"),
            $othersWrapGift = $this.closest(".othersWrapGift");

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: Routing.generate('personal_area_gifts.get', {get: get_id}),
            beforeSend: function () {
                $otherInfo.find('.loader').addClass("active");
            },
            complete: function () {
                $otherInfo.find('.loader').removeClass("active");
            },
            success: function (response) {
                if (response.status == 'success') {
                    $this.remove();

                    $othersWrapGift.append('<p>Подарок получен</p>');
                    $('.money_user').html(response.money);
                }
            }
        });
    })

    $(".event_show_photo").on('click', function () {
        $(".photos").show();
        $(".videos").hide();

        $(this).addClass('active');
        $(".event_show_youtube").removeClass('active');
    });

    $(".event_show_youtube").on('click', function () {
        $(".photos").hide();
        $(".videos").show();

        $(this).addClass('active');
        $(".event_show_photo").removeClass('active');
    });

    $(".type_event_change_normal").on('click', function () {
        $(".normal_type_event_show").show();
        $(".charity_type_event_show").hide();

        $(this).addClass('active');
        $(".type_event_change_charity").removeClass('active');
        $("#eventsbundle_event_charity").val(0)
    });

    $(".type_event_change_charity").on('click', function () {
        $(".normal_type_event_show").hide();
        $(".charity_type_event_show").show();

        $(this).addClass('active');
        $(".type_event_change_normal").removeClass('active');

        if ($("#eventsbundle_event_charity").length) {
            $("#eventsbundle_event_charity").val(1)
        }
        if ($("#eventsbundle_event_edit_charity").length) {
            $("#eventsbundle_event_edit_charity").val(1)
        }
    });

    $("body").on("click", ".blueImage span", function (e) {
        var $this = $(this);
        e.stopPropagation();
        console.log('click');
        $this.closest('.review').find('.moreAfterThen10').show();
        $this.parent().removeClass('blueImage');
        $this.parent().empty();
        return false;
    });

    $("body").on("click", ".js-remove-question", function () {
        var $this = $(this),
            quest_id = $this.data('id'),
            answer = $this.data('answer');

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: Routing.generate('personal_area_question_delete', {id: quest_id, answer: answer}),
            beforeSend: function () {
                $("#form_answer_question").appendTo($('body'));
                $('.switch_to_form').show();
                $('.switch_to_button').hide();
                $(".loader_questions").addClass("active");
            },
            complete: function () {
                $(".loader_questions").removeClass("active");
            },
            success: function (response) {
                if (response.status == 'success') {
                    var data = JSON.parse(response.content);


                    $this.closest('.js-block-question').remove();

                    $("#list_new_questions").empty().html(data.data_load)
                }
            }
        });
    });

    $("body").on("click", ".show_form_answer_queston", function () {
        var $this = $(this),
            id_quest = $this.data('id'),
            $parent = $this.parent(),
            $switcher = $this.closest('.switcher_form_button'),
            $button = $switcher.find('.switch_to_button'),
            $form = $("#form_answer_question");

        $('.switch_to_form').show();
        $('.switch_to_button').hide();
        $form.show();
        $parent.hide();
        $button.show();
        $form.appendTo($button);
        $form.children().attr('action', Routing.generate('personal_area_question_answer', {
            id: id_quest
        }))
    });

    $("body").on("click", ".show_form_make_comment", function () {
        var $this = $(this),
            id_event = $this.data('id'),
            id_feed = $this.data('feed'),
            $parent = $this.parent(),
            $switcher = $this.closest('.switcher_form_button'),
            $button = $switcher.find('.switch_to_button'),
            $form = $("#feedbundle_comment_form");

        $('.switch_to_form').show();
        $('.switch_to_button').hide();
        $form.show();
        $parent.hide();
        $button.show();
        $form.appendTo($button);
        $("#feedbundle_comment_event").val(id_event);
        $("#feedbundle_comment_feed").val(id_feed);
        //$form.children().attr('action', Routing.generate('personal_area_question_answer', {
        //    id: id_quest
        //}))
    });

    $("body").on("click", ".popup-link", function () {
        var $t = $(this),
            href = $t.data("href"),
            $w,
            effectIn = "bounceIn";

        show_popup(href, effectIn);

        return false;
    });


    $("body").on('click', '.load_next_data', function () {
        var $this = $(this),
            $parent = $this.parent(),
            href_load = $this.data('href');


        load_data_pagination($parent, href_load);
    })

    $("#change_to_reviews").on("click", function () {
        $(".question_block").hide()
        $(".reviews_block").show()
    })
    $("#change_to_questions").on("click", function () {
        $(".reviews_block").hide()
        $(".question_block").show()
    })

    $(".phone_mask").mask("+7 (999) 999-99-99");

    $("body").on("submit", ".form_ajax", function () {
        var $this = $(this);
        form_ajax($this);
        return false;
    });

    if ($(".form_with_dropzone").length) {
        $(".form_with_dropzone").each(function () {
            var $this = $(this);
            form_with_dropzone($this);
        })
    }

    $(".link_other_form").on('click', function () {
        var $this = $(this),
            href = $this.data('href');

        $this.closest(".autorizateWrap").hide();
        $(href).show();
    });


    $('body').on('click', ".fancy-close", function () {
        $.fancybox.close();
        return false;
    });

    if ($('#form_contact_offer_user_form').length) {
        logo_user_upload('form_contact_offer_user_form', 'form_contact_offer_user_setavatar', 'avatarUserShow', 'logoCompany');
    }


//Добавление и удаление поля в дополнительные услуги
    if ($("#eventsbundle_event_getService_0_name").length) {
        var input_name_dop = '<input type="text" id="eventsbundle_event_getService_{0}_name" name="eventsbundle_event[getService][{0}][name]" required="required">',
            label_name_dop = '<label for="eventsbundle_event_getService_{0}_name" class="required">Название</label>',
            input_price_dop = '<input type="text" id="eventsbundle_event_getService_{0}_price" name="eventsbundle_event[getService][{0}][price]" required="required">',
            label_price_dop = '<label for="eventsbundle_event_getService_{0}_price" class="required">Цена</label>';

        var id = 0;


        $('#addDop').click(function () {
            id++;
            var htmlDopTable =
                '<div class="serv">' +
                '<div class="inner">' +
                '<div class="input">' +
                label_name_dop.format(id) +
                input_name_dop.format(id) +
                '</div>' +
                '<div class="input">' +
                label_price_dop.format(id) +
                input_price_dop.format(id) +
                '<span>Р</span>' +
                '</div>' +
                '<div class="remove"><i></i>Удалить</div>' +
                ' </div>' +
                '</div>';
            $('.dopServices .serv').eq(-1).after(htmlDopTable);
        });
        removeDop();
    }
    if ($("#eventsbundle_event_edit_getService_0_name").length) {
        var input_name_dop = '<input type="text" id="eventsbundle_event_edit_getService_{0}_name" name="eventsbundle_event_edit[getService][{0}][name]" required="required">',
            label_name_dop = '<label for="eventsbundle_event_edit_getService_{0}_name" class="required">Название</label>',
            input_price_dop = '<input type="text" id="eventsbundle_event_edit_getService_{0}_price" name="eventsbundle_event_edit[getService][{0}][price]" required="required">',
            label_price_dop = '<label for="eventsbundle_event_edit_getService_{0}_price" class="required">Цена</label>';

        var id = 0;

        if ($(".service_block_event").length) {
            var max_index = 0;
            $(".service_block_event").each(function (i, m) {
                if ($(m).data('index') && (max_index < $(m).data('index'))) {
                    max_index = $(m).data('index');
                }
            });
            id = max_index;
        } else {
            id = 0;
        }


        $('#addDop').click(function () {
            id++;
            var htmlDopTable =
                '<div class="serv">' +
                '<div class="inner">' +
                '<div class="input">' +
                label_name_dop.format(id) +
                input_name_dop.format(id) +
                '</div>' +
                '<div class="input">' +
                label_price_dop.format(id) +
                input_price_dop.format(id) +
                '<span>Р</span>' +
                '</div>' +
                '<div class="remove"><i></i>Удалить</div>' +
                ' </div>' +
                '</div>';
            $('.dopServices .serv').eq(-1).after(htmlDopTable);
        });
        removeDop();
    }

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var hours = today.getHours();
    var minutes = today.getMinutes();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;

    if (typeof jQuery.datetimepicker != 'undefined') {
        jQuery.datetimepicker.setLocale('ru');
        var check_period_send = false;
        $('#appbundle_event_search_period_from').periodpicker({
            cells: [1, 1],
            withoutBottomPanel: true,
            end: '#appbundle_event_search_period_to',
            yearsLine: false,
            title: false,
            closeButton: false,
            fullsizeButton: false,
            minDate: today, //or 1986/12/08
            formatDate: 'YYYY-MM-DD',
            lang: 'ru',
            //clearButtonInButton: true,
            onAfterHide: function () {
                //var val = this.startinput.val();
                //if (!val || !moment(val, this.options.format).isValid()) {
                //this.addRange([new Date()]);
                //}
                //console.log('show 22222', val)
                if ($('#appbundle_event_search_period_from').periodpicker('value').length && check_period_send) {
                    check_period_send = false;
                    ajax_send_event()
                }
            },
            onAfterShow: function () {
                check_period_send = true;
            },
            //onOkButtonClick: function () {
            //    console.log('hide 111', this.startinput.val())
            //},
            //onSelectDate: function (dp, $input) {
            //    console.log('hide 222')
            //}
        });

        $('.pickadate').datetimepicker({
            timepicker: false,
            mask: '99.99.9999',
            format: 'd.m.Y',
            minDate: today, //or 1986/12/08
            validateOnBlur: true,
            dayOfWeekStart: 1,
        });
        $('.time').datetimepicker({
            datepicker: false,
            mask: '99:99',
            format: 'H:i',
            dayOfWeekStart: 1,
            step: 60
        });

        var tp_price_select = null;
        if ($('#datetimepicker_reserv_event').length) {
            var tp_array = JSON.parse(json_tp),
                allowDates = [],
                allowTimes = [],
                min_date = null,
                min_date_array = null,
                defaultDate = null;

            prices_array = JSON.parse(json_prices);
            min_date = yyyy + '-' + mm + '-' + dd;

            for (var n_date in tp_array) {
                if (!min_date_array || min_date_array < n_date) {
                    min_date_array = n_date;
                }
                if (!defaultDate && (n_date >= (yyyy + '-' + mm + '-' + dd ))) {
                    defaultDate = n_date;
                }
                if (n_date > yyyy + '-' + mm + '-' + dd) {
                    allowDates.push(n_date);
                }
            }

            if ((yyyy + '-' + mm + '-' + dd ) >= min_date) {
                min_date = (yyyy + '-' + mm + '-' + dd );
            }

            if (typeof tp_array[min_date_array] != 'undefined') {
                allowTimes = tp_array[min_date_array];
            }
            allowTimes.sort(function (a, b) {
                return a > b;
            });

            //console.log(allowDates, allowTimes)

            $('#datetimepicker_reserv_event').val(defaultDate)
            $('#datetimepicker_reserv_event').datetimepicker({
                format: 'Y-m-d H:i',
                inline: true,
                lang: 'ru',
                defaultDate: defaultDate, //or 1986/12/08
                //minDate: min_date, //or 1986/12/08
                allowDates: allowDates, //or 1986/12/08
                formatDate: 'Y-m-d',
                allowTimes: allowTimes,
                minTime: 0,
                dayOfWeekStart: 1,
                onChangeMonth: function (dp, $input) {
                    this.setOptions({
                        timepicker: false,
                    });

                    $("#eventsbundle_ticket_cntTickets").prop("disabled", true);
                    $(".service_ticket_select").prop("disabled", true);
                },
                onChangeYear: function (dp, $input) {
                    this.setOptions({
                        timepicker: false,
                    });

                    $("#eventsbundle_ticket_cntTickets").prop("disabled", true);
                    $(".service_ticket_select").prop("disabled", true);
                },
                onSelectDate: function (dp, $input) {
                    var check_date = new Date(dp);
                    var check_dd = check_date.getDate();
                    var check_mm = check_date.getMonth() + 1; //January is 0!
                    var check_yyyy = check_date.getFullYear();
                    if (check_dd < 10) {
                        check_dd = '0' + check_dd
                    }

                    if (check_mm < 10) {
                        check_mm = '0' + check_mm
                    }
                    var date_check = check_yyyy + '-' + check_mm + '-' + check_dd;

                    if (typeof tp_array[date_check] != 'undefined') {
                        allowTimes = tp_array[date_check];
                    }

                    allowTimes.sort(function (a, b) {
                        return a > b;
                    });

                    if ((yyyy + '-' + mm + '-' + dd) == (date_check)) {
                        this.setOptions({
                            timepicker: true,
                            minTime: 0,
                            allowTimes: allowTimes
                        });
                    } else {
                        this.setOptions({
                            timepicker: true,
                            minTime: false,
                            allowTimes: allowTimes
                        });
                    }

                    $("#eventsbundle_ticket_timeprices").val('');
                    $("#eventsbundle_gift_timeprices").val('');

                    $("#eventsbundle_ticket_cntTickets").prop("disabled", true);
                    $(".service_ticket_select").prop("disabled", true);
                },
                onSelectTime: function (dp, $input) {
                    check_full_price();

                    $("#eventsbundle_ticket_cntTickets").prop("disabled", false);
                    $(".service_ticket_select").prop("disabled", false);
                }
            });

            $(".service_ticket_select").on("change", function () {
                check_full_price();
            })

            $("#eventsbundle_ticket_cntTickets").on("change", function () {
                check_full_price();
            })

            $('#label_for_buy_ticket_button').on('click', function () {
                $.fancybox.close();
            })
        }
    }

    $('#label_for_donate_send_button').on('click', function () {
        $.fancybox.close();
    })

    $(".change_city").on('click', function () {
        var $this = $(this),
            city_id = $this.data('id');

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: Routing.generate('set_city'),
            data: {
                city: city_id
            },
            beforeSend: function () {
                $(".loader_citys").addClass("active");
            },
            complete: function () {
                //$(".loader_citys").removeClass("active");
            },
            success: function (response) {
                if (response.status == 'success') {
                    //console.log(response)
                    location.reload();
                }
            }
        });
    })


    $('body').on('keydown', "input[type=number]", function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    if ($(".main_filter_events").length) {
        $(".main_filter_events").on('change', function () {
            ajax_send_event()
        })
    }

    $('.set-stars').barrating({
        theme: 'fontawesome-stars-o',
        initialRating: null,
        showValues: false,
        showSelectedRating: true,
        deselectable: true,
        reverse: false,
        readonly: false,
        fastClicks: true,
        hoverState: true,
        silent: false,
    });

    if ($("#desktop_event_select").length) {
        $("#desktop_event_select").on('change', function () {
            var $this = $(this),
                val = $this.val();


            $.ajax({
                dataType: "json",
                type: 'POST',
                url: Routing.generate('personal_area_company.desktop.load', {event: val}),
                beforeSend: function () {
                    $("#desktop_events_all .loader").addClass("active");
                },
                complete: function () {
                    $("#desktop_events_all .loader").removeClass("active");
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $("#table_dates_event").empty().html(response.data_load)
                    }
                }
            });
        })
    }

    $('body').on('click', '.tdTime', function () {
        var $this = $(this),
            value_tp = $this.data('id'),
            max = $this.data('max');
        show_popup('#desktop_tp_click_popup');
        $("#personalareabundle_desktop_tp_tp").val(value_tp)
        $("#personalareabundle_desktop_tp_cnt_reserv").attr('max', max)
    });

    if ($('.removeEvent').length) {
        $(".removeEvent").on('click', function () {
            var $this = $(this),
                id = $this.data('id');

            $("#personalareabundle_delete_event_id").val(id);

            show_popup('#delete_event_question')
        })
    }

    if (typeof ymaps != 'undefined' && $("#map_main").length) {
        ymaps.ready(init_map_main);
    }


    $(".mapClicker").click(function () {
        $(".mapWrap").addClass("fixmap");
        $("body").addClass("bodyHidden");

        var windows_height = $(window).height();
        $("#map_main").css('height', windows_height + 'px');

        map_fitToViewport();

        $("#map_main").append('<div class="close"></div>');
        closeMap()

        load_markers(map_main_page.getBounds());
    })

    $(".buttonsReferal #users4 button").on('click', function () {
        var $this = $(this);
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: Routing.generate('personal_area_company.referral.update.all'),
            beforeSend: function () {
                $this.find('img').addClass('active');
                $this.prop('disabled', true);
            },
            complete: function () {
                //$this.find('img').removeClass('active')
            },
            success: function (response) {
                location.reload();
            }
        })
    })

    var currentdate = new Date();
    $(".js-send-message-vk").on('click', function () {
        var $this = $(this),
            vk_id = $this.data('vkid');
        var datetime = currentdate.getDate() + "."
            + (currentdate.getMonth() + 1) + "."
            + currentdate.getFullYear() + "  "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes();

        $.ajax({
            dataType: "json",
            type: 'POST',
            data: {
                user_id: vk_id
            },
            url: Routing.generate('personal_area_company.referral.vk.send_message'),
            beforeSend: function () {
                $this.html('Отправка...');
                $this.prop('disabled', true);
            },
            complete: function () {
                //$this.find('img').removeClass('active')
                $(".users2").append($this.closest('.block_vk_user'));
                $this.hide();
                $this.closest('.block_vk_user').find('.itogReferal').html(datetime).show();
            },
            success: function (response) {
            }
        });
    });

    if ($('.js-remove-comment-complaint').length) {
        $(".js-remove-comment-complaint").on('click', function () {
            var $this = $(this),
                id = $this.data('id');

            $.ajax({
                dataType: "json",
                type: 'POST',
                url: Routing.generate('remove_comment.request', {id: id}),
                beforeSend: function () {
                    $this.hide();
                },
                complete: function () {
                    $this.parent().find('.js-request-send-remove').show();
                },
                success: function (response) {
                }
            })
        })
    }

    $("#event_donation_money").on('keyup', function () {
        var $this = $(this),
            val = $this.val();

        $(".full_price_donation").html(val);
    })

    $("#eventsbundle_ticket_cntTickets").on('change', function () {
        var $this = $(this),
            val = $this.val();
        $("#eventsbundle_gift_cntTickets").val(val);
    });
});

function init_map_main() {

    map_main_page = new ymaps.Map('map_main', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [56.838607, 60.605514], // Москва
        zoom: 12,
        controls: ['zoomControl']
    }, {
        searchControlProvider: 'yandex#search'
    });


    // Создание макета балуна на основе Twitter Bootstrap.
    BalloonLayoutEvent = ymaps.templateLayoutFactory.createClass(
        '<div class="map-balloon-top">' +
        '<a class="close" href="#"></a>' +
        '<div class="arrow"></div>' +
        '<div class="map-balloon-inner">' +
        '$[[options.contentLayout observeSize minWidth=400 maxWidth=400 maxHeight=350]]' +
        '</div>' +
        '</div>', {
            /**
             * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
             * @function
             * @name build
             */
            build: function () {
                this.constructor.superclass.build.call(this);

                this._$element = $('.map-balloon-top', this.getParentElement());

                this.applyElementOffset();

                this._$element.find('.close')
                    .on('click', $.proxy(this.onCloseClick, this));
            },

            /**
             * Удаляет содержимое макета из DOM.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
             * @function
             * @name clear
             */
            clear: function () {
                this._$element.find('.close')
                    .off('click');

                this.constructor.superclass.clear.call(this);
            },

            /**
             * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name applyElementOffset
             */
            applyElementOffset: function () {
                this._$element.css({
                    left: -(this._$element[0].offsetWidth / 2),
                    top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight)
                });
            },

            /**
             * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name onCloseClick
             */
            onCloseClick: function (e) {
                e.preventDefault();

                this.events.fire('userclose');
            },
        }),


        // Создание макета содержимого балуна.
        // Макет создается с помощью фабрики макетов с помощью текстового шаблона.
        BalloonContentLayoutEvent = ymaps.templateLayoutFactory.createClass([
            '<div class="events mapevent">',
            '<div class="feedWrap map-feedWrap">',
            '<div>',
            '<div class="image" style="background:url({{ properties.mainImage }});">',
            '<a href="{{ properties.url }}" target="_blank"><div class="eye"></div></a>',
            '</div>',
            '<div class="description">',
            '<div class="date">26 января</div>',
            '<a href="{{ properties.url }}" target="_blank" class="title">{{properties.name}}</a>',
            '<div class="price">{{properties.minPrice}}</div>',
            '<div class="ratig">',
            '<span>Средний рейтинг:</span>',
            '<div class="wrapRating">',
            '{{properties.avgRating | raw }}',
            '</div>',
            '</div>',
            '</div>',
            '</div>',
            '</div>',
            '</div>',
        ].join(''), {

            // Переопределяем функцию build, чтобы при создании макета начинать
            // слушать событие click на кнопке-счетчике.
            //build: function () {
            //    // Сначала вызываем метод build родительского класса.
            //    BalloonContentLayout.superclass.build.call(this);
            //    // А затем выполняем дополнительные действия.
            //    $('#counter-button').bind('click', this.onCounterClick);
            //    $('#count').html(counter);
            //},
            //
            //// Аналогично переопределяем функцию clear, чтобы снять
            //// прослушивание клика при удалении макета с карты.
            //clear: function () {
            //    // Выполняем действия в обратном порядке - сначала снимаем слушателя,
            //    // а потом вызываем метод clear родительского класса.
            //    $('#counter-button').unbind('click', this.onCounterClick);
            //    BalloonContentLayout.superclass.clear.call(this);
            //},
            //
            //onCounterClick: function () {
            //    $('#count').html(++counter);
            //    if (counter == 5) {
            //        alert('Вы славно потрудились.');
            //        counter = 0;
            //        $('#count').html(counter);
            //    }
            //}
        });


    map_main_page.events.add('actiontick', function (mapEventObject) {
        ajax_send_map_main();
    })


    //var e = (new google.maps.MarkerImage("/bundles/app/img/mapMarker.png", new google.maps.Size(49, 49), new google.maps.Point(0, 0), new google.maps.Point(24, 24)), {
    //    zoom: 14,
    //    center: new google.maps.LatLng(56.89825197570902, 60.81831693649292)
    //}), t = document.getElementById("map_main"), n = new google.maps.Map(t, e), i = [];
    //$(".markers > div").each(function () {
    //    $(this);
    //    e = $(this).attr("x");
    //    var e = parseFloat(e);
    //    t = $(this).attr("y");
    //    var t = parseFloat(t);
    //    pos = new google.maps.LatLng(e, t);
    //    var o = new google.maps.Marker({
    //        map: n,
    //        position: pos,
    //        animation: google.maps.Animation.DROP,
    //        icon: $(this).attr("marker")
    //    });
    //    i.push(o);
    //    var r = $(this).find(".hideContent").html(), a = new google.maps.InfoWindow({content: r});
    //    o.addListener("click", function () {
    //        a.open(n, o)
    //    })
    //});
    $(".markers > div").click(function () {
        //var e = $(this).find(".hideContent").html(), t = new google.maps.InfoWindow({content: e}), o = $(this).index();
        //t.open(n, i[o]);
        //var r = $(window).width();
        //870 >= r && $("#map_main").addClass("openMap")
    });
    $("#map_main").click(function () {
        var window_width = $(window).width();
        870 >= window_width && $("#map_main").removeClass("openMap")
    })
}

function ajax_send_map_main() {
    window.clearTimeout(timer_ajax_map_main);

    timer_ajax_map_main = window.setTimeout(function () {
        load_markers();
    }, 1500)
}

function send_request_markers(bounds, unbounds) {

    var data = $("#appbundle_event_search_form").serializeArray(),
        data_send = {},
        route_name;

    data_send.page = 1;

    console.log(typeof event_id);
    if (typeof event_id != "undefined") {
        data_send.event = event_id;
    }

    for (var data_key in data) {
        data_send[data[data_key]['name']] = data[data_key]['value'];
    }
    if ($("#charity_search").length && $("#charity_search").val()) {
        route_name = 'charity.events.load';
    } else {
        route_name = 'homepage.events.load';
    }


    $.ajax({
        dataType: "json",
        type: 'POST',
        data: {
            bounds: bounds,
            unbounds: unbounds,
        },
        url: Routing.generate(route_name, data_send),
        beforeSend: function () {
            //console.log('send bounds', bounds)
        },
        complete: function () {
            //$(".loader_citys").removeClass("active");
            delete_points_border();
        },
        success: function (response) {
            //console.log('get bounds', response.data)
            var markers_data = response.data;
            for (var data_event in markers_data) {
                var myPlacemark = new ymaps.Placemark([markers_data[data_event]['lat'], markers_data[data_event]['lng']], {
                    hintContent: markers_data[data_event]['name'],
                    name: markers_data[data_event]['name'],
                    avgRating: markers_data[data_event]['avgRating'],
                    url: markers_data[data_event]['url'],
                    minPrice: markers_data[data_event]['minPrice'],
                    mainImage: markers_data[data_event]['mainImage'],
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: '/' + markers_data[data_event]['icon'],
                    // Размеры метки.
                    iconImageSize: [49, 49],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-3, -42],
                    balloonLayout: BalloonLayoutEvent,
                    balloonContentLayout: BalloonContentLayoutEvent
                });

                if (markers_data[data_event]['path_way']) {
                    // Создаем ломаную с помощью вспомогательного класса Polyline.
                    var myPolyline = new ymaps.Polyline(markers_data[data_event]['path_way'], {
                        // Описываем свойства геообъекта.
                        // Содержимое балуна.
                        balloonContent: markers_data[data_event]['name']
                    }, {
                        // Задаем опции геообъекта.
                        // Отключаем кнопку закрытия балуна.
                        balloonCloseButton: false,
                        // Цвет линии.
                        strokeColor: "#000000",
                        // Ширина линии.
                        strokeWidth: 4,
                        // Коэффициент прозрачности.
                        strokeOpacity: 0.5
                    });

                    // Добавляем линии на карту.
                    map_main_page.geoObjects.add(myPolyline);
                }

                map_main_page.geoObjects.add(myPlacemark);
            }
        }
    });
}

function delete_points_border() {

    var delete_markers = [];

    map_main_page.geoObjects.each(function (e) {
        if (e.geometry.getType() == 'Point') {
            var bound_placemark = e.geometry.getCoordinates(),
                bound_placemark_x = parseFloat(bound_placemark[0]),
                bound_placemark_y = parseFloat(bound_placemark[1]),
                bounds_map = map_main_page.getBounds(),
                map_x1 = bounds_map[0][0],
                map_y1 = bounds_map[0][1],
                map_x2 = bounds_map[1][0],
                map_y2 = bounds_map[1][1];

            //console.log(
            //    map_x1, map_x2, map_y1, map_y2,
            //    bound_placemark_x, bound_placemark_y,
            //    map_x1 <= bound_placemark_x, map_x2 > bound_placemark_x,
            //    map_x1 <= bound_placemark_x && map_x2 > bound_placemark_x,
            //    map_y1 <= bound_placemark_y, map_y2 > bound_placemark_y,
            //    map_y1 <= bound_placemark_y && map_y2 > bound_placemark_y,
            //    (!(map_x1 <= bound_placemark_x && map_x2 > bound_placemark_x && map_y1 <= bound_placemark_y && map_y2 > bound_placemark_y))
            //);

            if (!(map_x1 <= bound_placemark_x && map_x2 > bound_placemark_x && map_y1 <= bound_placemark_y && map_y2 > bound_placemark_y)) {
                delete_markers.push(e);
            }
        }
    });
    for (var delpm in delete_markers) {
        map_main_page.geoObjects.remove(delete_markers[delpm]);
    }
}
function load_markers() {
    var bounds = map_main_page.getBounds();
    var unbounds = [];
    if (!old_map_x1) {
        old_map_x1 = bounds[0][0];
        old_map_y1 = bounds[0][1];
        old_map_x2 = bounds[1][0];
        old_map_y2 = bounds[1][1];
    } else {
        //console.log('check 1 old: ', old_map_x1, old_map_y1, old_map_x2, old_map_y2)
        //console.log('check 1    : ', bounds[0][0], bounds[0][1], bounds[1][0], bounds[1][1])
        //console.log('test 1    : ', bounds[0][0] >= old_map_x1, bounds[1][0] < old_map_x2, old_map_y1 <= bounds[0][1], old_map_y2 <= bounds[1][1])
        //console.log('ifs 1: ',
        //    bounds[0][0] <= old_map_x1, bounds[0][0] < old_map_x2,
        //    bounds[0][1] >= old_map_y1, bounds[1][1] >= old_map_y2,
        //    (bounds[0][0] <= old_map_x1 && bounds[0][0] < old_map_x2) && (bounds[0][1] >= old_map_y1 && bounds[1][1] >= old_map_y2))
        if ((old_map_x1 >= bounds[0][0] && bounds[0][0] < old_map_x2) && (old_map_y1 <= bounds[0][1] && bounds[1][1] >= old_map_y2)) {
            bounds[1][0] = old_map_x1;
            send_request_markers(bounds);
            bounds = map_main_page.getBounds();

            bounds[0][1] = old_map_y2;
            bounds[0][0] = old_map_x1;
            send_request_markers(bounds);
        } else if (bounds[0][0] <= old_map_x1 && old_map_y1 <= bounds[1][1] && bounds[1][1] <= old_map_y2) {
            bounds[1][1] = old_map_y1;
            send_request_markers(bounds);
            bounds = map_main_page.getBounds();

            bounds[0][1] = old_map_y1;
            bounds[1][0] = old_map_x1;
            send_request_markers(bounds);
        } else if (bounds[0][0] >= old_map_x1 && bounds[1][0] >= old_map_x2 && bounds[0][1] <= old_map_y1 && bounds[1][1] <= old_map_y2) {
            bounds[1][1] = old_map_y1;
            send_request_markers(bounds);
            bounds = map_main_page.getBounds();

            bounds[0][1] = old_map_y1;
            bounds[0][0] = old_map_x2;
            send_request_markers(bounds);
        } else if (bounds[0][0] >= old_map_x1 && bounds[1][0] >= old_map_x2 && bounds[0][1] >= old_map_y1 && bounds[1][1] >= old_map_y2) {
            bounds[0][0] = old_map_x2;
            send_request_markers(bounds);
            //add_rectangel_for_map(bounds, '#7d00ff33', '#0033FF');
            bounds = map_main_page.getBounds();

            bounds[0][1] = old_map_y2;
            bounds[1][0] = old_map_x2;
            send_request_markers(bounds);
            //add_rectangel_for_map(bounds, '#7df90033', '#0022FF');
        } else if (bounds[0][0] >= old_map_x1 && bounds[1][0] <= old_map_x2 && bounds[0][1] >= old_map_y1 && bounds[1][1] <= old_map_y2) {
            bounds = map_main_page.getBounds();
            old_map_x1 = bounds[0][0];
            old_map_y1 = bounds[0][1];
            old_map_x2 = bounds[1][0];
            old_map_y2 = bounds[1][1];
            delete_points_border();

            return true;
        } else {
            unbounds[0] = [];
            unbounds[1] = [];
            unbounds[0][0] = old_map_x1;
            unbounds[0][1] = old_map_y1;
            unbounds[1][0] = old_map_x2;
            unbounds[1][1] = old_map_y2;
            send_request_markers(bounds, unbounds);
        }

        bounds = map_main_page.getBounds();
        old_map_x1 = bounds[0][0];
        old_map_y1 = bounds[0][1];
        old_map_x2 = bounds[1][0];
        old_map_y2 = bounds[1][1];
        //add_rectangel_for_map(bounds, '#AA004433', '#FF0000');
        return true;
    }

    send_request_markers(bounds);
}


function add_rectangel_for_map(bound_rectangle, color, stroke) {


    var myGeoObject = new ymaps.GeoObject({
        // Геометрия = тип геометрии + координаты геообъекта.
        geometry: {
            // Тип геометрии - прямоугольник.
            type: 'Rectangle',
            // Координаты.
            coordinates: bound_rectangle
        },
    }, {
        // Цвет и прозрачность заливки.
        fillColor: color, //'#7df9ff33',
        // Цвет и прозрачность границ.
        strokeColor: stroke, //'#0000FF',
        // Ширина линии.
        strokeWidth: 7
    });

    map_main_page.geoObjects.add(myGeoObject)
}


function closeMap() {
    $("#map_main .close").click(function () {
        $(".mapWrap").removeClass("fixmap");
        $("body").removeClass("bodyHidden");


        $("#map_main").css('height', '120px');
        map_fitToViewport();


        $(this).remove()
    })
}

function map_fitToViewport() {
    //setTimeout(function () {
    map_main_page.container.fitToViewport();
    //}, 200)
}

function check_full_price() {
    $("#eventsbundle_ticket_timeprices").val(prices_array[$("#datetimepicker_reserv_event").val()]['id']);
    $("#eventsbundle_gift_timeprices").val(prices_array[$("#datetimepicker_reserv_event").val()]['id']);
    var tp_price_select = prices_array[$("#datetimepicker_reserv_event").val()]['price'];
    var cnt_tickets = $("#eventsbundle_ticket_cntTickets").val();

    var price_services = 0;
    $(".service_ticket_select").each(function (i, m) {
        var price = $(m).data('price'),
            prop = $(m).prop('checked');

        if (prop && price) {
            price_services += price;
        }
    });

    $(".full_price_ticket").html(tp_price_select * cnt_tickets + price_services);

}

function load_data_pagination($parent, href_load) {

    $.ajax({
        dataType: "json",
        type: 'POST',
        url: href_load,
        beforeSend: function () {
        },
        complete: function () {
            //$(".loader_citys").removeClass("active");
        },
        success: function (response) {
            $parent.find('.load_next_data').remove();
            var data_load = $(response.data_load)

            data_load.each(function (i, m) {
                if ($(m).hasClass('load_next_data')) {
                    $parent.append($(m));
                } else {
                    if ($parent.children('.data_block').prop("tagName") == 'TABLE') {

                        $(m).find('tr').insertBefore($parent.find('.data_block .colors').last())
                    } else {
                        $parent.children('.data_block').append($(m))
                    }
                }
            })
        }
    });
}

function removeDop() {
    $('body').on('click', '.serv .remove', function () {
        $(this).parents('.serv').remove();
    });
}

//function show_popup(id_popup) {
//    $(id_popup).show().animate({opacity: 1}, 300);
//}


function form_ajax($this) {
    var action = $this.attr("action"),
        data = $this.serialize(),
        form_type = "POST",
        id_form = $this.attr("id");
    //
    //console.log("serialize", data, $this.serializeArray());
    //return false;
    if ($this.attr("method") != undefined) {
        form_type = $this.attr("method");
    }

    $.ajax({
        dataType: "json",
        type: form_type,
        url: action,
        data: data,
        beforeSend: function () {
            $this.find(".loader").addClass("active");
        },
        complete: function () {
            $this.find(".loader").removeClass("active");
        },
        success: function (response) {
            $this.find(".error").removeClass('error');
            $this.find(".error-input").remove();
            $this.find(".form_global_errors").empty();
            if (response.status == "success") {
                var id_callback = id_form + "_callback";
                console.log(id_callback, typeof(window[id_callback]))
                if (typeof(window[id_callback]) == "function") {
                    window[id_callback]($this, response);
                }
            } else {
                var fields = response.errors.fields;
                var globals = response.errors.global;
                for (var field in fields) {
                    add_error_field(field, fields[field]);
                }

                if (globals.length) {
                    if (!$("#" + id_form + "_global_errors").length) {
                        $("#" + id_form).append("<div id='" + id_form + "_global_errors' class='form_global_errors'></div>")
                    } else {
                        $("#" + id_form + "_global_errors").empty();
                    }
                    for (var global in globals) {
                        $("#" + id_form + "_global_errors").append('<div class="field_error" ' +
                            'style="color: red; ">' +
                            globals[global] +
                            '</div>');
                    }
                }
            }
        }
    });
}

function form_with_dropzone($this) {
    var action = $this.attr("action"),
        data = $this.serialize(),
        form_type = "POST",
        id_form = $this.attr("id"),
        processFiles = [];
    //
    //console.log("serialize", data, $this.serializeArray());
    //return false;
    if ($this.attr("method") != undefined) {
        form_type = $this.attr("method");
    }


    $("#" + id_form).dropzone(
        {
            url: action,
            method: form_type,
            maxFiles: 20,
            clickable: false,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictRemoveFile: "Удалить",
            dictCancelUpload: "Отменить",
            // The configuration we've talked about above
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100,
            complete: function (file) {
                $this.find(".loader").removeClass("active");
            },
            init: function () {
                var myDropzone = this;

                // First change the button to actually tell Dropzone to process the queue.
                $this.find("input[type=submit]").on("click", function (e) {
                    // Make sure that the form isn't actually being sent.
                    processFiles = myDropzone.getQueuedFiles();

                    e.preventDefault();
                    e.stopPropagation();


                    var action = $this.attr("action"),
                        data = $this.serialize(),
                        form_type = "POST",
                        id_form = $this.attr("id");

                    if ($this.attr("method") != undefined) {
                        form_type = $this.attr("method");
                    }

                    $.ajax({
                        dataType: "json",
                        type: form_type,
                        url: action,
                        data: data,
                        beforeSend: function () {
                            $this.find(".loader").addClass("active");
                        },
                        complete: function () {
                        },
                        success: function (response) {
                            $this.find(".error").removeClass('error');
                            $this.find(".error-input").remove();
                            if (response.status == "success") {
                                $this.attr('action', response.next_url);
                                myDropzone.options.url = response.next_url;

                                if (myDropzone.getQueuedFiles().length > 0) {
                                    $this.find(".loader").addClass("active");

                                    myDropzone.processQueue();
                                } else {
                                    form_ajax($this);
                                }
                            } else {
                                $this.find(".loader").removeClass("active");
                                var fields = response.errors.fields;
                                var globals = response.errors.global;
                                for (var field in fields) {
                                    add_error_field(field, fields[field]);
                                }

                                if (globals.length) {
                                    if (!$("#" + id_form + "_global_errors").length) {
                                        $("#" + id_form).append("<div id='" + id_form + "_global_errors' class='form_global_errors'></div>")
                                    } else {
                                        $("#" + id_form + "_global_errors").empty();
                                    }
                                    for (var global in globals) {
                                        $("#" + id_form + "_global_errors").append('<div class="field_error" ' +
                                            'style="color: red; ">' +
                                            globals[global] +
                                            '</div>');
                                    }
                                }
                            }
                        }
                    });
                });

                // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
                // of the sending event because uploadMultiple is set to true.
                //this.on("sendingmultiple", function () {
                //    console.log('sendingmultiple myDropzone.getQueuedFiles()', myDropzone.getQueuedFiles())
                //    console.log("sendingmultiple")
                //    // Gets triggered when the form is actually being sent.
                //    // Hide the success button or the complete form.
                //});
                //this.on("successmultiple", function (files, response) {
                //    console.log("successmultiple")
                //    console.log('successmultiple myDropzone.getQueuedFiles()', myDropzone.getQueuedFiles())
                //    // Gets triggered when the files have successfully been sent.
                //    // Redirect user or notify of success.
                //});
                //this.on("errormultiple", function (files, response) {
                //    console.log('errormultiple myDropzone.getQueuedFiles()', myDropzone.getQueuedFiles())
                //    console.log("errormultiple")
                //    // Gets triggered when there was an error sending the files.
                //    // Maybe show form again, and notify user of error
                //});
                this.on("addedfile", function (files, response) {

                    if ($('#preview-wrapper_' + id_form).length) {
                        $("#preview-wrapper_" + id_form).append($(files.previewElement));
                    }
                    return false;
                    // Gets triggered when there was an error sending the files.
                    // Maybe show form again, and notify user of error
                });
                this.on("success", function (file, response) {
                    $this.find(".error").removeClass('error');
                    $this.find(".error-input").remove();
                    response = JSON.parse(response);

                    if (response.status == "success") {
                        var id_callback = id_form + "_callback";

                        if (typeof(window[id_callback]) == "function") {
                            window[id_callback]($this, response);
                        }
                    } else {
                        var fields = response.errors.fields;
                        var globals = response.errors.global;
                        for (var field in fields) {
                            add_error_field(field, fields[field]);
                        }

                        if (globals.length) {
                            if (!$("#" + id_form + "_global_errors").length) {
                                $("#" + id_form).append("<div id='" + id_form + "_global_errors' class='form_global_errors'></div>")
                            } else {
                                $("#" + id_form + "_global_errors").empty();
                            }
                            for (var global in globals) {
                                $("#" + id_form + "_global_errors").append('<div class="field_error" ' +
                                    'style="color: red; ">' +
                                    globals[global] +
                                    '</div>');
                            }
                        }
                        $this.find(".loader").removeClass("active");
                    }
                });
            }
        });

}

function ajax_send_event() {
    window.clearTimeout(timer_ajax_search_event);

    timer_ajax_search_event = window.setTimeout(function () {
        var form = $("#appbundle_event_search_form");
        form_ajax(form);
    }, 1500)
}

function add_error_field(field_id, error) {

    //if (error == "Неправильно введена капча.") {
    //    $.ajax({
    //        dataType: "html",
    //        type: 'POST',
    //        url: Routing.generate('get_captcha'),
    //        success: function (response) {
    //            $("#recaptcha_show").html($(response).find("#recaptcha_show").html())
    //        }
    //    });
    //}
    $("#" + field_id).addClass('error');
    $("#" + field_id).closest(".box-label-input").append('<div id="' + field_id + '_error" class="error-input">' +
        error +
        '</div>')
}

function show_popup(href, effectIn) {
    effectIn = effectIn || "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: href
    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}

// Создание метки
function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
        iconContent: 'поиск...'
    }, {
        preset: 'islands#violetStretchyIcon',
        draggable: true
    });
}

// Определяем адрес по координатам (обратное геокодирование)
function getAddress(coords, myPlacemark) {
    myPlacemark.properties.set('iconContent', 'поиск...');
    ymaps.geocode(coords).then(function (res) {
        var firstGeoObject = res.geoObjects.get(0);

        myPlacemark.properties
            .set({
                iconContent: firstGeoObject.properties.get('name'),
                balloonContent: firstGeoObject.properties.get('text')
            });
    });
}
