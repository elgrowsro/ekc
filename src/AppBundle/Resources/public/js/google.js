 //Google
 google.maps.event.addDomListener(window, 'load', init);

function init() {
        var image = new google.maps.MarkerImage('../img/mapMarker.png',  
        new google.maps.Size(49, 49),
        new google.maps.Point(0,0),
        new google.maps.Point(24, 24));
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(56.89825197570902, 60.81831693649292)
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        
        var markers = [];
        $('.markers > div').each(function(){
            var $t = $(this);           
            x = $(this).attr('x'); var x = parseFloat(x);
            y = $(this).attr('y'); var y = parseFloat(y);
            pos = new google.maps.LatLng(x, y);
           var marker = new google.maps.Marker({
              map: map,
              position: pos,
              animation: google.maps.Animation.DROP,
              icon: $(this).attr('marker')
           });
            markers.push(marker);
           var contentString = $(this).find('.hideContent').html();
           var infowindow = new google.maps.InfoWindow({
              content: contentString
           });
           marker.addListener('click', function() {
             infowindow.open(map, marker);
           });
        });
        $('.markers > div').click(function(){
            var contentString = $(this).find('.hideContent').html();
            var infowindow = new google.maps.InfoWindow({
              content: contentString
           });
            var index = $(this).index();
            infowindow.open(map, markers[index]);
            var fullWidth = $(window).width();
            if (fullWidth <= 870){
               $('#map').addClass('openMap');
            }
        });
        $('#map').click(function(){
            var fullWidth = $(window).width();
            if (fullWidth <= 870){
                $('#map').removeClass('openMap');
            }
        });

}