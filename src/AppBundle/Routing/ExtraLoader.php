<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19.07.16
 * Time: 10:58
 */

namespace AppBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ExtraLoader extends Loader
{
    private $loaded = false;

    public function load($resource, $type = null)
    {
        $request = new Request();

        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        // prepare a new route
        $path = '/{sort}/{direction}/';
//        $path = '/';
        $defaults = [
            '_controller' => 'AppBundle:Default:index',
            'sort' => 'tickets',
            'direction' => 'desc',
        ];
//        var_dump($defaults);


        $requirements = [];
        $requirements = [
            'sort' => '(rating|tickets|create)?',
            'direction' => '(desc|asc)?',
        ];
        $route = new Route($path, $defaults, $requirements);

        // add the new route to the route collection
        $routeName = 'homepage';
        $routes->add($routeName, $route);

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'extra' === $type;
    }
}