<?php

namespace AppBundle\Block\Service;


use AppBundle\Entity\City;
use AppBundle\Service\Geo;
use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\CoreBundle\Model\Metadata;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HeaderMenuBlock extends BaseBlockService
{
    /** @var  Geo */
    public $geo;
    public $em;
    private $request;

//    public function setRequest(Request $request)
//    {
//
//        $this->request = $request;
//    }

    public function __construct($name, EngineInterface $templating, Geo $geo, EntityManager $em, Request $request)
    {
        $this->$name = $name;
        $this->templating = $templating;
        $this->em = $em;
        $this->geo = $geo;
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'template' => 'AppBundle:misc:header_base.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $cookies = $this->request->cookies;

        // merge settings
        $settings = $blockContext->getSettings();

        /** @var City $city_from_db */
        $city_from_db = $this->geo->getCity();
        $set_cookie = $this->geo->in_cookie;


        $city_from_db_all = $this->em->getRepository('AppBundle:City')->getActiveCity();

        $all_events_on_city_repo = $this->em->getRepository('EventsBundle:Event');

        $queryBuilder = $all_events_on_city_repo->createQueryBuilder('event');
        $queryBuilder
            ->select('count(event.id)')
            ->innerJoin('event.company', 'company')
            ->where('event.isActive = true ')
            ->andWhere('event.city = :city')
            ->setParameter(':city', $city_from_db);

        $all_events_on_city = $queryBuilder
            ->getQuery()
            ->getSingleScalarResult();

        $array_citys_by_first_word = [];

        $cnt_city = 0;

        /** @var City $item */
        foreach ($city_from_db_all as $item) {
            $word = mb_substr(mb_strtoupper($item->getName(), "UTF-8"), 0, 1, "utf-8");

            $array_citys_by_first_word[$word][] = $item;
            $cnt_city++;
        }
        ksort($array_citys_by_first_word);
//        print_r(array_keys($array_citys_by_first_word));

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
            'citys_all' => $array_citys_by_first_word,
            'city_from_db' => $city_from_db,
            'cnt_city' => ceil($cnt_city / 6),
            'set_cookie' => $set_cookie,
            'all_events_on_city' => $all_events_on_city,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockMetadata($code = null)
    {
        return new Metadata($this->getName(), (!is_null($code) ? $code : $this->getName()), false, 'SonataBlockBundle', array(
            'class' => 'fa fa-rss-square',
        ));
    }
}