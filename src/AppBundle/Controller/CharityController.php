<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.08.16
 * Time: 17:32
 */

namespace AppBundle\Controller;

use AppBundle\Form\Type\SearchEventCharityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CharityController extends Controller
{
    public function indexAction(Request $request, $sort = 'rating', $direction = 'desc', $page)
    {
        $form_search = $this->createForm(SearchEventCharityType::class, null, [
            'action' => $this->generateUrl('charity'),
            'method' => 'GET'
        ]);
        $form_search->get('charity')->setData(true);


        $form_search->handleRequest($request);


        $response_data_events = $this->forward('AppBundle:Event:render', array(
            'data' => $request->query->get('appbundle_event_search'),
            'page' => $page,
            'sort' => $sort,
            'direction' => $direction,
            'charity' => true
        ));

        if ($request->isXmlHttpRequest()) {
            return new Response($response_data_events->getContent());
        } else {
            return $this->render('AppBundle:default:index.html.twig', [
                'direction' => $direction,
                'sort' => $sort,
                'form_search' => $form_search->createView(),
                'content_event' => $response_data_events->getContent(),
                'charity_search' => true,
            ]);
        }
    }

    public function showSumAction()
    {
        $repo_charity = $this->getDoctrine()->getRepository('AppBundle:Charity');

        $sum = $repo_charity->sumMoney();

        return new Response(number_format($sum, 2, '.', ' '));
    }
}