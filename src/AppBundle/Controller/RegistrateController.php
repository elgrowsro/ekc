<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.04.16
 * Time: 17:10
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sonata\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrateController extends Controller
{
    public function indexAction(Request $request)
    {
        $user = $this->getUser();

        if ($user instanceof UserInterface && $user->getCompany()) {
//            $this->container->get('session')->getFlashBag()->set('sonata_user_error', 'sonata_user_already_authenticated');
            $url = $this->generateUrl('personal_area_company.about');

            return new RedirectResponse($url);
        }


        return $this->render('AppBundle:registrate:index.html.twig', [
        ]);
    }
}