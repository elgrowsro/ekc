<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 29.08.16
 * Time: 17:32
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\SearchEventCharityType;
use EventsBundle\Entity\Donations;
use EventsBundle\Form\DonationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DonationController extends Controller
{
    public function indexAction(Request $request, $url)
    {
        $em = $this->get('em');
        $event = $em->getRepository('EventsBundle:Event')->findOneBy(['url' => $url, 'charity' => true, 'isActive' => true]);
        if (!$event) {
            throw $this->createNotFoundException();
        }

        $donation_class = new Donations();


        $form_donate = $this->createForm(DonationType::class, $donation_class, [
            'validation_groups' => ['donation_event'],
        ]);

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_donate->handleRequest($request);
            $set_money = $form_donate->get('money')->getData();
            if ($set_money > $user->getMoney()) {
                $form_donate->addError(new FormError('Сумма пожертвования не должна привышать Ваш баланс'));
            }

            if ($form_donate->isValid()) {

                $donation_class->setEvent($event);
                $donation_class->setUser($user);
                $user->setMoney($user->getMoney() - $set_money);
                /** @var User $user_event */
                $user_event = $event->getCompany()->getUsercompany();
                $user_event->setWithdrawMoney($user_event->getWithdrawMoney() + $set_money);

                $em->persist($donation_class);
                $em->persist($user);
                $em->persist($user_event);
                $em->flush();


                $message_array = [
                    '{{ flname }}' => $user_event->getFLName(),
                    '{{ flname_donate }}' => $user->getFLName(),
                    '{{ sum }}' => $set_money,
                    '{{ event_name }}' => $event->getName(),
                    '{{ link_donate }}' => $this->generateUrl('profiles_view', ['url' => $user->getUrl()], true),
                    '{{ event_link }}' => $this->generateUrl('events_view.charity', ['url' => $event->getUrl()], true),
                ];

                $this->get('app.emailer')->sendMailToUser(6, $message_array, $user_event->getEmail());

                return new Response(json_encode(array(
                    'status' => 'success',
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_donate, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }
}