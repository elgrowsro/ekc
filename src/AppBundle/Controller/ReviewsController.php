<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 30.06.16
 * Time: 14:18
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use CompanysBundle\Form\CommentCompanyType;
use EventsBundle\Form\CommentEventType;
use FeedBundle\Form\CommentFeedType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReviewsController extends Controller
{
    public function loadAction(Request $request, $event_id = null, $user_id = null, $company_id = null, $page)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Comment');

        $queryBuld = $repository->createQueryBuilder('c');
//            ->select(["c"])
        $queryBuld
            ->select(["c", "images", "galleryHasMedias", "media"])
            ->leftJoin('c.images', 'images')
            ->leftJoin('images.galleryHasMedias', 'galleryHasMedias')
            ->leftJoin('galleryHasMedias.media', 'media')
            ->andWhere('c.isActive = true')
            ->orderBy('c.id', 'DESC');


        if ($event_id) {
            $queryBuld
                ->andWhere('c.event = :event_id')
                ->setParameter('event_id', $event_id);
        }
        if ($user_id) {
            $queryBuld
                ->andWhere('c.user = :user_id')
                ->setParameter('user_id', $user_id);
        }
        if ($company_id) {
            $queryBuld
                ->andWhere('c.company = :company_id')
                ->setParameter('company_id', $company_id);
        }

        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_comments */
        $pagination_comments = $paginator->paginate(
            $queryBuld,
            $page,
            10
        );

        if ($event_id) {
            $pagination_comments->setUsedRoute('events_load_comments');
        } elseif ($user_id) {
            $pagination_comments->setUsedRoute('user_load_comments');
        } elseif ($company_id) {
            $pagination_comments->setUsedRoute('company_load_comments');
        }


        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('AppBundle:comments:load.html.twig', [
                'comments' => $pagination_comments,
                'event_id' => $event_id,
                'user_id' => $user_id,
                'company_id' => $company_id,
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('AppBundle:comments:load.html.twig', [
                'comments' => $pagination_comments,
                'event_id' => $event_id,
                'user_id' => $user_id,
                'company_id' => $company_id,
            ]);
            return $events_render;
        }
    }

    public function requestRemoveAction(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createAccessDeniedException('Нужно быть авторизованным');
        }
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('AppBundle:Comment')->find($id);

        if (!$comment) {
            throw new NotFoundHttpException("Page not found");
        }

        if (!$comment->getRequestRemove()) {
            $comment->setRequestRemove(true);

            $em->persist($comment);
            $em->flush();

            $adminCode = 'app.admin.comment';

            $sonata_admin = $this->container->get('sonata.admin.pool')->getAdminByAdminCode($adminCode);

            $sonata_admin_edit_object = $sonata_admin->generateObjectUrl('edit', $comment, [], true);


            $message_array = [
                '{{ FLName }}' => $user->getFLName(),
                '{{ link }}' => $sonata_admin_edit_object,
            ];

            $this->get('app.emailer')->sendMailToAdmin(1, $message_array);
        }

        return new Response(json_encode(array(
            'status' => 'success',
//            'sonata_admin_edit_object' => $sonata_admin_edit_object,
        )));
    }

    public function addAction(Request $request)
    {
        $comment_class = new Comment();


        if ($request->request->has('eventbundle_comment')) {
            $form_question = $this->createForm(CommentEventType::class, $comment_class, [
                'validation_groups' => ['create_comment_event'],
            ]);
        }

        if ($request->request->has('profilebundle_comment')) {
            $form_question = $this->createForm(CommentCompanyType::class, $comment_class, [
                'validation_groups' => ['create_comment_company'],
            ]);
        }

        if ($request->request->has('feedbundle_comment')) {
            $form_question = $this->createForm(CommentFeedType::class, $comment_class, [
                'validation_groups' => ['create_comment_event'],
            ]);
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {
                $em = $this->get('em');

                $comment_class->setUser($user);
                if (!$comment_class->getCompany()) {
                    $comment_class->setCompany($comment_class->getEvent()->getCompany());
                }


                $uploadedFile = $request->files->get('file');

                if (count($uploadedFile)) {
                    $mediaManager = $this->container->get('sonata.media.manager.media');
                    $mediaGalleryManager = $this->get('sonata.media.manager.gallery');

                    /** @var Gallery $gallery */
                    $gallery = $mediaGalleryManager->create();
                    $gallery->setContext('Comments_gallery');
                    $gallery->setDefaultFormat('Comments_gallery_medium');
                    $gallery->setName('Картинки для комментария');
                    $gallery->setEnabled(true);

                    /** @var UploadedFile $item */
                    foreach ($uploadedFile as $item) {
                        /** @var Media $media */
                        $media = $mediaManager->create();
                        $media->setContext('Comments_gallery');

                        $ext = $item->getClientOriginalExtension();
                        $name = $item->getClientOriginalName();

                        switch ($ext) {
                            case "png";
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/png/';
                                break;
                            case "jpg";
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpg/';
                                break;
                            case "jpeg";
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/jpeg/';
                                break;
                            default:
                                $directory = $this->get('kernel')->getRootDir() . '/../web/uploads/tmp/files/';
                        }

                        $file = $item->move($directory, $name);

                        $media->setProviderName('sonata.media.provider.image');
                        $media->setBinaryContent($directory . $name);
                        $media->setAuthor($user);

                        $mediaManager->save($media);
                        $galleryHasMedia = new GalleryHasMedia();
                        $galleryHasMedia->setMedia($media);
                        $galleryHasMedia->setGallery($gallery);
                        $galleryHasMedia->setEnabled(true);
                        $gallery->addGalleryHasMedias($galleryHasMedia);

                        if ($request->request->has('eventbundle_comment')) {
                            $gallery_event = $comment_class->getEvent()->getImages();

                            $galleryHasMediaEvent = new GalleryHasMedia();
                            $galleryHasMediaEvent->setMedia($media);
                            $galleryHasMediaEvent->setGallery($gallery_event);
                            $galleryHasMediaEvent->setEnabled(true);
                            $gallery_event->addGalleryHasMedias($galleryHasMediaEvent);
                            $mediaGalleryManager->save($gallery_event);
                        }
                    }
                    $mediaGalleryManager->save($gallery);
                    $comment_class->setImages($gallery);
                }

                $em->persist($comment_class);
                $em->flush();

                if ($request->request->has('eventbundle_comment')) {
                    $response_data = $this->forward('AppBundle:Reviews:load', array(
                        'page' => 1,
                        'event_id' => $comment_class->getEvent()->getId(),
                    ));
                }
                if ($request->request->has('profilebundle_comment')) {
                    $response_data = $this->forward('AppBundle:Reviews:load', array(
                        'page' => 1,
                        'company_id' => $comment_class->getCompany()->getId(),
                    ));
                }


                if ($request->request->has('feedbundle_comment')) {
                    $feed_data = $request->request->get('feedbundle_comment');
                    if (isset($feed_data['feed'])) {
                        $feed_id = (int)$feed_data['feed'];
                        $feed_obj = $em->find('FeedBundle:Feed', $feed_id);

                        $em->remove($feed_obj);
                        $em->flush();
                    }
                    return new Response(json_encode(array(
                        'status' => 'success',
                        'comment' => $feed_data
                    )));
                }

                return new Response(json_encode(array(
                    'status' => 'success',
                    'content' => $response_data->getContent(),
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }

    public function checkAction(Request $request)
    {
        $comment_class = new Comment();


        if ($request->request->has('eventbundle_comment')) {
            $form_question = $this->createForm(CommentEventType::class, $comment_class, [
                'validation_groups' => ['create_comment_event'],
            ]);
        }
        if ($request->request->has('profilebundle_comment')) {
            $form_question = $this->createForm(CommentCompanyType::class, $comment_class, [
                'validation_groups' => ['create_comment_company'],
            ]);
        }
        if ($request->request->has('feedbundle_comment')) {
            $form_question = $this->createForm(CommentFeedType::class, $comment_class, [
                'validation_groups' => ['create_comment_event'],
            ]);
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isMethod('POST') && $user) {
            $form_question->handleRequest($request);

            if ($form_question->isValid()) {

                $next_url_form = $this->generateUrl('create_comment');

                return new Response(json_encode(array(
                    'status' => 'success',
                    'next_url' => $next_url_form,
                )));

            } else {
                $errors = $this->get('form_serializer')->serializeFormErrors($form_question, true, true);

                return new Response(json_encode(array(
                    'status' => 'error',
                    'errors' => $errors
                )));
            }
        }
    }
}