<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\SearchEventType;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function setCityAction(Request $request)
    {
//        print_r([1, 2, 3]);
//        return $this->redirect($this->generateUrl('registrate_homepage'));

        $id = $request->request->get('city');

        $response = new Response(json_encode(array(
            'status' => 'success'
        )));

        $response->headers->setCookie(new Cookie('user_city', $id));
//        new Cookie('user_city', $id, 0, '/', null, false, false);


        return $response;
    }

    public function indexAction(Request $request, $sort = 'rating', $direction = 'desc', $page)
    {
        $form_search = $this->createForm(SearchEventType::class, null, [
            'action' => $this->generateUrl('homepage'),
            'method' => 'GET'
        ]);
        $form_search->handleRequest($request);


        $response_data_events = $this->forward('AppBundle:Event:render', array(
            'data' => $request->query->get('appbundle_event_search'),
            'page' => $page,
            'sort' => $sort,
            'direction' => $direction,
            'charity' => false
        ));

        if ($request->isXmlHttpRequest()) {
            return new Response($response_data_events->getContent());
        } else {
            return $this->render('AppBundle:default:index.html.twig', [
                'direction' => $direction,
                'sort' => $sort,
                'form_search' => $form_search->createView(),
                'content_event' => $response_data_events->getContent(),
                'charity_search' => false,
            ]);
        }
    }

    public function renderModalsAction()
    {
        // replace this example code with whatever you need
        return $this->render(':default:modals.html.twig', [
        ]);
    }

    public function checkerrorAction()
    {
        return $this->redirect($this->generateUrl('_twig_error_test', [
            'code' => 404,
            '_format' => 'html'
        ]));
    }

    public function renderMenuAction($data_search = [])
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Menu');

        $data_repo = $repo->childrenHierarchy();

        $menu_array = [];
        if (isset($data_search['appbundle_event_search']) && isset($data_search['appbundle_event_search']['menu'])) {
            $menu_array = explode(',', $data_search['appbundle_event_search']['menu']);
        }

        return $this->render('AppBundle:misc:menu.html.twig', [
            'data' => $data_repo,
            'menu' => $menu_array,
        ]);
    }
}

