<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 14.07.16
 * Time: 18:04
 */

namespace AppBundle\Controller;

use AdvertBundle\Entity\Advert;
use AdvertBundle\Entity\Places;
use AppBundle\Entity\Menu;
use Doctrine\ORM\Query;
use EventsBundle\Entity\Event;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EventController extends Controller
{
    const cntViewPage = 12;

    public function renderAction(Request $request, $data = [], $page, $event = null, $sort = 'rating', $direction = 'desc', $charity = null)
    {
        $eventsRepo = $this->getDoctrine()->getRepository('EventsBundle:Event');
        $city = $this->get('ipgeobase');
        $page = $request->query->get('page') ? $request->query->get('page') : $page;


        $queryBuilder = $eventsRepo->createQueryBuilder('event');
        $queryBuilder->addSelect(['getTimePrice', 'charity', 'main_image', 'event.cnt_buy_tickets as HIDDEN tickets', 'event.avg_rating as HIDDEN rating', 'event.create_event as HIDDEN create'])
            ->innerJoin('event.company', 'company')
            ->leftJoin('event.main_image', 'main_image')
            ->leftJoin('event.getTimePrice', 'getTimePrice')
            ->leftJoin('getTimePrice.charitys', 'charity');
        $queryBuilder->where('event.isActive = true');
        $queryBuilder->andWhere('event.city = :city');

        if ($request->request->get('bounds')) {
            $for_map = true;
            $bounds = $request->request->get('bounds');
            $x1 = $bounds[0][0];
            $y1 = $bounds[0][1];
            $x2 = $bounds[1][0];
            $y2 = $bounds[1][1];

            $queryBuilder->andWhere(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->gte('event.lat', $x1),
                    $queryBuilder->expr()->lt('event.lat', $x2),
                    $queryBuilder->expr()->gte('event.lng', $y1),
                    $queryBuilder->expr()->lt('event.lng', $y2)
                )
            );
            if ($request->request->get('unbounds')) {
                $un_bounds = $request->request->get('unbounds');
                $un_x1 = $un_bounds[0][0];
                $un_y1 = $un_bounds[0][1];
                $un_x2 = $un_bounds[1][0];
                $un_y2 = $un_bounds[1][1];

                $queryBuilder->andWhere(
                    $queryBuilder->expr()->not(
                        $queryBuilder->expr()->andX(
                            $queryBuilder->expr()->gte('event.lat', $un_x1),
                            $queryBuilder->expr()->lt('event.lat', $un_x2),
                            $queryBuilder->expr()->gte('event.lng', $un_y1),
                            $queryBuilder->expr()->lt('event.lng', $un_y2)
                        )
                    )
                );
            }
        } else {
            $for_map = false;
        }

        if (isset($_GET['event'])) {
            $event = (int)$_GET['event'];
        }
        if (isset($event)) {
            $queryBuilder->andWhere('event.id=:event_id')
                ->setParameter(':event_id', $event);
        }

        $routeName = $request->get('_route');
        if ($routeName == 'homepage.events.load' || $routeName == 'charity.events.load') {
            $data = $request->query->get('appbundle_event_search');
        }

        if ($charity === true) {
            $queryBuilder->andWhere('event.charity = true');
        } elseif ($charity === false) {
            $queryBuilder->andWhere('event.charity = false');
        }

        if (count($data)) {
            $form_data = $data;
            $for_woman = isset($form_data['forWoman']) ? $form_data['forWoman'] : false;
            $for_man = isset($form_data['forMan']) ? $form_data['forMan'] : false;
            $for_elderly = isset($form_data['forElderly']) ? $form_data['forElderly'] : false;

            if ($charity === null) {
                $charity = isset($form_data['charity']) ? $form_data['charity'] : false;
            }

            $organizer_city = isset($form_data['organizer_city']) ? $form_data['organizer_city'] : false;
            $organizer_company = isset($form_data['organizer_company']) ? $form_data['organizer_company'] : false;
            $organizer_man = isset($form_data['organizer_man']) ? $form_data['organizer_man'] : false;

            $period_from = isset($form_data['period_from']) ? $form_data['period_from'] : false;
            $period_to = isset($form_data['period_to']) ? $form_data['period_to'] : false;
            $menu = (isset($form_data['menu']) && trim($form_data['menu'])) ? explode(',', $form_data['menu']) : false;

            if ($menu) {
                $repo = $this->getDoctrine()->getRepository('AppBundle:Menu');
                $array_lvl_menu = [];
                $delete_lvl_menu = [];
                $diff_lvl_menu = [];
                $result_menu_id = [];

                $node = $repo->createQueryBuilder('menu');
                $node->where($node->expr()->in('menu.id', $menu));
                $node->orderBy('menu.lvl', 'desc');

                $node_result = $node->getQuery()->getResult();
                /** @var Menu $item */
                foreach ($node_result as $item) {
//                    $item->getId();
                    $array_lvl_menu[$item->getLvl()][$item->getId()] = $item;

                    if ($item->getParent()) {
                        $delete_lvl_menu[$item->getLvl() - 1][$item->getParent()->getId()] = $item->getId();
                    }
                }

                foreach ($array_lvl_menu as $key_lvl => $items) {
                    if (isset($delete_lvl_menu[$key_lvl])) {
                        $diff_lvl_menu[$key_lvl] = array_diff_key($items, $delete_lvl_menu[$key_lvl]);
                    } else {
                        $diff_lvl_menu[$key_lvl] = $items;
                    }
                }

                foreach ($diff_lvl_menu as $key_lvl => $items_menu) {
                    foreach ($items_menu as $item) {
                        if ($key_lvl == 0) {
                            foreach ($item->getChildren() as $item_lvl1) {
                                foreach ($item_lvl1->getChildren() as $item_lvl2) {
                                    $result_menu_id[] = $item_lvl2->getId();
                                }
                            }
                        } elseif ($key_lvl == 1) {
                            foreach ($item->getChildren() as $item_lvl2) {
                                $result_menu_id[] = $item_lvl2->getId();
                            }
                        } else {
                            $result_menu_id[] = $item->getId();
                        }
                    }
                }

//                var_dump($result_menu_id);
                if (count($result_menu_id)) {
                    $queryBuilder->leftJoin('event.getMenu3', 'getMenu3');
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('getMenu3', $result_menu_id)
                    );
                }
            }

            if ($charity !== null) {
                if ($charity) {
                    $queryBuilder->andWhere('event.charity = true');
                } else {
                    $queryBuilder->andWhere('event.charity = false');
                }
            }

            if ($for_man && $for_woman && $for_elderly) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->eq('event.forMan', 'true'),
                        $queryBuilder->expr()->eq('event.forWoman', 'true'),
                        $queryBuilder->expr()->eq('event.forElderly', 'true')
                    )
                );
            } elseif (($for_man && $for_woman) || ($for_woman && $for_elderly) || ($for_man && $for_elderly)) {
                if ($for_man && $for_woman) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->eq('event.forMan', 'true'),
                            $queryBuilder->expr()->eq('event.forWoman', 'true')
                        )
                    );
                } elseif ($for_woman && $for_elderly) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->eq('event.forWoman', 'true'),
                            $queryBuilder->expr()->eq('event.forElderly', 'true')
                        )
                    );
                } elseif ($for_man && $for_elderly) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->eq('event.forMan', 'true'),
                            $queryBuilder->expr()->eq('event.forElderly', 'true')
                        )
                    );
                }
            } elseif ($for_man || $for_woman || $for_elderly) {
                if ($for_man) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->eq('event.forMan', 'true')
                    );
                } elseif ($for_woman) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->eq('event.forWoman', 'true')
                    );
                } elseif ($for_elderly) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->eq('event.forElderly', 'true')
                    );
                }
            }

            if ($organizer_city && $organizer_company && $organizer_man) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->in('company.typeCompany', [1, 2, 3, 4, 5])
                );
            } elseif (($organizer_city && $organizer_company) || ($organizer_company && $organizer_man) || ($organizer_city && $organizer_man)) {
                if ($organizer_city && $organizer_company) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('company.typeCompany', [1, 2, 3, 4])
                    );
                } elseif ($organizer_company && $organizer_man) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('company.typeCompany', [4, 5])
                    );
                } elseif ($organizer_city && $organizer_man) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('company.typeCompany', [1, 2, 3, 5])
                    );
                }
            } elseif ($organizer_city || $organizer_company || $organizer_man) {
                if ($organizer_city) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('company.typeCompany', [1, 2, 3])
                    );
                } elseif ($organizer_company) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('company.typeCompany', [4])
                    );
                } elseif ($organizer_man) {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->in('company.typeCompany', [5])
                    );
                }
            }

            if ($period_from && $period_to) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->gte('getTimePrice.date_event', ':date_from'),
                                $queryBuilder->expr()->lte('getTimePrice.date_event', ':date_to')
                            ),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->gte('getTimePrice.date_event_end', ':date_from'),
                                $queryBuilder->expr()->lte('getTimePrice.date_event_end', ':date_to')
                            ),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->gte('getTimePrice.date_event', ':date_from'),
                                $queryBuilder->expr()->lte('getTimePrice.date_event_end', ':date_to')
                            )
                        ),
                        $queryBuilder->expr()->gte('getTimePrice.date_event_end', 'CURRENT_TIMESTAMP()')
                    )
                )->setParameters(array(
                    'date_from' => $period_from,
                    'date_to' => $period_to,
                ));
            } elseif ($period_from) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->gte('getTimePrice.date_event', ':date_from'),
                        $queryBuilder->expr()->gte('getTimePrice.date_event_end', 'CURRENT_TIMESTAMP()')
                    )
                )->setParameters(array(
                    'date_from' => $period_from,
                ));
            } elseif ($period_to) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->lte('getTimePrice.date_event_end', ':date_to'),
                        $queryBuilder->expr()->gte('getTimePrice.date_event_end', 'CURRENT_TIMESTAMP()')
                    )
                )->setParameters(array(
                    'date_to' => $period_to,
                ));
            }
        }

        $queryBuilder->addOrderBy('event.id', 'DESC');
        $queryBuilder->setParameter(':city', $city->getCity());

//        $query = $queryBuilder->getQuery();
//        $sql = $query->getSQL();
//        print_r($sql);

        if ($for_map) {
            $data_events_all = $queryBuilder->getQuery()->getResult();
            $result_markers = [];

            $pr = $this->container->get('sonata.media.provider.image');
            $im = $this->get("snowcap_im.manager");


            /** @var Event $item_event */
            foreach ($data_events_all as $key_item => $item_event) {
//                print_r($item_event);
                $result_markers[$key_item]['lat'] = $item_event->getlat();
                $result_markers[$key_item]['lng'] = $item_event->getlng();
                $result_markers[$key_item]['name'] = $item_event->getname();

                if (isset($event)) {
                    $result_markers[$key_item]['path_way'] = array_map(function ($value) {
                        return explode(',', $value);
                    }, explode('),(', trim($item_event->getPathWay(), "()")));
                }
                $result_markers[$key_item]['avgRating'] = "";
                for ($i = 0; $i < $item_event->getAvgRating(); $i++) {
                    $result_markers[$key_item]['avgRating'] .= "<div></div>";
                }
                $result_markers[$key_item]['url'] = $this->generateUrl('events_view', ['url' => $item_event->geturl()]);

                $minPrice = $item_event->getMinPrice();
                $result_markers[$key_item]['minPrice'] = $minPrice ? $minPrice . "Р" : "";

                $main_image = $item_event->getMainImage();

                if ($main_image) {
                    $format = $pr->getFormatName($main_image, 'formap');
                    $path = $pr->generatePublicUrl($main_image, $format);
                    $im->convertRoundMap('event_icon_map', $path);

                    $icon = $im->getUrl('event_icon_map', $path);
                    $result_markers[$key_item]['icon'] = $icon;

                    $result_markers[$key_item]['mainImage'] = $path;
                } else {
                    $result_markers[$key_item]['mainImage'] = '/bundles/app/img/common_event_map.png';
                    $result_markers[$key_item]['icon'] = 'bundles/app/img/loader.png';
                }
            }

            return new Response(json_encode([
                'status' => 'success',
                'data' => $result_markers
            ]));
        } else {
            $result_view_advert = [];
            if (!isset($_GET['sort'])) {
                $session = $request->getSession();

                $update_cnt_ad = $session->get('update_cnt_ad');
                if (!$update_cnt_ad) {
                    $update_cnt_ad = new \DateTime('now');
                    $session->set('update_cnt_ad', $update_cnt_ad->format('Y-m-d'));
                } else {
                    $update_cnt_ad = new \DateTime($update_cnt_ad);
                }

                $now_day = new \DateTime('now');
                $dDiff = $update_cnt_ad->diff($now_day);
                $days_diff = $dDiff->days;

                if ($days_diff) {
                    $session->set('view_ad', []);
                    $session->set('closed_ad', []);
                }


                $view_ad = $session->get('view_ad', []);
                $closed_ad = $session->get('closed_ad', []);

                $query_adverts = $this->getDoctrine()->getRepository('AdvertBundle:Places')->createQueryBuilder('places');
                $query_adverts
                    ->addSelect(['adverts', 'event'])
                    ->innerJoin('places.adverts', 'adverts')
                    ->innerJoin('adverts.event', 'event')
                    ->andWhere('adverts.cntShow > 0')
                    ->andWhere('places.numPlace >= :numPlace_from')
                    ->andWhere('places.numPlace < :numPlace_to')
                    ->setParameter(':numPlace_from', self::cntViewPage * $page - self::cntViewPage)
                    ->setParameter(':numPlace_to', self::cntViewPage * $page);

                if (count($closed_ad)) {
                    $query_adverts->andWhere($query_adverts->expr()->notIn('adverts.id', $closed_ad));
                }

                if (isset($result_menu_id) && count($result_menu_id)) {
                    $query_adverts
                        ->andWhere($query_adverts->expr()->in('places.getMenu3', $result_menu_id));
                }
                $result_places = $query_adverts->getQuery()->getResult();

                if (count($result_places)) {
                    $array_events_advert = [];
                    /** @var Places $result_place */
                    /** @var Advert $advert */
                    foreach ($result_places as $result_place) {
                        foreach ($result_place->getAdverts() as $advert) {
                            $array_events_advert[$result_place->getNumPlace()][] = [
                                'event' => $advert->getEvent(),
                                'advert' => $advert
                            ];
                        }
                    }

                    if (count($array_events_advert)) {
                        $em = $this->getDoctrine()->getManager();
                        /** @var array $item */
                        /** @var Advert $advert_persist */
                        foreach ($array_events_advert as $key_numplace => $item) {
                            $select_rand = $item[array_rand($item, 1)];

                            $result_view_advert[$key_numplace] = $select_rand['event'];
                            $advert_persist = $select_rand['advert'];
                            $advert_persist->setCntShow($advert_persist->getCntShow() - 1);
                            $em->persist($advert_persist);

                            if (isset($view_ad[$advert_persist->getId()])) {
                                $view_ad[$advert_persist->getId()] += 1;
                            } else {
                                $view_ad[$advert_persist->getId()] = 1;
                            }
                            if ($view_ad[$advert_persist->getId()] == 3) {
                                $closed_ad[] = $advert_persist->getId();
                            }
                        }
                        $em->flush();

                        $session->set('view_ad', $view_ad);
                        $session->set('closed_ad', $closed_ad);
                    }
                }
            }

            $paginator = $this->get('knp_paginator');

            /** @var SlidingPagination $pagination_events_cabinet */
            $pagination_events_cabinet = $paginator->paginate(
//            $query, /* query NOT result */
                $queryBuilder, /* query NOT result */
                $page/*page number*/,
                self::cntViewPage/*,*//*limit per page*/
//            array('pageParameterName' => 'section', 'sortDirectionParameterName' => 'dir')
            );
//        $pagination_events_cabinet->order

            if (!$charity) {
                $pagination_events_cabinet->setUsedRoute('homepage.events.load');
            } elseif ($charity) {
                $pagination_events_cabinet->setUsedRoute('charity');
            }

            if (!$pagination_events_cabinet) {
                $pagination_events_cabinet = null;
            }

            if ($request->isXmlHttpRequest()) {
                $events_render = $this->renderView('AppBundle:Events:load.html.twig', [
                    'events' => $pagination_events_cabinet,
                    'result_view_advert' => $result_view_advert
                ]);

                $currentUrl = $request->getRequestUri();

                return new Response(json_encode([
                    'status' => 'success',
                    'data_load' => $events_render,
                    'url' => $currentUrl
                ]));
            } else {
                return $this->render('AppBundle:Events:load.html.twig', [
                    'events' => $pagination_events_cabinet,
                    'result_view_advert' => $result_view_advert
                ]);
            }
        }
    }
}