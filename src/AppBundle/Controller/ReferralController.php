<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.04.16
 * Time: 17:10
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReferralController extends Controller
{
    public function indexAction(Request $request, $code)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {

            $session = $request->getSession();

            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            /** @var User $user_find */
            $user_find = $userManager->findUserBy(['referral_link' => $code]);

            if ($user_find) {
                $session->set('referral_id', $user_find->getId());
            }
        }

        return $this->redirect($this->generateUrl('homepage'));
    }

    public function indexvkAction(Request $request, $code)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {

            $em = $this->getDoctrine()->getManager();

            $repo_vkrel = $em->getRepository('AppBundle:VKRelUser');

            $find_vk_rel_user = $repo_vkrel->findOneBy(['code' => $code]);

            if ($find_vk_rel_user) {
                $session = $request->getSession();


                /** @var User $user_find */
                $user_find = $find_vk_rel_user->getUser();

                if ($user_find) {
                    $session->set('referral_id', $user_find->getId());
                    $session->set('referral_rel_id', $find_vk_rel_user->getId());
                }
            }
        }

        return $this->redirect($this->generateUrl('homepage'));
    }
}