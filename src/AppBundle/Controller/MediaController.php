<?php

namespace AppBundle\Controller;

use CompanysBundle\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class MediaController extends Controller
{
    public function uploadAction($_type)
    {
        $request = $this->get('request');
        $files = $request->files;

        // configuration values

//        try {
//        var_dump($directory);
        if ($_type == "logoCompany") {
            $images_class = new Company();
        }
        // $file will be an instance of Symfony\Component\HttpFoundation\File\UploadedFile

        /** @var UploadedFile $uploadedFile */
        foreach ($files as $uploadedFile) {

            $ext = $uploadedFile->getClientOriginalExtension();

            $web_path = "/uploads/tmp";
            $RootDir = $this->get('kernel')->getRootDir();
            $directory = $RootDir . '/../web/uploads/tmp/';
            $image = false;

            switch ($ext) {
                case "png";
                    $directory = $RootDir . '/../web/uploads/tmp/png/';
                    $web_path = "/uploads/tmp/png";
                    $image = true;
                    break;
                case "jpg";
                    $directory = $RootDir . '/../web/uploads/tmp/jpg/';
                    $web_path = "/uploads/tmp/jpg";
                    $image = true;
                    break;
                case "jpeg";
                    $directory = $RootDir . '/../web/uploads/tmp/jpeg/';
                    $web_path = "/uploads/tmp/jpeg";
                    $image = true;
                    break;
                default:
                    $directory = $RootDir . '/../web/uploads/tmp/files/';
                    $web_path = "/uploads/tmp/files";
                    $image = false;
            }

            $name = $uploadedFile->getClientOriginalName();

            $file = $uploadedFile->move($directory, $name);

            if ($image) {
                $image = new \Imagick($directory . $name);

                $name_without_ext = explode('.', $name);
                array_pop($name_without_ext);

                $name = implode('.', $name_without_ext) . '.png';

                $image->setImageFormat("png");
                $image->writeImage($RootDir . '/../web/uploads/tmp/png/' . $name);

                $web_path = "/uploads/tmp/png";

                $im = $this->get("snowcap_im.manager");
                $im->convert($_type, $web_path . "/" . $name);
            }
            // return data to the frontend
            if ($image) {
                return new JsonResponse([
                    'result' => "success",
                    'name' => $name,
                    'path' => $web_path . "/" . $name,
                    'impath' => "/" . $im->getUrl($_type, $web_path . "/" . $name)
                ]);
            } else {
                return new JsonResponse([
                    'result' => "success",
                    'name' => $name,
                    'path' => $web_path . "/" . $name,
                ]);
            }
        }
    }
}
