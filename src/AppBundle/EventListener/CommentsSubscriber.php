<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\Comment;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Sonata\MediaBundle\Entity\GalleryManager;

class CommentsSubscriber implements EventSubscriber
{
    private $mg;

    public function __construct(GalleryManager $mg)
    {
        $this->mg = $mg;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();


        if ($entity instanceof Comment) {
            if (!$entity->getImages()) {
                $entityManager = $args->getEntityManager();

                $mediaGalleryManager = $this->mg;
                /** @var Gallery $gallery */
                $gallery = $mediaGalleryManager->create();
                $gallery->setContext('Comments_gallery');
                $gallery->setDefaultFormat('Comments_gallery');
                $gallery->setName('Картинки для комментария под id - "' . $entity->getId() . '"');
                $gallery->setEnabled(true);
                $mediaGalleryManager->save($gallery);

                $entity->setImages($gallery);

                $entityManager->persist($entity);
                $entityManager->flush();
            }
        }
    }

}