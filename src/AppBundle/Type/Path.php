<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.08.16
 * Time: 13:19
 */

namespace AppBundle\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class Path extends Type
{
    const PATH = 'path';

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getClobTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $array_points = explode(',', $value);
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        //(".$value->getLongitude().','.$value->getLatitude().")
        return $value;
    }

    public function getName()
    {
        return self::PATH;
    }
}