<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 08.07.16
 * Time: 10:44
 */

namespace AppBundle\Twig;


class PhoneExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('phone', array($this, 'phoneFilter')),
        );
    }

    public function phoneFilter($number_phone)
    {
        return ($number_phone) ? '+' . substr($number_phone, 0, 1) . ' (' . substr($number_phone, 1, 3) . ') ' . substr($number_phone, 4, 3) . '-' . substr($number_phone, 7, 2) . '-' . substr($number_phone, 9, 2) : '';
    }

    public function getName()
    {
        return 'phone_extension';
    }
}