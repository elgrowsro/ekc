<?php

namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Swift_Mailer;

class EmailerController
{
    private $templating;
    private $em;
    private $mailer;
    private $settings;

    public $template = '::standart_mail.html.twig';

    public function __construct(
        EngineInterface $templating,
        EntityManager $em,
        Swift_Mailer $mailer,
        EntityRepository $settings
    )
    {
        $this->templating = $templating;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->settings = $settings;
    }

    /**
     * @param $template_id
     * @param $message_array array
     * @param $message_template string
     * @return int
     */
    function sendMailToAdmin($template_id, $message_array, $message_template = null)
    {
        if ($message_template && is_string($message_template)) $this->template = $message_template;
        return $this->sendMail('EmailAdmin', $template_id, $message_array);
    }

    /**
     * @param $template_id
     * @param $message_array array
     * @param $to string
     * @param $message_template string
     * @return int
     */
    function sendMailToUser($template_id, $message_array, $to, $message_template = null)
    {
        if ($message_template && is_string($message_template)) $this->template = $message_template;
        return $this->sendMail('Email', $template_id, $message_array, null, $to);
    }

    /**
     * Отправляет почту по шаблону
     *
     * @param $mailtype int
     * @param $message_array array массив значений для плейсхолдеров ['placeholder'] => 'value'
     * @param null $from string
     * @param null $to string
     * @return int
     */
    function sendMail($mailtype, $template_id, $message_array, $from = null, $to = null)
    {
        $email_template = $this->em->getRepository('AppBundle:' . $mailtype)->find($template_id);
        $settings = $this->settings->findMainSectionSettings();

        if (!$from || !is_string($from)) $from = $email_template->getFrom();
        if (!$to || !is_string($to)) $to = $email_template->getTo();

        $message_mail = strtr($email_template->getText(), $message_array);

        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject($email_template->getSubject())
            ->setFrom($from)
            ->setTo($to)
            ->setContentType("text/html")
            ->setBody(
                $this->templating->render(
                    $this->template,
                    [
                        'title' => $email_template->getName(),
                        'text' => $message_mail,
                        'phone' => $settings["phoneadministrator"]->value,
                        'email' => $settings["emailadministrator"]->value
                    ]
                )
            );

        $send = $this->mailer->send($message);
        return true;

    }
}