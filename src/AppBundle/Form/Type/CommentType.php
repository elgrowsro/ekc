<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType
{
    protected $form_name;

    public function __construct($form_name = 'appbundle_comment')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', 'textarea', [
                'label' => 'Текст отзыва',
                'required' => true
            ])
            ->add('rating_quality', 'choice', [
                'label' => 'Качество',
                'required' => true,
                'attr' => [
                    'class' => 'stars set-stars'
                ],
                'choices' => [
                    '' => '',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                ]
            ])
            ->add('rating_saturation', 'choice', [
                'label' => 'Насыщеность',
                'required' => true,
                'attr' => [
                    'class' => 'stars set-stars'
                ],
                'choices' => [
                    '' => '',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                ]
            ])
            ->add('rating_interestingness', 'choice', [
                'label' => 'Интересность',
                'required' => true,
                'attr' => [
                    'class' => 'stars set-stars'
                ],
                'choices' => [
                    '' => '',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                ]
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comment',
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_with_dropzone form',
                'novalidate' => 'novalidate',
                'enctype' => 'multipart/form-data'
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }
}
