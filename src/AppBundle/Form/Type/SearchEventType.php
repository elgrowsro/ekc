<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchEventType extends AbstractType
{
    protected $form_name;

    public function __construct($form_name = 'appbundle_event_search')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organizer_city', 'checkbox', array(
                'required' => false,
                'label' => 'Город',
            ))
            ->add('organizer_company', 'checkbox', array(
                'required' => false,
                'label' => 'Компания',
            ))
            ->add('organizer_man', 'checkbox', array(
                'required' => false,
                'label' => 'Частник',
            ))
            ->add('forMan', 'checkbox', [
                'label' => 'М',
                'required' => false
            ])
            ->add('forWoman', 'checkbox', [
                'label' => 'Ж',
                'required' => false
            ])
            ->add('forElderly', 'checkbox', [
                'label' => 'Пожилим',
                'required' => false
            ])
            ->add('period_from', 'hidden', [
                'required' => false
            ])
            ->add('period_to', 'hidden', [
                'required' => false
            ])
            ->add('menu', 'hidden', [
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }
}
