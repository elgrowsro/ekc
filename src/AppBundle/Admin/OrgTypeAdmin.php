<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 01.02.16
 * Time: 10:18
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;


class OrgTypeAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text', [
            'label' => 'Название формы собственности'
        ])->add('isActive', 'checkbox', [
            'label' => 'Активная форма собственности'
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->add('isActive', null, array(
            'editable' => true,
            'label' => 'Активная'
        ));
    }
}