<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 01.02.16
 * Time: 10:18
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class EmailAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
//        $collection->remove('create');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('desc', 'text', [
                'label' => 'Описание письма',
                'required' => false
            ])
            ->add('subject', 'text', [
                'label' => 'Тема письма'
            ])
            ->add('from', 'text', [
                'label' => 'Почта, которая будет указана как отправитель'
            ])
            ->add('name', 'text', [
                'label' => 'Заголовок в письме'
            ])
            ->add('text', 'redactor', [
                'redactor' => 'admin',
                'label' => 'Текст письма'
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('desc');
        $listMapper->addIdentifier('name');
    }
}