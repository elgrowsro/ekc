<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.16
 * Time: 22:10
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CityAdmin extends AbstractAdmin
{

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'required' => false,
                'label' => 'Город'
            ))
            ->add('isActive', 'checkbox', array(
                'required' => false,
                'label' => 'Включен?'
            ))
            ->add('ipgeobase', null, array(
                'required' => true,
                'label' => 'Название в ipgeobase'
            ))
            ->add('latlng', 'oh_google_maps', [
                'label' => 'Где находится центр города?'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, [
                'label' => 'Город'
            ]);

        $listMapper->add('isActive', null, array(
            'editable' => true,
            'label' => 'Включен?'
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('name', null,[
            'label' => 'Город'
        ]);
    }

}
