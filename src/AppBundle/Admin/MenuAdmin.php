<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 09.06.16
 * Time: 13:22
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MenuAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', [
                'label' => 'Заголовок',
                'required' => false
            ])
            ->add('parent', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Родитель'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('imageBG', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Картинка для фона'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image'
                    )
                )
            );
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('parent.title');
        $listMapper->add('lvl');
    }
}