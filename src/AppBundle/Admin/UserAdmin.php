<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.16
 * Time: 22:10
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;

class UserAdmin extends SonataUserAdmin
{

    /**
     * переопределение шаблона админки
     *
     * @param string $name
     * @return null|string|void
     */
    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'AppBundle:Admin:edit_object.html.twig';
        } else {
            return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);


        $formMapper
            ->tab('User')
            ->with('Profile')
                ->add('money', 'text', [
                    'label' => 'Баланс'
                ])
                ->add('withdrawMoney', 'text', [
                    'label' => 'Доступные деньги для вывода'
                ])
                ->add('conclusionMoney', 'sonata_type_date_picker', [
                    'label' => 'Дата последнего вывода'
                ])
                ->add('avatar', 'sonata_type_model_list',
                    array(
                        'required' => false,
                        'label' => 'Аватарка'
                    ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'link_parameters' => array(
                            'context' => 'Users_avatars',
                            'provider' => 'sonata.media.provider.image'
                        )
                    )
                )
                ->add('referral_link', 'text',
                    array(
                        'required' => true,
                        'label' => 'Реферальная ссылка'
                    )
                )
//                ->add('avatar', 'sonata_type_model_list',
//                    array(
//                        'required' => false,
//                        'by_reference' => false,
//                        'label' => 'Аватарка'
//                    ),
//                    array(
//                        'edit' => 'inline',
//                        'inline' => 'table',
//                        'link_parameters' => array(
//                            'context' => 'Users_avatars',
//                            'provider' => 'sonata.media.provider.image'
//                        )
//                    )
//                )
            ->end()
            ->with('Social')
            ->add('ulogin', 'sonata_type_collection', array(
                'cascade_validation' => false,
                'type_options' => array('delete' => true),
                'label' => 'Соц. сети',
            ), array(
                    'edit' => 'inline',
                    'inline' => 'standard',
                    'link_parameters' => array('context' => 'widgets'),
                    'admin_code' => 'admin.user.ulogin' /*here provide service name for junction admin */
                )
            )
            ->end()
            ->end();


        $formMapper
            ->remove('facebookUid')
            ->remove('facebookName')
            ->remove('twitterUid')
            ->remove('twitterName')
            ->remove('gplusUid')
            ->remove('gplusName')
            ->remove('facebookUid')
            ->remove('facebookName')
            ->remove('twitterUid')
            ->remove('twitterName')
            ->remove('gplusUid')
            ->remove('gplusName')
            ->remove('token')
            ->remove('twoStepVerificationCode')
            ->remove('locale')
            ->remove('timezone')
            ->remove('expired')
            ->remove('credentialsExpired')
            ->remove('dateOfBirth')
            ->remove('gender')
            ->remove('username');
//        $formMapper
//            ->tab('Security')
//            ->with('Keys')
//            ->add('token', null, array(
//                'required' => false,
//                'read_only' => true,
//                'disabled' => true,
//            ))
//            ->end()
//            ->end();
//        $formMapper->remove('User.Social');
//        $formMapper->remove('Social');
//        $formMapper->remove('Security.Status');
//        $groups = $formMapper->getAdmin()->getFormGroups();
//        var_dump($groups);
//        unset($groups['User.Social']);
//        $formMapper->getAdmin()->setFormGroups($groups);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('email')
            ->addIdentifier('firstname')
            ->add('groups')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {

        parent::configureDatagridFilters($filterMapper);
    }


    public function preUpdate($object)
    {
        foreach ($object->getUlogin() as $image) {
            $image->setUserId($object);
        }
    }
}
