<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.16
 * Time: 22:10
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class VKUsersAdmin extends AbstractAdmin
{

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('firstName', 'text', array(
                'required' => false,
                'label' => 'Имя'
            ))
            ->add('lastName', 'text', array(
                'required' => false,
                'label' => 'Фамилия'
            ))
            ->add('photo100', 'text', array(
                'required' => false,
                'label' => 'Фото'
            ))
            ->add('sendRequest', 'checkbox', array(
                'required' => false,
                'label' => 'Был приглашен?'
            ))
            ->add('confirmedSuccess', 'checkbox', array(
                'required' => false,
                'label' => 'Принял приглашение?'
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('firstName', null, [
                'label' => 'Имя'
            ])
            ->addIdentifier('lastName', null, [
                'label' => 'Фамилия'
            ]);

        $listMapper->add('sendRequest', null, array(
            'editable' => true,
            'label' => 'Был приглашен?'
        ))->add('confirmedSuccess', null, array(
            'editable' => true,
            'label' => 'Принял приглашение?'
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('firstName', null, [
            'label' => 'Имя'
        ])->add('lastName', null, [
            'label' => 'Фамилия'
        ]);
    }

}
