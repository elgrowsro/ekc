<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CommentAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('text')
            ->add('isActive')
            ->add('requestRemove')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('text')
            ->add('user')
            ->add('event')
            ->add('company')
            ->add('isActive', null, array(
                'editable' => true,
                'label' => 'Показывать?'
            ))
            ->add('requestRemove', null, array(
                'editable' => true,
                'label' => 'Подана заявка на удаление'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user', 'sonata_type_model_list',
                array(
                    'required' => true,
                    'label' => 'Автор комментария'
                )
            )
            ->add('event', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Мероприятие, на которое оставлен отзыв'
                )
            )
            ->add('company', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Организатор, на которого оставлен отзыв'
                )
            )
            ->add('text','textarea',[
                'label' =>'Текст комментария'
            ])
            ->add('rating_quality','number',[
                'label' =>'Оценка качества'
            ])
            ->add('rating_saturation','number',[
                'label' =>'Оценка насыщености'
            ])
            ->add('rating_interestingness','number',[
                'label' =>'Оценка интересности'
            ])
            ->add('isActive', 'checkbox', [
                'label'=>'Показывать?',
                'required' => false
            ])
            ->add('requestRemove', 'checkbox', [
                'label'=>'Подана заявка на удаление',
                'required' => false
            ])
            ->add('images', 'sonata_type_model_list',
                array(
                    'required' => false,
                    //                    'multiple' => true,
                    'label' => 'Галерея картинок',
                    'btn_add' => false,
                    'btn_list' => false,
                    'btn_delete' => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'Comments_gallery',
                        //                        'provider' => 'sonata.media.provider.image'
                    )
                )
            )
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('text')
            ->add('user')
            ->add('event')
            ->add('company')
            ->add('createAt')
            ->add('updateAt')
            ->add('isActive')
            ->add('requestRemove')
        ;
    }
}
