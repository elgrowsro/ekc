<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GiftsAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('sum')
            ->add('get');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('sum')
            ->addIdentifier('get')
            ->add('user')
            ->add('userMake')
            ->add('event')
            ->add('ticket')
            ->add('createAt')
            ->add('updateAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('sum')
            ->add('get')
            ->add('user', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Пользователь, которому предназначается подарок'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                )
            )
            ->add('userMake', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Пользователь, который сделал подарок'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                )
            )
            ->add('event', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Мероприятие, к которому предназначается подарок'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                )
            )
            ->add('ticket', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Билет, на который предназначается подарок'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                )
            );
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('sum')
            ->add('get')
            ->add('user')
            ->add('userMake')
            ->add('event')
            ->add('ticket')
            ->add('createAt')
            ->add('updateAt');
    }
}
