<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.16
 * Time: 22:10
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class VKRelUsersAdmin extends AbstractAdmin
{

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Пользователь, который пригласил'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('vkusers', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Приглашенный пользователь'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('dateSendRequest', 'datetime', array(
                'required' => false,
                'label' => 'Дата и время отправки запроса'
            ))
            ->add('code', 'text', array(
                'required' => false,
                'read_only' => true,
                'label' => 'Отправленный код'
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, [
                'label' => 'Id'
            ])
            ->addIdentifier('user.FLName', null, [
                'label' => 'От кого был отправлен запрос'
            ])
            ->addIdentifier('vkusers.FLName', null, [
                'label' => 'Кому было отправлено приглашение'
            ])
            ->addIdentifier('dateSendRequest', null, [
                'label' => 'Дата и время отправки запроса'
            ])
            ->addIdentifier('code', null, [
                'label' => 'Отправленный код'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('code', null, [
            'label' => 'Отправленный код'
        ]);
    }

}
