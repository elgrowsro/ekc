<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 01.02.16
 * Time: 10:18
 */

namespace Ulogin\AuthBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;


class UloginAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('identity', 'text', [
            'required' => true,
        ]);
        $formMapper->add('network', 'text', [
            'required' => true,
        ]);

    }
}