<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 07.07.16
 * Time: 16:43
 */

namespace ProfilesBundle\Controller;

use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EventsController extends Controller
{
    public function loadAction(Request $request, $user_id = null, $type = 'old', $page)
    {
        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Ticket');

        $query = $repository->createQueryBuilder('t')
            ->select(["t", "timeprices"])
            ->andWhere("t.user = :user ")
            ->setParameter(":user", $user_id);
        $em = $query->getEntityManager();
        $em->getConfiguration()
            ->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        if ($type == 'old') {
            $query
                ->innerJoin('t.timeprices', 'timeprices', 'WITH', '((timeprices.date_event < CURRENT_DATE()) OR (timeprices.date_event = CURRENT_DATE() AND timeprices.timefrom < CURRENT_TIME()))')
                ->orderBy('t.id', 'DESC');
        } elseif ($type == 'new') {
            $query
                ->innerJoin('t.timeprices', 'timeprices', 'WITH', '((timeprices.date_event > CURRENT_DATE()) OR (timeprices.date_event = CURRENT_DATE() AND timeprices.timefrom > CURRENT_TIME()))')
                ->orderBy('t.id', 'ASC');
        }

        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_tickets */
        $pagination_tickets = $paginator->paginate(
            $query,
            $page,
            10
        );

        $pagination_tickets->setUsedRoute('user_load_events');


        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('ProfilesBundle:table_events:ajax_load.html.twig', [
                'tickets' => $pagination_tickets,
                'user_id' => $user_id,
                'type' => $type
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('ProfilesBundle:table_events:load.html.twig', [
                'tickets' => $pagination_tickets,
                'user_id' => $user_id,
                'type' => $type
            ]);
            return $events_render;
        }
    }

}