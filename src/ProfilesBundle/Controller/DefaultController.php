<?php

namespace ProfilesBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function indexAction(Request $request, $url)
    {
        $user_find = $this->getUserShow($url);
        $events_before = $this->getUserStatisricEventBefore($user_find);
        $events_after = $this->getUserStatisricEventAfter($user_find);

        return $this->render('ProfilesBundle:Default:index.html.twig', [
            'user_find' => $user_find,
            'events_before' => $events_before,
            'events_after' => $events_after
        ]);
    }

    public function eventsAction(Request $request, $url)
    {
        $user_find = $this->getUserShow($url);
        $events_before = $this->getUserStatisricEventBefore($user_find);
        $events_after = $this->getUserStatisricEventAfter($user_find);

        return $this->render('ProfilesBundle:Default:events.html.twig', [
            'user_find' => $user_find,
            'events_before' => $events_before,
            'events_after' => $events_after
        ]);
    }

    public function commentsAction(Request $request, $url)
    {
        $user_find = $this->getUserShow($url);
        $events_before = $this->getUserStatisricEventBefore($user_find);
        $events_after = $this->getUserStatisricEventAfter($user_find);

        return $this->render('ProfilesBundle:Default:comments.html.twig', [
            'user_find' => $user_find,
            'events_before' => $events_before,
            'events_after' => $events_after
        ]);
    }


    private function getUserStatisricEventBefore($user)
    {

        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Event');

        return $repository->getCntBefore($user);
    }


    private function getUserStatisricEventAfter($user)
    {

        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Event');

        return $repository->getCntAfter($user);
    }

    private function getUserShow($url)
    {

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:User');

        $query = $repository->createQueryBuilder('u')
            ->select(["u"])
            ->where('u.url = :url')
            ->setParameter('url', $url)
            ->getQuery();

        /** @var User $user */
        $user = $query->getOneOrNullResult();

        if (!$user) {
            throw new NotFoundHttpException("Page not found");
        }

        return $user;

    }
}
