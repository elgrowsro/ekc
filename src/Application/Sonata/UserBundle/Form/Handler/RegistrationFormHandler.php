<?php

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Form\Handler;

use AppBundle\Entity\Referrals;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;


class RegistrationFormHandler extends BaseHandler
{
    public $em;

    public function __construct($form, Request $request, UserManagerInterface $userManager, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator, EntityManager $em)
    {
        $this->form = $form;
        $this->request = $request;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->em = $em;
    }

    /**
     * @param boolean $confirmation
     */
    public function process($confirmation = false)
    {
        /** @var User $user */
        $user = $this->createUser();

        $generate_password = $this->generateStrongPassword(8, false, 'lud');
        $generate_referral_link = $this->generateStrongPassword(8, false, 'lud');

        $user->setPlainPassword($generate_password);
        $user->setRl($generate_referral_link);


        $this->form->setData($user);

        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($user, $confirmation);

                $session = $this->request->getSession();

                $referral_id = $session->get('referral_id');
                $referral_rel_id = $session->get('referral_rel_id');

                if ($referral_id) {
                    /** @var User $user_referral */
                    $user_referral = $this->userManager->findUserBy(['id' => $referral_id]);

                    $referral = new Referrals();
                    $referral->setUser($user_referral);
                    $referral->setUserReferral($user);
                    $this->em->persist($referral);
                    $this->em->flush();

                    if ($referral_rel_id) {
                        $vk_rel_user = $this->em->getRepository('AppBundle:VKRelUser')->find($referral_rel_id);

                        $vk_user = $vk_rel_user->getVkusers();

                        $vk_user->setConfirmedSuccess(true);
                        $vk_user->setConfirmUser($user_referral);

                        $this->em->persist($vk_user);
                        $this->em->flush();
                    }
                }


                return true;
            }
        }

        return false;
    }

    /**
     * @param boolean $confirmation
     * @var User $user
     */
    public function processNew($user, $confirmation = false)
    {

        $generate_password = $this->generateStrongPassword(8, false, 'lud');
        $generate_referral_link = $this->generateStrongPassword(8, false, 'lud');

        $user->setPlainPassword($generate_password);
        $user->setRl($generate_referral_link);


        $this->form->setData($user);

        if ('POST' === $this->request->getMethod()) {
            $this->onSuccess($user, $confirmation);
            return true;
        }

        return false;
    }


    private function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }


    /**
     * @param boolean $confirmation
     */
    protected function onSuccess(UserInterface $user, $confirmation)
    {
        if ($confirmation) {
            $user->setEnabled(false);
            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->tokenGenerator->generateToken());
            }

            $this->mailer->sendConfirmationEmailMessage($user);
        } else {
            $user->setEnabled(true);
        }

        $this->userManager->updateUser($user);
    }

    /**
     * @return UserInterface
     */
    protected function createUser()
    {
        return $this->userManager->createUser();
    }
}
