<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationFormType extends AbstractType
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var array
     */
    protected $mergeOptions;

    /**
     * @param string $class The User class name
     * @param array $mergeOptions Add options to elements
     */
    public function __construct($class, array $mergeOptions = array())
    {
        $this->class = $class;
        $this->mergeOptions = $mergeOptions;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array_merge(array(
                'label' => 'E-mail:',
                'attr' => ['placeholder' => 'E-mail:'],
                'translation_domain' => 'SonataUserBundle',
            ), $this->mergeOptions))
            ->add('firstname', 'text', array_merge(array(
                'label' => 'Имя:',
                'attr' => ['placeholder' => 'Имя:'],
                'translation_domain' => 'SonataUserBundle',
            ), $this->mergeOptions))
            ->add('lastname', 'text', array_merge(array(
                'label' => 'Фамилия:',
                'attr' => ['placeholder' => 'Фамилия:'],
                'translation_domain' => 'SonataUserBundle',
            ), $this->mergeOptions))
            ->add('phone', 'text', array_merge(array(
                'label' => 'Телефон:',
                'attr' => ['placeholder' => 'Телефон:'],
                'translation_domain' => 'SonataUserBundle',
            ), $this->mergeOptions));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention' => 'registration',
        ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sonata_user_registration';
    }
}
