<?php

namespace Application\Sonata\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController;
use Sonata\UserBundle\Controller\RegistrationFOSUser1Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegistrationFOSUser2Controller extends RegistrationFOSUser1Controller
{
    public function registerAction()
    {
        $form = $this->get('sonata.user.registration.form');
        $formHandler = $this->get('sonata.user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $process = $formHandler->process($confirmationEnabled);
//        var_dump($process);

        if ($process) {
            $user = $form->getData();

            return new Response(json_encode(array(
                'status' => 'success'
            )));

        } elseif ('POST' === $this->container->get('request')->getMethod()) {

            $errors = $this->get('form_serializer')->serializeFormErrors($form, true, true);
            return new Response(json_encode(array(
                'status' => 'error',
                'errors' => $errors
            )));
        }

        return $this->container->get('templating')->renderResponse('ApplicationSonataUserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction($token)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime());

        $this->container->get('fos_user.user_manager')->updateUser($user);
        $response = new RedirectResponse($this->container->get('router')->generate('fos_user_registration_confirmed'));
        $this->authenticateUser($user, $response);

        return $response;
    }
}
