<?php
/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Controller;

use FOS\UserBundle\Controller\ResettingController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//use FOS\UserBundle\Event\FilterUserResponseEvent;
//use FOS\UserBundle\Event\GetResponseUserEvent;
//use FOS\UserBundle\FOSUserEvents;
//use FOS\UserBundle\Event\FormEvent;

/**
 * Class ResettingFOSUser1Controller.
 *
 *
 * @author Hugo Briand <briand@ekino.com>
 */
class ResettingFOSUser2Controller extends ResettingController
{
    /**
     * Request reset user password: show form
     */
    public function requestAction()
    {
        return $this->container->get('templating')->renderResponse('ApplicationSonataUserBundle:Resetting:request.html.twig');
    }

    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction()
    {
        $username = $this->container->get('request')->request->get('username');

        /** @var $user UserInterface */
        $user = $this->container->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            return new Response(json_encode(array(
                'status' => 'error',
                'errors' => [
                    'global' => [],
                    'fields' => [
                        'username' => 'Пользователя с такой почтой не существует'
                    ]
                ]
            )));
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return new Response(json_encode(array(
                'status' => 'error',
                'errors' => [
                    'global' => [],
                    'fields' => [
                        'username' => 'Пароль для данного пользователя уже запрашивался за последние 24 часа.'
                    ]
                ]
            )));
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return new Response(json_encode(array(
            'status' => 'success'
        )));
    }

    /**
     * Reset user password
     */
    public function resetAction($token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            return new RedirectResponse($this->container->get('router')->generate('registrate_homepage'));
//            throw new AccessDeniedException('No token given or token is wrong.');
//            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        if (!$user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return new RedirectResponse($this->container->get('router')->generate('registrate_homepage'));
        }

        $form = $this->container->get('fos_user.resetting.form');
        $formHandler = $this->container->get('fos_user.resetting.form.handler');
        $process = $formHandler->process($user);

        if ($process) {
            return new Response(json_encode(array(
                'status' => 'success'
            )));
        }

        return $this->container->get('templating')->renderResponse('ApplicationSonataUserBundle:Resetting:reset.html.twig', array(
            'token' => $token,
            'form' => $form->createView(),
        ));
    }
}
