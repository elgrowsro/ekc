<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Mailer;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 */
class Mailer implements MailerInterface
{
    protected $mailer;
    protected $router;
    protected $templating;
    protected $parameters;
    protected $em;
    private $tokenGenerator;
    private $container;

    public function __construct($mailer, UrlGeneratorInterface $router, EngineInterface $templating, EntityManager $em, TokenGeneratorInterface $tokenGenerator, Container $container, array $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
        $this->em = $em;
        $this->tokenGenerator = $tokenGenerator;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     * @var User $user
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['confirmation.template'];

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }
        $email_1 = $this->em->getRepository('AppBundle:Email')->find(1);

        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $settings = $this->container->get('settings.all')->findMainSectionSettings();

        $str_array = [
            "{{ firstname }}" => $user->getFirstname(),
            '{{ password }}' => $user->getGeneratePassword(),
            "{{ link }}" => $url,
        ];

        $message_mail = strtr($email_1->getText(), $str_array);
        $title = $email_1->getName();


        $rendered = $this->templating->render($template, array(
            'title' => $title,
            'text' => $message_mail,
            'phone' => $settings["phoneadministrator"]->value,
            'email' => $settings["emailadministrator"]->value
        ));
        $this->sendEmailMessage($rendered, $email_1->getFrom(), $user->getEmail(), $email_1->getSubject());
    }

    /**
     * {@inheritdoc}
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['resetting.template'];
        $email_2 = $this->em->getRepository('AppBundle:Email')->find(2);

        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $str_array = [
            "{{ email }}" => $user->getUsername(),
            "{{ link }}" => $url,
        ];

        $message_mail = strtr($email_2->getText(), $str_array);
        $title = $email_2->getName();
        $settings = $this->container->get('settings.all')->findMainSectionSettings();

        $rendered = $this->templating->render($template, array(
            'title' => $title,
            'text' => $message_mail,
            'phone' => $settings["phoneadministrator"]->value,
            'email' => $settings["emailadministrator"]->value
        ));

        $this->sendEmailMessage($rendered, $email_2->getFrom(), $user->getEmail(), $email_2->getSubject());
    }

    /**
     * @param string $renderedTemplate
     * @param string $fromEmail
     * @param string $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail, $subject = null)
    {
        // Render the email, use the first line as the subject, and the rest as the body
        if (!$subject) {
            $renderedLines = explode("\n", trim($renderedTemplate));
            $subject = $renderedLines[0];
        }
        $body = $renderedTemplate;

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setContentType("text/html")
            ->setBody($body);

        $this->mailer->send($message);
    }
}
