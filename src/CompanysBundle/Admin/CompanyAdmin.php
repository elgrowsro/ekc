<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 13.03.16
 * Time: 22:10
 */

namespace CompanysBundle\Admin;

use Oh\GoogleMapFormTypeBundle\Form\Type\GoogleMapType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class CompanyAdmin extends Admin
{

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $date_start = new \DateTime();

        $formMapper
            ->add('typeCompany', 'choice', array(
                'required' => true,
                'label' => 'Тип организации',
                'choices' => [
                    '1' => 'Государственная организация',
                    '2' => 'Благотворительные организации',
                    '3' => 'Общественное движение',
                    '4' => 'Организация',
                    '5' => 'Физическое лицо',
                ]
            ))
            ->add('nameCompany', 'text', array(
                'required' => false,
                'label' => 'Название компании',
            ))
            ->add('logo', 'sonata_type_model_list',
                array(
                    'required' => false,
                    'label' => 'Логотип'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'Companys_logo',
                        'provider' => 'sonata.media.provider.image'
                    )
                )
            )
            ->add('dateRegistrateCompany', 'sonata_type_datetime_picker', array(
                'required' => false,
                'label' => 'Дата регистрации компании',
                'datepicker_use_button' => false,
                'dp_side_by_side' => false,
                'dp_pick_time' => false,
                'dp_use_seconds' => true,
                'format' => 'yyyy-MM-dd',
                'dp_default_date' => $date_start->format('m-Y'),
                'dp_language' => 'ru',
            ))
            ->add('phoneForAdmin', 'text', array(
                'required' => false,
                'label' => 'Номер телефона для администрации',
            ))
            ->add('phoneForUsers', 'text', array(
                'required' => false,
                'label' => 'Номер телефона для пользователей',
            ))
            ->add('email', 'email', array(
                'required' => false,
                'label' => 'Почта компании',
            ))
            ->add('orgn', 'text', array(
                'required' => false,
                'label' => 'ОРГН',
            ))
            ->add('dirname', 'text', array(
                'required' => false,
                'label' => 'ФИО директора',
            ))
            ->add('usercompany', 'sonata_type_model_list', [
                'required' => true,
                'class' => 'AppBundle\Entity\User',
//                'property' => 'username',
                'label' => 'Контактное лицо',
                'btn_add' => 'Добавить',
                'btn_list' => 'Выбрать',
                'btn_delete' => 'Удалить'
            ])
            ->add('formOrganization', 'sonata_type_model_list', [
                'required' => true,
                'class' => 'AppBundle\Entity\OrgType',
//                'property' => 'username',
                'label' => 'Организационная форма',
                'btn_add' => 'Добавить',
                'btn_list' => 'Выбрать',
                'btn_delete' => 'Удалить'
            ])
            ->add('textAbout', 'redactor', array('redactor' => 'admin',
                'label' => 'Текст о компании'
            ))
            ->add('isActive', 'checkbox', array(
                'label' => 'Пройдена модерация?',
                'required' => false,
            ))
            ->add('latlng', 'oh_google_maps', [
                'label' => 'Отметка на карте'
            ])
            ->add('documents', 'sonata_type_model_list',
                array(
                    'required' => false,
//                    'multiple' => true,
                    'label' => 'Сопутствующие документы'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'Documents',
//                        'provider' => 'sonata.media.provider.image'
                    )
                )
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, [
                'label' => 'Город'
            ])
            ->addIdentifier('nameCompany', null, [
                'label' => 'Название компании',
            ]);
        $listMapper->add('is_active', null, array(
            'editable' => true,
            'label' => 'Пройдена модерация?'
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('nameCompany', null, [
            'label' => 'Название компании',
        ]);
    }

}
