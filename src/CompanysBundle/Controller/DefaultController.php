<?php

namespace CompanysBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use CompanysBundle\Entity\Company;
use CompanysBundle\Form\CommentCompanyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

class DefaultController extends Controller
{
    public function viewAction(Request $request, $url)
    {
        $company = $this->getCompany($url);
        $repository_comments = $this->getDoctrine()
            ->getRepository('AppBundle:Comment');

        $evaluation = $repository_comments->getEvaluationCompany($company);

        /** @var User $user */
        $user = $this->getUser();

        if ($user) {
            $comment = new Comment();
            $comment->setCompany($company);

            $form_comment = $this->createForm(new CommentCompanyType(), $comment, [
                'validation_groups' => ['create_comment_company'],
                'action' => $this->generateUrl('check_comment')
            ]);
        }


        if ($user) {
            return $this->render('CompanysBundle:Default:view.html.twig', [
                'company' => $company,
                'evaluation' => $evaluation,
                'form_comment' => $form_comment->createView(),
            ]);
        } else {
            return $this->render('CompanysBundle:Default:view.html.twig', [
                'company' => $company,
                'evaluation' => $evaluation
            ]);
        }
    }

    public function eventsAction(Request $request, $url, $page = 1)
    {
        /** @var Company $company */
        $company = $this->getCompany($url);


        /** @var User $user */
        $user = $this->getUser();


        $repository_comments = $this->getDoctrine()
            ->getRepository('AppBundle:Comment');
        $evaluation = $repository_comments->getEvaluationCompany($company);

        return $this->render('CompanysBundle:Default:events.html.twig', [
            'company' => $company,
            'evaluation' => $evaluation
        ]);
    }

//    public function reviewsAction($url)
//    {
//        $company = $this->getCompany($url);
//
//        return $this->render('CompanysBundle:Default:reviews.html.twig', [
//            'company' => $company
//        ]);
//    }

    private function getCompany($url)
    {

        $repository = $this->getDoctrine()
            ->getRepository('CompanysBundle:Company');

        $query = $repository->createQueryBuilder('e')
            ->select(["e"])
            ->where('e.url = :url')
            ->setParameter('url', $url)
            ->getQuery();

        /** @var Company $company */
        $company = $query->getOneOrNullResult();

        if (!$company) {
            throw new NotFoundHttpException("Page not found");
        }

        return $company;

    }
}
