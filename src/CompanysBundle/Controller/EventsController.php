<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 08.07.16
 * Time: 11:03
 */

namespace CompanysBundle\Controller;


use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EventsController extends Controller
{
    public function loadAction(Request $request, $url = null, $page)
    {
        $company = $this->getDoctrine()
            ->getRepository('CompanysBundle:Company')->findOneBy(['url' => $url]);

        if (!$company) {
            throw new NotFoundHttpException("Page not found");
        }

        $city = $this->get('ipgeobase');

        $repository = $this->getDoctrine()
            ->getRepository('EventsBundle:Event');
        $query = $repository->createQueryBuilder('e')
            ->select("e")
            ->where('e.company = :company')
            ->andWhere('e.city = :city')
            ->setParameter('company', $company)
            ->setParameter(':city', $city->getCity());


        $paginator = $this->get('knp_paginator');

        /** @var SlidingPagination $pagination_events */
        $pagination_events = $paginator->paginate(
            $query,
            $page,
            10
        );
        $pagination_events->setUsedRoute('company_load_events');

        if ($request->isXmlHttpRequest()) {
            $events_render = $this->renderView('CompanysBundle:event:load.html.twig', [
                'events' => $pagination_events,
                'url' => $url,
            ]);
            return new Response(json_encode([
                'data_load' => $events_render
            ]));
        } else {
            $events_render = $this->render('CompanysBundle:event:load.html.twig', [
                'events' => $pagination_events,
                'url' => $url,
            ]);
            return $events_render;
        }
    }
}