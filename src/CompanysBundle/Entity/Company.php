<?php

namespace CompanysBundle\Entity;

/**
 * Company
 */
class Company
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $typeCompany;

    /**
     * @var string
     */
    private $nameCompany;

    /**
     * @var \DateTime
     */
    private $dateRegistrateCompany;

    /**
     * @var string
     */
    private $addressMap;

    /**
     * @var string
     */
    private $phoneForAdmin;

    /**
     * @var string
     */
    private $phoneForUsers;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $orgn;

    /**
     * @var string
     */
    private $fiodirector;

    /**
     * @var string
     */
    private $paymentBank;

    /**
     * @var string
     */
    private $paymentCorrAccount;

    /**
     * @var string
     */
    private $paymentBik;

    /**
     * @var string
     */
    private $paymentInn;

    /**
     * @var string
     */
    private $paymentKpp;

    /**
     * @var string
     */
    private $textAbout;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeCompany
     *
     * @param integer $typeCompany
     *
     * @return Company
     */
    public function setTypeCompany($typeCompany)
    {
        $this->typeCompany = $typeCompany;

        return $this;
    }

    /**
     * Get typeCompany
     *
     * @return integer
     */
    public function getTypeCompany()
    {
        return $this->typeCompany;
    }

    /**
     * Set nameCompany
     *
     * @param string $nameCompany
     *
     * @return Company
     */
    public function setNameCompany($nameCompany)
    {
        $this->nameCompany = $nameCompany;

        return $this;
    }

    /**
     * Get nameCompany
     *
     * @return string
     */
    public function getNameCompany()
    {
        return $this->nameCompany;
    }

    /**
     * Set dateRegistrateCompany
     *
     * @param \Date $dateRegistrateCompany
     *
     * @return Company
     */
    public function setDateRegistrateCompany($dateRegistrateCompany)
    {
        $this->dateRegistrateCompany = $dateRegistrateCompany;

        return $this;
    }

    /**
     * Get dateRegistrateCompany
     *
     * @return \Date
     */
    public function getDateRegistrateCompany()
    {
        return $this->dateRegistrateCompany;
    }

    /**
     * Set addressMap
     *
     * @param string $addressMap
     *
     * @return Company
     */
    public function setAddressMap($addressMap)
    {
        $this->addressMap = $addressMap;

        return $this;
    }

    /**
     * Get addressMap
     *
     * @return string
     */
    public function getAddressMap()
    {
        return $this->addressMap;
    }

    /**
     * Set phoneForAdmin
     *
     * @param string $phoneForAdmin
     *
     * @return Company
     */
    public function setPhoneForAdmin($phoneForAdmin)
    {
        preg_match_all('/\d+/', $phoneForAdmin, $matches_phone);

        $implode_phone = implode('', $matches_phone[0]);
        $this->phoneForAdmin = $implode_phone;

        return $this;
    }

    /**
     * Get phoneForAdmin
     *
     * @return string
     */
    public function getPhoneForAdmin()
    {
        return $this->phoneForAdmin;
    }

    /**
     * Set phoneForUsers
     *
     * @param string $phoneForUsers
     *
     * @return Company
     */
    public function setPhoneForUsers($phoneForUsers)
    {
        preg_match_all('/\d+/', $phoneForUsers, $matches_phone);

        $implode_phone = implode('', $matches_phone[0]);
        $this->phoneForUsers = $implode_phone;

        return $this;
    }

    /**
     * Get phoneForUsers
     *
     * @return string
     */
    public function getPhoneForUsers()
    {
        return $this->phoneForUsers;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set orgn
     *
     * @param string $orgn
     *
     * @return Company
     */
    public function setOrgn($orgn)
    {
        $this->orgn = $orgn;

        return $this;
    }

    /**
     * Get orgn
     *
     * @return string
     */
    public function getOrgn()
    {
        return $this->orgn;
    }

    /**
     * Set fiodirector
     *
     * @param string $fiodirector
     *
     * @return Company
     */
    public function setFiodirector($fiodirector)
    {
        $this->$fiodirector = $fiodirector;

        return $this;
    }

    /**
     * Get fiodirector
     *
     * @return string
     */
    public function getFiodirector()
    {
        return $this->fiodirector;
    }

    /**
     * Set paymentBank
     *
     * @param string $paymentBank
     *
     * @return Company
     */
    public function setPaymentBank($paymentBank)
    {
        $this->paymentBank = $paymentBank;

        return $this;
    }

    /**
     * Get paymentBank
     *
     * @return string
     */
    public function getPaymentBank()
    {
        return $this->paymentBank;
    }

    /**
     * Set paymentCorrAccount
     *
     * @param string $paymentCorrAccount
     *
     * @return Company
     */
    public function setPaymentCorrAccount($paymentCorrAccount)
    {
        $this->paymentCorrAccount = $paymentCorrAccount;

        return $this;
    }

    /**
     * Get paymentCorrAccount
     *
     * @return string
     */
    public function getPaymentCorrAccount()
    {
        return $this->paymentCorrAccount;
    }

    /**
     * Set paymentBik
     *
     * @param string $paymentBik
     *
     * @return Company
     */
    public function setPaymentBik($paymentBik)
    {
        $this->paymentBik = $paymentBik;

        return $this;
    }

    /**
     * Get paymentBik
     *
     * @return string
     */
    public function getPaymentBik()
    {
        return $this->paymentBik;
    }

    /**
     * Set paymentInn
     *
     * @param string $paymentInn
     *
     * @return Company
     */
    public function setPaymentInn($paymentInn)
    {
        $this->paymentInn = $paymentInn;

        return $this;
    }

    /**
     * Get paymentInn
     *
     * @return string
     */
    public function getPaymentInn()
    {
        return $this->paymentInn;
    }

    /**
     * Set paymentKpp
     *
     * @param string $paymentKpp
     *
     * @return Company
     */
    public function setPaymentKpp($paymentKpp)
    {
        $this->paymentKpp = $paymentKpp;

        return $this;
    }

    /**
     * Get paymentKpp
     *
     * @return string
     */
    public function getPaymentKpp()
    {
        return $this->paymentKpp;
    }

    /**
     * Set textAbout
     *
     * @param string $textAbout
     *
     * @return Company
     */
    public function setTextAbout($textAbout)
    {
        $this->textAbout = $textAbout;

        return $this;
    }

    /**
     * Get textAbout
     *
     * @return string
     */
    public function getTextAbout()
    {
        return $this->textAbout;
    }

    /**
     * @var \DateTime
     */
    private $create_company;

    /**
     * @var \DateTime
     */
    private $last_update;


    /**
     * Set createCompany
     *
     * @param \DateTime $createCompany
     *
     * @return Company
     */
    public function setCreateCompany($createCompany)
    {
        $this->create_company = $createCompany;

        return $this;
    }

    /**
     * Get createCompany
     *
     * @return \DateTime
     */
    public function getCreateCompany()
    {
        return $this->create_company;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Company
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * @var \AppBundle\Entity\User
     */
    private $usercompany;


    /**
     * Set usercompany
     *
     * @param \AppBundle\Entity\User $usercompany
     *
     * @return Company
     */
    public function setUsercompany(\AppBundle\Entity\User $usercompany = null)
    {
        $this->usercompany = $usercompany;

        return $this;
    }

    /**
     * Get usercompany
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsercompany()
    {
        return $this->usercompany;
    }

    /**
     * @var \AppBundle\Entity\OrgType
     */
    private $formOrganization;


    /**
     * Set formOrganization
     *
     * @param \AppBundle\Entity\OrgType $formOrganization
     *
     * @return Company
     */
    public function setFormOrganization(\AppBundle\Entity\OrgType $formOrganization = null)
    {
        $this->formOrganization = $formOrganization;

        return $this;
    }

    /**
     * Get formOrganization
     *
     * @return \AppBundle\Entity\OrgType
     */
    public function getFormOrganization()
    {
        return $this->formOrganization;
    }

    /**
     * @var integer
     */
    private $lat;

    /**
     * @var integer
     */
    private $lng;


    /**
     * Set lat
     *
     * @param integer $lat
     *
     * @return Company
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return integer
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param integer $lng
     *
     * @return Company
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return integer
     */
    public function getLng()
    {
        return $this->lng;
    }

    public function setLatLng($latlng)
    {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);
        return $this;
    }

    /**
     * @Assert\NotBlank()
     * @OhAssert\LatLng()
     */
    public function getLatLng()
    {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $logo;


    /**
     * Set logo
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $logo
     *
     * @return Company
     */
    public function setLogo(\Application\Sonata\MediaBundle\Entity\Media $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @var string
     */
    private $dirname;


    /**
     * Set dirname
     *
     * @param string $dirname
     *
     * @return Company
     */
    public function setDirname($dirname)
    {
        $this->dirname = $dirname;

        return $this;
    }

    /**
     * Get dirname
     *
     * @return string
     */
    public function getDirname()
    {
        return $this->dirname;
    }

    /**
     * @var boolean
     */
    private $is_active = 1;


    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Company
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return (boolean)$this->is_active;
    }

    /**
     * @var string
     */
    private $passport;


    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return Company
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $documents;


    /**
     * Set documents
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $documents
     *
     * @return Company
     */
    public function setDocuments(\Application\Sonata\MediaBundle\Entity\Gallery $documents = null)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * Get documents
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;

    /**
     * Add event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return Company
     */
    public function addEvent(\EventsBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \EventsBundle\Entity\Event $event
     */
    public function removeEvent(\EventsBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @var string
     */
    private $url;


    /**
     * Set url
     *
     * @param string $url
     *
     * @return Company
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comments_company;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments_company = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add commentsCompany
     *
     * @param \AppBundle\Entity\Comment $commentsCompany
     *
     * @return Company
     */
    public function addCommentsCompany(\AppBundle\Entity\Comment $commentsCompany)
    {
        $this->comments_company[] = $commentsCompany;

        return $this;
    }

    /**
     * Remove commentsCompany
     *
     * @param \AppBundle\Entity\Comment $commentsCompany
     */
    public function removeCommentsCompany(\AppBundle\Entity\Comment $commentsCompany)
    {
        $this->comments_company->removeElement($commentsCompany);
    }

    /**
     * Get commentsCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentsCompany()
    {
        return $this->comments_company;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        if ($this->getTypeCompany() == 5) {
            $user = $this->getUsercompany();
            return $user->getFirstname() . ' ' . $user->getLastname();
        } else {
            return $this->getNameCompany();
        }
    }


    /**
     * Lifecycle callback to upload the file to the server
     */
    public function prePersistCompany()
    {
        if (!$this->getNameCompany()) {
            $this->setNameCompany($this->getUsercompany()->getFLName());
        }
    }
}
