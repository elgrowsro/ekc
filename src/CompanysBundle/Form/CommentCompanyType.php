<?php

namespace CompanysBundle\Form;

use AppBundle\Form\Type\CommentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentCompanyType extends CommentType
{
    public function __construct($form_name = 'profilebundle_comment')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('company', 'entity_hidden', [
                'class' => 'CompanysBundle\Entity\Company'
            ]);
    }
}
