<?php
namespace PageBundle\Twig;

use Doctrine\ORM\Query\Expr\Join;
use PageBundle\Entity\PageSettings;
use SettingsBundle\Entity\MySettings;
use TopAdvertBundle\Entity\Slide;

class PageExtension extends \Twig_Extension
{
    private $em;
    private $array_return_by_route = [];

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return array(
            'pagedata' => new \Twig_SimpleFunction('pagedata', function ($route) {
                switch ($route) {
                    case 'ofd_by_page':
                        $route = 'ofd_homepage';
                        break;
                    case 'ofv_by_page':
                        $route = 'ofv_homepage';
                        break;
                    case 'analytics_by_page':
                        $route = 'analytics_homepage';
                        break;
                    default:
                        $route = $route;
                }

                $qb = $this->em->createQueryBuilder();
                $qb->select('p, settingsjoin')
                    ->from('PageBundle:SeoPage', 'p')
                    ->leftJoin('p.settingsjoin', 'settingsjoin')
                    ->andWhere('p.route = :route')
                    ->setParameter('route', $route);

//                var_dump(222222111111111222);
                $page = $qb->getQuery()->getOneOrNullResult();
                if (!$page) {
                    $page = new \PageBundle\Entity\SeoPage();
                    $page->setTitle('new Page ' . $route);
                    $page->setDatapage(json_encode(new \stdClass()));
                    $page->setDescription('');
                    $page->setKeywords('');
                    $page->setPagetype('default');
                    $page->setRoute($route);

                    $this->em->clear();
                    $this->em->persist($page);
                    $this->em->flush();
                }

                if (count($page->getSettingsjoin())) {
                    $data = [];
                    /** @var MySettings $setting */
                    foreach ($page->getSettingsjoin() as $setting) {
                        $data[$setting->getName()] = $setting->getCurrentValue();
                    }
                    $page->setPageSettings($data);
                }

                return $page;
            }),
            'data_byroute' => new \Twig_SimpleFunction('data_byroute', function ($route) {

                $array_return = [];
                if (count($this->array_return_by_route)) {
                    $array_return = $this->array_return_by_route;
                } else {
                    $em = $this->em->getRepository('PageBundle:SeoPage');

                    $pages = $em->findAll();

                    foreach ($pages AS $page) {
                        $array_return[$page->getRoute()] = [
                            'namelink' => $page->getNameLink(),
                            'enable' => $page->getEnablePage()
                        ];
                    }
                    $this->array_return_by_route = $array_return;
                }

                return $array_return;
            }),
        );
    }

    public function getName()
    {
        return 'seopage_extension';
    }
}