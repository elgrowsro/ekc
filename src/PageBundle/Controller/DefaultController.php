<?php

namespace PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($route)
    {
        $page = $this->getDoctrine()->getRepository('PageBundle:SeoPage')->findBy(
            [
                'route' => $route,
            ],
            null,
            1
        );

        return $this->render('PageBundle:Default:index.html.twig',
            array(
                'name' => $route,
                'this' => $this,
            )
        );
    }
}
