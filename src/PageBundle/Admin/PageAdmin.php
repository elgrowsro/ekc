<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 01.02.16
 * Time: 10:18
 */

namespace PageBundle\Admin;

use Oh\GoogleMapFormTypeBundle\Form\Type\GoogleMapType;
use PageBundle\Entity\PageSettings;
use PageBundle\Entity\SeoPage;
use SettingsBundle\Entity\MySettings;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class PageAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    public function setSC()
    {
//        echo 123;
    }


    /**
     * @param string $name
     * @param string $template
     */
    public function setTemplate($name, $template)
    {
        $this->templates[$name] = $template;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        $collection->add('pageedit');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->with('Настройки', array('class' => 'col-md-12'))
            ->add('route', 'text', [

                'read_only' => true,
                'disabled' => true,
            ])
            ->add('pagetype', 'choice',
                array(
                    'label' => 'Тип Наполнения (если изменить и сохранить, то данные наполнения удалятся)',
                    'choices' => array(
                        //case из getImmutableOptions()
                        'default' => 'Типовая страница',
//                        'ex_default' => 'Типовая страница (с подзаголовком)',
//                        'main_page' => 'Главная страница',
//                        'extended' => 'О компании',
//                        'ofdbofvb' => 'Разместиться на сайте (1 шаг)',
//                        'ofdb2' => 'Вендор / Разместиться на сайте (2 шаг)',
//                        'ofdb3' => 'Вендор / Разместиться на сайте (3 шаг)',
//                        'ofdb4' => 'Вендор / Разместиться на сайте (4 шаг)',
//                        'ofdb5' => 'Вендор / Разместиться на сайте (5 шаг)',
//                        'ofdb6' => 'Вендор / Разместиться на сайте (6 шаг)',
//                        'ofvb2' => 'Дилер / Разместиться на сайте (2 шаг)',
//                        'ofvb3' => 'Дилер / Разместиться на сайте (3 шаг)',
//                        'ofvb4' => 'Дилер / Разместиться на сайте (4 шаг)',
//                        'ofd' => 'Предложения для дилеров',
//                        'ofv' => 'Предложения для вендоров',
//                        'faq_homepage' => 'FAQ',
//                        'search' => 'Поиск',
//                        'service_promo' => 'Услуги / Реклама на портале',
//                        'service_audit' => 'Услуги / Аудит',
//                        'service_consulting' => 'Услуги / Консалтинг',
//                        'service_tariffs' => 'Услуги / Тарифы',
                    )
                )
            )
            ->end();

        $formMapper->with('Сео', array('class' => 'col-md-6'))
            ->add('title', 'text')
            ->add('name_link', 'text', [
                'required' => false,
                'label' => 'Текст для ссылки на данную страницу'
            ])
            ->add('enable_page', 'checkbox', [
                'label' => 'Включена страница?',
                'required' => false,
            ])
            ->add('keywords', 'text', [
                'required' => false,
            ])
            ->add('description', 'textarea', [
                'required' => false,
            ])
            ->end();
        $formMapper->with('Наполнение', array('class' => 'col-md-6'))
            ->add('settingsjoin', 'sonata_type_model', array(
                'property' => 'description',
                'label' => 'Наполнение страницы',
                'class' => 'SettingsBundle\Entity\MySettings',
                'required' => true,
                'multiple' => true,
                'expanded' => true,
//                'btn_add' => true,
//                'btn_list' => 'Показать',
//                'btn_delete' => true,
//                'btn_catalogue' => true
            ))
            ->add('data', 'sonata_type_immutable_array', array_merge(self::getImmutableOptions($this), ['label' => false]))
            ->end();
        $formMapper->setHelps([
            'enable_page' => 'Работает не для всех страниц'
        ]);

    }

    /**
     * Опции для кастомизации "поля"
     *
     * @param PageAdmin $set
     * @return array
     */
    public static function getImmutableOptions(PageAdmin $set)
    {
        $sb = $set->getSubject(); //SeoPage

        switch ($sb->getPagetype()) {
            case 'ofv':
                $options = array(
                    'keys' => array(
                        array('dropdown_invest_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_invest_from'),
                                'label' => 'Какие у Вас есть вложения? От (Через запятую)',
                            )
                        ),
                        array('dropdown_invest_to', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_invest_to'),
                                'label' => 'Какие у Вас есть вложения? До (Через запятую)',
                            )
                        ),
                        array('dropdown_cashback_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_cashback_from'),
                                'label' => 'Как быстро окупятся вложения в бизнес для дилера? От (Через запятую)',
                            )
                        ),
                        array('dropdown_cashback_to', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_cashback_to'),
                                'label' => 'Как быстро окупятся вложения в бизнес для дилера? До (Через запятую)',
                            )
                        ),
                        array('dropdown_profitFrom', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_profitFrom'),
                                'label' => 'Ожидаемая чистая прибыль? От (Через запятую)',
                            )
                        ),
                        array('dropdown_profitTo', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_profitFrom'),
                                'label' => 'Ожидаемая чистая прибыль? До (Через запятую)',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'service_tariffs':
                $options = array(
                    'keys' => array(
                        array('template', 'text',
                            array(
                                'data' => 'tariffs',
                                'label' => 'Шаблон',
                                'read_only' => true,
                            )
                        ),
                        array('subtitle', 'textarea',
                            array(
                                'data' => $sb->getDParray('subtitle'),
                                'label' => 'Подзаголовок (выделять слова можно <так>)',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'service_consulting':
                $icons = [
                    'icon-group' => 'icon-group',
                    'icon-note' => 'icon-note',
                    'icon-analize' => 'icon-analize',
                    'icon-law' => 'icon-law',
                    'icon-cash' => 'icon-cash',
                    'icon-question' => 'icon-question',
                    'icon-noteOk' => 'icon-noteOk',
                    'icon-view' => 'icon-view',
                    'icon-comp' => 'icon-comp',
                    'icon-light' => 'icon-light',

                    'icon-deal' => 'icon-deal', 'icon-graph' => 'icon-graph',
                    'icon-secNote' => 'icon-secNote', 'icon-rupor' => 'rupor',
                    'icon-nBook' => 'nBook', 'icon-package' => 'package',
                    'icon-person' => 'person',
                ];
                $options = array(
                    'keys' => array(
                        array('template', 'text',
                            array(
                                'data' => 'consulting',
                                'label' => 'Шаблон',
                                'read_only' => true,
                            )
                        ),
                        /* первый экран */
                        array('con_about', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_about'),
                                'label' => 'Введение',
                            )
                        ),
                        array('con_title', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_title'),
                                'label' => '1 Экран / Заголовок',
                            )
                        ),
                        array('con_text1', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_text1'),
                                'label' => '1 Экран / 1 текст',
                            )
                        ),
                        /* 1 экран / блок*/
                        array('con_b_1_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('con_b_1_icon'),
                                'label' => '1 Экран / Блок / 1 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('con_b_1_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_1_title'),
                                'label' => '1 Экран / Блок / 1 пункт / Заголовок',
                            )
                        ),
                        array('con_b_1_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_b_1_text'),
                                'label' => '1 Экран / Блок / 1 пункт / Текст',
                            )
                        ),

                        array('con_b_2_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('con_b_2_icon'),
                                'label' => '1 Экран / Блок / 2 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('con_b_2_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_2_title'),
                                'label' => '1 Экран / Блок / 2 пункт / Заголовок',
                            )
                        ),
                        array('con_b_2_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_b_2_text'),
                                'label' => '1 Экран / Блок / 2 пункт / Текст',
                            )
                        ),

                        array('con_b_3_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('con_b_3_icon'),
                                'label' => '1 Экран / Блок / 3 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('con_b_3_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_3_title'),
                                'label' => '1 Экран / Блок / 3 пункт / Заголовок',
                            )
                        ),
                        array('con_b_3_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_b_3_text'),
                                'label' => '1 Экран / Блок / 3 пункт / Текст',
                            )
                        ),

                        array('con_b_4_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('con_b_4_icon'),
                                'label' => '1 Экран / Блок / 4 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('con_b_4_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_4_title'),
                                'label' => '1 Экран / Блок / 4 пункт / Заголовок',
                            )
                        ),
                        array('con_b_4_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_b_4_text'),
                                'label' => '1 Экран / Блок / 4 пункт / Текст',
                            )
                        ),

                        array('con_b_5_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('con_b_5_icon'),
                                'label' => '1 Экран / Блок / 5 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('con_b_5_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_5_title'),
                                'label' => '1 Экран / Блок / 5 пункт / Заголовок',
                            )
                        ),
                        array('con_b_5_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_b_5_text'),
                                'label' => '1 Экран / Блок / 5 пункт / Текст',
                            )
                        ),

                        array('con_b_6_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('con_b_6_icon'),
                                'label' => '1 Экран / Блок / 6 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('con_b_6_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_6_title'),
                                'label' => '1 Экран / Блок / 6 пункт / Заголовок',
                            )
                        ),
                        array('con_b_6_text', 'text',
                            array(
                                'data' => $sb->getDParray('con_b_6_text'),
                                'label' => '1 Экран / Блок / 6 пункт / Текст',
                            )
                        ),
                        /* //1 экран / блок*/
                        /* 2 экран */
                        array('fpro_title', 'text',
                            array(
                                'data' => $sb->getDParray('fpro_title'),
                                'label' => '2 Экран / Заголовок',
                            )
                        ),
                        /* 2 экран / блок*/
                        array('fpro_b_1_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('fpro_b_1_icon'),
                                'label' => '2 Экран / Блок / 1 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('fpro_b_1_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('fpro_b_1_text'),
                                'label' => '2 Экран / Блок / 1 пункт / Текст',
                            )
                        ),
                        array('fpro_b_2_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('fpro_b_2_icon'),
                                'label' => '2 Экран / Блок / 2 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('fpro_b_2_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('fpro_b_2_text'),
                                'label' => '2 Экран / Блок / 2 пункт / Текст',
                            )
                        ),
                        array('fpro_b_3_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('fpro_b_3_icon'),
                                'label' => '2 Экран / Блок / 3 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('fpro_b_3_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('fpro_b_3_text'),
                                'label' => '2 Экран / Блок / 3 пункт / Текст',
                            )
                        ),
                        array('fpro_b_4_icon', 'choice',
                            array(
                                'data' => $sb->getDParray('fpro_b_4_icon'),
                                'label' => '2 Экран / Блок / 4 пункт / Иконка',
                                'choices' => $icons,
                            )
                        ),
                        array('fpro_b_4_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('fpro_b_4_text'),
                                'label' => '2 Экран / Блок / 4 пункт / Текст',
                            )
                        ),
                        /* //2 экран / блок*/
                        /* 3 экран */
                        array('cons_title', 'text',
                            array(
                                'data' => $sb->getDParray('cons_title'),
                                'label' => '3 Экран / Заголовок',
                            )
                        ),
                        array('cons_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('cons_text'),
                                'label' => '3 Экран / Текст',
                            )
                        ),
                        array('cons_green', 'textarea',
                            array(
                                'data' => $sb->getDParray('cons_green'),
                                'label' => '3 Экран / Зеленый текст (выделять слова можно <так>)',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'service_audit':
                $icons = [
                    'icon-deal' => 'icon-deal', 'icon-graph' => 'icon-graph',
                    'icon-secNote' => 'icon-secNote', 'icon-rupor' => 'rupor',
                    'icon-nBook' => 'nBook', 'icon-package' => 'package',
                    'icon-group' => 'group', 'icon-note' => 'note',
                    'icon-analize' => 'analize', 'icon-law' => 'law',
                    'icon-cash' => 'cash', 'icon-question' => 'question',
                    'icon-person' => 'person',
                ];
                $options = array(
                    'keys' => array(
                        array('template', 'text',
                            array(
                                'data' => 'audit',
                                'label' => 'Шаблон',
                                'read_only' => true,
                            )
                        ),
                        /* первый экран */
                        array('a_screen_1text', 'textarea',
                            array(
                                'data' => $sb->getDParray('a_screen_1text'),
                                'label' => '1 Экран / 1 текст',
                            )
                        ),
                        array('a_screen_2text', 'textarea',
                            array(
                                'data' => $sb->getDParray('a_screen_2text'),
                                'label' => '1 Экран / 2 текст',
                            )
                        ),
                        array('a_screen_block_1_i', 'choice',
                            array(
                                'data' => $sb->getDParray('a_screen_block_1_i'),
                                'label' => '1 Экран / Иконка голубого блока',
                                'choices' => $icons,
                            )
                        ),
                        array('a_screen_block_1_t', 'textarea',
                            array(
                                'data' => $sb->getDParray('a_screen_block_1_t'),
                                'label' => '1 Экран / Текст голубого блока',
                            )
                        ),
                        array('a_screen_block_2_i', 'choice',
                            array(
                                'data' => $sb->getDParray('a_screen_block_2_i'),
                                'label' => '1 Экран / Иконка синего блока',
                                'choices' => $icons,
                            )
                        ),
                        array('a_screen_block_2_t', 'textarea',
                            array(
                                'data' => $sb->getDParray('a_screen_block_2_t'),
                                'label' => '1 Экран / Текст синего блока',
                            )
                        ),
                        array('a_screen_block_3_i', 'choice',
                            array(
                                'data' => $sb->getDParray('a_screen_block_3_i'),
                                'label' => '1 Экран / Иконка зеленого блока',
                                'choices' => $icons,
                            )
                        ),
                        array('a_screen_block_3_t', 'textarea',
                            array(
                                'data' => $sb->getDParray('a_screen_block_3_t'),
                                'label' => '1 Экран / Текст зеленого блока',
                            )
                        ),
                        array('a_screen_3text', 'textarea',
                            array(
                                'data' => $sb->getDParray('a_screen_3text'),
                                'label' => '1 Экран / Текст под блоками',
                            )
                        ),
                        /* второй экран */
                        array('ainf_screen_title', 'text',
                            array(
                                'data' => $sb->getDParray('ainf_screen_title'),
                                'label' => '2 Экран / Заголовок',
                            )
                        ),
                        array('ainf_screen_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('ainf_screen_text'),
                                'label' => '2 Экран / Текст',
                            )
                        ),
                        /* третий экран */
                        array('apro_screen_title', 'text',
                            array(
                                'data' => $sb->getDParray('apro_screen_title'),
                                'label' => '3 Экран / Заголовок',
                            )
                        ),
                        array('apro_screen_block_p1', 'text',
                            array(
                                'data' => $sb->getDParray('apro_screen_block_p1'),
                                'label' => '3 Экран / Блок / 1 пункт',
                            )
                        ),
                        array('apro_screen_block_p2', 'text',
                            array(
                                'data' => $sb->getDParray('apro_screen_block_p2'),
                                'label' => '3 Экран / Блок / 2 пункт',
                            )
                        ),
                        array('apro_screen_block_p3', 'text',
                            array(
                                'data' => $sb->getDParray('apro_screen_block_p3'),
                                'label' => '3 Экран / Блок / 3 пункт',
                            )
                        ),
                        array('apro_screen_block_p4', 'text',
                            array(
                                'data' => $sb->getDParray('apro_screen_block_p4'),
                                'label' => '3 Экран / Блок / 4 пункт',
                            )
                        ),
                        /* четвертый экран */
                        array('con_screen_title', 'text',
                            array(
                                'data' => $sb->getDParray('con_screen_title'),
                                'label' => '4 Экран / Заголовок',
                            )
                        ),
                        array('con_screen_text', 'textarea',
                            array(
                                'data' => $sb->getDParray('con_screen_text'),
                                'label' => '4 Экран / Текст',
                            )
                        ),
                        array('con_screen_green', 'text',
                            array(
                                'data' => $sb->getDParray('con_screen_green'),
                                'label' => '4 Экран / Текст в зеленом блоке',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'service_promo':
                $options = array(
                    'keys' => array(
                        array('template', 'text',
                            array(
                                'data' => 'promo',
                                'label' => 'Шаблон',
                                'read_only' => true,
                            )
                        ),
                        array('subtitle', 'textarea',
                            array(
                                'data' => $sb->getDParray('subtitle'),
                                'label' => 'Подзаголовок (выделять слова можно <так>)',
                            )
                        ),
                        array('info', 'textarea',
                            array(
                                'data' => $sb->getDParray('info'),
                                'label' => 'Вступление.',
                            )
                        ),
                        array('text_infoanalityc', 'redactor',
                            array(
                                'redactor' => 'admin',
                                'data' => $sb->getDParray('text_infoanalityc'),
                                'label' => 'Текст к Размещение информационно аналитических статей о вашей компании и ваших товарах.',
                            )
                        ),
                        array('mprice_infoanalityc', 'text',
                            array(
                                'data' => $sb->getDParray('mprice_infoanalityc'),
                                'label' => 'Цена за месяц к Р и а с о в к и в т.',
                            )
                        ),
                        array('wprice_infoanalityc', 'text',
                            array(
                                'data' => $sb->getDParray('wprice_infoanalityc'),
                                'label' => 'Цена за неделю к Р и а с о в к и в т.',
                            )
                        ),
                        array('text_adtextslide', 'redactor',
                            array(
                                'redactor' => 'admin',
                                'data' => $sb->getDParray('text_adtextslide'),
                                'label' => 'Текст к Размещение рекламного текста на слайдере.',
                            )
                        ),
                        array('mprice_adtextslide', 'text',
                            array(
                                'data' => $sb->getDParray('mprice_adtextslide'),
                                'label' => 'Цена за месяц к Р р т н с.',
                            )
                        ),
                        array('wprice_adtextslide', 'text',
                            array(
                                'data' => $sb->getDParray('wprice_adtextslide'),
                                'label' => 'Цена за неделю к Р р т н с.',
                            )
                        ),
                        array('text_bannerad', 'redactor',
                            array(
                                'redactor' => 'admin',
                                'data' => $sb->getDParray('text_bannerad'),
                                'label' => 'Текст к Размещение баннерной рекламы.',
                            )
                        ),
                        array('mprice_bannerad', 'text',
                            array(
                                'data' => $sb->getDParray('mprice_bannerad'),
                                'label' => 'Цена за месяц к Р б р.',
                            )
                        ),
                        array('wprice_bannerad', 'text',
                            array(
                                'data' => $sb->getDParray('wprice_bannerad'),
                                'label' => 'Цена за неделю к Р б р.',
                            )
                        ),
                        array('text_horizbanner', 'redactor',
                            array(
                                'redactor' => 'admin',
                                'data' => $sb->getDParray('text_horizbanner'),
                                'label' => 'Текст к Размещение горизонтального баннера.',
                            )
                        ),
                        array('mprice_horizbanner', 'text',
                            array(
                                'data' => $sb->getDParray('mprice_horizbanner'),
                                'label' => 'Цена за месяц к Р г б.',
                            )
                        ),
                        array('wprice_horizbanner', 'text',
                            array(
                                'data' => $sb->getDParray('wprice_horizbanner'),
                                'label' => 'Цена за неделю к Р г б.',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'search':
                if (is_array($sb->getDParray('sections'))) {
                    $data = $sb->getDParray('sections');
                } else {
                    $data = [];
                }
                $options = array(
                    'keys' => array(
                        array('sections', 'choice',
                            array(
                                'data' => $data,
                                'label' => 'Разделы',
                                'multiple' => true,
                                'choices' => array(
                                    'analytics' => 'Аналитика',
                                    'vendors' => 'Вендоры',
                                    'dealers' => 'Дилеры'
                                )
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofd':
                $options = array(
                    'keys' => array(
                        array('dropdown_invest_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_invest_from'),
                                'label' => 'Вложения в бизнес От (Через запятую)',
                            )
                        ),
                        array('dropdown_invest_to', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_invest_to'),
                                'label' => 'Вложения в бизнес До (Через запятую)',
                            )
                        ),
                        array('dropdown_payback_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_payback_from'),
                                'label' => 'Сколько вы готовы ждать окупаемости вложений? От (Через запятую)',
                            )
                        ),
                        array('dropdown_payback_to', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_payback_to'),
                                'label' => 'Сколько вы готовы ждать окупаемости вложений? До (Через запятую)',
                            )
                        ),
                        array('dropdown_profit_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_profit_from'),
                                'label' => 'Какую прибыль Вы ожидаете зарабатывать? От (Через запятую)',
                            )
                        ),
                        array('dropdown_profit_to', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_profit_to'),
                                'label' => 'Какую прибыль Вы ожидаете зарабатывать? От (Через запятую)',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofvb4':
                $options = array(
                    'keys' => array(
                        array('tooltip1_invest', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_invest'),
                                'label' => 'Подсказка к Какие у Вас есть вложения',
                            )
                        ),
                        array('tooltip2_speed', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_speed'),
                                'label' => 'Подсказка к Как быстро окупятся вложения в бизнесс для дилера',
                            )
                        ),
                        array('tooltip3_avail_office', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_avail_office'),
                                'label' => 'Подсказка к Наличие офисных помещений',
                            )
                        ),
                        array('tooltip4_avail_flat', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_avail_flat'),
                                'label' => 'Подсказка к Наличие торговых площадей',
                            )
                        ),
                        array('tooltip5_avail_stock', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip5_avail_stock'),
                                'label' => 'Подсказка к Наличие складских площадей',
                            )
                        ),
                        array('tooltip6_about', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip6_about'),
                                'label' => 'Подсказка к Расскажите о себе в качестве дилера',
                            )
                        ),
                        array('tooltip7_location', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip7_location'),
                                'label' => 'Подсказка к В каких регионах Вы можете предоставлять интерес для вендоров?',
                            )
                        ),
                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofvb3':
                $options = array(
                    'keys' => array(
                        array('tooltip1_orgtype', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_orgtype'),
                                'label' => 'Подсказка к Организационная форма',
                            )
                        ),
                        array('tooltip1_fioip', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_fioip'),
                                'label' => 'Подсказка к ФИО ИП',
                            )
                        ),
                        array('tooltip1_tooltip1_entity', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_tooltip1_entity'),
                                'label' => 'Подсказка к Юридическое лицо',
                            )
                        ),
                        array('tooltip1_company', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_company'),
                                'label' => 'Подсказка к Название компании',
                            )
                        ),
                        array('tooltip3_city', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_city'),
                                'label' => 'Подсказка к Город',
                            )
                        ),
                        array('tooltip4_house', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_house'),
                                'label' => 'Подсказка к Дом',
                            )
                        ),
                        array('tooltip5_office', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip5_office'),
                                'label' => 'Подсказка к Офис\помещение',
                            )
                        ),
                        array('tooltip6_direct', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip6_direct'),
                                'label' => 'Подсказка к Направления и ваши сферы деятельности',
                            )
                        ),
                        array('tooltip7_contactname', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip7_contactname'),
                                'label' => 'Подсказка к Фото контактного лица',
                            )
                        ),
                        array('tooltip7_contactname', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip7_contactname'),
                                'label' => 'Подсказка к Фото контактного лица',
                            )
                        ),
                        array('tooltip8_street', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip8_street'),
                                'label' => 'Подсказка к Улица',
                            )
                        ),
                        array('tooltip9_site', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip9_site'),
                                'label' => 'Подсказка к Сайт компании',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofvb2':
                $options = array(
                    'keys' => array(
                        array('tooltip1_own', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_own'),
                                'label' => 'Подсказка к Форма собственности',
                            )
                        ),
                        array('tooltip2_name', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_name'),
                                'label' => 'Подсказка к Имя контактного лица',
                            )
                        ),
                        array('tooltip3_phone', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_phone'),
                                'label' => 'Подсказка к Контактный телефон',
                            )
                        ),
                        array('tooltip4_email', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_email'),
                                'label' => 'Подсказка к E-mail',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofdb6':
                $options = array(
                    'keys' => array(
                        array('tooltip1_conditions', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_conditions'),
                                'label' => 'Подсказка к Выберете оптимальные условия размещения',
                            )
                        ),
                        array('tooltip2_pay', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_pay'),
                                'label' => 'Подсказка к Способ оплаты',
                            )
                        ),
                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofdb5':
                $options = array(
                    'keys' => array(
                        array('tooltip1_invest', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_invest'),
                                'label' => 'Подсказка к Вложения в Ваш бизнес',
                            )
                        ),
                        array('tooltip2_wait', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_wait'),
                                'label' => 'Подсказка к Сколько вы готовы ждать окупаемости вложений',
                            )
                        ),
                        array('tooltip3_expect', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_expect'),
                                'label' => 'Подсказка к Какую прибыль Вы ожидаете зарабатывать?',
                            )
                        ),
                        array('tooltip4_office_avail', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_office_avail'),
                                'label' => 'Подсказка к Наличие офисных помещений',
                            )
                        ),
                        array('tooltip5_flat_avail', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip5_flat_avail'),
                                'label' => 'Подсказка к Наличие торговых площадей',
                            )
                        ),
                        array('tooltip6_stock_avail', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip6_stock_avail'),
                                'label' => 'Подсказка к Наличие складских площадей',
                            )
                        ),
                        array('tooltip7_hr_avail', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip7_hr_avail'),
                                'label' => 'Подсказка к Наличие персонала',
                            )
                        ),
                        array('tooltip8_legal_avail', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip8_legal_avail'),
                                'label' => 'Подсказка к Наличие Юр. Лица или ИП',
                            )
                        ),
                        array('tooltip9_bussines_avail', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip9_bussines_avail'),
                                'label' => 'Подсказка к Наличие у дилера действующего бизнеса в смежной или
                                аналогичной
                                сфере',
                            )
                        ),
                        array('tooltip10_special', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip10_special'),
                                'label' => 'Подсказка к Расскажите об особенностях Вашей компании',
                            )
                        ),
                        array('tooltip11_give', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip11_give'),
                                'label' => 'Подсказка к Что Вы готовы предложить дилерам',
                            )
                        ),
                        array('tooltip12_location', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip12_location'),
                                'label' => 'Подсказка к Выберете регионы и страны, в которых Вы хотите найти
                                дилеров',
                            )
                        ),
                        array('tooltip13_photo', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip13_photo'),
                                'label' => 'Подсказка к Загрузите фотографии Вашей продукции или Вашей компании',
                            )
                        ),
                        array('tooltip14_docs', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip14_docs'),
                                'label' => 'Подсказка к Загрузите отраслевые документы (прайсы, презентации)',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofdb4':
                $options = array(
                    'keys' => array(
                        array('tooltip1_city', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_city'),
                                'label' => 'Подсказка к Город',
                            )
                        ),
                        array('tooltip2_index', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_index'),
                                'label' => 'Подсказка к Индекс',
                            )
                        ),
                        array('tooltip3_street', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_street'),
                                'label' => 'Подсказка к Улица',
                            )
                        ),
                        array('tooltip4_house', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_house'),
                                'label' => 'Подсказка к Дом',
                            )
                        ),
                        array('tooltip5_office', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip5_office'),
                                'label' => 'Подсказка к Офис/Помещение',
                            )
                        ),
                        array('tooltip6_phone', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip6_phone'),
                                'label' => 'Подсказка к Общий телефон',
                            )
                        ),
                        array('tooltip7_email', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip7_email'),
                                'label' => 'Подсказка к E-mail компании',
                            )
                        ),
                        array('tooltip8_skype', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip8_skype'),
                                'label' => 'Подсказка к Skype компании',
                            )
                        ),
                        array('tooltip9_site', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip9_site'),
                                'label' => 'Подсказка к Сайт компании',
                            )
                        ),
                        array('tooltip10_head', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip10_head'),
                                'label' => 'Подсказка к В каком регионе располагается головной',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofdb3':
                $options = array(
                    'keys' => array(
                        array('tooltip1_meet', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_meet'),
                                'label' => 'Подсказка к С кем будут общаться потеницальные дилеры',
                            )
                        ),
                        array('tooltip2_photo', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_photo'),
                                'label' => 'Подсказка к Фото',
                            )
                        ),
                        array('tooltip3_skype', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_skype'),
                                'label' => 'Подсказка к Skype контактного лица',
                            )
                        ),
                        array('tooltip4_position', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_position'),
                                'label' => 'Подсказка к Должность',
                            )
                        ),
                        array('tooltip5_phone', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip5_phone'),
                                'label' => 'Подсказка к Телефон',
                            )
                        ),
                        array('tooltip6_email', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip6_email'),
                                'label' => 'Подсказка к E-mail',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofdb2':
                $options = array(
                    'keys' => array(
                        array('tooltip1_direct', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip1_direct'),
                                'label' => 'Подсказка к Направления и ваши сферы деятельности',
                            )
                        ),
                        array('tooltip2_legal', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip2_legal'),
                                'label' => 'Подсказка к Юридическое лицо',
                            )
                        ),
                        array('tooltip3_company', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip3_company'),
                                'label' => 'Подсказка к Коммерческое название Вашей компании',
                            )
                        ),
                        array('tooltip4_type', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip4_type'),
                                'label' => 'Подсказка к Организационно-правовая форма',
                            )
                        ),
                        array('tooltip5_logo', 'textarea',
                            array(
                                'data' => $sb->getDParray('tooltip5_logo'),
                                'label' => 'Подсказка к Логотип',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ofdbofvb':
                $options = array(
                    'keys' => array(
                        array('want_to_be_dealer', 'redactor',
                            array(
                                'data' => $sb->getDParray('want_to_be_dealer'),
                                'label' => 'Стать дилером',
                                'redactor' => 'admin',
                            )
                        ),
                        array('want_to_be_vendor', 'redactor',
                            array(
                                'data' => $sb->getDParray('want_to_be_vendor'),
                                'label' => 'Стать вендором',
                                'redactor' => 'admin',
                            )
                        ),
                        array('want_to_be_subscriber', 'redactor',
                            array(
                                'data' => $sb->getDParray('want_to_be_subscriber'),
                                'label' => 'Стать подписчиком',
                                'redactor' => 'admin',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'main_page':
                $options = array(
                    'keys' => array(
                        array('dropdown_attachment_to', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_attachment_to'),
                                'label' => 'Какие у вас есть вложения? (Через запятую)',
                            )
                        ),
                        array('placeholder_attachment_to', 'text',
                            array(
                                'data' => $sb->getDParray('placeholder_attachment_to'),
                                'label' => 'Какие у вас есть вложения? подпись',
                            )
                        ),
                        array('dropdown_profit_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_profit_from'),
                                'label' => 'Какую прибыль Вы ожидаете зарабатывать? (Через запятую)',
                            )
                        ),
                        array('placeholder_profit_from', 'text',
                            array(
                                'data' => $sb->getDParray('placeholder_profit_from'),
                                'label' => 'Какую прибыль Вы ожидаете зарабатывать? подпись',
                            )
                        ),
                        array('dropdown_payback_from', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_payback_from'),
                                'label' => 'Как быстро окупятся вложения в бизнес для дилера? (Через запятую)',
                            )
                        ),
                        array('placeholder_payback_from', 'text',
                            array(
                                'data' => $sb->getDParray('placeholder_payback_from'),
                                'label' => 'Как быстро окупятся вложения в бизнес для дилера? подпись',
                            )
                        ),
                        array('dropdown_investmentTo', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_investmentTo'),
                                'label' => 'Объем возможных инвестиций? (Через запятую)',
                            )
                        ),
                        array('placeholder_investmentTo', 'text',
                            array(
                                'data' => $sb->getDParray('placeholder_investmentTo'),
                                'label' => 'Объем возможных инвестиций? подпись',
                            )
                        ),
                        array('dropdown_returnTo', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_returnTo'),
                                'label' => 'Ожидаемая окупаемость? (Через запятую)',
                            )
                        ),
                        array('placeholder_returnTo', 'text',
                            array(
                                'data' => $sb->getDParray('placeholder_returnTo'),
                                'label' => 'Ожидаемая окупаемость? подпись',
                            )
                        ),
                        array('dropdown_profitFrom', 'text',
                            array(
                                'data' => $sb->getDParray('dropdown_profitFrom'),
                                'label' => 'Ожидаемая чистая прибыль? (Через запятую)',
                            )
                        ),
                        array('placeholder_profitFrom', 'text',
                            array(
                                'data' => $sb->getDParray('placeholder_profitFrom'),
                                'label' => 'Ожидаемая чистая прибыль? подпись',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'extended':
                $mapopts = [];
                if ($sb->getDParray("latlng")) {
                    $mapopts['data'] = $sb->getDParray("latlng");
                    $mapopts['label'] = 'Отметка на карте';
                }
                $options = array(
                    'keys' => array(
                        array('textpage_about', 'redactor',
                            array(
                                'mapped' => false,
                                'required' => false,
                                'redactor' => 'admin',
                                'data' => $sb->getDParray("textpage_about"),
                                'label' => 'Текст',
                            )
                        ),
                        array('videopage_about', 'text',
                            array(
                                'mapped' => false,
                                'required' => false,
                                'data' => $sb->getDParray("videopage_about"),
                                'label' => 'Ссылка на видео',
                            )
                        ),
                        array('latlng', new GoogleMapType(), $mapopts)
                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'date_example':
                $options = array(
                    'keys' => array(
                        array('test_date', 'sonata_type_datetime_picker',
                            array(
                                'required' => false,
                                'data' => new \DateTime($sb->getDParray("test_date")),
                            )
                        ),
                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'ex_default':
                $options = array(
                    'keys' => array(
                        array('subtitle', 'textarea',
                            array(
                                'data' => $sb->getDParray("subtitle"),
                                'label' => 'Подзаголовок (выделять слова можно <так>)',
                            )
                        ),
                        array('textpage', 'redactor',
                            array(
                                'mapped' => false,
                                'required' => false,
                                'redactor' => 'admin',
                                'data' => $sb->getDParray("textpage"),
                                'label' => 'Контент',
                            )
                        ),
                    ), 'mapped' => false, 'required' => false
                );
                break;
            case 'faq_homepage':
                $options = array(
                    'keys' => array(
                        array('faq_title_page', 'text',
                            array(
                                'data' => $sb->getDParray('faq_title_page'),
                                'label' => 'Заголовок страницы',
                            )
                        ),
                        array('faq_wysiwyg_page', 'redactor',
                            array(
                                'mapped' => false,
                                'required' => false,
                                'redactor' => 'admin',
                                'data' => $sb->getDParray("faq_wysiwyg_page"),
                                'label' => 'Контент',
                            )
                        ),
                        array('faq_video_mp4_page', 'text',
                            array(
                                'data' => $sb->getDParray('faq_video_mp4_page'),
                                'label' => 'Mp4',
                            )
                        ),
                        array('faq_video_Webm_page', 'text',
                            array(
                                'data' => $sb->getDParray('faq_video_Webm_page'),
                                'label' => 'Webm',
                            )
                        ),

                    ), 'mapped' => false, 'required' => false
                );
                break;
            default:
                $options = array(
                    'keys' => array(
                        array('titlepage_documents', 'text',
                            array(
                                'data' => $sb->getDParray('titlepage_documents'),
                                'label' => 'Заголовок контента',
                            )
                        ),
                        array('textpage_documents', 'redactor',
                            array(
                                'mapped' => false,
                                'required' => false,
                                'redactor' => 'admin',
                                'data' => $sb->getDParray("textpage_documents"),
                                'label' => 'Контент',
                            )
                        ),
                    ), 'mapped' => false, 'required' => false
                );
        }

        return $options;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title'/*, null, array(
            'route' => array(
                'name' => 'pageedit'
            )
        )*/);
        $listMapper->addIdentifier('keywords'/*, null, array(
            'route' => array(
                'name' => 'pageedit'
            )
        )*/);
        $listMapper->addIdentifier('description'/*, null, array(
            'route' => array(
                'name' => 'pageedit'
            )
        )*/);

//        $listMapper->add('_action', 'actions', array(
//            'label' => 'Настройки',
//            'actions' => array(
//                'settings' => array(
//                    'template' => 'PageBundle:CRUD:seopage_settings.html.twig'
//                )
//            )
//        ));
    }


    /**
     * @var SeoPage $post
     */
    public function preUpdate($post)
    {
        $uniqid = $this->getRequest()->query->get('uniqid');
        $formData = $this->getRequest()->request->get($uniqid);
        $post->setDatapage(json_encode($formData["data"]));


        /** @var MySettings $setting */
        foreach ($post->getSettingsjoin() as $setting) {
            $setting->setPagejoin($post);
        }

    }


}