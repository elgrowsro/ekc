<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.04.16
 * Time: 15:16
 */

namespace PageBundle\Admin;


use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\Admin;

class PageSettingsAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {

            $formMapper
                ->add('settingsjoin', 'sonata_type_model_list', [
                    'required' => true,
                    'by_reference' => false,
                    'label' => 'Настройка',
                    'btn_add' => 'Добавить',
                    'btn_list' => false,
                    'btn_delete' => 'Удалить',
                ], [
                        'edit' => 'inline',
                        'inline' => 'table'
                    ]
                );
        } else {

            $formMapper
                ->add('settingsjoin', 'sonata_type_model_list', [
                    'required' => true,
                    'by_reference' => false,
                    'label' => 'Настройка',
                    'btn_add' => false,
                    'btn_list' => false,
                    'btn_delete' => false,
                ], [
                        'edit' => 'inline',
                        'inline' => 'table'
                    ]
                );
        }
//        $formMapper
//            ->add('onAdmin', 'checkbox', [
//                'label' => 'Только для админа?',
//                'required' => false
//            ]);
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('onAdmin');
    }

    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);

        $query
            ->andWhere('o.onAdmin != 1');


        return $query;
    }
}