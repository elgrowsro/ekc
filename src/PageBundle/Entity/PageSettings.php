<?php

namespace PageBundle\Entity;

/**
 * SeoPage
 */
class PageSettings
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \SettingsBundle\Entity\MySettings
     */
    private $settingsjoin;

    /**
     * @var \SettingsBundle\Entity\MySettings
     */
    private $pagejoin;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set settingsjoin
     *
     * @param \SettingsBundle\Entity\MySettings $settingsjoin
     *
     * @return PageSettings
     */
    public function setSettingsjoin(\SettingsBundle\Entity\MySettings $settingsjoin = null)
    {
        $this->settingsjoin = $settingsjoin;

        return $this;
    }

    /**
     * Get settingsjoin
     *
     * @return \SettingsBundle\Entity\MySettings
     */
    public function getSettingsjoin()
    {
        return $this->settingsjoin;
    }

    /**
     * @var boolean
     */
    private $onAdmin = 0;


    /**
     * Set onAdmin
     *
     * @param boolean $onAdmin
     *
     * @return PageSettings
     */
    public function setOnAdmin($onAdmin)
    {
        $this->onAdmin = $onAdmin;

        return $this;
    }

    /**
     * Get onAdmin
     *
     * @return boolean
     */
    public function getOnAdmin()
    {
        return (boolean)$this->onAdmin;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return 'название';
    }

    /**
     * Set pagejoin
     *
     * @param \PageBundle\Entity\SeoPage $pagejoin
     *
     * @return PageSettings
     */
    public function setPagejoin(\PageBundle\Entity\SeoPage $pagejoin = null)
    {
        $this->pagejoin = $pagejoin;

        return $this;
    }

    /**
     * Get pagejoin
     *
     * @return \PageBundle\Entity\SeoPage
     */
    public function getPagejoin()
    {
        return $this->pagejoin;
    }
}
