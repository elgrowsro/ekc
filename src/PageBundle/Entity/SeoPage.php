<?php

namespace PageBundle\Entity;

/**
 * SeoPage
 */
class SeoPage
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $keywords;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $datapage;

    /**
     * @var array
     */
    private $datapage_array;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return SeoPage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return SeoPage
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SeoPage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datapage
     *
     * @param string $datapage
     *
     * @return SeoPage
     */
    public function setDatapage($datapage)
    {
        $this->datapage = $datapage;

        return $this;
    }

    /**
     * Get datapage
     *
     * @return string
     */
    public function getDatapage()
    {
        return $this->datapage;
    }

    /**
     * Get string value
     *
     * @return string
     */
    public function getDParray($name)
    {

        if (!$this->datapage_array && is_null($this->datapage_array)) {
            $this->datapage_array = json_decode($this->datapage, true);
        }

        if (!isset($this->datapage_array[$name])) {
            return "";
        } else {
            return $this->datapage_array[$name];
        }
    }

    public function setLatitude($latlng)
    {
        $js_decode = [];
        if ($this->datapage) {
            $js_decode = json_decode($this->datapage, true);
        }
        $js_decode["latitude"] = $latlng;
        $this->datapage = json_encode($js_decode);

        return $this;
    }

    public function setLongitude($latlng)
    {
        $js_decode = [];
        if ($this->datapage) {
            $js_decode = json_decode($this->datapage, true);
        }
        $js_decode["longitude"] = $latlng;
        $this->datapage = json_encode($js_decode);

        return $this;
    }

    public function setLatLng($latlng)
    {
        $this->setLatitude($latlng['latlng']['lat'])
            ->setLongitude($latlng['latlng']['lng']);

        return $this;
    }

    public function getLatLng()
    {
        $array = array('latlng' => array('lat' => $this->getDParray("latitude"), 'lng' => $this->getDParray("longitude")));
//        var_dump($array);
        return $array;
    }

    /**
     * @var string
     */
    private $pagetype;


    /**
     * Set pagetype
     *
     * @param string $pagetype
     *
     * @return SeoPage
     */
    public function setPagetype($pagetype)
    {
        $this->pagetype = $pagetype;

        return $this;
    }

    /**
     * Get pagetype
     *
     * @return string
     */
    public function getPagetype()
    {
        return $this->pagetype;
    }

    /**
     * @var string
     */
    private $route;


    /**
     * Set route
     *
     * @param string $route
     *
     * @return SeoPage
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @var string
     */
    private $name_link;

    /**
     * @var boolean
     */
    private $enable_page = 1;


    /**
     * Set nameLink
     *
     * @param string $nameLink
     *
     * @return SeoPage
     */
    public function setNameLink($nameLink)
    {
        $this->name_link = $nameLink;

        return $this;
    }

    /**
     * Get nameLink
     *
     * @return string
     */
    public function getNameLink()
    {
        return $this->name_link;
    }

    /**
     * Set enablePage
     *
     * @param boolean $enablePage
     *
     * @return SeoPage
     */
    public function setEnablePage($enablePage)
    {
        $this->enable_page = $enablePage;

        return $this;
    }

    /**
     * Get enablePage
     *
     * @return boolean
     */
    public function getEnablePage()
    {
        return (boolean)$this->enable_page;
    }

    public $pagesettings = [];

    public function setPageSettings($ps)
    {
        $this->pagesettings = $ps;
    }

    public function getPageSettings()
    {
        return $this->pagesettings;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $settingsjoin;


    /**
     * Add settingsjoin
     *
     * @param \SettingsBundle\Entity\MySettings $settingsjoin
     *
     * @return SeoPage
     */
    public function addSettingsjoin(\SettingsBundle\Entity\MySettings $settingsjoin)
    {
        $this->settingsjoin[] = $settingsjoin;

        return $this;
    }

    /**
     * Remove settingsjoin
     *
     * @param \SettingsBundle\Entity\MySettings $settingsjoin
     */
    public function removeSettingsjoin(\SettingsBundle\Entity\MySettings $settingsjoin)
    {
        $this->settingsjoin->removeElement($settingsjoin);
    }

    /**
     * Get settingsjoin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettingsjoin()
    {
        return $this->settingsjoin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->settingsjoin = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
