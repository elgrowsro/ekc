<?php

namespace AdvertBundle\Entity;

/**
 * Places
 */
class Places
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $cntPlace;

    /**
     * @var integer
     */
    private $cntAdvert;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cntPlace
     *
     * @param integer $cntPlace
     *
     * @return Places
     */
    public function setCntPlace($cntPlace)
    {
        $this->cntPlace = $cntPlace;

        return $this;
    }

    /**
     * Get cntPlace
     *
     * @return integer
     */
    public function getCntPlace()
    {
        return $this->cntPlace;
    }

    /**
     * Set cntAdvert
     *
     * @param integer $cntAdvert
     *
     * @return Places
     */
    public function setCntAdvert($cntAdvert)
    {
        $this->cntAdvert = $cntAdvert;

        return $this;
    }

    /**
     * Get cntAdvert
     *
     * @return integer
     */
    public function getCntAdvert()
    {
        return $this->cntAdvert;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Places
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Places
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @var string
     */
    private $price;


    /**
     * Set price
     *
     * @param string $price
     *
     * @return Places
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $adverts;

    /**
     * @var integer
     */
    private $numPlace;


    /**
     * Set numPlace
     *
     * @param integer $numPlace
     *
     * @return Places
     */
    public function setNumPlace($numPlace)
    {
        $this->numPlace = $numPlace;

        return $this;
    }

    /**
     * Get numPlace
     *
     * @return integer
     */
    public function getNumPlace()
    {
        return $this->numPlace;
    }

    /**
     * Add advert
     *
     * @param \AdvertBundle\Entity\Advert $advert
     *
     * @return Places
     */
    public function addAdvert(\AdvertBundle\Entity\Advert $advert)
    {
        $this->adverts[] = $advert;

        return $this;
    }

    /**
     * Remove advert
     *
     * @param \AdvertBundle\Entity\Advert $advert
     */
    public function removeAdvert(\AdvertBundle\Entity\Advert $advert)
    {
        $this->adverts->removeElement($advert);
    }

    /**
     * Get adverts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdverts()
    {
        return $this->adverts;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return (string)$this->getNumPlace();
    }
    /**
     * @var \AppBundle\Entity\Menu
     */
    private $getMenu3;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adverts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set getMenu3
     *
     * @param \AppBundle\Entity\Menu $getMenu3
     *
     * @return Places
     */
    public function setGetMenu3(\AppBundle\Entity\Menu $getMenu3 = null)
    {
        $this->getMenu3 = $getMenu3;

        return $this;
    }

    /**
     * Get getMenu3
     *
     * @return \AppBundle\Entity\Menu
     */
    public function getGetMenu3()
    {
        return $this->getMenu3;
    }
}
