<?php

namespace AdvertBundle\Entity;

/**
 * Advert
 */
class Advert
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;

    /**
     * @var string
     */
    private $fullSum = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Advert
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Advert
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set fullSum
     *
     * @param string $fullSum
     *
     * @return Advert
     */
    public function setFullSum($fullSum)
    {
        $this->fullSum = $fullSum;

        return $this;
    }

    /**
     * Get fullSum
     *
     * @return string
     */
    public function getFullSum()
    {
        return $this->fullSum;
    }

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AdvertBundle\Entity\Places
     */
    private $place;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Advert
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @var \EventsBundle\Entity\Event
     */
    private $event;


    /**
     * Set event
     *
     * @param \EventsBundle\Entity\Event $event
     *
     * @return Advert
     */
    public function setEvent(\EventsBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \EventsBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @var integer
     */
    private $cntShow = 0;


    /**
     * Set cntShow
     *
     * @param integer $cntShow
     *
     * @return Advert
     */
    public function setCntShow($cntShow)
    {
        $this->cntShow = $cntShow;

        return $this;
    }

    /**
     * Get cntShow
     *
     * @return integer
     */
    public function getCntShow()
    {
        return $this->cntShow;
    }

    /**
     * Set place
     *
     * @param \AdvertBundle\Entity\Places $place
     *
     * @return Advert
     */
    public function setPlace(\AdvertBundle\Entity\Places $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \AdvertBundle\Entity\Places
     */
    public function getPlace()
    {
        return $this->place;
    }
}
