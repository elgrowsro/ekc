<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 04.07.16
 * Time: 17:08
 */

namespace AdvertBundle\EventListener;


use AdvertBundle\Entity\Advert;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

class AdvertSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();


        if ($entity instanceof Advert) {
            $entityManager = $args->getEntityManager();
            $full_price = 0;

            $place = $entity->getPlace();

            $full_price = $place->getPrice() * $entity->getCntShow();

            $entity->setFullSum($full_price);
            $entityManager->persist($entity);
            $entityManager->flush();
        }
    }

}