<?php

namespace AdvertBundle\Admin;

use AdvertBundle\Entity\Places;
use AdvertBundle\Repository\PlacesRepository;
use AppBundle\Entity\Menu;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Validator\ErrorElement;

class PlacesAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('acl');
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        /**
         * @var Places $object
         * @var PlacesRepository $repo_places
         */
        $repo_places = $this->modelManager->getEntityManager('AdvertBundle\Entity\Places')->getRepository('AdvertBundle:Places');

        $query_places = $repo_places->createQueryBuilder('p');

        $menu3_selected_id = $object->getGetMenu3()->getId();

        if ($menu3_selected_id) {
            $query_places
                ->innerJoin('p.getMenu3', 'getMenu3')
                ->andWhere('p.numPlace = :numplace')
                ->andWhere('getMenu3.id = :menu3_selected_id')
                ->setParameter(':menu3_selected_id', $menu3_selected_id)
                ->setParameter(':numplace', $object->getNumPlace());

            /** @var Places $result_places */
            $result_places = $query_places->getQuery()->getOneOrNullResult();


            if ($result_places && $result_places->getGetMenu3()) {
                $errorElement
                    ->with('getMenu3')
                    ->addViolation('Ошибка: Для мероприятия «' . $result_places->getGetMenu3()->getTitle() . '» и номер места «' . $object->getNumPlace() . '» уже назначена цена ')
                    ->end();
            }
        }
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('numPlace')
            ->add('cntAdvert')
            ->add('price');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('numPlace')
            ->addIdentifier('cntAdvert')
            ->addIdentifier('price')
            ->addIdentifier('getMenu3')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('numPlace', 'text', [
                'label' => 'Номер места',
                'required' => true,
            ])
            ->add('cntAdvert', 'text', [
                'label' => 'Количество рекламы на место',
                'required' => true,
            ])
            ->add('price', 'text', [
                'label' => 'Цена за 1 место за 1 показ',
                'required' => true,
            ])
            ->add('getMenu3', 'sonata_type_model_list', [
                'required' => true,
                'label' => 'Меню',
            ]);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('numPlace')
            ->add('cntAdvert')
            ->add('price')
            ->add('getMenu3')
            ->add('createAt')
            ->add('updateAt');
    }
}
