<?php

namespace AdvertBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AdvertAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('fullSum');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('fullSum')
            ->addIdentifier('cntShow')
            ->add('place')
            ->add('event')
            ->add('user')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $em = $this->modelManager->getEntityManager('AppBundle\Entity\Menu');

        $query = $em->createQueryBuilder('m')
            ->select('m')
            ->from('AppBundle:Menu', 'm')
            ->where('m.lvl = 2');

        $formMapper
            ->add('cntShow', 'text', [
                'label' => 'Количество показов'
            ])
            ->add('user', 'sonata_type_model_list', [
                'required' => true,
                'label' => 'Пользователь'
            ])
            ->add('place', 'sonata_type_model_list', [
                'required' => true,
                'label' => 'Место'
            ])
            ->add('event', 'sonata_type_model_list', [
                'required' => true,
                'label' => 'Мероприятие'
            ])/*
            ->add('getMenu3', 'sonata_type_model', [
                'required' => true,
                'property' => 'title',
                'multiple' => true,
                'compound' => false,
                'query' => $query
            ])*/;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('cntShow')
            ->add('place')
            ->add('event')
            ->add('createAt')
            ->add('updateAt')
            ->add('fullSum');
    }
}
