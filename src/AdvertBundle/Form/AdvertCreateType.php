<?php

namespace AdvertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdvertCreateType extends AbstractType
{
    private $form_name;

    public function __construct($form_name = 'advertbundle_advert_create')
    {
        $this->form_name = $form_name;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('event', 'entity', [
                'class' => 'EventsBundle\Entity\Event',
                'label' => 'Выбор мероприятие:',
                'choice_label' => 'name',
                'required' => true
            ])
            ->add('cntShow', 'number', [
                'label' => 'Количество показов суммарно:',
                'required' => true
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdvertBundle\Entity\Advert',
            'validation_groups' => ['create_advert'],
            'attr' => [
                'id' => $this->getName() . "_form",
                'class' => 'form_ajax form',
                'novalidate' => 'novalidate'
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->form_name;
    }
}
